package com.phoneshake;
import cl.json.ShareApplication;
import android.app.Application;
import com.facebook.react.ReactApplication;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.ninty.system.setting.SystemSettingPackage;
import com.jadsonlourenco.RNShakeEvent.RNShakeEventPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.rnfs.RNFSPackage;
import cl.json.RNSharePackage;
import io.branch.rnbranch.RNBranchPackage;
import io.branch.referral.Branch;
import com.evollu.react.fcm.FIRMessagingPackage;
import org.reactnative.camera.RNCameraPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
import com.actionsheet.ActionSheetPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.burnweb.rnpermissions.RNPermissionsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.phoneshake.RCTChirpConnectPackage;
import java.util.Arrays;
import java.util.List;
import io.github.mr03web.softinputmodemodule.SoftInputModePackage;

public class MainApplication extends Application implements ShareApplication, ReactApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  @Override
     public String getFileProviderAuthority() {
            return "com.phoneshake.provider";
     }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNI18nPackage(),
            new SystemSettingPackage(),
            new RNShakeEventPackage(),
            new RNDeviceInfo(),
            new RNFSPackage(),
            new RNBranchPackage(),
            new FIRMessagingPackage(),
            new RNCameraPackage(),
            new SvgPackage(),
            new RNSharePackage(),
            new RCTSplashScreenPackage(),
            new ActionSheetPackage(),
            new PickerPackage(),
            new RNGoogleSigninPackage(),
            new FBSDKPackage(mCallbackManager),
            new ReactNativeContacts(),
            new RNPermissionsPackage(),
            new RCTChirpConnectPackage(),
            new SoftInputModePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Branch.getAutoInstance(this);
    SoLoader.init(this, /* native exopackage */ false);
  }
}
