//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//

package com.phoneshake;
import java.io.*;
import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import android.util.Log;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.DeviceEventManagerModule;



import io.chirp.connect.ChirpConnect;
import io.chirp.connect.interfaces.ConnectEventListener;
import io.chirp.connect.interfaces.ConnectSetConfigListener;
import io.chirp.connect.models.ChirpError;
import io.chirp.connect.models.ConnectState;


public class RCTChirpConnectModule extends ReactContextBaseJavaModule implements LifecycleEventListener {

    private static final String TAG = "ChirpConnect";
    private ChirpConnect chirpConnect;
    private ReactContext context;
    private boolean started = false;
    private boolean wasStarted = false;
    String KEY = "c36673F9A0CE8fE4C286AceeD";
    String SECRET = "24BcD224E4f5F5a6b2E7eCd6DBCf80eF6dE5342177eAaDD05c";
    String CONFIG = "WugjoADWpV93thxzqpLEpeX15+6rLm10IYILvpMxxqgP+oeTZCZ2kd2yQd89uWvAJq1NGKya5hoo2Q2oPhqvy3Gn7dZpYWkEOsw0X4Ys+3dJgCEf26heJhttARdNl147NRplliUCM3bQD6P5V/Q1vMNw9Il/cLWLY3Lk47ZgPQ563CXtC02BbbS/hUU+qGQ19cSEjxQxs9yscQ1kpk0MNxQiyO46Yf19498eUVwCwvEdjZzIB9w3Ay9LPWtdZmiqyQbuJ77q/epTCB05XVm1hh8LabwXiqvOTrEwtJgJa3gSWRfVSdlQPQjuWQjNOuI0o2R5hv+FqNdGCLI5612XST/+a2V5WpA8aKHTMMQ5YJjEIimFeyufxLgmFMMY4QkvNElQDETgRfwOEi1ekj9YZWp4srdV+hRqmc70pTwjzsL/02p5JBliTxp3OePTenIy7wXzcq+HCoyHRALAaV/60S4IZPLKmS11gPgNlXV2kFu3MGXdlmmm50/n2SfRxjVKodunfZZ2cgVKqyIHWP+NljoT4b5RNTL2geVYGi9zygB0GJ4OhWFxVSeormRA68IRqphMf/CiZua/JO9Pg0qJPvZddpTZgsHu8ZBKxrhuPH+bCPBFjDqT7RAnrDkEjHGaBkW2QOjD7XyQqHEBIVINXWUrBCFQ4Be3HZwEF27RwaePEEnSxpjOI+RNTWZorTDdGeubl6eXMmW4Gp1BhHwigcEJ84u6d8H+XxcQnKC/3a96jGEacEEDeuOmS06Rq4NgtveZl5EYU8k2kEl+OodRq0lnj//NqCvMQ/dlK17plpwvidJDjUAdoIMgMR4Syrr2dyRG3/hMu1woDW9nmmiOTb2oxzOu3t70RzFIopdP06FkaY+4Js24cl8bO7xRjsgAMxVAzzCCzbJDAC+2GXSVK/gLaFv3YD5xiXGhHItqaseC05g5Gw5IVW64N/S1fkSyLxWXUEJs8ALRVvFtftEmVefW0NAgtchMqeGohPSQaIZgBzczTsIxEh7dUlWrQDxr675hiGL98Y4VB5iegLa6Pr+pxbKjPNbL3EGcgSaBfgCgU/yTqHSC8WZtyzVxQ2OHTROctlT5cPHllel2m4Qy7E0hn35d7Iyo9Dc8ajzGD5l8uzs/MHddH2UsHyOQeu+YKWwn2wJorJ/JZwYmc/xdKjUgPmL50/6vjJko4eJKDqGCuoHcFjmO8eJzHWbZBByUwIuOwcCj5bf1sVXIUHAxju3ObkfZK751dX0epPIzBybYD9nS+Ezg/x5bVXlT1g6TBUfVpsdcsu59XyU91cmW/xerVy+3yg6Fnpot9GgMNcVNE42Tm/2GCgDaP5+7rUFxXCHnfhosv18UycKn6UrFM1HzONDgzfptvwEleMdY8qFoqfYrinS197q7TGFpz9e0qqEYy9innz0dKOqqTDB0NxRdv8AgK5lcd4zGr3JXke6GPjSL+arnGMnZQsd/ygYo3ktWBsrhW9+cy+NZry9CV2Y5hcssfbWo1hdG1i2A8dObKoMK8yz/zntGXaBQ2R1er9NHffdN7o6ZrlkIM2Joffzz7LkGUqjIxB6+sHsasD5tMeE7IPIg1VPoInMDzazLMoQga6WDHYLq5Z3yU97VPGjQvRCkuDR1mMYZOMHCw1p1gVGr4lmym5n+jECzq0As6PmHKWc88+qjVrzqG6AOuyMyzTYss0KNe6SEI5E/nmBy1b+wd+t3wMkMtT3KpzSt5eWCHqCAArL9zTbiE5FrJM6ybNWu1uDk7chfTx+ao7YKnVNPCs4u/ZN4XaxED+JMhw22uvyd+Eb2EuX8cvlgvAa/dwwG+zA0iXlzoW2oSla12t1MlvUrPiZdCR7khGydgiCwJLKMxjSShgQ9uYOXTdRTntpKWC4H0BFmomi7IokzccT9/mzzDGQr5fIHdgF7Hfvv1oZoCOtNWkh+EokVTslZp69vQsQK4CUMjAuX4eOnAAwrHA1d1KcPzDXUPFbQ5/nCXSZJVexQ87e7gGgjRkawbB5COJdCrYpcIuV6QGVyMcCWO4VgSZp7QFPBLHd3fQyFowYwkfrkrpLuTOw/YpOkxIxhdLtX9Keb8wif6kEPw0bkZDvpvfUii7cUTcMMo7lDZfyGSOn2nBKV6JXTB58eRW7TBe18Rfju5Q1wyg+ARy1bRRGkunfKfRQETqaVZd1DW2w82w8fdSfKw3hbO7lXmjtKwR/FienoxPLwO9a/ebDHik03jCoyCmTHE2UXEmGcqIMbpJ7QR/Q5qoZ2GIdhxZElNLTgt2GDQuhqmddMA34m2t3Uy7s0ptHvFdPesc180PWZUlJSwnfN1bxfbRsSO8aMLlGNXpm5vEIMQUKUznkWzgiRoWvNjWxqAIYiw1/L0BEt7mUHYAomoIRAiHUCyex+vDhCyIdIsczhXv0rDKH+V55VjFuTxEeyN7g1y31iQgVnzmKq2p4vP8tV0vam6Nb7qAGvMenaXAFwcbJqYiYjf0HyiROJeykSDOUSts8uudzBzAd7OfgglaUNUh+yopyOaWZwxzVtc1vnos7K5Cutg9RBBd8f8/WMmINcSkR6lr9braGlYX1moYstgAZP/JPlcWwVnoOKy3HsTtk=";

    @Override
    public String getName() {
        return TAG;
    }

    public RCTChirpConnectModule(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
        reactContext.addLifecycleEventListener(this);
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("CHIRP_CONNECT_NOT_CREATED", ConnectState.ConnectNotCreated.ordinal());
        constants.put("CHIRP_CONNECT_STATE_STOPPED", ConnectState.AudioStateStopped.ordinal());
        constants.put("CHIRP_CONNECT_STATE_PAUSED", ConnectState.AudioStatePaused.ordinal());
        constants.put("CHIRP_CONNECT_STATE_RUNNING", ConnectState.AudioStateRunning.ordinal());
        constants.put("CHIRP_CONNECT_STATE_SENDING", ConnectState.AudioStateSending.ordinal());
        constants.put("CHIRP_CONNECT_STATE_RECEIVING", ConnectState.AudioStateReceiving.ordinal());
        return constants;
    }

    /**
     * init(key, secret)
     *
     * Initialise the SDK with an application key and secret.
     * Callbacks are also set up here.
     */
    @ReactMethod
    public void init(String key, String secret) {
        chirpConnect = new ChirpConnect(this.getCurrentActivity(), KEY, SECRET);
        chirpConnect.setConfig(CONFIG, new ConnectSetConfigListener() {
            @Override
            public void onSuccess() {
                Log.i("setConfig", "Config successfully set.");
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onSetConfigSuccess", "success");
            }

            @Override
            public void onError(ChirpError setConfigError) {
                Log.e("setConfig", setConfigError.getMessage());
            }
        });

        chirpConnect.setListener(new ConnectEventListener() {

            @Override
            public void onSent(byte[] data, byte b) {
                WritableMap params = assembleData(data);
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onSent", params);
            }

            @Override
            public void onSending(byte[] data, byte b) {
                WritableMap params = assembleData(data);
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onSending", params);
            }

            @Override
            public void onReceived(byte[] data, byte b) {
                WritableMap params = assembleData(data);
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onReceived", params);
            }

            @Override
            public void onReceiving(byte b) {
                WritableMap params = Arguments.createMap();
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onReceiving", params);
            }

            @Override
            public void onStateChanged(byte oldState, byte newState) {
                WritableMap params = Arguments.createMap();
                params.putInt("status", newState);
                context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onStateChanged", params);
            }

            @Override
            public void onSystemVolumeChanged(int oldVolume, int newVolume) {}
        });
    }

    /**
     * getLicence()
     *
     * Fetch default licence from the network to configure the SDK.
     */
    // @ReactMethod
    // public void getLicence(final Promise promise) {
    //     chirpConnect.getLicence(new ConnectLicenceListener() {

    //         @Override
    //         public void onSuccess(String networkLicence) {
    //             ChirpError setLicenceError = chirpConnect.setLicence(networkLicence);
    //             if (setLicenceError.getCode() > 0) {
    //                 promise.reject("Licence Error", setLicenceError.getMessage());
    //             } else {
    //                 promise.resolve("Initialisation Success");
    //             }
    //         }

    //         @Override
    //         public void onError(ChirpError chirpError) {
    //             promise.reject("Network Error", chirpError.getMessage());
    //         }
    //     });
    // }

    /**
     * setLicence(licence)
     *
     * Configure the SDK with a licence string.
     */
    // @ReactMethod
    // public void setLicence(String licence) {
    //     ChirpError setLicenceError = chirpConnect.setLicence(licence);
    //     if (setLicenceError.getCode() > 0) {
    //         onError(context, setLicenceError.getMessage());
    //     }
    // }

    /**
     * start()
     *
     * Starts the SDK.
     */
    @ReactMethod
    public void start() {
        ChirpError error = chirpConnect.start();
        if (error.getCode() > 0) {
            onError(context, error.getMessage());
        }
        started = true;
    }

    /**
     * stop()
     *
     * Stops the SDK.
     */
    @ReactMethod
    public void stop() {
        ChirpError error = chirpConnect.stop();
        if (error.getCode() > 0) {
            onError(context, error.getMessage());
        }
        started = false;
    }

    /**
     * send(data)
     *
     * Encodes a payload of bytes, and sends to the speaker.
     */
    @ReactMethod
    public void send(ReadableArray data) {
        byte[] payload = new byte[data.size()];
        for (int i = 0; i < data.size(); i++) {
            payload[i] = (byte)data.getInt(i);
        }

        long maxSize = chirpConnect.getMaxPayloadLength();
        if (maxSize < payload.length) {
            onError(context, "Invalid payload");
            return;
        }
        ChirpError error = chirpConnect.send(payload);
        if (error.getCode() > 0) {
            onError(context, error.getMessage());
        }
    }

    /**
     * sendRandom()
     *
     * Sends a random payload to the speaker.
     */
    @ReactMethod
    public void sendRandom() {
        Random r = new Random();
        long length = (long)r.nextInt((int)chirpConnect.getMaxPayloadLength() - 1);
        byte[] payload = chirpConnect.randomPayload(length);


        ChirpError error = chirpConnect.send(payload);
        if (error.getCode() > 0) {
            onError(context, error.getMessage());
        }
    }

    /**
     * asString(data)
     *
     * Returns a payload represented as a hexadecimal string.
     */
    public static String asString(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for(byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    private static WritableMap assembleData(byte[] data) {
        WritableArray payload = Arguments.createArray();
        for (int i = 0; i < data.length; i++) {
            payload.pushInt(data[i]);
        }
        WritableMap params = Arguments.createMap();
        params.putArray("data", payload);
        return params;
    }

    private void onError(ReactContext reactContext,
                         String error) {
        WritableMap params = Arguments.createMap();
        params.putString("message", error);
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onError", params);
    }

    @Override
    public void onHostResume() {
        if (wasStarted) {
            chirpConnect.start();
        }
    }

    @Override
    public void onHostPause() {
        wasStarted = started;
        if(wasStarted) {
            chirpConnect.stop();
        }
    }

    @Override
    public void onHostDestroy() {
        wasStarted = started;
        if(wasStarted) {
            chirpConnect.stop();
        }
        // try {
        //     chirpConnect.close();
        // } catch(IOException err) {}
    }
}
