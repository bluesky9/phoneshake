package com.phoneshake;
import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import com.facebook.react.ReactActivity;
import io.branch.rnbranch.*;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import android.content.Intent;
import android.util.Log;
import org.json.JSONObject;
import com.burnweb.rnpermissions.RNPermissionsPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreen;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends ReactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      RCTSplashScreen.openSplashScreen(this);
      Fabric.with(this, new Crashlytics());
      //Crashlytics.start(this);
      super.onCreate(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        RNPermissionsPackage.onRequestPermissionsResult(requestCode, permissions, grantResults); // very important event callback
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    @Override
    protected String getMainComponentName() {
        return "PhoneShake";
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }
    @Override
      protected void onStart() {
          super.onStart();
        //   Branch branch = Branch.getInstance();
          RNBranchModule.initSession(getIntent().getData(), this);
          // Branch init
        // branch.initSession(new Branch.BranchReferralInitListener() {
        // @Override
        // public void onInitFinished(JSONObject referringParams, BranchError error) {
        //     if (error == null) {
        //         // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
        //         // params will be empty if no data found
        //         // ... insert custom logic here ...
        //         Log.i("BRANCH SDK", referringParams.toString());
        //     } else {
        //         Log.i("BRANCH SDK", error.getMessage());
        //     }
        // }
        // }, this.getIntent().getData(), this);
      }

      @Override
      public void onNewIntent(Intent intent) {
          setIntent(intent);
      }
}
