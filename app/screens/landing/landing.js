import React, { Component } from 'react'
import { Text, View, Image, AsyncStorage } from 'react-native';
import { Content, Button } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Dropdown } from 'react-native-material-dropdown';
import I18n from 'react-native-i18n';
import { Images, Colors, Fonts } from '../../themes/'
import styles from'./landing.styles.js'
import { strings, setLanguage } from '../../locale/';

let data = [{
  value: 'en', label: 'EN'
}, {
  value: 'ar', label: 'AR'
}];

class Landing extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });
  
	constructor() {
		super();
		this.state = {
      language: 'en'
		};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.changeLang = this.changeLang.bind(this);
  }
  async componentDidMount(){
    var language = await AsyncStorage.getItem('PhoneShakeLocal')
    if (language !== null) {
      setLanguage(language);
      this.setState({language});
    }
  }
  handleSubmit() {
    this.props.navigation.navigate('LoginScreen')
  }

  changeLang(language){
    console.log('language', language)
    I18n.locale = language
    this.setState({language});
    AsyncStorage.setItem('PhoneShakeLocal', language);
  }

	render() {
    return (
			<View style={{flex:1, backgroundColor:'#fff'}}>
      <View style={styles.languageBox}>
        <Dropdown
          label=''
          value={this.state.language}
          data={data}
          renderAccessory={() => {
            return (<Icon name="caret-down"  style={styles.arrowDown} size={16}/>)
          }}
          dropdownPosition={0}
          itemTextStyle={{fontSize: 30, fontFamily:Fonts.type.displayRegular}}
          labelTextStyle={{fontSize: 30, fontFamily:Fonts.type.displayRegular}}
          textColor={Colors.blueTheme}
          style={{color: Colors.blueTheme,
          fontSize: 16,
          fontFamily: Fonts.type.displayRegular}}
          containerStyle={{width: 50}}
          inputContainerStyle={[{borderBottomColor: 'transparent',borderBottomWidth:0}]}
          onChangeText={this.changeLang}
        />
      </View>
      <Content contentContainerStyle={styles.container} bounces={false}>
				<Image source={Images.appLogo} style={styles.logo} />
        {/* <Text style={styles.title}>
          {`Welcome to\nPhoneshake`}
        </Text> */}
        <Text style={styles.title}>
          {strings('landing.welcome')}
        </Text>
        <Button style={styles.btnContinue} onPress={this.handleSubmit}>
          {/* <Text style={styles.txtContinue}>Get Started</Text> */}
          <Text style={styles.txtContinue}>{strings('landing.login_button')}</Text>
        </Button>
      </Content>
		</View>
    );
	}
}

export default Landing;
