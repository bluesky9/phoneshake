import { StyleSheet, Dimensions, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
	container: {
		flex: 1,
		// justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	logo: {
		height: Metrics.screenWidth * 0.32,
		width: Metrics.screenWidth * 0.32,
		marginTop: Metrics.screenHeight * 0.2,
		marginBottom: Metrics.screenHeight * 0.015
	},
  title: {
    fontSize: Metrics.screenWidth * 0.08,
    width: '70%', //(Platform.OS === 'ios') ? '43%': '50%',
		textAlign: 'center',
		color:'#000',
    paddingBottom: 20,
		fontFamily: Fonts.type.displayRegular,
		lineHeight: Metrics.screenWidth * 0.096
  },
  btnContinue: {
    position: 'absolute',
    bottom: Metrics.screenHeight * 0.089,
		backgroundColor: Colors.blueTheme,
    width: '75%',
    borderRadius: 4,
    justifyContent: 'center',
    alignSelf: 'center',
		height: Metrics.screenHeight * 0.08,
		shadowOffset: { height: 0, width: 0 },
		shadowOpacity: 0,
		elevation: 0
  },
	txtContinue: {
		color:'#fff',
		fontSize: 16,
		fontFamily: Fonts.type.SFTextSemibold
	},
	languageBox:{
		flexDirection:'row',
		marginTop: 20,
		marginRight: 10,
		justifyContent: 'flex-end'
	},
	languageText: {
		color: Colors.blueTheme,
		fontSize: 18,
		
		fontFamily: Fonts.type.displayRegular,
	},
	arrowDown:{
		marginTop: 2, marginLeft: -20,
		color: Colors.blueTheme
	}
});
