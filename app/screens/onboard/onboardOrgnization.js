import React, { Component } from 'react'
import { TouchableOpacity, Image } from 'react-native';
import { Images } from '../../themes/'
import InputForm from '../../components/inputForm/inputForm.js'
import styles from './onboard.styles.js'

class OnboardOrgnization extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.goBack()}>
				<Image source={Images.backIcon} style={styles.headerLeft} />
			</TouchableOpacity>
		),
		headerStyle: {
       backgroundColor: '#fff',
			 borderBottomWidth: 0,
       elevation: null
		 },
  });

	constructor() {
		super();
		this.state = {
      organization: '', //'Dummy Technologies'
		};
    this.handleSubmit = this.handleSubmit.bind(this);
	}

  handleSubmit() {
    // Keyboard.dismiss();
		const keyboardHeight = this.props.navigation.state.params.keyboardHeight
		let userInfo = this.props.navigation.state.params.userInfo
		userInfo.organization = this.state.organization
    this.props.navigation.navigate('OnboardPosition', { keyboardHeight, userInfo })
  }

	render() {
    let disabled = this.state.organization.length < 2
    return (
			<InputForm
        titleText={`What’s your\ncurrent organization?`}
        placeholder='Organization Name'
				value={this.state.organization}
				maxLength={100}
				keyboardHeight={this.props.navigation.state.params.keyboardHeight}
        informationText='Add your company/organization name'
        onChangeText={(organization) => this.setState({organization})}
        handleSubmit={this.handleSubmit}
        />
    );
	}
}



export default OnboardOrgnization;
