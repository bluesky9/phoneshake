import React, { Component } from 'react'
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import { Images } from '../../themes/'
import InputForm from '../../components/inputForm/inputForm.js'

class OnboardPosition extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.goBack()}>
				<Image source={Images.backIcon} style={styles.headerLeft} />
			</TouchableOpacity>
		),
		headerStyle: {
       backgroundColor: '#fff',
			 borderBottomWidth: 0,
       elevation: null
		 },
  });

	constructor() {
		super();
		this.state = {
      designation: '', //'Engineering Manager'
		};
    this.handleSubmit = this.handleSubmit.bind(this);
	}

  handleSubmit() {
    // Keyboard.dismiss();
		const keyboardHeight = this.props.navigation.state.params.keyboardHeight
		let userInfo = this.props.navigation.state.params.userInfo
		userInfo.designation = this.state.designation
    this.props.navigation.navigate('OnboardChooseHandle', { keyboardHeight, userInfo })
  }

	render() {
    return (
			<InputForm
        titleText={`What’s your\nposition/designation?`}
        placeholder='Position'
				value={this.state.designation}
				maxLength={100}
				keyboardHeight={this.props.navigation.state.params.keyboardHeight}
        informationText='Add your occupation title'
        onChangeText={(designation) => this.setState({designation})}
        handleSubmit={this.handleSubmit}
        />
    );
	}
}

const styles = StyleSheet.create({
	headerLeft: {
		height: 24,
		width: 24,
		marginLeft: 12
	}
})

export default OnboardPosition;
