import React, { Component } from 'react'
import { Image, Platform, TouchableOpacity } from 'react-native';
import InputForm from '../../components/inputForm/inputForm.js'
import { connect } from 'react-redux';
import { Images } from '../../themes/'
import styles from './onboard.styles.js'
import { strings } from '../../locale/';

class OnboardName extends Component {
  static navigationOptions = ({ navigation }) => ({
    // headerLeft: (
    //   <TouchableOpacity onPress={() => navigation.goBack()}>
    //     <Image source={Images.backIcon} style={styles.headerLeft} />
    //   </TouchableOpacity>
    // ),
    headerLeft: null,
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  });

	constructor() {
		super();
		this.state = {
      name: '',
			visibleHeight: 0
		};
    this.handleSubmit = this.handleSubmit.bind(this);
	}

  handleSubmit() {
    const {name} = this.state;
    const { isRTL  } = this.props.user;
    var regex = new RegExp("[\u0600-\u06FF]");
    if (isRTL === false && (!/^[a-zA-Zء-ي]*$/g.test(name) || name.length > 60)) {
      alert(strings('login.validName'));
      return false;
    } if (isRTL && !regex.test(name) || this.state.name.length > 60) {
      alert(strings('login.validName'));
      return false;
    } 
		const keyboardHeight = this.props.navigation.state.params.keyboardHeight
		let userInfo = this.props.navigation.state.params.userInfo
    userInfo.name = this.state.name;
    this.props.navigation.navigate('OnboardChooseHandle', { keyboardHeight, userInfo })
    // if(userInfo.accountType === 'individual'){
    //   this.props.navigation.navigate('OnboardChooseHandle', { keyboardHeight, userInfo })
    // } else {
    //   this.props.navigation.navigate('OnboardBusinessType', { keyboardHeight, userInfo })
    // }
  }

	render() {
    let width = (Platform.OS === 'ios') ? '46%' : '50%';
    const {userInfo} = this.props.navigation.state.params;

    // return (
    //   <InputForm
    //     titleText={(userInfo.accountType === 'individual') ? strings('meTab.settingsWhatsName') : "What's the name of\nyour business?"}
    //     titleWidth={width}
    //     placeholder={(userInfo.accountType === 'individual') ? strings('meTab.settingsFullName') : "Business Name"}
    //     value={this.state.name}
    //     maxLength={60}
		// 		keyboardHeight={this.props.navigation.state.params.keyboardHeight}
    //     informationText={(userInfo.accountType === 'individual') ? strings('meTab.settingsAddFirstAndLastName'): "Ex. Phoneshake inc."}
    //     onChangeText={(name) => this.setState({name})}
    //     handleSubmit={this.handleSubmit}
    //     name="fullName"
    //     />
    // );

    return (
      <InputForm
        titleText={(true) ? strings('meTab.settingsWhatsName') : "What's the name of\nyour business?"}
        titleWidth={width}
        placeholder={(true) ? strings('meTab.settingsFullName') : "Business Name"}
        value={this.state.name}
        maxLength={60}
				keyboardHeight={this.props.navigation.state.params.keyboardHeight}
        informationText={(userInfo.accountType === 'individual') ? strings('meTab.settingsAddFirstAndLastName'): "Ex. Phoneshake inc."}
        onChangeText={(name) => this.setState({name})}
        handleSubmit={this.handleSubmit}
        name="fullName"
        />
    );

	}
}


const stateToProps = (state) => ({
  user: state.login
});


export default connect(stateToProps, null)(OnboardName)
