import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Content } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import PhoneShakeButton from  '../../components/phoneShakeButton/'
import { Metrics, Images, Fonts } from '../../themes/'
import * as actions from '../login/login.actions.js';
import { strings } from '../../locale/';


class OnboardBusinessType extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.headerLeft} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: '#fff',
            borderBottomWidth: 0,
            elevation: null
        },
    });

    componentDidMount(){
        this.props.getBusinessTypes();
    }

	constructor() {
        super();
        this.state = {
            businessTypeId: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
	}

    handleSubmit() {
        const keyboardHeight = this.props.navigation.state.params.keyboardHeight
        let userInfo = this.props.navigation.state.params.userInfo
        userInfo.accountType = this.state.accountType
        this.props.navigation.navigate('OnboardName', { keyboardHeight, userInfo })
    }

    onChange(businessTypeId){
        this.setState({businessTypeId});
    }

	render() {
    const { businessTypes } = this.props;
    let items = [];
    if(businessTypes && businessTypes.length){
        businessTypes.map((item) => {
            items.push({value: item.id, label: item.name});
        })
    }
    return (
        <View style={{flex:1, backgroundColor:'#fff'}}>
            <Content contentContainerStyle={styles.container} bounces={false} scrollEnabled={false}>
                <Text style={styles.topTitle}>
                    {`What's your business?`}
                </Text>
                <View style={styles.dropDownOuter}>
                <Dropdown
                    label={(!this.state.businessTypeId) ? 'Select a business type' : ""}
                    value={''}
                    data={items}
                    renderAccessory={() => {
                        return (<Icon name="caret-down"  style={{color: '#7F7F7F', marginTop: 2}} size={18}/>)
                    }}
                    dropdownPosition={0}
                    rippleCentered={true}
                    textColor={'#7F7F7F'}
                    fontSize={18}
                    itemTextStyle={{color: '#7F7F7F',
                    fontFamily: Fonts.type.displayRegular}}
                    containerStyle={{width: "73%",  paddingHorizontal: 10}}
                    inputContainerStyle={[{borderBottomColor: '#707070',borderBottomWidth:0}]}
                    onChangeText={this.onChange}
                    itemCount={10}
                    />
                </View>
                <Text style={styles.infoText}>Allow Phoneshake to suggest your business</Text>
                <PhoneShakeButton
                disabled={(this.state.businessTypeId) ? false : true}
                onPress={() => this.handleSubmit()}
                text={strings('constants.continue')}
                bottomHeight={this.props.keyboardHeight}/>
            </Content>
        </View>
    )
    }
	
}


const stateToProps = (state) => ({
	businessTypes: state.login.businessTypes,
});

const dispatchToProps = (dispatch) => {
	return {
		getBusinessTypes: () => dispatch(actions.getBusinessTypes())
	}
}
const Container = connect( stateToProps, dispatchToProps )(OnboardBusinessType)
export default Container;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Metrics.screenHeight * 0.015,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    topTitle: {
        fontSize: Metrics.screenWidth * 0.053,
        textAlign: 'center',
        paddingBottom: Metrics.screenHeight * 0.029,
        fontFamily: Fonts.type.SFTextMedium,
        color: '#000'
    },
    dropDownOuter: {
        flexDirection: 'row', marginTop: 30, borderBottomColor: '#707070', borderBottomWidth: 0.5, paddingHorizontal: 10
    },
    infoText: {
		marginTop: 15,
        fontSize: Metrics.screenWidth * 0.032,
        textAlign: 'center',
        fontFamily: Fonts.type.SFTextRegular,
        color: 'rgba(0, 0, 0, 0.34)',
        marginBottom:  Metrics.screenHeight * 0.2,
  },
});
