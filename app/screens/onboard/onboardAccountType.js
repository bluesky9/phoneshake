import React, { Component } from 'react'
import { Image, View, Text, Platform, TouchableOpacity, StyleSheet } from 'react-native';
import { Content, Item } from 'native-base';
import InputForm from '../../components/inputForm/inputForm.js'
import PhoneShakeButton from  '../../components/phoneShakeButton/'
import { Images, Metrics, Fonts } from '../../themes/'
import { strings } from '../../locale/';

class OnboardAccountType extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: null,
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  });

	constructor() {
		super();
		this.state = {
            accountType: '',
			visibleHeight: 0,
		};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.selectType = this.selectType.bind(this);
	}

  handleSubmit() {
    const keyboardHeight = this.props.navigation.state.params.keyboardHeight
    let userInfo = this.props.navigation.state.params.userInfo
    userInfo.accountType = this.state.accountType
    this.props.navigation.navigate('OnboardName', { keyboardHeight, userInfo })
  }

  selectType(accountType){
    this.setState({accountType});
  }

	render() {
    const {accountType} = this.state;
    let disabled = (accountType) ? false : true;
    return (
        <View style={{flex:1, backgroundColor:'#fff'}}>
            <Content contentContainerStyle={styles.container} bounces={false} scrollEnabled={false}>
                <Text style={styles.topTitle}>
                    {`Select the account type you\nwish to create`}
                </Text>
                <View style={{flexDirection: 'row', marginVertical: 50}}>
                    <TouchableOpacity style={[styles.box, {marginRight: Metrics.screenWidth * 0.06, borderColor : (accountType === 'individual') ? "#208EFB" : "#A8A8A8"}]} activeOpacity={0.5} onPress={() => this.selectType('individual')}>
                        <Image source={Images.individualIcon} />
                        <Text style={styles.boxText}>INDIVIDUAL</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.box, {borderColor : (accountType === 'business') ? "#208EFB" : "#A8A8A8"}]}  activeOpacity={0.5} onPress={() => this.selectType('business')}>
                        <Image source={Images.businessIcon} />
                        <Text style={[styles.boxText, {color: (accountType === 'business') ?'#208EFB' : "#000"}]}>BUSINESS, BRAND OR ORGANIZATION</Text>
                    </TouchableOpacity>
                </View>
                <PhoneShakeButton
                disabled={disabled}
                onPress={() => this.handleSubmit()}
                text={strings('constants.continue')}
                bottomHeight={this.props.keyboardHeight}/>
            </Content>
        </View>
    )
    }
	
}

export default OnboardAccountType;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Metrics.screenHeight * 0.015,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    topTitle: {
        fontSize: Metrics.screenWidth * 0.053,
        textAlign: 'center',
        paddingBottom: Metrics.screenHeight * 0.029,
        fontFamily: Fonts.type.SFTextMedium,
        color: '#000'
    },
    box: {
        height: Metrics.screenWidth * 0.42,
        width: Metrics.screenWidth * 0.42,
        borderRadius: 4,
        borderColor: '#A8A8A8',
        borderWidth: 2,
        justifyContent: "center",
        alignItems: 'center'
    },
    boxText: {
        textAlign: 'center', marginTop: 10,
        fontFamily: Fonts.type.SFTextSemibold,
        color: '#000',
        fontSize: 12
    }
});
