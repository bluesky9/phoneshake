import React, { Component } from 'react'
import { View, Platform, TouchableOpacity, Image, StyleSheet, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import FCM from 'react-native-fcm';
import * as actions from '../login/login.actions.js';
var DeviceInfo = require('react-native-device-info');
import { Images } from '../../themes/'
import InputForm from '../../components/inputForm/inputForm.js'
import Loader from '../../components/loader/';

import { strings } from '../../locale/';
// import { setupPushForChat } from '../../api/sendBird'

class ChooseHandle extends Component {
  static navigationOptions = ({ navigation }) => ({
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.goBack()}>
				<Image source={Images.backIcon} style={styles.headerLeft} />
			</TouchableOpacity>
		),
		headerStyle: {
       backgroundColor: '#fff',
			 borderBottomWidth: 0,
       elevation: null
		 },
  });

	constructor() {
		super();
		this.state = {
      handleName: '',
      isHandleNotUnique: false,
      loading: false,
      errorMessage: '',
      fcmToken: '',
      showLoading: false,
      isAvailable: false
		};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.isHandleNameUnique = this.isHandleNameUnique.bind(this);
    this.updateDeviceToken = this.updateDeviceToken.bind(this);
  }
  
  componentWillMount() {
    FCM.getFCMToken().then(token => {
      if (token) {
        this.setState({ fcmToken: token })
      }
    });
  }

  onChangeText = (handleName) => {
    this.setState({ handleName })
  }

  async updateDeviceToken(userResponse) {
    const uniqueId = DeviceInfo.getUniqueID();
    let fcmToken = this.state.fcmToken
    if (fcmToken === '') {
      fcmToken = await FCM.getFCMToken().then(token => {
        if (token) {
          return token
        } else {
          return ''
        }
      });
    }
    let token = userResponse.data.content.token
    let userId = userResponse.data.content.user_id
    let data = {
      "userId": userId,
      "token": token,
      "uniqueDeviceId": uniqueId,
      "deviceType": (Platform.OS === 'ios') ? "iOS" : "Android",
      "deviceToken": fcmToken
    }
    this.props.updateDeviceToken(data)
    .then((response) => {
      this.setState({ loading: false })
      this.props.navigation.navigate('PrivacyPolicy')
    })
  }

  isHandleNameUnique(handleName) {    
    var _this = this;
    if(handleName.length <= 2) {
      this.setState({ handleName, isAvailable: false })
    }
    if(handleName.length > 2) {
      this.setState({ isHandleNotUnique: false, showLoading: true, handleName })
    _this.props.isHandleNameUnique(handleName)
      .then((response) => {
          if(response.payload.data &&
              response.payload.data.status === 200 &&
              response.payload.data.content &&
              response.payload.data.content.isAvailable) {
            this.setState({ showLoading: false, isAvailable: true, errorMessage: '', isHandleNotUnique: false })
          } else {
            let error = strings("login.handleNotAvailable", {"handleName": handleName})
            _this.setState({
              loading: false,
              isHandleNotUnique: true,
              errorMessage: error,
              showLoading: false,
              isAvailable: false
            })
          }
      })
    }
  }
  
  handleSubmit() {
    let userInfo = this.props.navigation.state.params.userInfo
    userInfo.handlename = "@" + this.state.handleName
    this.setState({ isHandleNotUnique: false, loading: true })
    this.props.registerUser(userInfo)
    .then((response) => {
      console.log('response', response)
      this.setState({ loading: false })
      if(response.payload.data && response.payload.data.status !== 200) {
        let message = (response.payload && response.payload.data && response.payload.data.message) ? response.payload.data.message : 'Something went wrong. Please try again.'
        setTimeout(() => {
          alert(message)
        }, 300)
      } else {
        if(response.payload.data && response.payload.data.status === 200) {
          response.payload.data.content.socialMediaLinks = []
          response.payload.checkForLogin = true
          AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(response.payload));
          let name = response.payload.data.content.name
          let profile_pic = response.payload.data.content.profile_pic
          let userId = response.payload.data.content.user_id
          this.updateDeviceToken(response.payload)
        }
      }
      
    })
  }

	render() {
    let width = (Platform.OS === 'ios') ? '60%' : '50%'
    let infoText = (this.state.isHandleNotUnique) ? this.state.errorMessage : (this.state.isAvailable) ? strings("login.handleNameAvailable", {"handleName": this.state.handleName}) : 'phoneshake.me/@' + this.state.handleName
    return (
      <View style={{flex:1}}>
        <Loader loading={this.state.loading} />
        <InputForm
          titleText={strings('login.chooseHandle')}
          titleWidth={width}
          placeholder={strings('meTab.settingsHandle')}
          name='username'
          isHandleNotUnique={this.state.isHandleNotUnique}
          value={this.state.handleName}
          keyboardHeight={(this.props.navigation.state.params && this.props.navigation.state.params.keyboardHeight) ? this.props.navigation.state.params.keyboardHeight : 220}
          informationText={infoText}
          onChangeText={(handleName) => this.isHandleNameUnique(handleName)}
          fromOnboard={true}
          handleAvailable={this.state.isAvailable}
          showLoading={this.state.showLoading}
          handleSubmit={this.handleSubmit}
          />
      </View>
    );
	}
}

const styles = StyleSheet.create({
	headerLeft: {
		height: 24,
		width: 24,
		marginLeft: 12
	}
})

const stateToProps = (state) => ({
	//loginStatus: state.login.status
});

const dispatchToProps = (dispatch) => {
	return {
		registerUser: (userInfo) => dispatch(actions.registerUser(userInfo)),
    isHandleNameUnique: (handleName) => dispatch(actions.isHandleNameUnique(handleName)),
    updateDeviceToken: (data) => dispatch(actions.updateDeviceToken(data)),
	}
}
const Container = connect( stateToProps, dispatchToProps )(ChooseHandle)
export default Container;
