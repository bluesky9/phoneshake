import { StyleSheet } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
  topTitle: {
    fontSize: Metrics.screenWidth * 0.053,
    textAlign: 'center',
    paddingBottom: Metrics.screenHeight * 0.029,
		fontFamily: Fonts.type.SFTextMedium
  },
	bottomView: {
		alignItems:'center',
		justifyContent:'center',
		width: '100%',
		backgroundColor: '#fff',
		paddingTop: Metrics.screenHeight * 0.029,
		paddingBottom: Metrics.screenHeight * 0.022,
	},
	iconStyleUnfilled: {
		color: 'rgba(0, 0, 0, 0.34)',
		fontSize: Metrics.screenWidth * 0.058
	},
	iconStyleFilled: {
		color: Colors.blueTheme,
		fontSize: Metrics.screenWidth * 0.058
	},
	checkbox: {
		marginRight: Metrics.screenWidth * 0.026,
	},
	bottomTextInctive: {
		fontSize: Metrics.screenWidth * 0.032,
		color: Colors.lightBlack,
		fontFamily: Fonts.type.SFTextRegular
	},
	bottomTextActive: {
		fontSize: Metrics.screenWidth * 0.032,
		fontFamily: Fonts.type.SFTextRegular,
		color: '#000'
	},
	bottomTextMuted: {
		fontSize: Metrics.screenWidth * 0.032,
		color: Colors.lightBlueTheme,
		fontFamily: Fonts.type.SFTextRegular
	},
	bottomText: {
		fontSize: Metrics.screenWidth * 0.032,
		color: Colors.blueTheme,
		fontFamily: Fonts.type.SFTextRegular
	},
	bottomItems: {
		flexDirection: 'row',
		marginBottom: Metrics.screenHeight * 0.029,
		alignItems: 'center',
		justifyContent: 'center'
	},
	content: {
		paddingHorizontal: Metrics.screenWidth * 0.032,
		backgroundColor: 'transparent'
	},
	heading: {
		fontSize: Metrics.screenWidth * 0.032,
		color: Colors.lightBlack,
		fontFamily: Fonts.type.SFTextSemibold,
		marginBottom: 8
	},
	description: {
		fontSize: Metrics.screenWidth * 0.037,
		color: '#000',
		fontFamily: Fonts.type.SFTextRegular,
		marginBottom: Metrics.screenHeight * 0.023,
	},
	headerLeft: {
		height: 24,
		width: 24,
		marginLeft: 12
	},
	splashContainer: {
		flex: 1,
		backgroundColor: 'white',
		alignItems: 'center'
	}
});
