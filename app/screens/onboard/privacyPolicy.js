import React, { Component } from 'react'
import { Keyboard, View, Text, TouchableOpacity, Image, WebView, Platform, ActivityIndicator } from 'react-native';
import { Container, Content, Icon } from 'native-base'
import { StackActions, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import { MaterialIndicator } from 'react-native-indicators';
import { Fonts, Metrics, Colors, Images } from '../../themes/'
import styles from './onboard.styles.js'
import PhoneShakeButton from  '../../components/phoneShakeButton/'
import { strings } from '../../locale/';

class PrivacyPolicy extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
			<TouchableOpacity onPress={() => navigation.goBack()}>
				<Image source={Images.backIcon} style={styles.headerLeft} />
			</TouchableOpacity>
		),
		headerStyle: {
       backgroundColor: '#fff',
			 borderBottomWidth: 0,
       elevation: null
		},
    headerTitleStyle: {
      fontFamily: Fonts.type.SFTextRegular,
      fontWeight: "200",
      fontSize: Metrics.screenWidth * 0.053,
      alignSelf: 'center',
      textAlign: 'center',
      width: '70%'
    },
  });

	constructor() {
		super();
		this.state = {
      checked: false,
      loading: false
		};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleCheckbox = this.toggleCheckbox.bind(this);
  }

  handleSubmit() {
    Keyboard.dismiss();
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: "TabContainer",
          params: {
            fromRegistration: true
          }
        })
      ]
    });
    this.props.navigation.dispatch(resetAction);
    // this.props.navigation.navigate('TabContainer', { fromRegistration: true })
  }

  toggleCheckbox() {
    this.setState({ checked: !this.state.checked})
  }

  ActivityIndicatorLoadingView() {
    if(Platform.OS === 'ios') {
      return (
        <MaterialIndicator
          size={35}
          color={Colors.blueTheme}
          animating={true} />
      );
    } else {
      return (
        <ActivityIndicator
          size={35}
          color={Colors.blueTheme}
          animating={true} />
      );
    }
    
  }

  renderPrivacy(disabled){
    let content = [];
    const {isRTL} = this.props.user;
    if(isRTL === true){
      content.push(
        <Text style={(disabled) ? styles.bottomTextMuted : styles.bottomText}>{strings("constants.terms")}</Text>
      )
      content.push(
        <Text style={(disabled) ? styles.bottomTextInctive : styles.bottomTextActive}>{strings("constants.and")} &nbsp;</Text>
      )
      content.push(
        <Text style={(disabled) ? styles.bottomTextMuted : styles.bottomText}>{strings("constants.privacy")} &nbsp;</Text>
      )
      content.push(
        <Text style={(disabled) ? styles.bottomTextInctive : styles.bottomTextActive}>{strings("constants.accept")} &nbsp;</Text>
      )
      
    } else {
      content.push(
        <Text style={(disabled) ? styles.bottomTextInctive : styles.bottomTextActive}>{strings("constants.accept")} &nbsp;</Text>
      )
      content.push(
        <Text style={(disabled) ? styles.bottomTextMuted : styles.bottomText}>{strings("constants.privacy")} &nbsp;</Text>
      )
      content.push(
        <Text style={(disabled) ? styles.bottomTextInctive : styles.bottomTextActive}>{strings("constants.and")} &nbsp;</Text>
      )
      content.push(
        <Text style={(disabled) ? styles.bottomTextMuted : styles.bottomText}>{strings("constants.terms")}</Text>
      )
    }
    return content;
  }

	render() {
    
    let disabled = !this.state.checked
    let btnStyle = (disabled) ? [styles.btnContinue, { backgroundColor: Colors.lightBlueTheme }] : styles.btnContinue
    let iconStyle = (disabled) ? styles.iconStyleUnfilled : styles.iconStyleFilled
    return (
			<Container style={styles.container}>
        {/* <WebView
          source={{ uri: 'https://s3-us-west-2.amazonaws.com/phoneshake/privacy-policy-v1.html' }}
          startInLoadingState={true}
          renderLoading={this.ActivityIndicatorLoadingView}
          style={styles.content}
        /> */}
        <Content style={styles.content}>
          <View style={{paddingTop: 22}}>
          <Text style={styles.description}>
            At Phoneshake, Inc. (“Phoneshake”), your right to privacy is very important to us. This privacy policy governs your use of the software application Phoneshake ("Application") for mobile devices that was created by Phoneshake, Inc. We provide our users with the ability to improve the ease and speed of contact information, online account information, and other basic profile based information sharing between people, among other features. In order to provide you with this service effectively, we collect certain specific information from you, and we also automatically collect information and data relating to the usage of our services by all our users, including you. Because we are committed to protecting your privacy rights and the information you provide to us, we have developed this privacy policy. It explains how we will protect your information and describes how we will use and share it.
          </Text>
          <Text style={styles.heading}>What information does the Application obtain and how is it used?</Text>
          <Text style={styles.heading}>User Provided Information and Personally Identifiable Information</Text>
          <Text style={styles.description}>
              {`The Application obtains the information you provide when you download and register the Application. Registration with us is optional. However, please keep in mind that you may not be able to use some or most of the features offered by the Application unless you register with us. You will simply be able to open the application.\n\nWhen you register with us and use the Application, you generally provide (a) your name, email address, user name/handle and other registration information; (b) information you provide us when you contact us for help and; (c) information you enter into our system when using the Application, such as contact information and other personal or business information.`}
          </Text>
          <Text style={styles.description}>
              {`We may also use the information you provided us to contact you from time to time to provide you with important information, required notices and marketing promotions.\n\nPhoneshake reserves the right to use your provided information, including but not limited to, Personally Identifiable Information, to their discretion. This includes but is not limited to improvements to experience, future improvements, general analytics and serving different content, and commercial use and resale. Users also guarantee a third party to also access this information and use for the same reasons as Phoneshake when they 'Share Contact' with others in or outside Phoneshake.`}
          </Text>
          <Text style={styles.heading}>Automatically Collected Information</Text>
          <Text style={styles.description}>
            In addition, the Application may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile devices unique device ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browsers you use, and information about the way you use the Application.
          </Text>
          <Text style={styles.heading}>Do third parties see and/or have access to information obtained by the Application?</Text>
          <Text style={styles.description}>
            {`Although we currently do not, in the future, when you visit the mobile application, we may use GPS technology (or other similar technology) to determine your current location in order to determine the city you are located within and display any relevant information, including advertisements. We will not share your current location with other users or partners unless provided permission by you.\n\nIf you do not want us to use your location for the purposes set forth above, you should turn off the location services for the mobile application located in your account settings or in your mobile phone settings and/or within the mobile application.`}
          </Text>
          <Text style={styles.heading}>Do third parties see and/or have access to information obtained by the Application?</Text>
            <Text style={styles.description}>
              {`Yes. We will share your information with third parties only in the ways that are described in this privacy statement.\n\nWe may disclose User Provided and Automatically Collected Information:`}
            </Text>
            <Text style={styles.description}>
              {`      • as required by law, such as to comply with a subpoena, or similar legal process;
      • when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request;
      • with our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them, and have agreed to adhere to the rules set forth in this privacy statement.
      • if Phoneshake is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of this information, as well as any choices you may have regarding this information.
      `}
            </Text>
            <Text style={styles.heading}>What are my opt-out rights?</Text>
            <Text style={styles.description}>
              {`You can stop all collection of information by the Application easily by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network. You can also request to opt-out via email, at hello@phoneshake.me.`}
            </Text>
            <Text style={styles.heading}>Data Retention Policy, Managing Your Information</Text>
            <Text style={styles.description}>
              {`We will retain User Provided data for as long as you use the Application and for a reasonable time thereafter. We will retain Automatically Collected information for up to 120 months and thereafter may store it in aggregate. If you'd like us to delete User Provided Data that you have provided via the Application, please contact us at hello@phoneshake.me and we will respond in a reasonable time. Please note that some or all of the User Provided Data may be required in order for the Application to function properly.`}
            </Text>
            <Text style={styles.heading}>Children</Text>
            <Text style={styles.description}>
              {`We do not use the Application to knowingly solicit data from or market to children under the age of 13. If a parent or guardian becomes aware that his or her child has provided us with information without their consent, he or she should contact us at hello@phoneshake.me. We will delete such information from our files within a reasonable time.`}
            </Text>
            <Text style={styles.heading}>Security</Text>
            <Text style={styles.description}>
              {`We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain. For example, we limit access to this information to authorized employees and contractors who need to know that information in order to operate, develop or improve our Application. Please be aware that, although we endeavor provide reasonable security for information we process and maintain, no security system can prevent all potential security breaches.`}
            </Text>
            <Text style={styles.heading}>Changes</Text>
            <Text style={styles.description}>
              {`This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new Privacy Policy here (https://phoneshake.me) and/or informing you via email or in app notifications You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes. You can check the history of this policy on our website at https://phoneshake.me.`}
            </Text>
            <Text style={styles.heading}>Your Consent</Text>
            <Text style={styles.description}>
              {`By using the Application, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us. "Processing," means using cookies on a computer/hand held device or using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information, all of which activities will take place in the United States. If you reside outside the United States your information will be transferred, processed and stored there under United States privacy standards.`}
            </Text>
            <Text style={styles.heading}>Contact us</Text>
            <Text style={styles.description}>
              {`If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at hello@phoneshake.me.`}
            </Text>
            <Text style={styles.heading}>Phoneshake Platform Privacy Policy</Text>
            <Text style={styles.description}>
              {`This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.`}
            </Text>
            <Text style={styles.heading}>What personal information do we collect from the people that visit our website or app?</Text>
            <Text style={styles.description}>
              {`When registering on our site, as appropriate, you may be asked to enter your name, email address, phone number, external social network credentials or other details to help you with your experience.`}
            </Text>
            <Text style={styles.heading}>When do we collect information?</Text>
            <Text style={styles.description}>
              {`We collect information from you when you register on our site, subscribe to a newsletter, fill out a form, Use Live Chat or enter information on our site.`}
            </Text>
            <Text style={styles.heading}>How do we use your information?</Text>
            <Text style={styles.description}>
              {`We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:`}
            </Text>
            <Text style={styles.description}>
              {`      • To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.
      • To improve our website in order to better serve you.
      • To allow us to better service you in responding to your customer service requests.
      • To quickly process your transactions.
      • To ask for ratings and reviews of services or products
      • To follow up with them after correspondence (live chat, email or phone inquiries)
`}
            </Text>
            <Text style={styles.heading}>How do we protect your information?</Text>
            <Text style={styles.description}>
              {`We do not use vulnerability scanning and/or scanning to PCI standards.\n\nWe only provide articles and information. We never ask for credit card numbers. We do not use Malware Scanning. Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.`}
            </Text>
            <Text style={styles.heading}>Do we use 'cookies'?</Text>
            <Text style={styles.description}>
              {`Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.`}
            </Text>
            <Text style={styles.heading}>We use cookies to:</Text>
            <Text style={styles.description}>
              {`      • Understand and save user's preferences for future visits.
      • Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.\n\nYou can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.`}
            </Text>
            <Text style={styles.heading}>If users disable cookies in their browser:</Text>
            <Text style={styles.description}>
              {`If you turn cookies off, some features will be disabled. Some of the features that make your site experience more efficient and may not function properly. You will lose some features such as, but not limited to, the ability to login and use the website.`}
            </Text>
            <Text style={styles.heading}>Third-party disclosure</Text>
            <Text style={styles.heading}>Do we disclose the information we collect to Third-Parties?</Text>
            <Text style={styles.description}>
              {`We do not at the time of this release, sell, trade, or otherwise transfer to outside parties your name, or other information attached to your Phoneshake profile including, but not limited to Personally Identifiable Information. Phoneshake, Inc. reserves the right to do so in the future.`}
            </Text>
            <Text style={styles.heading}>Third-party links</Text>
            <Text style={styles.description}>
              {`Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.`}
            </Text>
            <Text style={styles.heading}>Google</Text>
            <Text style={styles.description}>
              {`Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en. We have not enabled Google AdSense on our site but we may do so in the future.`}
            </Text>
            <Text style={styles.heading}>California Online Privacy Protection Act</Text>
            <Text style={styles.description}>
              {`CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf.`}
            </Text>
            <Text style={styles.heading}>According to CalOPPA, we agree to the following:</Text>
            <Text style={styles.description}>
              {`Users can visit our site anonymously. Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website. Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.\n\nYou will be notified of any Privacy Policy changes:
      • On our Privacy Policy Page and/or the Application
Can change your personal information:
      • By emailing us
      • By logging in to your account
      • By chatting with us or by sending us a support ticket`}
            </Text>
            <Text style={styles.heading}>Does our site allow third-party behavioral tracking?</Text>
            <Text style={styles.description}>
              {`It's important to note that we allow third-party behavioral tracking.`}
            </Text>
            <Text style={styles.heading}>COPPA (Children Online Privacy Protection Act)</Text>
            <Text style={styles.description}>
              {`When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online. We do not specifically market to children under the age of 13 years old.`}
            </Text>
            <Text style={styles.heading}>Fair Information Practices</Text>
            <Text style={styles.description}>
              {`The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.`}
            </Text>
            <Text style={styles.heading}>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</Text>
            <Text style={styles.description}>
              {`We will notify you via email\n
      • Other
      Within 30 days of breach being determined.\n\nWe also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.`}
            </Text>
            <Text style={styles.heading}>CAN SPAM Act</Text>
            <Text style={styles.description}>
              {`The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.`}
            </Text>
            <Text style={styles.heading}>We collect your email address in order to:</Text>
            <Text style={styles.description}>
              {`      • Send information, respond to inquiries, and/or other requests or questions
      • Process orders and to send information and updates pertaining to orders.
      • Send you additional information related to your product and/or service
      • Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.`}
            </Text>
            <Text style={styles.heading}>To be in accordance with CANSPAM, we agree to the following:</Text>
            <Text style={styles.description}>
              {`      • Not use false or misleading subjects or email addresses.
      • Identify the message as an advertisement in some reasonable way.
      • Include the physical address of our business or site headquarters.
      • Monitor third-party email marketing services for compliance, if one is used.
      • Honor opt-out/unsubscribe requests quickly.
      • Allow users to unsubscribe by using the link at the bottom of each email.`}
            </Text>
            <Text style={styles.description}>If at any time you would like to unsubscribe from receiving future emails, you can email us at hello@phoneshake.me and we will promptly (within 30 days) remove you from ALL correspondence.</Text>
            <Text style={styles.heading}>Contacting Us</Text>
            <Text style={styles.description}>
              {`If there are any questions regarding this privacy policy, you may contact us using the information below.\n\nhttps://phoneshake.me\n973 NW Cleveland Ave #3\nCorvallis, OR 97330\nUnited States\nhello@phoneshake.me`}
            </Text>
          </View>
        </Content>
        <View style={styles.bottomView}>
          <View style={styles.bottomItems}>
            {this.props.user.isRTL === false && <TouchableOpacity onPress={this.toggleCheckbox} style={styles.checkbox}>
              <Icon name={(this.state.checked) ? 'md-checkbox' : 'md-square-outline'} style={iconStyle}/>
            </TouchableOpacity>}

            {this.renderPrivacy(disabled)} 
            
            {this.props.user.isRTL === true && <TouchableOpacity onPress={this.toggleCheckbox} style={styles.checkbox}>
              <Icon name={(this.state.checked) ? 'md-checkbox' : 'md-square-outline'} style={[iconStyle, {marginLeft: 10}]}/>
            </TouchableOpacity>}
          </View>
          <PhoneShakeButton
            disabled={disabled}
            onPress={() => this.handleSubmit()}
            text={strings("constants.complete")}/>
        </View>
      </Container>
    );
	}
}

const stateToProps = (state) => ({
  user: state.login
});


export default connect(stateToProps, null)(PrivacyPolicy)
