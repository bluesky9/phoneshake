

export function getAllMessages(messages) {
  return {
    type: "ALL_MESSAGES",
    payload: messages
  }
}

export function updateConnectionStatus(status) {
  return {
    type: "CONNECTION_STATUS",
    payload: status
  }
}

export function updateCameraPermission(status) {
  return {
    type: "PERMISSION_STATUS",
    payload: status
  }
}

export function clearMessages() {
  return {
    type: "CLEAR_MESSAGES",
    payload: true
  }
}