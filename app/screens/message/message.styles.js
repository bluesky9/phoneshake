import { StyleSheet, Platform } from 'react-native'
import { Metrics, Colors, Fonts } from '../../themes'
export default StyleSheet.create({
  container: {
    flex: 1,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  subContainer: {
    backgroundColor: 'rgb(247, 247, 247)',
    flex: 1
  },
  header: {
    marginTop: Metrics.screenHeight * 0.022,
    marginLeft: Metrics.screenWidth * 0.04,
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  label: {
    zIndex: 1,
    fontSize: Metrics.fontSizeSubheading,
    color: 'black',
    backgroundColor: 'transparent'
  },
  headerLeft: {
    height: 24,
    width: 24,
    marginLeft: 12
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 16,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalNav: {
    height: Metrics.screenHeight * 0.065,
    paddingTop: (Platform.OS === 'ios') ? 0 : Metrics.screenHeight * 0.001,
    width: '100%',
    paddingHorizontal: Metrics.screenWidth * 0.042,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)'
  },
  modalTitle: {
    marginTop: 10,
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.05,
    color: '#000',
    flex: 2,
    textAlign: 'center'
  },
  skipButton: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.04,
    color: Colors.blueTheme
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 25
  },
  swipeMenu: {
    fontSize: Metrics.screenWidth * 0.037,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#fff',
    // textAlign: 'center'
  },
  add: {
    fontSize: Metrics.screenWidth * 0.064,
    color: Colors.blueTheme
  },
  emptyMessages: {
    backgroundColor: 'rgb(247, 247, 247)',
    flex: 1,
    alignItems: 'center',
    paddingTop: Metrics.screenHeight * 0.232
  },
  emptyMessageTitle: {
    fontSize: Metrics.screenWidth * 0.053,
    fontFamily: Fonts.type.SFTextMedium,
    color: '#000',
    textAlign: 'center',
    marginBottom: Metrics.screenHeight * 0.018
  },
  emptyMessageDesc: {
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)',
    textAlign: 'center',
    marginBottom: Metrics.screenHeight * 0.034
  },
  listItem: {
    width: Metrics.screenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0,
    borderColor: 'rgb(204, 204, 204)',
    paddingLeft: Metrics.screenWidth * 0.024,
    marginLeft: 0,
    paddingTop: 0,
    // backgroundColor: 'red',
    height: 70
  },
  userImage: {
    height: Metrics.screenWidth * 0.13,
    width: Metrics.screenWidth * 0.13,
    borderRadius: (Platform.OS === 'android') ? Metrics.screenWidth * 0.13 : Metrics.screenWidth * 0.065,
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    width: (Platform.OS === 'ios') ? '65%' : '75%',
    lineHeight: 22
  },
  listNote: {
    marginTop: 5,
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.037,
    fontFamily: Fonts.type.SFTextRegular,
    marginBottom: 9
  },
  listTime: {
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    backgroundColor: 'transparent',
    lineHeight: 14
  },
  listLeft: {
    height: '100%',
    borderBottomWidth: 0,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginRight: 10
    // borderBottomColor: 'rgb(204, 204, 204)'
  },
  listRight: {
    height: '100%',
    borderBottomWidth: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingRight: 9,
    paddingTop: 11,
    borderBottomColor: 'rgb(204, 204, 204)'
  },
  listBody: {
    flex: 5,
    height: '100%',
    borderBottomWidth: 0.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomColor: 'rgb(204, 204, 204)'
  },
  flatList: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgb(247, 247, 247)'
  },
  separator: {
    position: 'absolute',
    bottom: 0,
    height: 0.5,
    backgroundColor: 'rgb(204, 204, 204)',
    width: '100%'
  },
  badge: {
    marginTop:6,
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: Colors.blueTheme,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end'
  },
  badgeCount: {
    fontSize: 12,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#fff',
    textAlign: 'center'
  }
});
