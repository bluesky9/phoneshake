import React, { Component } from 'react';
import { View, FlatList, Linking, TouchableOpacity, RefreshControl, Platform, Text, Modal as RNModal, ActivityIndicator } from 'react-native';
import { Button, Icon } from "native-base";
import { SafeAreaView } from 'react-navigation'
import SendBird from 'sendbird';
import { MaterialIndicator } from 'react-native-indicators';
import * as actions from './message.actions';
import Modal from "react-native-modal";
import Swipeable from 'react-native-swipeable';
import { connect } from 'react-redux'
import styles from './message.styles';
import modalStyle from '../me/signupInfo/signupInfo.styles'
import { contactBlockUnblock } from '../contacts/contacts.actions'
import PhoneShakeButton from '../../components/phoneShakeButton'
import MessageItem from '../../components/message/messageItem'
import MyContacts from '../contacts/myContact/myContact'
import Loader from '../../components/loader/index'
import { Metrics, Colors } from '../../themes';
import { SENDBIRD_APPID, SENDBIRD_TOKEN } from '../../api/constants'
import { strings } from '../../locale';
const sb = new SendBird({ appId: SENDBIRD_APPID });

class Messages extends Component {
  swipeable = null;
  constructor() {
    super();
    this.state = {
      fetching: true,
      modalVisible: false,
      refreshing: false,
      showMoreOptions: false,
      currentlyOpenSwipeable: null,
      blocking: false,
      selectedChannel: {},
      selectedUser: {}
    };
    this.renderMessages = this.renderMessages.bind(this);
    this.renderEmptyMessages = this.renderEmptyMessages.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderMoreOptions = this.renderMoreOptions.bind(this);
    this._connect = this._connect.bind(this);
    this.handleSelectContact = this.handleSelectContact.bind(this);
    this.gotoChat = this.gotoChat.bind(this);
    this.reportAction = this.reportAction.bind(this);
    this.blockAction = this.blockAction.bind(this);
    this.muteAction = this.muteAction.bind(this);
    this.createChannel = this.createChannel.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.connectionStatus === 'connected' || newProps.connectionStatus === 'failed') {
      this.setState({ fetching: false })
    }
  }

  componentDidMount() {
    if (this.props.connectionStatus === 'connected' || this.props.connectionStatus === 'failed' || this.props.messages.length > 0) {
      this.setState({ fetching : false })
    }
    
  }
  
  createChannel(user) {
    fetch('https://api.sendbird.com/v3/group_channels', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Api-Token': SENDBIRD_TOKEN,
      },
      body: JSON.stringify({
        user_ids: [user.id, this.props.userId],
        operator_ids: [user.id, this.props.userId],
        is_distinct : true
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({ modalVisible: false })
      this.props.navigation.navigate('Chat', { user, channelUrl: responseJson.channel_url, })
    })
    .catch((error) => {
      console.error(error);
      return;
    });
  }

  handleSelectContact = (user, handleName, isFriend, username, image) => {
    let _this = this;
    user.handleName = handleName
    user.isFriend = isFriend
    user.nickname = user.name
    user.profile_pic = image
    user.user_id = user.id
    this.props.navigation.navigate('Chat', { user })
    this.setState({ modalVisible: false })
    // this.createChannel(user)
  }

  _connect() {
    if (this.props.connectionStatus === 'connected') {
      let _this = this;
      var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
      channelListQuery.includeEmpty = false;
      channelListQuery.limit = 100; // pagination limit could be set up to 100

      if (channelListQuery.hasNext) {
        channelListQuery.next(function (channelList, error) {
          if (error) {
            console.error(error);
            _this.setState({ fetching: false })
            return;
          }
          _this.setState({ fetching: false })
          _this.props.getAllMessages(channelList)
        });
      }
    }
  }

  muteAction = () => {
    const { selectedChannel, selectedUser } = this.state;
    if (this.state.currentlyOpenSwipeable) {
      this.state.currentlyOpenSwipeable.recenter();
    }
    this.setState({ showMoreOptions: false })
    // this.state.channel.muteUserWithUserId(user.userId)
    console.log('selected: ', selectedUser.userId)
  }

  blockAction = () => {
     let { selectedUser } = this.state;
    if (this.state.currentlyOpenSwipeable) {
      this.state.currentlyOpenSwipeable.recenter();
    }
    this.setState({ showMoreOptions: false })
    setTimeout(() => {
      this.setState({ blocking: true })
      let data = {
        "requestorUserId": this.props.userId,
        "requesteeUserId": selectedUser.userId,
        "isBlocked": true
      }
      this.props.contactBlockUnblock(data)
        .then((response) => {
          this.setState({ blocking: false })
          if (response.payload.data && response.payload.data.status !== 200) {
            setTimeout(() => {
              alert(strings('constants.blockingAlert'))
            }, 300);
          } else {
            const sb = SendBird.getInstance();
            if(sb) {
              let _this = this;
              sb.blockUserWithUserId(selectedUser.userId, (user, error) => {
                if (error) {
                  console.log('error block: ', error);
                } else {
                  console.log('block User: ', user);
                  _this._connect()
                }
              })
            }
          }
        })
    }, 500);
  }

  reportAction = () => {
    let { selectedUser } = this.state;
    if (this.state.currentlyOpenSwipeable) {
      this.state.currentlyOpenSwipeable.recenter();
    }
    this.setState({ showMoreOptions: false })
    let url = `mailto:hello@phoneshake.me?subject=Reporting ${selectedUser.nickname}`
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  renderContacts() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.modalNav}>
          <Button transparent style={{ flex: 0.7 }}  >
            <Text style={styles.skipButton}>{''}</Text>
          </Button>
          <Text style={styles.modalTitle}>{strings('constants.contacts')}</Text>
          <Button
            transparent
            style={{ flex: 0.5, justifyContent:'flex-end' }} 
            onPress={() => this.setState({ modalVisible: false })} >
              <Text style={styles.skipButton}>{strings('constants.cancel')}</Text>
          </Button>
        </View>

        <MyContacts handleSelectContact={this.handleSelectContact} />
      </View>
    )
  }

  renderNewChat() {
    return (
      <RNModal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => { console.log('modal closed') }}>
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
          {
            this.renderContacts()
          }
        </SafeAreaView>
      </RNModal>
    )
  }

  renderEmptyMessages() {
    return(
      <View style={[styles.emptyMessages]}>
        <Text style={styles.emptyMessageTitle}>{strings('messageTab.emptyMessageTitle')}</Text>
        <Text style={styles.emptyMessageDesc}>{strings('messageTab.emptyMessageDesc')}</Text>
        <PhoneShakeButton
          onPress={() => {this.setState({ modalVisible: true })}}
          text={strings('messageTab.beginChatting')}
          disabled={false} />
      </View>
    )
  }

  renderMoreOptions() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.showMoreOptions} >
        <View style={styles.modalContainer} key={'actions'}>
          <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.3 }]}>
            <View style={[styles.modalActionSheet]}>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.muteAction}>
                <Text style={modalStyle.regularText}>{strings('constants.muteNotifications')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.blockAction}>
                <Text style={modalStyle.distructiveText}>{strings('constants.blockContact')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.reportAction}>
                <Text style={modalStyle.distructiveText}>{strings('constants.report')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ showMoreOptions: false })}>
                <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  gotoChat = (item, user) => {
    user.profile_pic = user.profileUrl
    user.user_id = user.userId
    this.props.navigation.navigate('Chat', { channelUrl: item.url, user })
  }

  renderItem({ item, index }) {
    const { currentlyOpenSwipeable } = this.state;
    const itemProps = {
      onOpen: (event, gestureState, swipeable) => {
        if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
          currentlyOpenSwipeable.recenter();
        }

        this.setState({ currentlyOpenSwipeable: swipeable });
      },
      onClose: () => this.setState({ currentlyOpenSwipeable: null })
    };

    let user = item.members.filter(user => {
      return parseInt(user.userId) !== this.props.userId
    })[0]
    
    if(!user) return;

    return (
      <Swipeable
        key={index}
        onRef={ref => this.swipeable = ref}
        rightButtons={[
          <TouchableOpacity activeOpacity={0.7}
            style={[styles.rightSwipeItem, { backgroundColor: Colors.blueTheme }]}
            onPress={() => {}}>
              <Text style={styles.swipeMenu}>{strings('constants.mute')}</Text>
          </TouchableOpacity>,

          <TouchableOpacity activeOpacity={0.7}
            style={[styles.rightSwipeItem, { backgroundColor: 'rgb(168, 168, 168)' }]}
            onPress={() => {this.setState({ showMoreOptions: true, selectedChannel: item, selectedUser: user })}}>
              <Text style={styles.swipeMenu}>{strings('constants.more')}</Text>
          </TouchableOpacity>
        ]}
        onRightButtonsOpenRelease={itemProps.onOpen}
      > 
        <MessageItem
          user={user}
          item={item}
          _connect={() => this._connect()}
          onPress={(x, y) => this.gotoChat(x, y)}/>
      </Swipeable>
    )
  }

  keyExtractor = (item, index) => index.toString();

  renderMessages() {
    let messages = this.props.messages.filter(channel => {
      return channel.members.length > 1
    })
    
    return (
      <View style={styles.flatList}>
        <FlatList
          data={messages}
          removeClippedSubviews={false}
          initialNumToRender={20}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          style={{ width: '100%' }}
          refreshControl={
            <RefreshControl
              tintColor={Colors.blueTheme}
              refreshing={this.state.refreshing}
              onRefresh={this._connect}
            />
          }
        />
      </View>
    )
  }

  render() {
      let messages = this.props.messages.filter(channel => {
        return channel.members.length > 1
      })
      return (
        <SafeAreaView style={styles.container}>
          <View style={[styles.modalNav, { paddingRight: 0 }]}>
            <Button transparent style={{ flex: 0.65 }}  >
              <Text style={styles.skipButton}>{''}</Text>
            </Button>
            <Text style={styles.modalTitle}>{strings('constants.message')}</Text>
            <Button transparent style={{ flex: 0.8, justifyContent: 'flex-end' }} onPress={() => this.setState({ modalVisible: true })} >
              <Icon name='md-add' style={styles.add} />
            </Button>
          </View>
          { 
            (this.state.fetching) ?
              <View style={[styles.container]}>
                {
                  (Platform.OS === 'ios') ?
                    <MaterialIndicator
                      size={35}
                      color={Colors.blueTheme}
                      animating={this.state.fetching} />
                    :
                    <ActivityIndicator
                      size={35}
                      color={Colors.blueTheme}
                      animating={this.state.fetching} />
                }
              </View>
            : 

            (messages.length > 0) ?
              this.renderMessages()
              :
              this.renderEmptyMessages()
          }
          {this.renderNewChat()}
          {this.renderMoreOptions()}
          <Loader loading={this.state.blocking} />
        </SafeAreaView>
      );
    }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  name: state.login.name,
  messages: state.messages.messages,
  connectionStatus: state.messages.connectionStatus
});

const dispatchToProps = (dispatch) => {
  return {
    getAllMessages: (messages) => dispatch(actions.getAllMessages(messages)),
    contactBlockUnblock: (data) => dispatch(contactBlockUnblock(data)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(Messages)

export default Container;
