const initialState = {
  messages: [],
  messageCount: 0,
  connectionStatus: 'failed',
  permissionStatus: 'unknown'
}


export default function messageReducer(state = initialState, action) {
  switch (action.type) {
    case "ALL_MESSAGES": {
      let unreadCount = 0
      action.payload.map((message) => {
        if (message.unreadMessageCount > 0) {
          unreadCount = unreadCount + 1
        }
      })
      return {
        ...state,
        messages: action.payload,
        messageCount: unreadCount,
        connectionStatus: 'connected'
      }
    }
    case "CONNECTION_STATUS": {
      return {
        ...state,
        connectionStatus: action.payload
      }
    }
    case "PERMISSION_STATUS": {
      return {
        ...state,
        permissionStatus: action.payload
      }
    }
    case "CLEAR_MESSAGES": {
      return {
        ...state,
        messages: [],
        messageCount: 0,
        connectionStatus: 'failed'
      }
    }
    default: {
      return {
        ...state
      };
    }
  }
}
