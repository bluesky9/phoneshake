import { StyleSheet, Platform } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    // width: Metrics.screenWidth,
    backgroundColor: 'rgb(247, 247, 247)'
  },
  containerStyle: {
    marginBottom: 15
  },
  textStyle: {
    fontSize: 10
  },
  headerView: {
    backgroundColor: '#fff',
    height: Metrics.screenHeight * 0.065,
    width: Metrics.screenWidth,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: (Platform.OS === 'ios') ? 0 : 0,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)'
  },
  navbarview: {
    marginTop: Metrics.navBarTop,
    flexDirection: 'row',
  },
  backIcon: {
    fontSize: 32,//24,
    color: Colors.blueTheme,
    backgroundColor: 'transparent'
  },
  navbarCenterView: {
    flex: 2,
    alignItems: 'center',
    flexDirection: 'row',
  },
  backButtonView: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5
  },
  titleView: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    color: Colors.blueTheme,
    textAlign: 'left',
    backgroundColor: 'transparent',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextMedium,
    marginTop: 2
  },
  statusMessage: {
    color: 'white',
    textAlign: 'left',
    backgroundColor: 'transparent',
    fontSize: Metrics.screenWidth * 0.035,
    marginTop: 1
  },
  userImage: {
    height: Metrics.screenWidth * 0.08,
    width: Metrics.screenWidth * 0.08,
    borderRadius: Metrics.screenWidth * 0.04,
    marginTop: Metrics.screenHeight * 0.005,
    marginRight: 10
  },
  requestView: {
    width: Metrics.screenWidth - Metrics.screenWidth * 0.106,
    position: 'absolute',
    top: Metrics.screenHeight * 0.149 + 2,
    backgroundColor: '#b2b2b2',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Metrics.screenHeight * 0.05,
    paddingHorizontal: Metrics.screenWidth * 0.053,
    paddingVertical: 2,
    marginHorizontal: Metrics.screenWidth * 0.053
  },
  headerText: {
    fontSize: Metrics.screenWidth * 0.041,
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  closeIcon: {
    height: Metrics.screenWidth * 0.064,
    width: Metrics.screenWidth * 0.064
  },
  closeButton: {
    flex: 0.4,
    justifyContent: 'center',
    marginRight: Metrics.screenWidth * 0.042
  },
  timeLeft: {
    color: 'rgb(127, 127, 127)',
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032
  },
  timeRight: {
    color: 'rgb(127, 127, 127)',
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032
  },
  timeContainer: {
    marginLeft: 0,
    marginRight: 0,
    marginBottom: -20,
    bottom: -3
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 16,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
});
