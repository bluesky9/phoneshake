import React, { Component } from 'react';
import { View, TouchableOpacity, Platform, Text, Image, ActivityIndicator, Linking, SafeAreaView } from 'react-native';
import SoftInputMode from 'react-native-set-soft-input-mode';
import { GiftedChat, Bubble, MessageText, Time } from 'react-native-gifted-chat';
import { Icon, Button } from 'native-base';
import { connect } from 'react-redux'
import Modal from "react-native-modal";
import { MaterialIndicator } from 'react-native-indicators';
import modalStyle from '../../me/signupInfo/signupInfo.styles'
import SendBird from 'sendbird';
import styles from './chat.styles.js';
import Loader from '../../../components/loader/index'
import * as actions from '../message.actions';
import { contactBlockUnblock } from '../../contacts/contacts.actions'
import { Images, Fonts, Metrics, Colors } from '../../../themes/index.js'
import { SENDBIRD_APPID, SENDBIRD_TOKEN } from '../../../api/constants'
import { strings } from '../../../locale';
const sb = new SendBird({ appId: SENDBIRD_APPID });
var ChannelHandler = new sb.ChannelHandler();

class Conversation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      channel: {},
      messages: [],
      loading: true,
      blocking: false,
      showMoreOptions: false,
      is_muted: false,
      isConnected: false
    }
    this.mounted = false
    this.renderLoading = this.renderLoading.bind(this);
    this.onSend = this.onSend.bind(this);
    this.backAction = this.backAction.bind(this);
    this.renderMessages = this.renderMessages.bind(this);
    this.renderTime = this.renderTime.bind(this);
    this.renderAvatar = this.renderAvatar.bind(this);
    this.getChannelMetaData = this.getChannelMetaData.bind(this);
    this.renderMoreOptions = this.renderMoreOptions.bind(this);
    this.reportAction = this.reportAction.bind(this);
    this.blockAction = this.blockAction.bind(this);
    this.muteAction = this.muteAction.bind(this);
    this.gotoUserProfile = this.gotoUserProfile.bind(this);
    this.createChannel = this.createChannel.bind(this);
  }

  componentDidMount() {
    if(Platform.OS === 'android') {
      SoftInputMode.set(SoftInputMode.ADJUST_RESIZE);
    }
    this.mounted = true
    if (this.props.connectionStatus === 'connected') {
      this.setState({ isConnected: true })
      let { user } = this.props.navigation.state.params
      this.createChannel(user)
    } else if (this.props.connectionStatus === 'failed') {
      this.setState({ isConnected: false, loading: false })
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.connectionStatus === 'connected' && this.mounted) {
      this.setState({ isConnected: true })
      let { user } = this.props.navigation.state.params
      this.createChannel(user)
    } else if (newProps.connectionStatus === 'failed' && this.mounted) {
      this.setState({ isConnected: false, loading: false })
    }
  }

  createChannel(user) {
    let _this = this;
    fetch('https://api.sendbird.com/v3/group_channels', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Api-Token': SENDBIRD_TOKEN,
      },
      body: JSON.stringify({
        user_ids: [user.user_id, this.props.userId],
        operator_ids: [user.user_id, this.props.userId],
        is_distinct : true
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log('responseJson.channel_url: ', responseJson.channel_url);
      
      _this.setupChannel(responseJson.channel_url)
      _this.checkForMute(user, responseJson.channel_url)
    })
    .catch((error) => {
      console.error(error);
      return;
    });
  }

  checkForMute(user, channelUrl) {
    fetch(`https://api.sendbird.com/v3/group_channels/${channelUrl}/mute/${user.user_id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Api-Token': SENDBIRD_TOKEN,
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(this.mounted && !responseJson.error)
      this.setState({ is_muted: responseJson.is_muted })
    })
    .catch((error) => {
      console.error(error);
      return;
    });
  }

  refreshList() {
    let _this = this;
    var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
    channelListQuery.includeEmpty = false;
    channelListQuery.limit = 100; // pagination limit could be set up to 100

    if (channelListQuery.hasNext) {
      channelListQuery.next(function (channelList, error) {
        if (error) {
          console.error(error);
          return;
        }
        _this.props.getAllMessages(channelList)
      });
    }
  }

  setupChannel(channelUrl) {
    let _this = this;
    sb.GroupChannel.getChannel(channelUrl, function (channel, error) {
      if (error) {
        console.error(error);
        return;
      }
      // console.log('channel: ', channel);
      
      _this.setState({ channel })
      channel.markAsRead();
      _this.getChannelMetaData(channel)
    });

    ChannelHandler.onMessageReceived = function (channel, message) {
      if(channel.url === _this.state.channel.url) {
        let formattedMessage = {
          _id: message.messageId,
          text: message.message,
          createdAt: message.createdAt,
          user: {
            _id: parseInt(message._sender.userId),
            name: message._sender.nickname
          },
        }
        channel.markAsRead();
        if(_this.mounted) {
          _this.setState((previousState) => {
            return { messages: GiftedChat.append(previousState.messages, formattedMessage), channel };
          });
        }
      }
    };

    ChannelHandler.onUserMuted = function (channel, user) {
      if (channel.url === _this.state.channel.url) {
        _this.setState({ is_muted: true })
      }
    };

    ChannelHandler.onUserUnmuted = function (channel, user) {
      if (channel.url === _this.state.channel.url) {
        _this.setState({ is_muted: false })
      }
    };

    sb.addChannelHandler('onReceive', ChannelHandler);
  }

  componentWillUnmount() {
    if(Platform.OS === 'android') {
      SoftInputMode.set(SoftInputMode.ADJUST_PAN);
    }
    this.mounted = false
    sb.removeChannelHandler('onReceive');
  }

  getChannelMetaData(channel) {
    if (channel) {
      const self = this;
      const messagesQuery = channel.createPreviousMessageListQuery();
      messagesQuery.load(50, true, (messages, error) => {
        if (error) {
          console.error(error);
          this.setState({ loading: false })
        } else {
          // console.log('last messages: ', messages);
          let formatedMessages = []
          messages.map((message) => {
            formatedMessages.push({
              _id: message.messageId,
              text: message.message,
              createdAt: message.createdAt,
              user: {
                _id: parseInt(message._sender.userId),
                name: message._sender.nickname
              },
            })
          })
          this.setState({
            messages: formatedMessages,
            loading: false
          });
        }
      });
    }
    return;
  }

  muteAction = () => {
    let { user } = this.props.navigation.state.params

    this.setState({ showMoreOptions: false })
    if(this.state.is_muted) {
      this.state.channel.unmuteUserWithUserId(user.userId, (user, error) => {
        if (error) {
          console.log('error unmute: ', error);
        }
      })
    } else {
      this.state.channel.muteUserWithUserId(user.userId, (user, error) => {
        if (error) {
          console.log('error mute: ', error);
        }
      })
    }
  }

  blockAction = () => {
    let { user } = this.props.navigation.state.params
    this.setState({ showMoreOptions: false })
    setTimeout(() => {
      this.setState({ blocking: true })
      let data = {
        "requestorUserId": this.props.userId,
        "requesteeUserId": user.userId,
        "isBlocked": true
      }
      this.props.contactBlockUnblock(data)
        .then((response) => {
          this.setState({ blocking: false })
          if (response.payload.data && response.payload.data.status !== 200) {
            setTimeout(() => {
              alert(strings('constants.blockingAlert'))
            }, 300);
          } else {
            const sb = SendBird.getInstance();
            if(sb) {
              sb.blockUserWithUserId(user.userId, (user, error) => {
                if (error) {
                  // console.log('error block: ', error);
                } else {
                  // console.log('block User: ', user);
                  this.props.navigation.goBack()
                }
              })
            }
          }
        })
    }, 500);
  }

  reportAction = () => {
    let { user } = this.props.navigation.state.params
    this.setState({ showMoreOptions: false })
    let url = `mailto:hello@phoneshake.me?subject=Reporting ${user.nickname}`
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  renderLoading() {
    return (
      <View style={{ marginTop: 20 }}>
        <ActivityIndicator animating={true} />
      </View>
    );
  }

  renderAvatar(props) {
    return <View />;
  }

  onSend(messages) {
    const handle = this;
    const sb = SendBird.getInstance();
    const channel = this.state.channel;
    this.setState((previousState) => {
      var userMessageParams = new sb.UserMessageParams();
      userMessageParams.message = messages[0].text;
      channel.sendUserMessage(userMessageParams, (response, error) => {
        if (!error) {
          // handle.getChannelMetaData(channel);
        }
      });
      return { messages: GiftedChat.append(previousState.messages, messages) };
    });
  }

  renderBubble(props) {
    const style = {
      left: { backgroundColor: Colors.blueTheme, borderRadius: 5, marginBottom: 20 },
      right: { backgroundColor: '#fff', borderRadius: 5, marginBottom: 20 },
    };
    return <Bubble {...props} wrapperStyle={style} />
  }

  renderTime(props) {
    return <Time
      {...props}
      containerStyle={{ left: (Platform.OS === 'ios') ? [styles.timeContainer] : null, right: (Platform.OS === 'ios') ? [styles.timeContainer] : null }}
      textStyle={{ left: [styles.timeLeft, { color: (Platform.OS === 'ios') ? 'rgb(127, 127, 127)' : '#fff'}], right: styles.timeRight }}
    />
  }

  renderMessageText(props) {
    return (
      <MessageText
        {...props}
        textStyle={{
          left: {
            color: 'white',
            fontFamily: Fonts.type.SFTextRegular,
            fontSize: Metrics.screenWidth * 0.042,
          },
          right: {
            color: 'rgb(42, 42, 42)',
            fontFamily: Fonts.type.SFTextRegular,
            fontSize: Metrics.screenWidth * 0.042,
          }
        }}
        linkStyle={{
          left: {
            color: 'white',
            fontFamily: Fonts.type.SFTextRegular,
            fontSize: Metrics.screenWidth * 0.042,
          },
          right: {
            color: 'rgb(42, 42, 42)',
            fontFamily: Fonts.type.SFTextRegular,
            fontSize: Metrics.screenWidth * 0.042,
          }
        }}
      />
    );
  }

  isCloseToTop({ layoutMeasurement, contentOffset, contentSize }) {
    const paddingToTop = 80;
    return contentSize.height - layoutMeasurement.height - paddingToTop <= contentOffset.y;
  }

  renderMessages() {
    const user = { _id: parseInt(this.props.userId) };
    return (
      
      <GiftedChat
        messages={this.state.messages}
        placeholder={strings('messageTab.typeMessage')}
        onSend={this.onSend}
        user={user}
        showAvatarForEveryMessage
        renderAvatar={this.renderAvatar}
        showUserAvatar={false}
        renderBubble={this.renderBubble}
        renderTime={this.renderTime}
        keyboardShouldPersistTaps={"never"}
        renderMessageText={this.renderMessageText}
        bottomOffset={(Platform.OS === 'ios') ? (Metrics.screenHeight > 800) ? 35 : 0 : 35}
        listViewProps={{
          removeClippedSubviews: false
        }}
        textInputProps={{
          editable:(this.state.isConnected) ? true : false
        }}
      />
    );
  }

  backAction() {
    if (this.props.connectionStatus === 'connected') {
      this.refreshList()
    }
    this.props.navigation.goBack()
  }

  gotoUserProfile = () => {
    let { user } = this.props.navigation.state.params
    let userData = {
      userId: user.userId,
      name: user.nickname,
      profile_pic: user.profile_pic
    }
    this.props.navigation.navigate('UserProfile', { user: userData, isFromChat: true })
  }

  renderHeader() {
    return (
      <View style={styles.headerView}>
        <TouchableOpacity style={styles.backButtonView} onPress={this.backAction}>
          <Image source={Images.backIcon} style={styles.closeIcon} resizeMode='contain' />
        </TouchableOpacity>
        {this.renderTitleView()}
        <TouchableOpacity style={styles.backButtonView} onPress={() => {
          if(this.state.isConnected)
            this.setState({ showMoreOptions: true })
        }}>
          <Image source={Images.moreIcon} style={styles.closeIcon} resizeMode='contain' />
        </TouchableOpacity>
      </View>
    );
  }

  renderTitleView() {
    let { user } = this.props.navigation.state.params
    return (
      <View style={[styles.titleView, { flex: 2 }]}>
        <Image borderRadius={15} source={(user.profile_pic) ? { uri: user.profile_pic } : Images.defaultUserImage} style={styles.userImage} />
        <TouchableOpacity onPress={this.gotoUserProfile}>
          <Text style={styles.title} >{user.nickname}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderMoreOptions() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.showMoreOptions} >
        <View style={styles.modalContainer} key={'actions'}>
          <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.3 }]}>
            <View style={[styles.modalActionSheet]}>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.muteAction}>
                <Text style={modalStyle.regularText}>{(!this.state.is_muted) ? strings('constants.muteNotifications') : strings('constants.unmuteNotifications')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.blockAction}>
                <Text style={modalStyle.distructiveText}>{strings('constants.blockContact')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.reportAction}>
                <Text style={modalStyle.distructiveText}>{strings('constants.report')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ showMoreOptions: false })}>
                <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  keyExtractor = (item, index) => index.toString();
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
      <View style={styles.container}>
        {this.renderHeader()}
        {(this.state.loading) ?
          <View style={styles.container}>
            {
              (Platform.OS === 'ios') ?
                <MaterialIndicator
                  size={20}
                  color={Colors.blueTheme}
                  animating={this.state.loading} />
                :
                <ActivityIndicator
                  size={20}
                  color={Colors.blueTheme}
                  animating={this.state.loading} />
            }
          </View>
          : null
        }
        {(!this.state.loading) ? this.renderMessages() : null }
        {this.renderMoreOptions()}
        <Loader loading={this.state.blocking} />
      </View>
      </SafeAreaView>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  name: state.login.name,
  connectionStatus: state.messages.connectionStatus
});

const dispatchToProps = (dispatch) => {
  return {
    contactBlockUnblock: (data) => dispatch(contactBlockUnblock(data)),
    getAllMessages: (messages) => dispatch(actions.getAllMessages(messages)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(Conversation)

export default Container;
