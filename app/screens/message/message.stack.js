import { createStackNavigator } from 'react-navigation'
import MessageScreen from './message'
import ChatScreen from "./chat/chat";
import UserDetails from '../contacts/userDetail/userDetail'
import ResetAccount from '../tabs/resetAccount'
import { strings } from '../../locale';

const MessageStack = createStackNavigator({
  Messages: {
    screen: MessageScreen,
    navigationOptions: {
      title: strings('constants.message')
    }
  },
  Chat: {
    screen: ChatScreen,
    navigationOptions: {
      title: '',
    }
  },
  UserProfile: {
    screen: UserDetails,
    navigationOptions: {
      title:  strings('constants.profile')
    }
  },
  ResetAccount: {
    screen: ResetAccount,
    navigationOptions: {
      title:  strings('constants.resetAccount')
    }
  }
}, {
    headerMode: 'none',
    initialRouteName: 'Messages',
    cardStyle: {
      backgroundColor: '#fff'
    },
    navigationOptions: {}
  });

MessageStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default MessageStack;
