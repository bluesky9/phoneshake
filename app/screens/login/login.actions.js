import { auth, user } from '../../api/api.js';

export function sendConfirmationCode(email, confirmationCode) {
	return {
		type: "SEND_CONFIRMATION_CODE",
		payload: auth.sendConfirmationCode(email, confirmationCode)
	}
}

export function sendConfirmationCodeBySMS(phone, confirmationCode) {
	return {
		type: "SEND_CONFIRMATION_CODE_BY_SMS",
		payload: auth.sendConfirmationCodeBySMS(phone, confirmationCode)
	}
}

export function registerUser(userInfo) {
	return {
		type: "REGISTER_USER",
		payload: user.registerUser(userInfo)
	}
}

export function deleteAccount(userId) {
	return {
		type: "DELETE_ACCOUNT",
		payload: user.deleteAccount(userId)
	}
}

export function logout(userId, token) {
	return {
		type: "LOGOUT_USER",
		payload: user.logout(userId, token)
	}
}

export function autoLogout() {
	return {
		type: "LOGOUT_USER",
		payload: true
	}
}

export function resumeUser(userInfo) {
	return {
		type: "GET_USER_BY_USERNAME",
		payload: userInfo
	}
}

export function isHandleNameUnique(handleName) {
	return {
		type: "IS_HANDLE_NAME_UNIQUE",
		payload: user.isHandleNameUnique(handleName)
	}
}

export function addNewUsername(data) {
	return {
		type: "ADD_NEW_USERNAME",
		payload: user.addNewUsername(data)
	}
}

export function removeUsername(userId, username) {
	return {
		type: "DELETE_USERNAME",
		payload: user.deleteUsername(userId, username)
	}
}

export function getUserByUsername(username, checkForLogin, isSocialMediaLinksRequired, isNotificationSettingsRequired, requestorUserId) {
	return {
		type: "GET_USER_BY_USERNAME",
		payload: user.getUserByUsername(username, checkForLogin, isSocialMediaLinksRequired, isNotificationSettingsRequired, requestorUserId)
	}
}

export function getUserByHandlename(handleName, requestorUserId) {
	return {
		type: "GET_USER_BY_HANDLENAME",
		payload: user.getUserByHandlename(handleName, requestorUserId)
	}
}

export function getUserByUserId(userId, requestorUserId) {
	return {
		type: "GET_USER_BY_HANDLENAME",
		payload: user.getUserByUserId(userId, requestorUserId)
	}
}

export function updateProfileSettings(data) {
	return {
		type: "UPDATE_PROFILE_SETTINGS",
		payload: user.updateProfileSettings(data)
	}
}

export function addSocialMediaDetails(data) {
	return {
		type: "ADD_SOCIAL_MEDIA_DETAILS",
		payload: user.addSocialMediaDetails(data)
	}
}

export function getSocialMediaDetailsByUserId(userId) {
	return {
		type: "GET_SOCIAL_MEDIA_DETAILS_BY_USERID",
		payload: user.getSocialMediaDetailsByUserId(userId)
	}
}

export function uploadPicture(data, type) {
	return {
		type: "UPLOAD_PICTURE",
		payload: user.uploadPicture(data, type)
	}
}

export function updateDeviceToken(data) {
	return {
		type: "UPDATE_DEVICE_TOKEN",
		payload: user.updateDeviceToken(data)
	}
}

export function getFriendRequests(data, fromGeneral) {
	return {
		type: "GET_FRIEND_REQUESTS",
		payload: user.getNotifications(data, fromGeneral)
	}
}

export function getFriendRequestsFromNotif(data, fromGeneral) {
	return {
		type: "GET_FRIEND_REQUESTS_FROM_NOTIFICATION",
		payload: user.getNotifications(data, fromGeneral)
	}
}

export function startFetching(value) {
	return {
		type: "START_FETCHING",
		payload: value
	}
}

export function getContactListWhoAddedMe(data) {
	return {
		type: "GET_CONTACT_WHO_ADDED_ME",
		payload: user.getContactListWhoAddedMe(data)
	}
}

export function acceptFriendRequest(data, userdata) {
	return {
		type: "ACCEPT_FRIEND_REQUEST",
		payload: user.acceptFriendRequest(data, userdata)
	}
}

export function setLocal(local) {
	return {
		type: "SET_LOCALE",
		payload: local
	}
}

export function getBusinessTypes() {
	return {
		type: "GET_BUSINESS_TYPES",
		payload: user.getBusinessTypes()
	}
}
