import initialState from './login.initialState'
import _ from 'lodash'

export default function loginReducer(state=initialState, action) {
	switch(action.type) {
		case "SEND_CONFIRMATION_CODE": {
			return {
				...state
			}
		}
		case "SEND_CONFIRMATION_CODE_BY_SMS": {
			return {
				...state
			}
		}
		case "DELETE_ACCOUNT": {
			return {
				...state,
				initialState
			}
		}
		case "START_FETCHING": {
			return {
				...state,
				isFetching: action.payload
			}
		}
		case "LOGOUT_USER": {
			state.linkedAccounts.map((item) => {
					item.id = '',
					item.link = ''
			})
			return {
				...state,
				token: null,
				initialState
			}
		}
		case "IS_HANDLE_NAME_UNIQUE": {
			return {
				...state
			}
		}
		case "UPDATE_DEVICE_TOKEN": {
			return {
				...state
			}
		}
		case "DELETE_USERNAME": {
			if (action.payload.data && action.payload.data.status === 200) {
				let removedUsername = action.payload.deletedUsername
				let newUserNames = state.usernames.filter((item) => item.username !== removedUsername)
				return {
					...state,
					usernames: newUserNames
				}
			} else {
				return {
					...state
				}
			}
		}
		case "ADD_NEW_USERNAME": {
			let addedUserInfo = action.payload.addedInfo
			let newUserNames = state.usernames
			newUserNames.push(addedUserInfo)
			return {
				...state,
				usernames: newUserNames
			}
		}
		case "REGISTER_USER": {
			if (action.payload.data && action.payload.data.status === 200 && action.payload.data.content) {
				let content = action.payload.data.content
				const userId = content.user_id
				const name = content.name
				const handleName = content.handle_name
				const organization = content.organization
				const profilePic = content.profile_pic
				const businessLogo = content.business_logo
				const address = content.address
				const description = content.description
				const designation = content.designation
				const fax = content.fax
				const isPrivateAccount = content.is_private_account
				const landLineNo = content.land_line_no
				const latitude = content.latitude
				const longitude = content.longitude
				const token = content.token
				const notificationSettings = (content.notificationSettings) ? content.notificationSettings : { acceptRequestWhenPrivate: 'Y', addRequestWhenPublic: 'Y', receiveRequestWhenPrivate: 'Y'}
				const usernames = content.usernames
				const email = content.email
				const mobileNo = content.mobile_no
				return {
					...state, userId, name, email, mobileNo, handleName, organization, profilePic, businessLogo, address, description, designation, fax, isPrivateAccount, landLineNo, latitude, longitude, token, usernames, notificationSettings
				}
			} else {
				return {
					...state
				}
			}
			
		}
		case "GET_USER_BY_USERNAME": {
			console.log('action.payload.data: - - - -- ', action.payload);
			
			if(action.payload.data && action.payload.data.status === 200 && action.payload.data.content) {
				let content = action.payload.data.content
				const userId = content.user_id
				const name = content.name
				const businessLogo = content.business_logo
				const handleName = content.handle_name
				const organization = content.organization
				const profilePic = content.profile_pic
				const address = content.address
				const description = content.description
				const designation = content.designation
				const mobileNo = content.mobile_no
				const email = content.email
				const fax = content.fax
				const isPrivateAccount = (content.is_private_account) ? content.is_private_account : false
				const landLineNo = content.land_line_no
				const latitude = content.latitude
				const longitude = content.longitude
				const token = content.token
				const notificationSettings = (content.notificationSettings) ? content.notificationSettings : {}
				const usernames = content.usernames
				let linkedAccounts = state.linkedAccounts
				let socialMediaAccounts = (content.socialMediaLinks && content.socialMediaLinks.length > 0) ? content.socialMediaLinks : []
				if(action.payload.checkForLogin === true) {
					socialMediaAccounts.map((media) => {
						linkedAccounts.map((item) => {
							if (item.type.toLowerCase() === media.type.toLowerCase()) {
								item.id = media.id,
								item.link = media.link	
							}
						})
					})
					return {
						...state, userId, name, handleName, organization, profilePic, businessLogo, address, email, description, designation, mobileNo, fax, isPrivateAccount, landLineNo, latitude, longitude, token, usernames, linkedAccounts, notificationSettings
					}
				} else {
					return {
						...state,
					}
				}
				
			} else {
				return {
					...state
				}
			}
		}
		case "UPDATE_PROFILE_SETTINGS": {
			if (action.payload.data && action.payload.data.status === 200) {
				let key = _.camelCase(action.payload.type)
				if(key === 'address') {
					return {
						...state,
						[key]: action.payload.value,
						latitude: action.payload.latitude,
						longitude: action.payload.longitude
					}
				} else {
					return {
						...state,
						[key]: action.payload.value
					}
				}
			} else {
				return {
					...state
				}
			}
		}
		case "ADD_SOCIAL_MEDIA_DETAILS": {
			if (action.payload.data && action.payload.data.status === 200) {
				let updatedAccount = action.payload.data.content.socialMediaAccount
				let linkedAccounts = state.linkedAccounts
				linkedAccounts.map((item) => {
					if (item.type.toUpperCase() === updatedAccount.type) {
						item.id = updatedAccount.id
						item.link = updatedAccount.link
					}
				})
				return {
					...state, linkedAccounts
				}
			} else {
				return {
					...state
				}
			}
		}
		case "GET_SOCIAL_MEDIA_DETAILS_BY_USERID": {			
			if (action.payload.data && action.payload.data.status === 200) {
				let linkedAccounts = state.linkedAccounts
				let socialMediaAccounts = action.payload.data.content.socialMediaAccount
				socialMediaAccounts.map((media) => {
					linkedAccounts.map((item) => {
						if(item.type.toUpperCase() === media.type) {
							item.id = media.id
							item.link = media.link
						}
					})
				})
				return {
					...state, linkedAccounts
				}
			} else {
				return {
					...state
				}
			}
		}
		case "UPLOAD_PICTURE": {
			if (action.payload.data && action.payload.data.status === 200) {
				if(action.payload.type === 'logo') {
					return {
						...state,
						businessLogo: action.payload.data.content.user.business_logo
					}
				} else {
					return {
						...state,
						profilePic: action.payload.data.content.user.profile_pic
					}
				}
			} else {
				return {
					...state
				}
			}
		}
		case "GET_FRIEND_REQUESTS": {
			if (action.payload.data && action.payload.data.status === 200) {
				let fromGeneral = action.payload.fromGeneral
				let friendRequests = (action.payload.data.content && action.payload.data.content.notifications) ? action.payload.data.content.notifications : []
				let friendRequestsCount = (action.payload.data.content && action.payload.data.content.unseenCount) ? action.payload.data.content.unseenCount : 0
				return {
					...state,
					friendRequests,
					friendRequestsFromNotif: [],
					friendRequestsCount: (fromGeneral) ? 0 : friendRequestsCount
				}
			} else if (action.payload.data && action.payload.data.status === 404) {
				return {
					...state,
					friendRequests: [],
					friendRequestsFromNotif: [],
					friendRequestsCount: 0
				}
			} else {
				return {
					...state
				}
			}
		}
		case "SET_LOCALE": {
			return {
				...state, 
				locale: action.payload,
				isRTL: action.payload.indexOf('he') === 0 || action.payload.indexOf('ar') === 0
			}
		}
		case "GET_FRIEND_REQUESTS_FROM_NOTIFICATION": {
			if (action.payload.data && action.payload.data.status === 200) {
				let friendRequestsFromNotif = (action.payload.data.content && action.payload.data.content.notifications) ? action.payload.data.content.notifications : []
				return {
					...state,
					friendRequestsFromNotif,
					friendRequestsCount: 0,
					isFetching: false
				}
			} else if (action.payload.data && action.payload.data.status === 404) {
				return {
					...state,
					friendRequestsFromNotif: [],
					friendRequestsCount: 0,
					isFetching: false
				}
			} else {
				return {
					...state
				}
			}
		}
		case "GET_CONTACT_WHO_ADDED_ME": {
			if (action.payload.data && action.payload.data.status === 200) {
				let addedMe = (action.payload.data.content && action.payload.data.content.contacts) ? action.payload.data.content.contacts : []
				return {
					...state,
					addedMe
				}
			} else if (action.payload.data && action.payload.data.status === 404) {
				return {
					...state,
					addedMe: []
				}
			} else {
				return {
					...state
				}
			}
		}
		case "ACCEPT_FRIEND_REQUEST": {
			if (action.payload.data && action.payload.data.status === 200) {
				let type = action.payload.type
				let userAccepted = action.payload.user
				
				if (type === 'Y') {
					let user = userAccepted.senderDetails
					user.usernames = userAccepted.senderUsernames
					user.notificationId = userAccepted.id
					return {
						...state,
						userAccepted: user
					}
				} else if(type === 'D') {
					return {
						...state
					}
				}
				
			} else {
				return {
					...state
				}
			}
		}
		case "GET_BUSINESS_TYPES": {
			if (action.payload.data && action.payload.data.status === 200) {
				return {
					...state,
					businessTypes:  action.payload.data.content
				}
			}
			else { 
				return {
					...state,
					businessTypes: action.payload
				}
			}
		}
		default: {
			return {
				...state
			};
		}
	}
}
