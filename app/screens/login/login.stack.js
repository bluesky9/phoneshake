import React, { Component } from "react";
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import { createStackNavigator } from 'react-navigation'
import SplashScreen from 'react-native-smart-splash-screen'
import Landing from '../landing/landing.js'
import LoginScreen from './login.js'
import Verification from './verifyOtp/verifyOtp.js'
import AccountType from '../onboard/onboardAccountType.js'
import OnboardName from '../onboard/onboardName.js'
import OnboardBusinessType from '../onboard/onboardBusinessType'
// import OnboardOrgnization from '../onboard/onboardOrgnization.js'
// import OnboardPosition from '../onboard/onboardPosition.js'
import OnboardChooseHandle from '../onboard/onboardChooseHandle.js'
import PrivacyPolicy from '../onboard/privacyPolicy.js'
import TabContainer from '../tabs/tabs.container.js'
import { strings } from '../../locale/';
const LoginStack = ({ initialRouteName }) => {
	const AuthStack = createStackNavigator({
		LandingScreen: {
			screen: Landing,
			navigationOptions: {
				title: ''
			}
		},
		LoginScreen: {
			screen: LoginScreen,
			navigationOptions: {
				title: ''
			}
		},
		Verification: {
			screen: Verification,
			navigationOptions: {
				title: ''
			}
		},
		AccountType: {
			screen: AccountType,
			navigationOptions: {
				title: ''
			}
		},
		OnboardName: {
			screen: OnboardName,
			navigationOptions: {
				title: ''
			}
		},
		OnboardBusinessType: {
			screen: OnboardBusinessType,
			navigationOptions: {
				title: ''
			}
		},
		// OnboardOrgnization: {
		// 	screen: OnboardOrgnization,
		// 	navigationOptions: {
		// 		title: ''
		// 	}
		// },
		// OnboardPosition: {
		// 	screen: OnboardPosition,
		// 	navigationOptions: {
		// 		title: ''
		// 	}
		// },
		OnboardChooseHandle: {
			screen: OnboardChooseHandle,
			navigationOptions: {
				title: ''
			}
		},
		PrivacyPolicy: {
			screen: PrivacyPolicy,
			navigationOptions: {
				title: strings('meTab.settingsPrivacyPolicy')
			}
		},
		TabContainer: {
			screen: TabContainer,
			navigationOptions: {
				title: '',
				header: null
			}
		}
	}, {
			headerMode: 'screen',
			cardStyle: { shadowColor: 'transparent' },
			initialRouteName,
			navigationOptions: {}
		});
	return <AuthStack />
}


class StackWithProps extends Component {
	constructor() {
		super();
		this.state = {
		};
	}
	componentDidMount() {
		SplashScreen.close({
			animationType: SplashScreen.animationType.scale,
			duration: 850,
			delay: 500,
		})
	}

	render() {
		return (
				<LoginStack initialRouteName={this.props.isLogin ? 'TabContainer' : 'LandingScreen'} />
		)
	}
}

export default StackWithProps;
