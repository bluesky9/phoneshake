import React, { Component } from 'react'
import { Text, Image, View, Platform, Keyboard, TouchableOpacity, LayoutAnimation, AsyncStorage } from 'react-native';
import { Content, Item } from 'native-base';
import moment from 'moment';
import { StackActions, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import * as actions from '../login.actions.js';
import { TextInputMask } from 'react-native-masked-text'
var DeviceInfo = require('react-native-device-info');
import FCM from 'react-native-fcm';
import { Colors, Images } from '../../../themes/'
import styles from'../login.styles.js'
import PhoneShakeButton from  '../../../components/phoneShakeButton/'
import Loader from '../../../components/loader/'
import { strings } from '../../../locale/'
// import { setupPushForChat } from '../../../api/sendBird'

class VerifyOTP extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (navigation.state.params.isFromNewSignupInfo) ? (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={{ height: 24, width: 24, marginLeft: 12 }} />
      </TouchableOpacity>
    ) : null,
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  });
  // static navigationOptions = ({ navigation }) => ({
  //   header: null
  // });
  
	constructor() {
		super();
		this.state = {
      otp: '',
      fcmToken: '',
      loading: false,
      incorrectOtp: false,
      keyboardHeight: 15,
      errorMessage: '',
      confirmationCode: null,
      otpTime: null,
      sendingCode: true
		};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkUserExists = this.checkUserExists.bind(this);
    this.updateDeviceToken = this.updateDeviceToken.bind(this);
    this.dontReceiveCode = this.dontReceiveCode.bind(this);
		this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
    this.addNewSignupInfo = this.addNewSignupInfo.bind(this);
	}

	componentWillMount () {
    FCM.getFCMToken().then(token => {
      if (token) {
        this.setState({ fcmToken: token })
      }
    });
    // this.dontReceiveCode()

		if(Platform.OS === 'ios') {
			this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
			this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }
  }

  componentDidMount() {
    this.dontReceiveCode()
  }

	keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = e.endCoordinates.height
    this.setState({
      keyboardHeight: newSize + 15
    })
  }

	keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  componentWillUnmount () {
		if(Platform.OS === 'ios') {
      this.keyboardDidShowListener.remove()
      this.keyboardDidHideListener.remove()
    }
  }

  generateOTP = () => {
    return otp = Math.floor(Math.random() * 899999 + 100000);
  }

  handleSubmit() {
    const { isFromNewSignupInfo } = this.props.navigation.state.params
    if (this.state.sendingCode) return false;
    Keyboard.dismiss();
    this.setState({ loading: true, incorrectOtp: false, errorMessage: '' })
    const { confirmationCode, otpTime } = this.state
    
    let otpEnterd = this.otpInput.getRawValue()
    var beginningTime = moment(otpTime).add(5, 'm');
    var endTime = moment();
    // let userInfo = this.props.navigation.state.params.userInfo
    
    if (confirmationCode === parseInt(otpEnterd) && endTime.isBefore(beginningTime, 'minute')) {
      this.setState({ incorrectOtp: false })
      AsyncStorage.removeItem('PhoneShakeOtp');
      AsyncStorage.removeItem('PhoneShakeOtpTime');
      if (isFromNewSignupInfo) {
        this.addNewSignupInfo()
      } else {
        this.checkUserExists()
      }
    } else {
      if (!endTime.isBefore(beginningTime, 'minute')) {
        let message = strings('verifyOtp.codeExpired')
        this.setState({ incorrectOtp: true, errorMessage: message, loading: false, otp: '' })
      } else {
        let message = strings('verifyOtp.codeIncorrect')
        this.setState({ incorrectOtp: true, errorMessage: message, loading: false, otp: '' })
      }
    }
  }

  addNewSignupInfo() {
    let userInfo = this.props.navigation.state.params.userInfo
    let username = userInfo.username
    let data = {
      userId: this.props.userId,
      username: username,
      type: userInfo.type
    }
    
    this.props.addNewUsername(data)
    .then((response) => {
      if (response.payload.data && response.payload.data.status === 200) {
        this.setState({ loading: false })
        this.updateUserLocal(data).done();
        this.props.navigation.navigate('SignupInfo')
      } else if (response.payload.data && response.payload.data.status === 401) {
          this.setState({ loading: false })
          this.props.navigation.navigate('ResetAccount')
      } else {
        this.setState({ loading: false })
        let message = (response.payload.message) ? response.payload.message : strings('verifyOtp.errorMessage')
        setTimeout(() => {
          alert(message)
        }, 300);
      }
    })
  }

  async updateUserLocal(data) {
    let userData = data
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content.usernames.push(userData)
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  async updateDeviceToken(userResponse) {
    const uniqueId = DeviceInfo.getUniqueID();
    let fcmToken = this.state.fcmToken
    if (fcmToken === '') {
      fcmToken = await FCM.getFCMToken().then(token => {
        if (token) {
          return token
        } else {
          return ''
        }
      });
    }
    let token = userResponse.data.content.token
    let userId = userResponse.data.content.user_id
    let data = {
      "userId": userId,
      "token": token,
      "uniqueDeviceId": uniqueId,
      "deviceType": (Platform.OS === 'ios') ? "iOS" : "Android",
      "deviceToken": fcmToken
    }
    
    this.props.updateDeviceToken(data)
    .then((response) => {  
      this.setState({ loading: false })
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "TabContainer"
          }, { fromRegistration: false })
        ]
      });
      this.props.navigation.dispatch(resetAction);
    })
  }

  checkUserExists() {
    let userInfo = this.props.navigation.state.params.userInfo
    const keyboardHeight = this.props.navigation.state.params.keyboardHeight
    let username = userInfo.username
    userInfo.accountType = 'individual';
    this.props.getUserByUsername(username, true, true, true, 0)
    .then((response) => {
      if (response.payload.data && response.payload.data.status === 200) {
        AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(response.payload));        
        this.updateDeviceToken(response.payload)
      } else if (response.payload.data && response.payload.data.status === 404) {
          this.setState({ loading: false })
          // this.props.navigation.navigate('AccountType', { keyboardHeight, userInfo })
          this.props.navigation.navigate('OnboardName', { keyboardHeight, userInfo })
      } else {
        this.setState({ loading: false })
        let message = (response.payload.message) ? response.payload.message : strings('constants.errorMessage')
        setTimeout(() => {
          alert(message)
        }, 300);
      }
    })
  }

  dontReceiveCode() {
    Keyboard.dismiss();
    this.setState({ loading: false, incorrectOtp: false, errorMessage: '' })
    const userInfo = this.props.navigation.state.params.userInfo
    if(userInfo.type === "PHONE") {
      this.sendConfirmationCodeBySMS()
    } else {
      this.sendConfirmationCode()
    }
  }

  sendConfirmationCodeBySMS() {
    let otp = this.generateOTP()
    console.log('otp: ', otp);
    
    let otpTime = moment()
    const userInfo = this.props.navigation.state.params.userInfo
    let phoneNumber = userInfo.username
    this.props.sendConfirmationCodeBySMS(phoneNumber, otp)
		.then((response) => {
      let data = {
        'startTime': otpTime,
        'endTime': moment()
      }
      AsyncStorage.setItem('SendOtpApi', JSON.stringify(data));
      this.setState({ loading: false, sendingCode: false });
      if (response.payload.data && response.payload.data.status === 200) {
        this.setState({ confirmationCode: otp, otpTime })
			}
		})
  }

  sendConfirmationCode() {
    let otp = this.generateOTP()
    let otpTime = moment()
    console.log('OTP: ', otp);
    const userInfo = this.props.navigation.state.params.userInfo
    let email = userInfo.username
    this.props.sendConfirmationCode(email, otp)
		.then((response) => {
      console.log('response', response);
        let data = {
          'startTime': otpTime,
          'endTime': moment()
        }
        AsyncStorage.setItem('SendOtpApi', JSON.stringify(data));
        this.setState({ loading: false, sendingCode: false })
				if(response.payload.data.status === 200) {
          this.setState({ confirmationCode: otp, otpTime })
				}
			})
  }

	render() {
    let disabled = this.state.otp.length < 9
    let message = (!this.state.incorrectOtp) ? strings('verifyOtp.enterConformationCode') : this.state.errorMessage
		let btnStyle = (disabled) ? [styles.btnContinue, { backgroundColor: Colors.lightBlueTheme }] : styles.btnContinue
    return (
			<View style={{flex:1, backgroundColor:'#fff'}}>
      <Loader loading={this.state.loading} />
      <Content contentContainerStyle={styles.container} bounces={false}>
        <Text style={[styles.topTitle]}>
          {strings('verifyOtp.topTitle')}
        </Text>
        <TouchableOpacity style={styles.linkButton} onPress={this.dontReceiveCode}>
          <Text style={styles.link}>
            {strings('verifyOtp.linkButton')}
          </Text>
        </TouchableOpacity>

        <Item style={styles.itemStyle}>
            <TextInputMask
              ref={(ref) => this.otpInput = ref}
              style={[styles.phoneTextInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10, width: '100%', paddingLeft: 0, textAlign: 'center' }]}
              placeholder={strings('verifyOtp.placeholder')}
              value={this.state.otp}
              keyboardType='numeric'
              placeholderTextColor='rgba(0, 0, 0, 0.5)'
              returnKeyType={'go'}
              autoFocus
              maxLength={9}
              underlineColorAndroid='transparent'
              autoCapitalize={'none'}
              enablesReturnKeyAutomatically
              onSubmitEditing={this.handleSubmit}
              onChangeText={(text) => this.setState({ otp: text })}
              type={'cel-phone'}
              options={{
                dddMask: '999 - 999',
              }} />
          
        </Item>

        <Text style={[styles.infoText, { color: (this.state.incorrectOtp) ? Colors.red : 'rgba(0, 0, 0, 0.34)'}]}>
          {message}
        </Text>

				<PhoneShakeButton
					disabled={disabled}
					onPress={() => this.handleSubmit()}
					text={(this.state.sendingCode) ? strings('verifyOtp.sending') : strings('constants.continue')}
					bottomHeight={this.props.navigation.state.params.keyboardHeight} />

      </Content>
		</View>
    );
	}
}

const stateToProps = (state) => ({
  userId: state.login.userId
});

const dispatchToProps = (dispatch) => {
  return {
    sendConfirmationCode: (email, confirmationCode) => dispatch(actions.sendConfirmationCode(email, confirmationCode)),
    sendConfirmationCodeBySMS: (phone, confirmationCode) => dispatch(actions.sendConfirmationCodeBySMS(phone, confirmationCode)),
    getUserByUsername: (username, checkForLogin, isSocialMediaLinksRequired, isNotificationSettingsRequired, requestorUserId) => dispatch(actions.getUserByUsername(username, checkForLogin, isSocialMediaLinksRequired, isNotificationSettingsRequired, requestorUserId)),
    getSocialMediaDetailsByUserId: (userId) => dispatch(actions.getSocialMediaDetailsByUserId(userId)),
    updateDeviceToken: (data) => dispatch(actions.updateDeviceToken(data)),
    addNewUsername: (data) => dispatch(actions.addNewUsername(data)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(VerifyOTP)
export default Container;
