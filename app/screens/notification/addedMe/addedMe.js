import React, { Component } from 'react';
import { View, Text, Image, FlatList, ActivityIndicator, RefreshControl, Platform } from 'react-native';
import { ListItem, Left, Body } from "native-base";
import { connect } from 'react-redux';
import { MaterialIndicator } from 'react-native-indicators';
import * as actions from '../../login/login.actions';
import styles from '../../me/settings/settings.styles';
import { Images, Colors } from '../../../themes/index.js';
import { strings } from '../../../locale';
let Indicator = ActivityIndicator
if (Platform.OS === 'ios') {
  Indicator = MaterialIndicator
}

class AddedMe extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      refreshing: false,
      pageNoWhoAddedMe: 1,
      isLoadingMore: false,
      addedMe: []
    };
    this._onRefresh = this._onRefresh.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.onPressItem = this.onPressItem.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  async handleLoadMore() {
    if (!this.state.isLoadingMore && this.state.addedMe.length === 100) {
      await this.setState({ isLoadingMore: true, pageNoWhoAddedMe: this.state.pageNoWhoAddedMe + 1 })
      this.getPendingRequests(true)
    }
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.getPendingRequests(false)
  }

  componentDidMount() {
    this.getPendingRequests(false)
  }

  componentWillReceiveProps(newProps) {
    if (newProps.userAccepted && !this.props.userAccepted) {
      this.setState({
        addedMe: [...this.state.addedMe, newProps.userAccepted]
      })
    } else if (newProps.userAccepted && this.props.userAccepted && newProps.userAccepted.user_id !== this.props.userAccepted.user_id) {
      let data = this.state.addedMe
      data.splice(0, 0, newProps.userAccepted);
      this.setState({
        addedMe: data
      })
      // this.setState({
      //   addedMe: [...this.state.addedMe, newProps.userAccepted]
      // })
    }
  }

  onPressItem = (user) => {
    user.handleName = user.handle_name
    user.isFriend = false
    user.username = user.usernames[0].username
    this.props.navigation.navigate('UserProfile', { user })
  }

  getPendingRequests(fromPaginate) {
    let data = {
      userId: this.props.userId,
      type: 'I',
      pageNo: (!fromPaginate) ? 1 : this.state.pageNoWhoAddedMe,
      requestType: 'Y'
    }
    this.props.getContactListWhoAddedMe(data)
      .then((response) => {
        if (response.payload.data && response.payload.data.status === 200) {
          let addedMe = (response.payload.data.content && response.payload.data.content.contacts) ? response.payload.data.content.contacts : []
          if (fromPaginate) {
            this.setState({ addedMe: [...this.state.addedMe, ...this.props.addedMe], loading: false, isLoadingMore: false, refreshing: false })
          } else {
            this.setState({ addedMe, loading: false, isLoadingMore: false, refreshing: false, pageNoWhoAddedMe: 1 })
          }
        } else if (response.payload.data && response.payload.data.status === 401) {
          this.setState({ addedMe: [], loading: false, isLoadingMore: false, refreshing: false })
          this.props.navigation.navigate('ResetAccount')
        } else {
          this.setState({ addedMe: [], loading: false, isLoadingMore: false, refreshing: false })
        }
      })
  }


  renderItem({ item, index }) {
    if (item.name === 'No result found' && !this.state.loading) {
      return (
        <View style={styles.emptyViewContainer}>
          <Text style={styles.emptyBlockList}>{strings('notification.general_peopleHaveNotAdded')}</Text>
          <Text style={[styles.privateAccountInfo, { textAlign: 'center' }]}>{strings('notification.general_whenTheyAddYou')}</Text>
        </View>
      );
    } else {
      let userImage = (item.profile_pic && item.profile_pic !== 'No pic') ? { uri: item.profile_pic } : Images.defaultUserImage

      return (
        <ListItem key={index} style={[styles.sectionListItem, { backgroundColor: '#fff' }]} onPress={() => this.onPressItem(item)}>
          <Left style={styles.listLeft}>
            <Image source={userImage} style={styles.blockedUserImage} />
          </Left>
          <Body style={{ flex: 4.1, borderWidth: 0 }}>
            <Text style={[styles.listName, styles.sectionListName]} numberOfLines={1}>{item.name}</Text>
            <Text style={styles.listUsername} numberOfLines={1}>
              {(item.designation && item.organization) ? item.designation + " at " + item.organization : (item.designation) ? item.designation : ''}
            </Text>
          </Body>
        </ListItem>
      )
    }
  }

  renderFooter() {
    if (this.state.isLoadingMore) {
      return <ListItem style={styles.listItem} >
        <Body style={[styles.body, { justifyContent: 'center' }]}>
          <Indicator size={20} color={Colors.blueTheme} animating />
        </Body>
      </ListItem>
    } else return null
  }

  renderEmptyNotifications() {
    return (
      <View style={styles.emptyViewContainer}>
        <Text style={styles.emptyBlockList}>{strings('notification.general_peopleHaveNotAdded')}</Text>
        <Text style={[styles.privateAccountInfo, { textAlign: 'center' }]}>{strings('notification.general_whenTheyAddYou')}</Text>
      </View>
    )
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Indicator
            size={35}
            color={Colors.blueTheme}
            animating={this.state.fetching} />
        </View>
      )
    } else {
      let data = (this.state.addedMe.length > 0) ? this.state.addedMe : [{ name: 'No result found' }]

      return (
        <View style={{ flex: 1, backgroundColor: 'rgb(247, 247, 247)' }}>
            <FlatList
              data={data}
              keyExtractor={this.keyExtractor}
              initialNumToRender={20}
              extraData={this.state}
              removeClippedSubviews={false}
              ListFooterComponent={this.renderFooter()}
              renderItem={this.renderItem}
              style={{ width: '100%' }}
              onEndReached={this.handleLoadMore}
              onEndReachedThreshold={0}
              refreshControl={
                <RefreshControl
                  tintColor={Colors.blueTheme}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />
              }
            />
        </View>
      )
    }
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  addedMe: state.login.addedMe,
  userAccepted: state.login.userAccepted
});

const dispatchToProps = (dispatch) => {
  return {
    getContactListWhoAddedMe: (data) => dispatch(actions.getContactListWhoAddedMe(data))
  }
}

const Container = connect(stateToProps, dispatchToProps)(AddedMe)

export default Container;