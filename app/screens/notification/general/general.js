import React, { Component } from 'react';
import { View, Text, Image, FlatList, ActivityIndicator, RefreshControl, Platform, AsyncStorage } from 'react-native';
import { Button, ListItem, Left, Body } from "native-base";
import { connect } from 'react-redux';
import { MaterialIndicator } from 'react-native-indicators';
import * as actions from '../../login/login.actions';
import styles from '../../me/settings/settings.styles';
import Loader from '../../../components/loader/'
import { Images, Colors } from '../../../themes/index.js';
import { strings } from '../../../locale';
let Indicator = ActivityIndicator
if (Platform.OS === 'ios') {
  Indicator = MaterialIndicator
}

class Notifications extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      isLoadingNotifications: true,
      refreshing: false,
      pageNo: 1,
      isLoadingMore: false,
      friendRequests: []
    };
    this._onRefresh = this._onRefresh.bind(this);
    this.acceptRequest = this.acceptRequest.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.onPressItem = this.onPressItem.bind(this);
  }

  async handleLoadMore() {
    if (!this.state.isLoadingMore && this.state.friendRequests.length === 100) {
      await this.setState({ isLoadingMore: true, pageNo: this.state.pageNo + 1 })
      this.getPendingRequests(true)
    }
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.getPendingRequests(false)
  }

  componentDidMount() {
    this.getPendingRequests(false)
  }

  componentWillReceiveProps(newProps) {
    if (!newProps.userAccepted && newProps.friendRequestsFromNotif && newProps.friendRequestsFromNotif.length) {
      this.setState({ friendRequests: newProps.friendRequestsFromNotif, loading: false, isLoadingMore: false, refreshing: false, pageNo: 1, isLoadingNotifications: false })
    } else if ((newProps.userAccepted && !this.props.userAccepted) || (newProps.userAccepted && this.props.userAccepted && newProps.userAccepted.notificationId !== this.props.userAccepted.notificationId)) {
      let requests = this.state.friendRequests
      requests.map((item) => {
        if (item.id === newProps.userAccepted.notificationId) {
          item.isAcceptedNow = true
        }
      })
      this.setState({
        friendRequests: requests
      })
    }
  }

  getPendingRequests(fromPaginate) {
    let data = {
      userId: this.props.userId,
      pageNo: (!fromPaginate) ? 1 : this.state.pageNo,
    }
    this.props.getFriendRequests(data, true)
    .then((response) => {
      if (response.payload.data && response.payload.data.status === 200) {
        let friendRequests = (response.payload.data.content && response.payload.data.content.notifications) ? response.payload.data.content.notifications : []
        friendRequests = friendRequests.filter(notif => { return notif.connection_state !== 'REJECTED'})
        if (fromPaginate) {
          this.setState({ friendRequests: [...this.state.friendRequests, ...this.props.friendRequests], loading: false, isLoadingMore: false, refreshing: false, isLoadingNotifications: false })
        } else {
          this.setState({ friendRequests, loading: false, isLoadingMore: false, refreshing: false, pageNo: 1, isLoadingNotifications: false })
        }
      } else if (response.payload.data && response.payload.data.status === 401) {
        this.setState({ friendRequests: [], loading: false, isLoadingMore: false, refreshing: false, isLoadingNotifications: false })
        this.props.navigation.navigate('ResetAccount')
      } else {
        this.setState({ friendRequests: [], loading: false, isLoadingMore: false, refreshing: false, isLoadingNotifications: false })
      }
    })
  }

  acceptRequest = (user, type) => () => {
    this.setState({ loading: true })
    let isRequestAccepted = (type === 'accept') ? 'Y' : 'D'
    let data = {
      "requestorUserId": user.senderDetails.user_id,
      "requesteeUserId": this.props.userId,
      "isRequestAccepted": isRequestAccepted
    }
    this.props.acceptFriendRequest(data, user)
    .then((response) => {
      this.setState({ loading: false })
      if (response.payload.data && response.payload.data.status !== 200) {
        setTimeout(() => {
          alert(strings('constants.errorMessage'))
        }, 300);
      } else if(response.payload.data && response.payload.data.status === 200) {
        if(isRequestAccepted === 'D') {
          let requests = this.state.friendRequests.filter(item => { return item.id !== user.id})

          this.setState({
            friendRequests: requests
          })
        }
      }
    })
  }

  onPressItem = (item) => {
    if (item && item.senderDetails && item.senderUsernames) {
      let user = item.senderDetails
      user.username = item.senderUsernames[0].username
      this.props.navigation.navigate('UserProfile', { user })
    }
  }

  renderItem({ item, index }) {
    if (item.name === 'No result found' && !this.props.isLoadingNotifications) {
      return (
        <View style={styles.emptyViewContainer}>
          <Text style={styles.emptyBlockList}>{strings('notification.general_emptyBlockList')}</Text>
          <Text style={[styles.privateAccountInfo, { textAlign: 'center' }]}>{strings('notification.general_contactRequest')}</Text>
        </View>
      );
    } else if (item.isAcceptedNow || (item.connection_state === 'ACCEPTED' && item.notificationType === 'ADD_CONTACT_REQUEST')){
      let userImage = (item.senderDetails && item.senderDetails.profile_pic && item.senderDetails.profile_pic !== 'No pic') ? { uri: item.senderDetails.profile_pic } : Images.defaultUserImage
      return (
        <ListItem key={index} style={[styles.sectionListItem, { backgroundColor: '#fff' }]} onPress={() => this.onPressItem(item)}>
          <Left style={styles.listLeft}>
            <Image source={userImage} style={styles.blockedUserImage} />
          </Left>
          <Body style={{ flex: 4.1, borderWidth: 0 }}>
            <Text style={[styles.listName, styles.sectionListName, { width: '95%'}]} numberOfLines={2}>
              {strings('notification.general_acceptedRequest', {name: `${item.senderDetails.name}`})}
            </Text>
          </Body>
        </ListItem>
      )
    } else if (item.notificationType === 'NEW_CONTACT_JOINED') {
      let userImage = (item.senderDetails && item.senderDetails.profile_pic && item.senderDetails.profile_pic !== 'No pic') ? { uri: item.senderDetails.profile_pic } : Images.defaultUserImage
      return (
        <ListItem key={index} style={[styles.sectionListItem, { backgroundColor: '#fff' }]} onPress={() => this.onPressItem(item)}>
          <Left style={styles.listLeft}>
            <Image source={userImage} style={styles.blockedUserImage} />
          </Left>
          <Body style={{ flex: 4.1, borderWidth: 0 }}>
            <Text style={[styles.listName, styles.sectionListName, { width: '95%' }]} numberOfLines={2}>
              {strings('notification.general_userOnPhoneShake', {name: `${item.senderDetails.name}`})}
            </Text>
          </Body>
        </ListItem>
      )
    } else if (item.notificationType === 'CONTACT_REQUEST_ACCEPTED') {
      let userImage = (item.senderDetails && item.senderDetails.profile_pic && item.senderDetails.profile_pic !== 'No pic') ? { uri: item.senderDetails.profile_pic } : Images.defaultUserImage
      return (
        <ListItem key={index} style={[styles.sectionListItem, { backgroundColor: '#fff' }]} onPress={() => this.onPressItem(item)}>
          <Left style={styles.listLeft}>
            <Image source={userImage} style={styles.blockedUserImage} />
          </Left>
          <Body style={{ flex: 4.1, borderWidth: 0 }}>
            <Text style={[styles.listName, styles.sectionListName, { width: '95%' }]} numberOfLines={2}>
              {item.body}
            </Text>
          </Body>
        </ListItem>
      )
    } else {
      let userImage = (item.senderDetails && item.senderDetails.profile_pic && item.senderDetails.profile_pic !== 'No pic') ? { uri: item.senderDetails.profile_pic } : Images.defaultUserImage
      
      return (
        <ListItem key={index} style={[styles.sectionListItem, { backgroundColor: '#fff' }]} onPress={() => this.onPressItem(item)}>
          <Left style={styles.listLeft}>
            <Image source={userImage} style={styles.blockedUserImage} />
          </Left>
          <Body style={{ flex: 4.1, borderWidth: 0 }}>
            <Text style={[styles.listName, styles.sectionListName]} numberOfLines={1}>{item.senderDetails.name}</Text>
            <Text style={styles.listUsername} numberOfLines={1}>
              {(item.senderDetails && item.senderDetails.designation && item.senderDetails.organization) ? item.senderDetails.designation + " at " + item.senderDetails.organization : (item.senderDetails.designation) ? item.senderDetails.designation : ''}
            </Text>
            {
              (item.notificationType === 'ADD_CONTACT_REQUEST') ?
              <View style={{ flexDirection: 'row', marginTop: 8 }}>
                <Button style={styles.btnAllow} onPress={this.acceptRequest(item, 'accept')}>
                  <Text style={styles.allowText}>{strings('constants.allow')}</Text>
                </Button>
                <Button style={styles.btnDecline} onPress={this.acceptRequest(item, 'decline')}>
                  <Text style={styles.allowText}>{strings('constants.decline')}</Text>
                </Button>
              </View>
              : null
            }
          </Body>
        </ListItem>
      )
    }
  }

  renderFooter() {
    if (this.state.isLoadingMore) {
      return <ListItem style={styles.listItem} >
        <Body style={[styles.body, { justifyContent: 'center' }]}>
          <Indicator size={20} color={Colors.blueTheme} animating />
        </Body>
      </ListItem>
    } else return null
  }

  renderEmptyNotifications() {
    return (
      <View style={styles.emptyViewContainer}>
        <Text style={styles.emptyBlockList}>{strings('notification.general_peopleHaveNotAdded')}</Text>
        <Text style={[styles.privateAccountInfo, { textAlign: 'center' }]}>{strings('notification.general_whenTheyAddYou')}</Text>
      </View>
    )
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    if (this.state.isLoadingNotifications) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Indicator
            size={35}
            color={Colors.blueTheme}
            animating={this.state.fetching} />
        </View>
      )
    } else {
      let data = (this.state.friendRequests.length > 0) ? this.state.friendRequests : [{ name: 'No result found'}]
      
      return (
        <View style={{ flex: 1, backgroundColor: 'rgb(247, 247, 247)' }}>
          <FlatList
            data={data}
            keyExtractor={this.keyExtractor}
            initialNumToRender={20}
            extraData={this.state}
            removeClippedSubviews={false}
            ListFooterComponent={this.renderFooter()}
            renderItem={this.renderItem}
            style={{ width: '100%' }}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0}
            refreshControl={
              <RefreshControl
                tintColor={Colors.blueTheme}
                refreshing={this.state.refreshing || this.props.isBackgroundFetch}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          />
          <Loader loading={this.state.loading} />
        </View>
      )
    }
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  friendRequests: state.login.friendRequests,
  userAccepted: state.login.userAccepted,
  friendRequestsFromNotif: state.login.friendRequestsFromNotif,
  isBackgroundFetch: state.login.isFetching
});

const dispatchToProps = (dispatch) => {
  return {
    acceptFriendRequest: (data, user) => dispatch(actions.acceptFriendRequest(data, user)),
    getFriendRequests: (data, fromGeneral) => dispatch(actions.getFriendRequests(data, fromGeneral))
  }
}

const Container = connect(stateToProps, dispatchToProps)(Notifications)

export default Container;