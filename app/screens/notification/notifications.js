import React, { Component } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { SafeAreaView } from "react-navigation";
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import General from './general/general'
import AddedMe from './addedMe/addedMe'
// import styles from '../me/settings/settings.styles';
import { Colors, Metrics, Fonts } from '../../themes/index.js';
import { strings } from '../../locale';

const styles = StyleSheet.create({
  defaultTabbar: {
    borderBottomWidth: 0.7,
    borderBottomColor: 'rgb(204, 204, 204)',
    height: Metrics.screenHeight * 0.065
  },
  tabBorder: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Colors.blueTheme,
    borderLeftColor: 'transparent',
    marginBottom: (Platform.OS === 'android') ? -1 : (Metrics.screenWidth < 321) ? -1 : 0
  },
  tabBarTextStyle: {
    fontSize: Metrics.screenWidth * 0.037,
    marginTop: Metrics.screenHeight * 0.02,
    fontFamily: Fonts.type.SFTextRegular,
  }
})

class Notifications extends Component {
  constructor() {
    super();
  }

  componentWillReceiveProps(newProps) {
    // console.log('newProps in  ***************************************: ', newProps);
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
      <View style={{ flex: 1, backgroundColor: '#fff', paddingTop: 0 }}>
          <ScrollableTabView
            scrollWithoutAnimation={false}
            initialPage={0}
            tabBarActiveTextColor={Colors.blueTheme}
            tabBarInactiveTextColor={'rgb(168, 168, 168)'}
            tabBarUnderlineStyle={styles.tabBorder}
            tabBarBackgroundColor={'white'}
            tabBarTextStyle={styles.tabBarTextStyle}
            renderTabBar={() => <DefaultTabBar style={styles.defaultTabbar} />} >
            <General tabLabel={strings('notification.general')} navigation={this.props.navigation} />
            <AddedMe tabLabel={strings('notification.addedMe')} navigation={this.props.navigation} />
          </ScrollableTabView>
        </View>
        </SafeAreaView>
    )
  }
}

export default Notifications;