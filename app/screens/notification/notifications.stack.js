
import { createStackNavigator } from 'react-navigation'
import UserDetailForNotification from '../contacts/userDetail/userDetail.js'
import Notifications from './notifications'
import ChatForNotification from '../message/chat/chat'
import ResetAccount from '../tabs/resetAccount'
import { strings } from '../../locale/index.js';

const NotificationStack = createStackNavigator({
  UserProfile: {
    screen: UserDetailForNotification,
    navigationOptions: {
      title: strings('constants.profile')
    }
  },
  NotificationScreen: {
    screen: Notifications,
    navigationOptions: {
      title: strings('notification.notifications')
    }
  },
  ChatScreen: {
    screen: ChatForNotification,
    navigationOptions: {
      title: strings('constants.chat')
    }
  },
  ResetAccount: {
    screen: ResetAccount,
    navigationOptions: {
      title: strings('constants.resetAccount')
    }
  }
}, {
    headerMode: 'none',
    initialRouteName: 'NotificationScreen',
    navigationOptions: {}
  });

NotificationStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default NotificationStack;
