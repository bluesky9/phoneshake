import { StyleSheet, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  topTitle: {
    fontSize: Metrics.screenWidth * 0.048,
    width: '40%',
    textAlign: 'center',
    paddingBottom: 5
  },
  defaultTabbar: {
    borderBottomWidth: 0.7,
    borderBottomColor: 'rgb(204, 204, 204)',
    height: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.055 : (Metrics.screenHeight > 668) ? Metrics.screenHeight * 0.042 : Metrics.screenHeight * 0.055
  },
  tabBorder: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Colors.blueTheme,
    borderLeftColor: 'transparent',
    marginBottom: (Platform.OS === 'android') ? -1 : 0
  },
  btnAccess: {
    width: '50%',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  listItem: {
    flex: 1,
    height: Metrics.screenHeight * 0.104,
    borderWidth: 0,
    borderColor: 'transparent',
    marginLeft: 0,
    paddingHorizontal: Metrics.screenWidth * 0.04
  },
  tabBarTextStyle: {
    fontSize: Metrics.screenWidth * 0.037,
    marginTop: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.01 : (Metrics.screenHeight > 668) ? Metrics.screenHeight * 0.001 : Metrics.screenHeight * 0.01,
    fontFamily: Fonts.type.SFTextRegular
  },
  inputStyle: {
    // width: '100%',
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032,
    // textAlign: 'center'
  },
  txtAccess: {
    color: '#fff',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.heavy,
    backgroundColor: 'transparent'
  },
  userImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: Metrics.screenWidth * 0.06
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    width: (Platform.OS === 'ios') ? '65%' : '75%'
  },
  listUsername: {
    color: '#717171',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextMedium,
    backgroundColor: 'transparent'
  },
  txtInvite: {
    fontSize: Metrics.screenWidth * 0.032,
    color: Colors.blueTheme,
    fontFamily: Fonts.type.SFTextSemibold
  },
  btnInvite: {
    height: Metrics.screenHeight * 0.044,
    width: Metrics.screenWidth * 0.19,
    justifyContent: 'center',
    borderColor: Colors.blueTheme
  },
  itemStyle: {
    height: Metrics.screenHeight * 0.044,
    backgroundColor: 'rgb(247, 247, 247)',
    borderBottomWidth: 0,
    paddingLeft: 0,
    borderRadius: 5
  },
  searchIconStyle: {
    color: 'rgb(168, 168, 168)',
    fontSize: Metrics.screenWidth * 0.048,
    marginTop: (Platform.OS === 'ios') ? -Metrics.screenHeight * 0.0074 : -2.5
  },
  navbar: {
    height: (Platform.OS === 'ios') ? 54 : Metrics.screenHeight * 0.077,
    paddingHorizontal: Metrics.screenWidth * 0.025,
    paddingTop: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.017 : Metrics.screenHeight * 0.011, //Metrics.screenHeight * 0.037
    backgroundColor: '#fff',
    borderBottomWidth: 0,
  },
  cancelTextStyle: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.036
  },
});
