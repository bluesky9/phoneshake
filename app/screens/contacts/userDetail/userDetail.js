import React, { Component } from 'react'
import { View, Platform, ActivityIndicator, Text, TextInput, Keyboard, KeyboardAvoidingView, Linking, TouchableOpacity, AsyncStorage } from 'react-native';
import { Button, Icon } from "native-base";
import branch from 'react-native-branch'
import { SafeAreaView } from "react-navigation";
import _ from 'lodash'
import Modal from "react-native-modal";
import SendBird from 'sendbird';
import moment from 'moment';
import Share from 'react-native-share';
import initialState from '../../login/login.initialState'
import { MaterialIndicator } from 'react-native-indicators';
import { Metrics } from "../../../themes/";
import { connect } from 'react-redux';
import * as actions from "../../login/login.actions";
import { addContact, contactBlockUnblock } from '../contacts.actions'
import styles from './userDetail.styles'
import modalStyle from '../../me/signupInfo/signupInfo.styles'
import Profile from '../../../components/userProfile/userProfile';
import Loader from '../../../components/loader/'
import BlockModal from '../blockModal/blockModal'
import LocationModal from '../locationModal/locationModal'
import { strings } from '../../../locale';
// import { SENDBIRD_TOKEN } from '../../../api/constants'
var Contacts = require('react-native-contacts')
import { PROFILE_LINK } from '../../../api/constants';

class UserDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarVisible: false
  });

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      addingNote: false,
      adding: false,
      showAddModal: false,
      requestSent: false,
      showMoreOptions: false,
      blockModalVisible: false,
      note: '',
      user: null,
      isFriend: false
    }
    this.mounted = false;
    this.renderCloseButton = this.renderCloseButton.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderMoreButton = this.renderMoreButton.bind(this);
    this.handleMorePress = this.handleMorePress.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.renderAlreadyAdded = this.renderAlreadyAdded.bind(this);
    this.renderRequestedData = this.renderRequestedData.bind(this);
    this.renderMoreOptions = this.renderMoreOptions.bind(this);
    this.blockContact = this.blockContact.bind(this);
    this.renderCallOptions = this.renderCallOptions.bind(this);
    this.renderLocationOptions = this.renderLocationOptions.bind(this);
    this.handleContactOptionPressed = this.handleContactOptionPressed.bind(this);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    let { user, isAllInfo, isFromChat } = this.props.navigation.state.params
    if(isAllInfo) {
      user = _.mapKeys(user, (v, k) => _.camelCase(k))
      let socialMediaAccounts = [];
      if (user.socialMediaLinks) {
        user.socialMediaLinks.map((media) => {
          initialState.linkedAccounts.map((item) => {
            if (item.type.toLowerCase() === media.type.toLowerCase()) {
              socialMediaAccounts.push({
                ...item,
                id: media.id,
                link: media.link,
              })
            }
          })
        })
      }
      user.linkedAccounts = socialMediaAccounts;
      let showAddModal = (user.connectionState !== "ACCEPTED")
      let requestSent = (user.connectionState === "PENDING") ? true : false
      let isFriend = (user.connectionState === "ACCEPTED") ? true : false
      this.setState({ user, loading: false, showAddModal, requestSent, isFriend })
    } else if(isFromChat) {
      this.getUserById(user.userId)
    } else {
      this.getUserDetailsFromServer()
    }
  }

  getUserById(userId) {
    let user = this.props.navigation.state.params.user
    this.setState({ loading: true, user })
    this.props.getUserByUserId(userId, this.props.userId)
      .then((response) => {
        if (response && response.payload.data && response.payload.data.content && response.payload.data.content.name) {
          let user = response.payload.data.content;
          user = _.mapKeys(user, (v, k) => _.camelCase(k))
          let socialMediaAccounts = [];
          if (user.socialMediaLinks) {
            user.socialMediaLinks.map((media) => {
              initialState.linkedAccounts.map((item) => {
                if (item.type.toLowerCase() === media.type.toLowerCase()) {
                  socialMediaAccounts.push({
                    ...item,
                    id: media.id,
                    link: media.link,
                  })
                }
              })
            })
          }
          user.linkedAccounts = socialMediaAccounts;
          let showAddModal = (user.connectionState !== "ACCEPTED")
          let requestSent = (user.connectionState === "PENDING") ? true : false
          let isFriend = (user.connectionState === "ACCEPTED") ? true : false
          if (this.mounted)
          this.setState({ user, loading: false, showAddModal, requestSent, isFriend })
        } else {
          if (this.mounted) {
            this.setState({ loading: false, user: null })
            setTimeout(() => {
              alert(strings('contactTab.userDetail_errorAlert'))
            }, 300);
          }
        }
      });
  }

  getUserDetailsFromServer () {
    let user = this.props.navigation.state.params.user
    let userId = this.props.userId
    delete user.description
    this.setState({ loading: true, user })
    let startTime = moment()
    this.props.getUserByUsername(user.username, false, true, false, userId)
    .then((response) => {
      let data = {
        'startTime': startTime,
        'endTime': moment()
      }
      AsyncStorage.setItem('LoadingContactProfileApi', JSON.stringify(data));
      if(response.payload.data && response.payload.data.status === 200) {
        let user = response.payload.data.content
        user = _.mapKeys(user, (v, k) => _.camelCase(k))
        let socialMediaAccounts = [];
        if(user.socialMediaLinks) {
          user.socialMediaLinks.map((media) => {
            initialState.linkedAccounts.map((item) => {
              if (item.type.toLowerCase() === media.type.toLowerCase()) {
                socialMediaAccounts.push({
                  ...item,
                  id: media.id,
                  link: media.link,
                })
              }
            })
          })
        }
        user.linkedAccounts = socialMediaAccounts;
        let showAddModal = (user.connectionState !== "ACCEPTED")
        let requestSent = (user.connectionState === "PENDING") ? true : false
        let isFriend = (user.connectionState === "ACCEPTED") ? true : false
        if(this.mounted)
        this.setState({ user, loading: false, showAddModal, requestSent, isFriend })
      } else {
        if (this.mounted) {
          this.setState({ loading: false, user: null })
          setTimeout(() => {
            alert(strings('contactTab.userDetail_errorAlert'))
          }, 300);
        }
      }
    })
  }

  addContactToPhonebook() {
    this.setState({ showMoreOptions: false })
    const { user } = this.state
    var newPerson = {
      emailAddresses: [{
        label: "work",
        email: user.email || '',
      }],
      phoneNumbers: [{
        label: "mobile",
        number: user.mobileNo || '',
      }],
      familyName: '',
      givenName: user.name,
    }

    Contacts.addContact(newPerson, (err) => {
      if (err){
        setTimeout(() => {
          alert(strings('contactTab.userDetail_errorSaveContactAlert'))
        }, 500);
      } else {
        setTimeout(() => {
          alert(strings('contactTab.userDetail_saveAlert'))
        }, 500);
      }
      
    })
  }

  async shareProfile() {
    const { user } = this.state

    let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
      automaticallyListOnSpotlight: true,
      contentMetadata: { 
        customMetadata: {
          handleName: user.handleName
        }
      },
      title: user.name,
      contentDescription: `${user.designation} at ${user.organization}`,
    })
    let linkProperties = {
      feature: 'share',
      channel: 'share',
      alias: `${user.handleName}`
    }
    let controlPrams = {
      $uri_redirect_mode: 1,
    }

    try{
      let { url } = await branchUniversalObject.generateShortUrl(linkProperties, controlPrams)
      console.log('url', url)
      this.openShareSheet({
        url: url,
        message: `${user.designation} at ${user.organization}`,
        title: user.name,
        subject: user.name
      })
    }
    catch(e) {
      if(e && e.code.includes('DuplicateResourceError')){
        let url = `${PROFILE_LINK}${user.handleName}`
        this.openShareSheet({
          url: url,
          message: `${user.designation} at ${user.organization}`,
          title: user.name,
          subject: user.name
        })
      }
    }
  }

  openShareSheet(options){
    Share.open(options)
    .then((res) => { console.log(res) })
    .catch((err) => { err && console.log(err); });
  }

  blockContact() {
    this.setState({ blockModalVisible: false, showMoreOptions: false })
    setTimeout(() => {
      this.setState({ adding: true })
      let data = {
        "requestorUserId": this.props.userId,
        "requesteeUserId": this.state.user.userId,
        "isBlocked": true
      }
      this.props.contactBlockUnblock(data)
      .then((response) => {
        this.setState({ adding: false })
        if(response.payload.data && response.payload.data.status !== 200) {
          setTimeout(() => {
            alert(strings('contactTab.userDetail_errorBlockinglert'))
          }, 300);
        } else {
          const sb = SendBird.getInstance();
          let _this = this;
          sb.blockUserWithUserId(this.state.user.userId, (user, error) => {
            // _this.props.navigation.goBack()
            if (error) {
              console.log('error block: ', error);
            } else {
              console.log('block User: ');
            }
          })
        }
      })
    }, 500);
  }

  renderCloseButton() {
    return (
      <Button transparent style={styles.settingButton} onPress={() => { this.props.navigation.goBack()}} >
        <Icon name='ios-close' style={styles.settingIcon} />
      </Button>
    )
  }
  
  handleMorePress() {
    if(this.state.user) {
      this.setState({ showMoreOptions: true })
    }
  }

  renderMoreButton() {
    return (
      <Button transparent style={styles.moreButton} onPress={this.handleMorePress} >
        <Icon name='ios-more' style={styles.moreIcon} />
      </Button>
    )
  }

  handleContactOptionPressed(url) {
    
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  gotoChat = () => {
    const { user } = this.state;
    let userData = {
      user_id: user.userId,
      profile_pic: user.profilePic,
      nickname: user.name
    }
    this.props.navigation.navigate('ChatScreen', { user: userData })
  }


  renderAlreadyAdded() {
    const { user } = this.state
    let callVisible = user.mobileNo || user.landLineNo
    let messageVisible = true
    let emailVisible = user.email || false
    if(!callVisible && !messageVisible && !emailVisible) {
      return null;
    }
    return (
      <View style={[modalStyle.bottomButtonView, {backgroundColor: 'rgb(247, 247, 247)'}]}>
        {(callVisible) && <Button block transparent style={modalStyle.addNoteSaveButton} onPress={() => { this.setState({ showCallOptions: true }) }}>
          <Text style={modalStyle.addNoteSaveText}>{strings('constants.call')}</Text>
        </Button>}
        { (messageVisible) && 
          <Button block transparent style={[modalStyle.addNoteCancelButton, { backgroundColor: '#fff' }]} onPress={() => { this.gotoChat(); /*this.handleContactOptionPressed(`sms:${this.state.user.mobileNo}`)*/}}>
            <Text style={modalStyle.addNoteCancelText}>{strings('constants.message')}</Text>
          </Button>}
        {(emailVisible) && <Button block transparent style={[modalStyle.addNoteCancelButton, { backgroundColor: '#fff' }]} onPress={() => this.handleContactOptionPressed(`mailto:${this.state.user.email}?subject=`)}>
          <Text style={modalStyle.addNoteCancelText}>{strings('constants.email')}</Text>
        </Button>}
        
      </View>
    )
  }

  handleSubmit() {
    if (this.state.addingNote) return false
    const { user, note } = this.state
    let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": user.userId,
      "description": note.trim(),
      "type": "I",
      "reference": "INVITE"
    }
    
    let userdata = {
      id: user.userId,
      isPrivateAccount: user.isPrivateAccount,
      handleName: user.handleName
    }
    this.setState({ addingNote: true })
    this.props.addContact(data, userdata)
    .then((response) => {
      this.setState({ addingNote: false })
      if (response.payload.data && response.payload.data.status === 200) {
        Keyboard.dismiss();
        if(user.isPrivateAccount === 'Y') {
          this.setState({ requestSent: true })
        } else {
          this.setState({ showAddModal: false, isFriend: true })
        }
      } else if (response.payload.data && response.payload.data.status === 409) {
        let message = (response.payload.data.message) ? response.payload.data.message : strings('constants.errorMessage')
          alert(message)
          // Keyboard.dismiss();
          // this.setState({ requestSent: true })
      } else {
        setTimeout(() => {
          alert(strings('contactTab.userDetail_errorAddingAlert'))
        }, 300);
      }
    })
  }

  renderCallOptions() {
    const { user } = this.state
    var mobileOption = []
    if(this.state.user.mobileNo){
      mobileOption.push(
        <TouchableOpacity key={'home'} style={styles.callOptionView} onPress={() => this.handleContactOptionPressed(`tel:${user.mobileNo}`)}>
          <Text style={styles.numberType}>{strings('contactTab.userDetail_mobile')}</Text>
          <Text style={styles.mobileNumber}>{user.mobileNo}</Text>
        </TouchableOpacity>
      )
    }
    if (this.state.user.landLineNo) {
      mobileOption.push(
        <TouchableOpacity key={'work'} style={styles.callOptionView} onPress={() => this.handleContactOptionPressed(`tel:${user.landLineNo}`)}>
          <Text style={styles.numberType}>{strings('contactTab.userDetail_landline')}</Text>
          <Text style={styles.mobileNumber}>{user.landLineNo}</Text>
        </TouchableOpacity>
      )
    }
    user.linkedAccounts.map((media) => {
      if(media.type.toUpperCase() === 'WHATSAPP') {
        mobileOption.push(
          <TouchableOpacity key={'whatsapp'} style={styles.callOptionView} onPress={() => this.handleContactOptionPressed(`whatsapp://send?phone=${media.link}`)}>
            <Text style={styles.numberType}>{strings('contactTab.userDetail_whatsapp')}</Text>
            <Text style={styles.mobileNumber}>{media.link}</Text>
          </TouchableOpacity>
        )
      }
    })

    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.showCallOptions} >
        <View style={styles.modalContainer}>
          <KeyboardAvoidingView style={styles.modalContainer} behavior="padding" enabled>
            <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.075 * (mobileOption.length + 1) }]}>
              <View style={[styles.modalActionSheet]}>
                { mobileOption }
                <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ showCallOptions: false })}>
                  <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
                </Button>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    )
  }

  

  renderLocationOptions() {
    return(
      <LocationModal
        key={'location'}
        visible={this.state.showLocationOptions}
        setModalVisible={(value) => this.setState({ showLocationOptions: false })}
        user={this.state.user} />
    )
  }

  renderRequestedData() {
    return (
      <View style={[styles.modalContent, { height: Metrics.screenHeight * 0.082 }]}>
        <View style={[modalStyle.bottomButtonView, { backgroundColor: 'transparent' }]}>
          <Button block transparent style={[modalStyle.addNoteCancelButton, { backgroundColor: 'rgb(247, 247, 247)' }]} onPress={() => { this.setState({ showAddModal: false }); this.props.navigation.goBack() }}>
            <Text style={modalStyle.addNoteCancelText}>{strings('constants.back')}</Text>
          </Button>
          <Button block transparent style={[modalStyle.addNoteCancelButton, { backgroundColor: 'rgb(168, 168, 168)' }]} onPress={() => { }}>
            <Text style={modalStyle.addNoteSaveText}>{strings('constants.requested')}</Text>
          </Button>
        </View>
      </View>
    )
  }

  renderModal() {
    const { isRTL } = this.props;
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.showAddModal} >
        <View style={styles.modalContainer}>
          <KeyboardAvoidingView style={{ flex: 1, justifyContent: 'flex-end', margin: 0 }} behavior="position" enabled={(Platform.OS === 'android') ? false : true}>
            {
              (this.state.requestSent) ?
                this.renderRequestedData()
              :
                <View style={styles.modalContent}>
                  <View style={[modalStyle.noteInputView, { paddingHorizontal: 0 }]}>
                    <View style={modalStyle.itemStyle}>
                      <TextInput
                        ref={(ref) => this.emailInput = ref}
                        style={[modalStyle.textInput, modalStyle.multiline, { paddingBottom: (Platform.OS === 'ios') ? 0 : 7, width: '100%', textAlign: (isRTL === true) ? "right" : "left" }]}
                        placeholder={strings('contactTab.invite_placeholder')}
                        value={this.state.note}
                        keyboardType='default'
                        placeholderTextColor='rgba(0, 0, 0, 0.5)'
                        autoFocus={false}
                        multiline={true}
                        maxLength={150}
                        autoCorrect={false}
                        underlineColorAndroid='transparent'
                        autoCapitalize={'sentences'}
                        onChangeText={(text) => this.setState({ note: text })}
                      />
                    </View>
                    <Text style={[modalStyle.infoText, {textAlign: (isRTL === true) ? "left" : "right"}]}>
                      {`${strings('meTab.settingsBioLimit')}: ${this.state.note.length}/150`}
                    </Text>
                  </View>
                  <View style={[modalStyle.bottomButtonView, { paddingLeft: 0 }]}>
                    <Button block transparent style={modalStyle.addNoteCancelButton} onPress={() => { this.setState({ showAddModal: false }); this.props.navigation.goBack() }}>
                      <Text style={modalStyle.addNoteCancelText}>{strings('constants.back')}</Text>
                    </Button>
                    <Button block transparent style={[modalStyle.addNoteSaveButton, { marginRight: 0 }]} onPress={() => this.handleSubmit()}>
                      {
                        (this.state.addingNote) ? 
                        (Platform.OS === 'ios') ?
                          <MaterialIndicator
                            size={20}
                            color='#fff'
                            animating={this.state.addingNote} /> 
                          : 
                            <ActivityIndicator
                              size={20}
                              color='#fff'
                              animating={this.state.addingNote} /> 
                        :
                        <Text style={modalStyle.addNoteSaveText}>{strings('constants.add')}</Text>
                        }
                    </Button>
                  </View>
                </View>
            }
          </KeyboardAvoidingView>
        </View>
      </Modal>
    )
  }

  handleActionPress = (modalKey) => () => {
    // this.setState({ showMoreOptions: false })
    if(modalKey === 'Share') {
      this.setState({ showMoreOptions: false })
      setTimeout(() => {
        this.shareProfile()
      }, 500);
    } else {
      // setTimeout(() => {
        this.setState({ [modalKey]: true })
      // }, 500);
    }
  }

  renderMoreOptions() {
    
    let content = []
    if (this.state.blockModalVisible && this.state.isFriend) {
      content.push(
        <BlockModal
          key={'block'}
          visible={this.state.blockModalVisible}
          setModalVisible={(value) => this.setState({ blockModalVisible: value, showMoreOptions: false })}
          selectedContact={this.state.user}
          callback={this.blockContact} />
      )
    } else if (this.state.showMoreOptions) {
      content.push(
        <View style={styles.modalContainer} key={'actions'}>
          <KeyboardAvoidingView style={styles.modalContainer} behavior="padding" enabled>
            <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.3 }]}>
              <View style={[styles.modalActionSheet]}>
                <Button block transparent style={modalStyle.modalRemoveButton} onPress={() => this.addContactToPhonebook()}>
                  <Text style={modalStyle.regularText}>{strings('contactTab.userDetail_saveToAddressbook')}</Text>
                </Button>
                <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('Share')}>
                  <Text style={modalStyle.regularText}>{strings('contactTab.userDetail_shareVia')}</Text>
                </Button>
                <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('blockModalVisible')}>
                  <Text style={modalStyle.distructiveText}>{strings('contactTab.contact_blockContact')}</Text>
                </Button>
                <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ showMoreOptions: false })}>
                  <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
                </Button>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      )
    }
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.showMoreOptions} >
        { content }
      </Modal>
    )
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'rgb(247, 247, 247)' }}>
      <View style={styles.container}>
        <Loader loading={this.state.adding} />
        {this.renderCloseButton()}
        {this.renderMoreButton()}
        {(this.state.user) ? <Profile navigation={this.props.navigation} user={this.state.user} me={false} openLocation={() => this.setState({ showLocationOptions: true })} loading={this.state.loading} /> : null}
        {(this.state.user && !this.state.isFriend) ? this.renderModal() : null}
        {(this.state.user && this.state.isFriend) ? this.renderAlreadyAdded() : null}
        {(this.state.user && this.state.isFriend) ? this.renderMoreOptions() : null}
        {(this.state.user && this.state.isFriend) ? this.renderCallOptions() : null}
        {(this.state.user && this.state.user.address) ? this.renderLocationOptions() : null}
      </View>
      </SafeAreaView>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  isRTL: state.login.isRTL,
});

const dispatchToProps = (dispatch) => {
  return {
    getUserByUsername: (username, isCheckForLogin, isSocialMediaRequired, isNotificationSettingsRequired, requestorUserId) => dispatch(actions.getUserByUsername(username, isCheckForLogin, isSocialMediaRequired, isNotificationSettingsRequired, requestorUserId)),
    addContact: (data, userdata) => dispatch(addContact(data, userdata)),
    contactBlockUnblock: (data) => dispatch(contactBlockUnblock(data)),
    getUserByUserId: (userId, requestorUserId) => dispatch(actions.getUserByUserId(userId, requestorUserId)),
  }
}

const Container = connect(stateToProps, dispatchToProps)(UserDetail)
export default Container;
