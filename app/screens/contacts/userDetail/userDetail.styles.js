import { StyleSheet, Platform } from 'react-native'
import { Metrics, Fonts } from '../../../themes/';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    width: '100%'
  },
  settingButton: {
    position: 'absolute',
    left: 5,
    top: (Platform.OS === 'ios') ? 12 : 5,
    zIndex: 1111
  },
  settingIcon: {
    color: '#000',
    fontSize: 38,
  },
  moreButton: {
    position: 'absolute',
    right: 5,
    top: (Platform.OS === 'ios') ? 15 : 5,
    zIndex: 1111
  },
  moreIcon: {
    color: '#000',
    fontSize: 18,
    transform: [{
      rotate: '90deg'
    }]
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 16,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  numberType: {
    fontSize: 12,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  mobileNumber: {
    fontSize: 14,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  callOptionView: {
    height: Metrics.screenHeight * 0.074,
    paddingHorizontal: Metrics.screenWidth * 0.042,
    justifyContent: 'center'
  }
});
