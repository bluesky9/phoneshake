import React, { Component } from 'react'
import { View, Platform } from 'react-native';
import { Icon} from 'native-base';
import { SafeAreaView } from "react-navigation";
import Search from 'react-native-search-box';
import styles from './contacts.styles'
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import Invite from './invite/invite'
import SearchView from '../../components/search/search';
import MyContacts from './myContact/myContact'
import { strings } from '../../locale/'
import { Colors, Metrics } from '../../themes/'

class Contact extends Component {
  constructor() {
    super();
    this.state = {
      searchText: '',
      searching: false,
      showSearchView: false,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.renderSearchView = this.renderSearchView.bind(this);
    this.handleSelectContact = this.handleSelectContact.bind(this);
    this.dismiss = this.dismiss.bind(this);
  }

  handleSearch(searchText) {
    this.setState({ searching: true, searchText })
  }

  handleSelectContact = (user, handleName, isFriend, username, image) => {
    user.handleName = handleName
    user.isFriend = isFriend
    user.username = username
    user.profile_pic = image
    this.props.navigation.navigate('UserProfile', { user })
  }

  dismiss = () => {
    this.refs.search_box.onCancel()
  }

  renderSearchView() {
    if(this.state.showSearchView) {
      return (
        <SearchView
          searchText={this.state.searchText}
          top={(Platform.OS === 'ios') ? (Metrics.screenHeight > 668) ? '9%' : '9.5%' : (Metrics.screenHeight > 668) ? '7.5%' : '8.5%'}
          handleSelectContact={this.handleSelectContact}
          dismiss={() => (this.state.searchText.trim().length === 0) ? this.dismiss() : console.log('dismiss search') }
          />
      )
    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
      <View style={styles.container}>
        <View style={styles.navbar}>
          <Search
            ref="search_box"
            onChangeText={(text) => this.handleSearch(text)}
            backgroundColor='transparent'
            placeholderTextColor='rgb(168,168,168)'
            titleCancelColor={Colors.blueTheme}
            tintColorSearch={Colors.blueTheme}
            returnKeyType='search'
            onFocus={() => { this.setState({ showSearchView: true })}}
            onCancel={() => { this.setState({ showSearchView: false, searchText: '' }) }}
            searchIconCollapsedMargin={30}
            searchIconExpandedMargin={16}
            placeholderCollapsedMargin={10}
            placeholderExpandedMargin={30}
            keyboardDismissOnSubmit={true}
            inputHeight={35}
            autoCapitalize={'none'}
            inputStyle={styles.inputStyle}
            cancelButtonTextStyle={styles.cancelTextStyle}
            iconDelete={null}
            tintColorDelete='transparent'
            placeholder={strings('constants.search')}
            cancelTitle={strings('constants.cancel')}
            iconSearch={(<Icon active name='search' style={styles.searchIconStyle} />)}
          />
          
        </View>
        <ScrollableTabView
          scrollWithoutAnimation={false}
          tabBarActiveTextColor={Colors.blueTheme}
          tabBarInactiveTextColor={'rgb(168, 168, 168)'}
          tabBarUnderlineStyle={styles.tabBorder}
          tabBarBackgroundColor={'white'}
          tabBarTextStyle={styles.tabBarTextStyle}
          renderTabBar={() => <DefaultTabBar style={styles.defaultTabbar} />} >
            <MyContacts 
              tabLabel={strings('contactTab.contact')}
              navigation={this.props.navigation}
              handleSelectContact={this.handleSelectContact}
              screenProps={this.props.screenProps}
              />

            <Invite
              tabLabel={strings('contactTab.invite')}
              handleSelectContact={this.handleSelectContact}
              navigation={this.props.navigation}
              screenProps={this.props.screenProps} />
        </ScrollableTabView>
        {this.renderSearchView()}
      </View>
      </SafeAreaView>
    );
  }
}

export default Contact;
