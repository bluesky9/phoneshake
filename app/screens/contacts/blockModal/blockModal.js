import React, { Component } from 'react';
import { View, Text, Platform, StyleSheet } from 'react-native';
import Modal from "react-native-modal";
import modalStyle from '../../me/signupInfo/signupInfo.styles';
import { Button } from 'native-base';
import { Metrics } from '../../../themes/'
import { strings } from '../../../locale';

export default class BlockModal extends Component {

  render() {
    return (
      <View style={styles.modalContainer}>
        <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.3 }]}>
        <View style={[modalStyle.modalContentBox, { paddingTop: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.01 : Metrics.screenHeight * 0.022 }]}>
          <View style={[modalStyle.titleView, { height: Metrics.screenHeight * 0.137 }]}>
            <Text style={modalStyle.alertTitle}>{`${strings('contactTab.contact_blockContact')} ${this.props.selectedContact.name}${strings('constants.questionMark')}`}</Text>
            <Text style={modalStyle.alertDescription}>{strings('contactTab.blackModal_alertDescription')}</Text>
          </View>
          <Button block transparent style={modalStyle.modalRemoveButton} onPress={() => this.props.callback(false)}>
            <Text style={modalStyle.distructiveText}>{strings('constants.block')}</Text>
          </Button>
          <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.props.setModalVisible(false)}>
            <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
          </Button>
        </View>
        </View>
      </View>
    )
  }
}

{/*
  <Modal
        backdropColor='rgb(25, 25, 25)'
        isVisible={this.props.visible}
        backdropOpacity={0.5}
        style={{ margin: 0 }}
      >
      

      
      </Modal>
*/}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});