import React, { Component } from 'react';
import { View, Text, Linking, StyleSheet, Platform, Clipboard } from 'react-native';
import Modal from "react-native-modal";
import modalStyle from '../../me/signupInfo/signupInfo.styles';
import { Button } from 'native-base';
import { Metrics } from '../../../themes/'
import { strings } from '../../../locale';

export default class LocationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMapAvailable: false
    }
    this.renderContent = this.renderContent.bind(this);
    this.openMapApp = this.openMapApp.bind(this);
    this.copyAddress = this.copyAddress.bind(this);
  }

  async componentDidMount() {
    const { user } = this.props
    Linking.canOpenURL(`comgooglemaps://?center=${user.latitude},${user.longitude}`)
    .then((response) => {
      this.setState({ isMapAvailable: response })
    })
  }

  copyAddress = async(user) => {
    await Clipboard.setString(user.address)
    this.props.setModalVisible(false)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.visible !== this.props.visible || nextState.isMapAvailable !== this.state.isMapAvailable) {
      return true
    }
    return false
  }

  openMapApp = (user, type) => () => {
    this.props.setModalVisible(false)
    if (type === 'googlemap') {
      if(this.state.isMapAvailable) {
        Linking.openURL(`comgooglemaps://?q=${user.address}&zoom=14`)
      } else {
        let url = (Platform.OS === 'ios') ? `http://maps.google.com/maps/place/${user.address}/=(${user.latitude},${user.longitude})` : `https://www.google.com/maps/search/?api=1&query=${user.address}`
        Linking.openURL(url)
      }
    } else if (type === 'apple') {
      Linking.openURL(`http://maps.apple.com/?daddr=${user.address}`)
    }
  }

  renderContent() {
    const { user } = this.props
    let mapType = (Platform.OS === 'ios') ? 'apple' : 'googlemap'
    var locationOptions = []
      locationOptions.push(
        <Button key={'map'} block transparent style={modalStyle.modalRemoveButton} onPress={this.openMapApp(user, mapType)} >
          <Text style={modalStyle.regularText}>{strings('contactTab.locationModal_openInMaps')}</Text>
        </Button>
      )
    
    if (this.state.isMapAvailable) {
      locationOptions.push(
        <Button block key={'google-map'} transparent style={modalStyle.modalRemoveButton} onPress={this.openMapApp(user, 'googlemap')}>
          <Text style={modalStyle.regularText}>{strings('contactTab.locationModal_copy')}</Text>
        </Button>
      )
    }
    return (
      <View style={styles.modalContainer}>
        <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.075 * (locationOptions.length + 3) }]}>
          <View style={[styles.modalActionSheet]}>
            <View style={[modalStyle.titleView, { height: Metrics.screenHeight * 0.074 }]}>
              <Text style={modalStyle.alertDescription} numberOfLines={1}>{user.address}</Text>
            </View>
            {locationOptions}
            <Button block transparent style={modalStyle.modalRemoveButton} onPress={() => this.copyAddress(user)}>
              <Text style={modalStyle.regularText}>{strings('contactTab.locationModal_copy')}</Text>
            </Button>
            <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.props.setModalVisible(false)}>
              <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
            </Button>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        isVisible={this.props.visible}
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        onRequestClose={() => console.log('modal closed')}
      >
        {this.renderContent()}
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});