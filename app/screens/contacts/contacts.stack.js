
import { createStackNavigator } from 'react-navigation'
import Contact from './contacts.js'
import UserDetails from './userDetail/userDetail'
import Chat from '../message/chat/chat'
import ResetAccount from '../tabs/resetAccount'
import { strings } from '../../locale/index.js';

const ContactStack = createStackNavigator({
  ContactScreen: {
    screen: Contact,
    navigationOptions: {
      title: strings('contactTab.contact')
    }
  },
  UserProfile: {
    screen: UserDetails,
    navigationOptions: {
      title: strings('constants.profile')
    }
  },
  ChatScreen: {
    screen: Chat,
    navigationOptions: {
      title: strings('constants.chat')
    }
  },
  ResetAccount: {
    screen: ResetAccount,
    navigationOptions: {
      title: strings('constants.resetAccount')
    }
  }
}, {
    headerMode: 'none',
    initialRouteName: 'ContactScreen',
    navigationOptions: {}
});

ContactStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default ContactStack;
