import { AsyncStorage } from "react-native";
const initialState = {
myContacts: [],
phonebookContacts: [],
syncedContacts: [],
blockedList: []
}

export default function contactReducer(state = initialState, action) {
  switch (action.type) {
    case "GET_CONTACT_LIST": {
      if (action.payload.data && action.payload.data.status === 200 && action.payload.data.content && action.payload.data.content.contacts) {
        let myContacts = action.payload.data.content.contacts
        AsyncStorage.setItem('MyContacts', JSON.stringify(myContacts));
        return {
          ...state,
          myContacts
        }
      } else {
        let contacts = []
        AsyncStorage.setItem('MyContacts', JSON.stringify(contacts));
        return {
          ...state,
          // myContacts: contacts
        }
      }
    }
    case "SAVE_MY_CONTACTS": {
      return {
        ...state,
        myContacts: action.payload || []
      }
    }
    case "SAVE_SYNCED_CONTACTS": {
      return {
        ...state,
        syncedContacts: action.payload || []
      }
    }
    case "CLEAR_CONTACTS": {
      return {
        ...state,
        myContacts: [],
        phonebookContacts: [],
        syncedContacts: [],
        blockedList: []
      }
    }
    case "SAVE_PHONEBOOK_CONTACTS": {
      return {
        ...state,
        phonebookContacts: action.payload || []
      }
    }
    case "SYNC_CONTACTS": {
      // console.log('SYNC_CONTACTS: ', action.payload);
      if (action.payload.data && action.payload.data.status === 200) {
        let syncedContacts = []
        
        if(action.payload.data.content && action.payload.data.content.contacts){
          let matchedContact = action.payload.data.content.contacts
          matchedContact.map((item) => {
              syncedContacts.push({
                username: item.username,
                handleName: item.handle_name,
                name: item.name,
                id: item.id,
                designation: item.designation,
                organization: item.organization,
                description: item.description,
                profilePic: item.profile_pic,
                businessLogo: item.business_logo,
                connectionState: item.connection_state
              })
          })

          AsyncStorage.setItem('SyncedContact', JSON.stringify(syncedContacts));
          return {
            ...state,
            syncedContacts
          }
        }
        
        return {
          ...state
        }
      } else {
        return {
          ...state
        }
      }
    }
    case "ADD_NOTE": {
      if (action.payload.data && action.payload.data.status === 200) {
        let desc = action.payload.description
        let myContactsPrev = []
        state.myContacts.map((contact) => {
          if(contact.id === action.payload.userId) {
            contact.description = desc
          }
          myContactsPrev.push(contact)
        })
        return {
          ...state,
          myContacts: myContactsPrev
        }
      } else {
        return {
          ...state
        }
      }
    }
    case "BLOCK_CONTACT": {
      if (action.payload.data && action.payload.data.status === 200) {
        if(action.payload.isBlocked) {
          let myContacts = state.myContacts
          myContacts = myContacts.filter((item) => item.id !== action.payload.userId)
          AsyncStorage.setItem('MyContacts', JSON.stringify(myContacts));
          return {
            ...state,
            myContacts
          }
        } else {
          let blockedList = state.blockedList
          blockedList = blockedList.filter((item) => item.id !== action.payload.userId)
          return {
            ...state,
            blockedList
          }
        }
      } else {
        return {
          ...state
        }
      }
    }
    case "REMOVE_CONTACT": {
      if (action.payload.data && action.payload.data.status === 200) {
        let myContactsCurrent = state.myContacts
        let removedUserId = action.payload.removedUserId 
        let newMyContacts = myContactsCurrent.filter((item) => item.id !== removedUserId)
        AsyncStorage.setItem('MyContacts', JSON.stringify(newMyContacts));
        return {
          ...state,
          myContacts: newMyContacts
        }
      } else {
        return {
          ...state
        }
      }
    }
    case "ADD_CONTACT": {
      // console.log('action.payload.data ADD CONTACT: ', action.payload);
      if (action.payload.data && action.payload.data.status === 200) {
        let syncedContacts = state.syncedContacts
        let addedUser = action.payload.addedUser
        syncedContacts.map((contact) => {
          if(contact.id === addedUser.id) {
            contact.connectionState = (addedUser.isPrivateAccount === 'Y') ? 'PENDING' : 'ACCEPTED' 
          }
        })
        return {
          ...state,
          syncedContacts
        }
      } else {
        return {
          ...state
        }
      }
    }
    case "GET_BLOCKED_CONTACT_LIST": {
      // console.log('BlockedList: ', action.payload);
      
      if (action.payload.data && action.payload.data.status === 200) {
        let blockedList = action.payload.data.content.contacts
        return {
          ...state,
          blockedList,
        }
      } else {
        return {
          ...state,
          blockedList: []
        }
      }
    }
    case "SEND_INVITATION": {
      return {
        ...state        
      }
    }
    default: {
      return {
        ...state
      };
    }
  }
}
