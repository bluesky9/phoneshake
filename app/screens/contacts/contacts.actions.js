import { contacts } from '../../api/api.js';

export function getContactList(userId, type, pageNumber) {
  return {
    type: "GET_CONTACT_LIST",
    payload: contacts.getContactList(userId, type, pageNumber)
  }
}

export function contactBlockUnblock(data) {
  return {
    type: "BLOCK_CONTACT",
    payload: contacts.contactBlockUnblock(data)
  }
}

export function removeContact(data) {
  return {
    type: "REMOVE_CONTACT",
    payload: contacts.removeContact(data)
  }
}

export function addContact(data, addedUser) {
  return {
    type: "ADD_CONTACT",
    payload: contacts.addContact(data, addedUser)
  }
}

export function addNote(data) {
  return {
    type: "ADD_NOTE",
    payload: contacts.addNote(data)
  }
}

export function syncContacts(data) {
  return {
    type: "SYNC_CONTACTS",
    payload: contacts.syncContacts(data)
  }
}

export function saveSyncedContacts(contacts) {
  return {
    type: "SAVE_SYNCED_CONTACTS",
    payload: contacts
  }
}

export function saveMyContacts(contacts) {
  return {
    type: "SAVE_MY_CONTACTS",
    payload: contacts
  }
}

export function savePhonebookContacts(contacts) {
  return {
    type: "SAVE_PHONEBOOK_CONTACTS",
    payload: contacts
  }
}

export function getBlockedContactList(userId, type, pageNumber) {
  return {
    type: "GET_BLOCKED_CONTACT_LIST",
    payload: contacts.getBlockedContactList(userId, type, pageNumber)
  }
}

export function sendInvitation(data, type) {
  return {
    type: "SEND_INVITATION",
    payload: contacts.sendInvitation(data, type)
  }
}

export function clearContacts() {
  return {
    type: "CLEAR_CONTACTS",
    payload: true
  }
}