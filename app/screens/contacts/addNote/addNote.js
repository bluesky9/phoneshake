import React, { Component } from 'react';
import { View, Text, Image, Platform, TextInput, ActivityIndicator, Keyboard, KeyboardAvoidingView, StyleSheet } from 'react-native';
import { MaterialIndicator } from 'react-native-indicators';
import modalStyle from '../../me/signupInfo/signupInfo.styles';
import { Button } from 'native-base';
import { Images, Metrics, Colors } from '../../../themes/';
import { strings } from '../../../locale';
let Loader = ActivityIndicator
if (Platform.OS === 'ios') {
  Loader = MaterialIndicator
}

export default class AddNote extends Component {

  constructor() {
    super();
    this.state = {
      note: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderAddButton = this.renderAddButton.bind(this);
  }

  handleSubmit() {
    Keyboard.dismiss();
    if (!this.props.saveButtonText){
      this.props.setModalVisible(false)
    }
    setTimeout(() => {
      this.props.callback()
    }, 300);
  }

  renderAddButton() {
    if(this.props.showLoading) {
      return (
        <Button block transparent style={modalStyle.addNoteSaveButton} >
          <Loader
            size={20}
            color='#fff'
            animating={this.props.showLoading} />
        </Button>
      )
    } else {
      return (
        <Button block transparent style={modalStyle.addNoteSaveButton} onPress={() => this.handleSubmit()}>
          <Text style={modalStyle.addNoteSaveText}>{(this.props.saveButtonText) ? this.props.saveButtonText : strings('constants.save')}</Text>
        </Button>
      )
    }
  }

  render() {
    let { selectedContact, note, isRTL } = this.props
    console.log('isRTL', this.props)
    return (
        <KeyboardAvoidingView style={styles.modalContainer} behavior="position" enabled={(Platform.OS === 'android') ? false : true}>
        <View style={[styles.modalContent, { height: Metrics.screenHeight * 0.45 }]}>
          <View style={[modalStyle.titleView, { alignItems: 'center', paddingTop: 16, marginBottom: 5}]}>
            <Image source={(selectedContact.profile_pic) ? { uri: selectedContact.profile_pic } : Images.defaultUserImage} style={styles.alertUserImage} />
            <Text style={modalStyle.alertUsername}>{selectedContact.name}</Text>
          </View>
          <View style={modalStyle.noteInputView}>
            <View style={modalStyle.itemStyle}>
              <TextInput
                ref={(ref) => this.emailInput = ref}
                style={[modalStyle.textInput, modalStyle.multiline, { paddingBottom: (Platform.OS === 'ios') ? 0 : 7, width: '100%', textAlign: (isRTL === true) ? "right" : "left" }]}
                placeholder={(this.props.placeholder) ? this.props.placeholder : strings('contactTab.addNote_addNoteToRember')}
                value={note}
                keyboardType='default'
                placeholderTextColor='rgba(0, 0, 0, 0.5)'
                returnKeyType={'go'}
                // autoFocus={(Platform.OS === 'ios') ? true : false}
                multiline={true}
                maxLength={150}
                autoCorrect={false}
                underlineColorAndroid='transparent'
                autoCapitalize={'sentences'}
                // enablesReturnKeyAutomatically
                onSubmitEditing={this.handleSubmit}
                onChangeText={(text) => this.props.onChangeText(text)}
              />
            </View>
            <Text style={[modalStyle.infoText, {textAlign: (isRTL === true) ? "left" : "right"}]}>
              {`${strings('meTab.settingsBioLimit')}: ${this.props.note.length}/150`}
            </Text>
          </View>
          <View style={modalStyle.bottomButtonView}>
            <Button block transparent style={modalStyle.addNoteCancelButton} onPress={() => this.props.setModalVisible(false)}>
              <Text style={modalStyle.addNoteCancelText}>{strings('constants.cancel')}</Text>
            </Button>
            {this.renderAddButton()}
          </View>
        </View>
        </KeyboardAvoidingView>
    )
  }
}

{/*<Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.props.visible}
      >
      <View style={styles.modalContainer}>*/}
      
{/*</View>
        </Modal>*/}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  alertUserImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: (Metrics.screenWidth * 0.12) / 2,
    marginBottom: 5,
    marginTop: Metrics.screenHeight * 0.024
  },
})



