import { StyleSheet, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../../themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  inviteContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  invitePermissionContainer: {
    alignItems: 'center',
    flex: 1,
    paddingTop: Metrics.screenHeight * 0.15
  },
  btnAccess: {
    width: '50%',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    elevation: 0,
    backgroundColor: Colors.blueTheme
  },
  title: {
    fontSize: Metrics.screenWidth * 0.053,
    textAlign: 'center',
    marginBottom: Metrics.screenHeight * 0.0149,
    fontFamily: Fonts.type.SFTextMedium,
    color: '#000'
  },
  subtitle: {
    fontSize: Metrics.screenWidth * 0.032,
    marginBottom: 20,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  listItem: {
    flex: 1,
    height: Metrics.screenHeight * 0.104,
    borderWidth: 0,
    borderColor: 'transparent',
    marginLeft: 0,
    paddingHorizontal: 15
  },
  txtAccess: {
    color: '#fff',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent'
  }
});
