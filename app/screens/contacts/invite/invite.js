import React, { Component } from 'react'
import { Text, View, ActivityIndicator, FlatList, RefreshControl, Alert, Platform, AsyncStorage } from 'react-native';
import { Button } from 'native-base';
import { MaterialIndicator } from 'react-native-indicators';
import * as actions from '../contacts.actions.js';
import { connect } from 'react-redux';
import _ from 'lodash'
import styles from './invite.styles'
import Permissions from 'react-native-permissions'
var Contacts = require('react-native-contacts')
import { Colors } from '../../../themes/'
import Loader from '../../../components/loader/'
import Contact from '../../../components/contactItem/contactItem'
import AddNote from '../addNote/addNote'
import Modal from "react-native-modal";
import { strings } from '../../../locale/index.js';
let Indicator = ActivityIndicator
if (Platform.OS === 'ios') {
  Indicator = MaterialIndicator
}

class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactPermission: 'authorized',
      fetching: true,
      contacts: [],
      usernames: [],
      refreshing: false,
      loading: false,
      modalVisible: false,
      selectedContact: {},
      note: '',
      addingContact: false
    }
    this.renderItem = this.renderItem.bind(this);
    this.getContacts = this.getContacts.bind(this);
    this._onRefresh = this._onRefresh.bind(this);
    this.addContactWithNote = this.addContactWithNote.bind(this);
    this.renderActionModal = this.renderActionModal.bind(this);
    this.handleSelectContact = this.handleSelectContact.bind(this);
  }

  componentWillMount() {
    this.checkContacts()
  }

  checkContacts() {
    Permissions.check('contacts').then(response => {
      if (response === 'authorized') {
        this.getContacts(false)
      } else {
        this.setState({ contactPermission: '', fetching: false })
      }
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.fetching !== this.state.fetching
        || nextState.loading !== this.state.loading
        || nextState.refreshing !== this.state.refreshing
        || nextState.contacts !== this.state.contacts
        || nextState.contactPermission !== this.state.contactPermission
        || nextState.modalVisible !== this.state.modalVisible
        || nextState.note !== this.state.note
        || nextState.selectedContact !== this.state.selectedContact
        || nextState.addingContact !== this.state.addingContact) {
      return true
    }
    return false
  }

  getPermission() {
    Permissions.check('contacts').then(response => {
      if (response === 'denied') {
        Alert.alert(
          strings('contactTab.invite_permissionAlert'),
          strings('contactTab.invite_permissionPathAlert'),
          [
            { text: strings('constants.ok'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: strings('constants.openSetting'), onPress: () => Permissions.openSettings() }
          ],
          { cancelable: false }
        )
      } else if (response != 'authorized') {
        this.askForPermission()
      }
    })
  }

  askForPermission() {
    Permissions.request('contacts').then(userResponse => {
      console.log('Permission: ', userResponse);
      if (userResponse === 'authorized') {
        this.getContacts(true).then(() => {
          this.setState({ contactPermission: userResponse })
        })
      }
    })
  }

  async getContacts(showLoading) {
    if(showLoading) this.setState({ fetching: true })

    let userCountryCode = ''
    if (this.props.mobileNo) {
      let ph = this.props.mobileNo
      userCountryCode = ph.substring(0, ph.length - 10);
      
    }
    let filtered = [];
    let usernames = []

    return response = await Contacts.getAll((err, contacts) => {
      if (err) throw err;
      contacts.filter((item, i) => {
        let itemUsernames = []
        let handleName = ''
        let name = ''
        let id = ''
        item.phoneNumbers.filter((phone) => {
          if (phone.number) {
            let phoneNumber = phone.number.replace(/([(),' ',-,\s])/g, '').replace(/-/g, "");
            phoneNumber = phoneNumber.replace(/^0+/, '');
            if (phoneNumber.length === 10) {
              phoneNumber = userCountryCode + phoneNumber
            }
            
            if (usernames.indexOf(phoneNumber) === -1) {
              usernames.push(phoneNumber);
              itemUsernames.push(phoneNumber)
            }
          }
        });
        item.emailAddresses.filter((emailAddress) => {
          if (emailAddress.email) {
            usernames.push(emailAddress.email);
            itemUsernames.push(emailAddress.email)
          }
        });
        
        this.props.syncedContacts.map((synced) => {
          if(itemUsernames.indexOf(synced.username) !== -1) {
            handleName = synced.handleName
            id = synced.id
          }
        })

        let nameArray = []
        let givName = (item.givenName) ? item.givenName : ''
        nameArray.push(givName)
        if (item.familyName) {
          nameArray.push(item.familyName)
        }
        let tempName = (name !== '') ? name : nameArray.join(' ')
        let user = {
          name: tempName,
          image: (item.hasThumbnail) ? item.thumbnailPath : '',
          usernames: itemUsernames,
          handleName: handleName,
          id: id,
          isFriend: false,
          isNameAvailable: (tempName) ? 'A' : 'N'
        }
        if (itemUsernames.length > 0 && (item.phoneNumbers.length > 0 || item.emailAddresses.length > 0)) {
          let isFriend = this.props.myContacts.filter((item) => {
            let matched = item.usernames.filter((contactUsernames) => {
              return itemUsernames.indexOf(contactUsernames.username) !== -1
            })
            return matched.length
          })

          isFriend = (isFriend.length) ? true : false
          if(!isFriend)
          filtered.push(user);
        }
      });
      
      let filteredShortedName = _.orderBy(filtered, ['handleName', 'isNameAvailable', 'name'], ['desc', 'asc', 'asc']);
      // let filteredShortedHandleName = _.orderBy(filteredShortedName, ['handleName'], ['desc'])
      AsyncStorage.setItem('PhonebookContacts', JSON.stringify(filteredShortedName))
      this.props.savePhonebookContacts(filteredShortedName)
      this.setState({ contacts: filteredShortedName, fetching: false })
      this.syncContacts(usernames)
    })
  }

  syncContacts(usernames) {
    let data = {
      "usernames": usernames,
      "requestorUserId": this.props.userId
    }
    this.props.syncContacts(data)
    .then((response) => {
      this.setState({ refreshing: false })
      if(response.payload.data && response.payload.data.status === 200) {
        this.getContacts(false)
      }
    })
  }

  addContactWithNote() {
    const { selectedContact } = this.state
    this.setState({ addingContact: true })
    let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": selectedContact.id,
      "description": this.state.note.trim(),
      "type": "I",
      "reference": "INVITE"
    }
    
    this.props.addContact(data, selectedContact)
    .then((response) => {
      this.setState({ addingContact: false, modalVisible: false, note: '' })
      if (response.payload.data && response.payload.data.status === 200) {
        if (selectedContact.isPrivateAccount === 'Y') {
          let contact = this.state.selectedContact
          contact.connectionState = 'PENDING'
          this.setState({
            selectedContact: contact
          })
        } else {
          let contact = this.state.selectedContact
          contact.connectionState = 'ACCEPTED'
          this.setState({
            selectedContact: contact
          })
        }
      } else if (response.payload.data && response.payload.data.status === 409) {
        let message = (response.payload.data.message) ? response.payload.data.message : 'Something went wrong. Try again.'
        setTimeout(() => {
          alert(message)
        }, 500);
      } else {
        setTimeout(() => {
          alert(strings('contactTab.invite_addContactErrorAlert'))
        }, 500);
      }
    })
  }

  renderActionModal() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.modalVisible}
        animationDuration={200}
      >
        <AddNote
          key={'add'}
          saveButtonText={'Add'}
          isRTL={this.props.isRTL}
          showLoading={this.state.addingContact}
          visible={this.state.modalVisible}
          placeholder={strings('contactTab.invite_placeholder')}
          setModalVisible={(value) => this.setState({ modalVisible: false })}
          selectedContact={this.state.selectedContact}
          handleSubmit={this.addContactWithNote}
          note={this.state.note}
          onChangeText={(text) => this.setState({ note: text })}
          callback={this.addContactWithNote} />
      </Modal>
    )
  }

  handleSelectContact = (user, handleName, isFriend, username, image) => {
    user.handleName = handleName
    user.isFriend = isFriend
    user.username = username
    user.profile_pic = image
    this.setState({ selectedContact: user, modalVisible: true })
  }

  renderItem({ item, index }) {    
    return (
      <Contact
        key={index}
        contact={item}
        onPress={(handleName, isFriend, username, image) => this.props.handleSelectContact(item, handleName, isFriend, username, image)}
        sendInvitation={this.props.sendInvitation}
        handleAddPress={(handleName, isFriend, username, image) => this.handleSelectContact(item, handleName, isFriend, username, image)}
        />
    )
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.getContacts(false)
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    if (this.state.fetching && this.state.contacts.length === 0) {
      return (
        <View style={styles.inviteContainer}>
          <Indicator
            size={35}
            color={Colors.blueTheme}
            animating={this.state.fetching} />
        </View>
      );
    } else if (this.state.contactPermission != 'authorized') {
      return (
        <View style={[styles.invitePermissionContainer]}>
          <Text style={styles.title}>
           {strings('contactTab.invite_title')}
          </Text>
          <Text style={styles.subtitle}>
            {strings('contactTab.invite_subtitle')}
          </Text>
          <Button style={styles.btnAccess} onPress={() => this.getPermission()}>
            <Text style={styles.txtAccess}>{strings('contactTab.invite_btnAccess')}</Text>
          </Button>
        </View>
      );
    } else {
      return (
        <View style={styles.inviteContainer}>
          <FlatList
            data={this.state.contacts}
            removeClippedSubviews={false}
            initialNumToRender={20}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            extraData={this.props}
            style={{ width: '100%' }}
            refreshControl={
              <RefreshControl
                tintColor={Colors.blueTheme}
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
          {this.renderActionModal()}
          <Loader loading={this.state.loading} />
        </View>
      );
    }
  }
}

const stateToProps = (state) => ({
  syncedContacts: state.contacts.syncedContacts,
  myContacts: state.contacts.myContacts,
  userId: state.login.userId,
  name: state.login.name,
  handleName: state.login.handleName,
  mobileNo: state.login.mobileNo,
  isRTL: state.login.isRTL
});

const dispatchToProps = (dispatch) => {
  return {
    syncContacts: (data) => dispatch(actions.syncContacts(data)),
    savePhonebookContacts: (contacts) => dispatch(actions.savePhonebookContacts(contacts)),
    sendInvitation: (data, type) => dispatch(actions.sendInvitation(data, type)),
    addContact: (data, addedUser) => dispatch(actions.addContact(data, addedUser)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(Invite)
export default Container;