import React, { Component } from 'react'
import { Text, View, ActivityIndicator, Platform, SectionList, RefreshControl, AsyncStorage, ScrollView } from 'react-native';
import { Button } from 'native-base';
import { connect } from 'react-redux';
import { MaterialIndicator } from 'react-native-indicators';
// import AtoZList from 'react-native-atoz-list';
import Modal from "react-native-modal";
import * as actions from '../contacts.actions.js';
import styles from './myContact.styles'
import modalStyle from '../../me/signupInfo/signupInfo.styles';
import Loader from '../../../components/loader/'
import { Colors, Metrics } from '../../../themes/'
import BlockModal from '../blockModal/blockModal'
import RemoveContactModal from '../removeContactModal/removeContactModal'
import AddNote from '../addNote/addNote'
import MyContact from '../../../components/myContactItem/myContactItem'
import moment from 'moment';
import { strings } from '../../../locale/index.js';

class MyContacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      note: '',
      loading: false,
      fetching: false,
      modalVisible: false,
      blockModalVisible: false,
      removeContactModalVisible: false,
      addNoteModalVisible: false,
      selectedContact: {},
      contacts: [],
      refreshing: false
    };
    this.mounted = false;
    this.setModalVisible = this.setModalVisible.bind(this);
    this.renderActionModal = this.renderActionModal.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.handleActionPress = this.handleActionPress.bind(this);
    this.handleItemPress = this.handleItemPress.bind(this);
    this.blockContact = this.blockContact.bind(this);
    this.removeContact = this.removeContact.bind(this);
    this.addNote = this.addNote.bind(this);
    this._onRefresh = this._onRefresh.bind(this);
    this._renderHeader = this._renderHeader.bind(this);
  }

  componentDidMount() {
    this.mounted = true
    this.setState({ fetching: true })
    this.getMyContacts()
  }

  componentWillUnmount() {
    this.mounted = false
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextState.fetching !== this.state.fetching
      || nextState.loading !== this.state.loading
      || nextProps.myContacts !== this.props.myContacts
      || nextState.modalVisible !== this.state.modalVisible
      || nextState.blockModalVisible !== this.state.blockModalVisible
      || nextState.removeContactModalVisible !== this.state.removeContactModalVisible
      || nextState.addNoteModalVisible !== this.state.addNoteModalVisible
      || nextState.selectedContact !== this.state.selectedContact
      || nextState.refreshing !== this.state.refreshing
      || nextState.note !== this.state.note) {
      return true
    }

    return false
  }

  getMyContacts() {
    let startTime = moment()
    this.props.getContactList(this.props.userId, 'I', 1)
    .then((response) => {
      this.setState({ loading: false })
      let data = {
        'startTime': startTime,
        'endTime': moment()
      }
      AsyncStorage.setItem('LoadingContactListApi', JSON.stringify(data));
      if(this.mounted) {
        this.setState({ fetching: false, refreshing: false })
      }
      if(response.payload.data && response.payload.data.status === 200) {
        // console.log('contacts: ', response.payload.data.content.contacts);
      } else if (response.payload.data && response.payload.data.status === 401) {
        this.props.navigation.navigate('ResetAccount')
      } else if (response.payload.data && response.payload.data.status === 404) {
        this.setState({ fetching: false, refreshing: false })
      }  else {
        setTimeout(() => {
          alert(strings('contactTab.contact_alertContactError'))
        }, 300);
      }
    })
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.getMyContacts()
  }

  setModalVisible = (modal, value) => () => {
    this.setState({ [modal]: value })
  }

  handleActionPress = (modalKey) => () => {
    this.setState({ [modalKey]: true })
  }

  addNote() {
    this.setState({ addNoteModalVisible: false, modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": this.state.selectedContact.id,
      "description": this.state.note
    }
    this.props.addNote(data)
    .then((response) => {
      // this.setState({ loading: false })
      if (response.payload.data && response.payload.data.status === 200) {
        this.getMyContacts()
        this.setState((previousState) => ({
          selectedContact: { ...previousState.selectedContact, ['description']: response.payload.description },
          note: '',
        }))
      } else {
        setTimeout(() => {
          alert(strings('contactTab.contact_updatingAlert'))
        }, 300);
      }
    })
    }, 500);
  }

  removeContact() {
    this.setState({ removeContactModalVisible: false, modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": this.state.selectedContact.id
    }
    this.props.removeContact(data)
    .then((response) => {
      this.setState({ loading: false })
      if (response.payload.data && response.payload.data.status !== 200) {
        setTimeout(() => {
          alert(strings('contactTab.contact_removingAlert'))
        }, 300);
      }
    })
    }, 500);
  }

  blockContact() {
    this.setState({ blockModalVisible: false, modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": this.state.selectedContact.id,
      "isBlocked": true
      }
      this.props.contactBlockUnblock(data)
      .then((response) => {
        this.setState({ loading: false })
        if(response.payload.data && response.payload.data.status !== 200) {
          setTimeout(() => {
            alert(strings('contactTab.contact_blockingAlert'))
          }, 300);
        }
      })
    }, 500);
  }

  renderActionModal() {
    let content = []
    if (this.state.addNoteModalVisible){
      content.push(
        <AddNote
          key={'add'}
          isRTL={this.props.isRTL}
          visible={this.state.addNoteModalVisible}
          setModalVisible={(value) => this.setState({ addNoteModalVisible: value, modalVisible: false })}
          selectedContact={this.state.selectedContact}
          handleSubmit={this.addNote}
          note={this.state.note}
          onChangeText={(text) => this.setState({ note: text })}
          callback={this.addNote} />
      )
    } else if (this.state.blockModalVisible) {
      content.push(
        <BlockModal
          key={'block'}
          visible={this.state.blockModalVisible}
          setModalVisible={(value) => this.setState({ blockModalVisible: value, modalVisible: false })}
          selectedContact={this.state.selectedContact}
          callback={this.blockContact} />
      )
    } else if (this.state.removeContactModalVisible) {
      content.push(
        <RemoveContactModal
          key={'remove'}
          visible={this.state.removeContactModalVisible}
          setModalVisible={(value) => this.setState({ removeContactModalVisible: value, modalVisible: false })}
          selectedContact={this.state.selectedContact}
          callback={this.removeContact} />
      )
    } else if (this.state.modalVisible) {
      content.push(
        <View style={styles.modalContainer} key={'actions'}>
          <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.3 }]}>
            <View style={styles.modalActionSheet}>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('addNoteModalVisible')}>
                <Text style={modalStyle.regularText}>{strings('contactTab.contact_editNotes')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('blockModalVisible')}>
                <Text style={modalStyle.distructiveText}>{strings('constants.blockContact')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('removeContactModalVisible')}>
                <Text style={modalStyle.distructiveText}>{strings('constants.removeContact')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ modalVisible: false })}>
                <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
              </Button>
            </View>
          </View>
        </View>
      )
    }
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.modalVisible}
        animationDuration={200}
      >
        { content }
      </Modal>
    )
  }

  sortData() {
    let listData = (this.props.myContacts.length > 0) ? this.props.myContacts : [] //[{ value: "emptydata" }]
    var data = { 'A': [], 'B': [], 'C': [], 'D': [], 'E': [], 'F': [], 'G': [], 'H': [], 'I': [], 'J': [], 'K': [], 'L': [], 'M': [], 'N': [], 'O': [], 'P': [], 'Q': [], 'R': [], 'S': [], 'T': [], 'U': [], 'V': [], 'W': [], 'X': [], 'Y': [], 'Z': [], '#': [] };
    listData.map((item) => {
      if (item !== undefined && item !== null) {
        if (item.name == null)
          item.name = ' ';
        var keyword = item.name[0].toUpperCase();
        if (!((keyword >= 'a' && keyword <= 'z') || (keyword >= 'A' && keyword <= 'Z')))
          keyword = '#';
        if (data[keyword] == undefined)
          data[keyword] = [];
        data[keyword].push(item);
      }
    });
    let shortedList = []
    Object.keys(data).map((item) => {
      if (data[item].length > 0)
        shortedList.push({
          title: item,
          data: data[item]
        })
    });
    
    return shortedList;
  }

  _renderHeader({section}) {
    return (
      <View style={styles.sectionHeader}>
        <Text style={styles.headerTitle}>{section.title}</Text>
      </View>
    )
  }

  handleItemPress(item) {
    this.setState({
      modalVisible: true,
      selectedContact: item,
      note: (item.description) ? item.description : '' })
  }

  renderItem({item, index}) {
    if (item.value === 'emptydata') {
      return (
        <View style={styles.emptyViewContainer}>
          <Text style={styles.emptyContactList}>{strings('contactTab.contact_noContact')}</Text>
          <Text style={[styles.emptyConactInfo, { textAlign: 'center' }]}>{strings('contactTab.contact_emptyConactInfo')}</Text>
        </View>
      )
    } else {
      return (
        <MyContact
          key={index}
          contact={(this.state.selectedContact && item.handle_name === this.state.selectedContact.handleName) ? this.state.selectedContact : item}
          onLongPress={() => this.handleItemPress(item)}
          onPress={(handleName, isFriend, username, image) => this.props.handleSelectContact(item, handleName, isFriend, username, image)}
        />
      )
    }
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    
    if (this.state.fetching && this.props.myContacts.length === 0) {
      return (
        <View style={[styles.inviteContainer]}>
          {
            (Platform.OS === 'ios') ?
              <MaterialIndicator
                size={35}
                color={Colors.blueTheme}
                animating={this.state.fetching} />
              :
              <ActivityIndicator
                size={35}
                color={Colors.blueTheme}
                animating={this.state.fetching} />
          }
        </View>
      );
    } else {
      // let data = (this.props.myContacts.length > 0) ? this.props.myContacts : [{ value: "emptydata" }]
      return (
        <View style={[styles.inviteContainer]}>
          {
            (this.props.myContacts.length > 0) ?
              <SectionList
                sections={this.sortData()}
                renderSectionHeader={this._renderHeader}
                removeClippedSubviews={false}
                initialNumToRender={10}
                // stickySectionHeadersEnabled={false}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
                extraData={this.props}
                style={{ width: '100%', paddingTop: 0 }}
                refreshControl={
                  <RefreshControl
                    tintColor={Colors.blueTheme}
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
              />
              :
              <ScrollView contentContainerStyle={styles.emptyViewContainer}
                refreshControl={
                  <RefreshControl
                    tintColor={Colors.blueTheme}
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
              >
                <Text style={styles.emptyContactList}>{strings('contactTab.contact_noContact')}</Text>
                <Text style={[styles.emptyConactInfo, { textAlign: 'center' }]}>{strings('contactTab.contact_emptyConactInfo')}</Text>
              </ScrollView>
          }
          
          {this.renderActionModal()}
          <Loader loading={this.state.loading} />
        </View>
      );
    }
  }
}

const stateToProps = (state) => ({
  myContacts: state.contacts.myContacts,
  userId: state.login.userId,
  isRTL: state.login.isRTL
});

const dispatchToProps = (dispatch) => {
  return {
    getContactList: (userId, type, pageNumber) => dispatch(actions.getContactList(userId, type, pageNumber)),
    contactBlockUnblock: (data) => dispatch(actions.contactBlockUnblock(data)),
    removeContact: (data) => dispatch(actions.removeContact(data)),
    addNote: (data) => dispatch(actions.addNote(data))
  }
}
const Container = connect(stateToProps, dispatchToProps)(MyContacts)
export default Container;