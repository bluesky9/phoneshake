import { StyleSheet, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../../themes/'

export default StyleSheet.create({
  inviteContainer: {
    justifyContent: 'center',
    // alignItems: 'center',
    flex: 1
  },
  letterStyle: {
    color: Colors.blueTheme,
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextMedium
  },
  modalActionSheet: {
    // height: Metrics.screenHeight * 0.374
  },
  emptyContactList: {
    fontFamily: Fonts.type.SFTextMedium,
    fontSize: Metrics.screenWidth * 0.053,
    color: '#000',
    textAlign: 'center'
  },
  emptyViewContainer: {
    marginTop: Metrics.screenHeight * 0.277,
    flex: 1,
    alignItems: 'center'
  },
  emptyConactInfo: {
    marginTop: 7,
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032,
    color: 'rgb(168, 168, 168)',
    paddingHorizontal: Metrics.screenWidth * 0.042
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  headerTitle: {
    fontSize: 12,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  sectionHeader: {
    height: 20,
    justifyContent: 'center',
    backgroundColor: 'rgb(247, 247, 247)',
    paddingLeft: 10
  }
});
