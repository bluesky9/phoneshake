import React, { Component } from "react";
import { TabNavigator, NavigationActions, StackActions, SafeAreaView } from "react-navigation";
import { View, AppState, StatusBar, Image, Platform } from 'react-native';
import branch from 'react-native-branch'
import SplashScreen from 'react-native-smart-splash-screen'
import HomeStack from '../home/home.stack.js';
import SettingStack from '../me/me.stack.js';
import ContactStack from '../contacts/contacts.stack.js';
import SendBird from 'sendbird';
import OnboardSecond from './onboardSecond';
import styles from "./tabs.styles.js";
import { Images, Colors, Metrics } from '../../themes/'
import MessageStack from "../message/message.stack.js";
import NotificationStack from '../notification/notifications.stack'
import NotificationIcon from './notificationIcon'
import NotificationCounter from './notificationCounter'
import SendBirdConnect from './sendbirdConnect'
import { strings } from '../../locale';

const tabBarOptions = (Platform.OS === 'ios') ? {
  activeTintColor: Colors.blueTheme,
  inactiveTintColor: 'rgb(168,168,168)',
  style: {
    backgroundColor: '#fff',
    shadowOffset: { width: 2, height: -5, },
    shadowColor: 'rgba(168,168,168, 0.16)',
    shadowOpacity: 0.9,
    borderTopWidth: 0
  },
  showIcon: true,
  showLabel: true,
  tabStyle: { height: 49, paddingBottom: 2, marginTop: 1 }
} : {
    activeTintColor: Colors.blueTheme,
    inactiveTintColor: 'rgb(168,168,168)',
    showIcon: true,
    showLabel: true,
    tabStyle: { height: 49, paddingBottom: 2, marginTop: 0, backgroundColor: 'transparent' },
    style: { backgroundColor: '#fff' },
    indicatorStyle: { backgroundColor: 'transparent' },
    labelStyle: { fontSize: Metrics.screenWidth * 0.021, marginTop: 4, marginHorizontal:0 }
}
console.disableYellowBox = true;

const Tabsnavigator = ({ logout, initialRouteName, handleName }) => {
  const TabsScreenRouter = TabNavigator({
    Contact: { 
      screen: ContactStack,
      navigationOptions: ({ navigation }) => ({
        title: strings('tabBar.contact')
      }) 
    },
    Message: { 
      screen: MessageStack,
      navigationOptions: ({ navigation }) => ({
        title: strings('tabBar.message')
      }) 
     },
    Share: { 
      screen: HomeStack,
      navigationOptions: ({ navigation }) => ({
        title: strings('tabBar.share')
      }) 
    },
    Notification: { 
      screen: NotificationStack,
      navigationOptions: ({ navigation }) => ({
        title: strings('tabBar.notification')
      }) 
    },
    Me: { 
      screen: SettingStack,
      navigationOptions: ({ navigation }) => ({
        title: strings('tabBar.me')
      }) 
    }
  }, {
      lazy: true,
      swipeEnabled: false,
      headerMode:'screen',
      tabBarPosition: "bottom",
      initialRouteName,
      tabBarOptions,
      navigationOptions: ({ navigation }) => ({
        tabBarOnPress: ({ scene, jumpToIndex }) => {
          const { route, focused, index } = scene;
          if (focused) {
            if (route.index > 0) {
              let currentIndex = route.index;
              while (currentIndex > 0) {
                navigation.dispatch(NavigationActions.back({}));
                currentIndex -= 1;
              }
            }
          } else {
            jumpToIndex(index);
          }
        },
        tabBarIcon: ({ focused, tintColor }) => {
          const { routeName } = navigation.state;
          let iconName;
          if (routeName === 'Me') {
            iconName = `me${focused ? 'Active' : 'Inactive'}`;
          } else if (routeName === 'Message') {
            iconName = `message${focused ? 'Active' : 'Inactive'}`;
            return <NotificationIcon source={Images[iconName]} />;
          } else if (routeName === 'Contact') {
            iconName = `contact${focused ? 'Active' : 'Inactive'}`;
          } else if (routeName === 'Share') {
            iconName = `home${focused ? 'Active' : 'Inactive'}`;
          } else if (routeName === 'Notification') {
            iconName = `notification${focused ? 'Active' : 'Inactive'}`;
            return <NotificationCounter source={Images[iconName]} />;
          }
          return <Image source={Images[iconName]} style={styles.tabImage} resizeMode='contain' />;
        },
      })
    })
    
    return (
      <TabsScreenRouter screenProps={{ initialRouteName, logout, handleName }} />
    )
}

class TabsWithProps extends Component {
  constructor() {
    super();
    this.state = {
      initialRouteName: 'Share',
      handleName: ''
    };
    this.changeTab = this.changeTab.bind(this);
  }

  componentWillMount() {
    this.subscribeToBranch()
  }

  componentDidMount() {
    SplashScreen.close({
      animationType: SplashScreen.animationType.scale,
      duration: 850,
      delay: 500,
    })

    AppState.addEventListener("change", this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  subscribeToBranch() {
    //branch.skipCachedEvents()
    try {
      branch.subscribe(({ error, params }) => {
        if (error) {
          console.error('Error from Branch: ' + error)
          return;
        }

        if (params['+non_branch_link']) {
          const nonBranchUrl = params['+non_branch_link']
          
          console.log('Route non-Branch URL if appropriate.', params);
          return;
        }

        if (!params['+clicked_branch_link']) {
          console.log('No link was opened.');
          return;
        }

        console.log('Params: ', params);
        this.navigate(params.handleName)
      })
    } catch (err) {
      console.log('branch error:', err);
    }
  }

  navigate(handleName) {
    this.setState({ handleName })
  }

  changeTab = (tabName) => {
    this.setState({ initialRouteName: tabName })
  }

  _handleAppStateChange = (nextAppState) => {
    const sb = SendBird.getInstance();
    if (sb) {
      if (nextAppState === 'active') {
        sb.setForegroundState();
      } else if (nextAppState === 'background') {
        sb.setBackgroundState();
      }
    }
  }

  render() {
    let isModalVisible = (this.props.navigation.state.params && this.props.navigation.state.params.fromRegistration) ? true : false
    const LogoutAction = StackActions.reset({
      index: 0, actions: [{ type: NavigationActions.NAVIGATE, routeName: 'LandingScreen' }], key: null, params: {
        transition: 'SlideInFromRight'
      } })
    let logoutUser = () => this.props.navigation.dispatch(LogoutAction);
    return (
      
      <View style={{ flex: 1 }}>
        <StatusBar barStyle='dark-content' backgroundColor='transparent' />
        <Tabsnavigator logout={logoutUser}
          initialRouteName={this.state.initialRouteName}
          handleName={this.state.handleName}
          changeTab={this.changeTab}
           />
        <SendBirdConnect />
        <OnboardSecond modalVisible={isModalVisible} changeTab={this.changeTab}/>
      </View>
    );
  }
}

export default TabsWithProps;
