import React from 'react';
import { AsyncStorage, Platform, PermissionsAndroid } from 'react-native';
import { connect } from 'react-redux';
import FCM, { FCMEvent } from 'react-native-fcm';
import SendBird from 'sendbird';
import { SENDBIRD_APPID } from '../../api/constants'
let sb = new SendBird({ appId: SENDBIRD_APPID });
import { updateConnectionStatus, getAllMessages, updateCameraPermission } from '../message/message.actions'

class SendBirdSetup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apnsToken: ''
    }
    this.setupPush = this.setupPush.bind(this);
  }

  async componentWillMount() {
    if(Platform.OS === 'ios') {
      await FCM.getAPNSToken().then(token => {
        if (token) {
          this.setState({ apnsToken: token })
        }
      });
    } else {
      await FCM.getFCMToken().then(token => {
        if (token) {
          console.log('got token will mount -- - - - -: ', token);
          
          this.setState({ apnsToken: token })
        }
      });
      // this.getPermission()
      this.requestExternalStoragePermission().done()
    }
  }

  componentDidMount() {
    if(Platform.OS === 'ios') {
      this.connectToSendBird().done();
    }
  }

  requestExternalStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Phoneshake Storage Permission',
          message: 'Phoneshake needs access to your storage ' +
            'so you can save your QR Code',
        },
      );
      // return granted;
    } catch (err) {
      console.error('Failed to request permission ', err);
      // return null;
    }

    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Phoneshake Storage Permission',
          message: 'Phoneshake needs access to your storage ' +
            'so you can save your QR Code',
        },
      );
      // return granted;
    } catch (err) {
      console.error('Failed to request permission ', err);
      // return null;
    }

    this.getPermission()
  }

  async getPermission() {
    let _this = this;
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Allow "Phoneshake" to access your Camera',
          message: 'Phoneshake allows you to capture and update an image from your camera to your profile.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        _this.props.updateCameraPermission('authorized')
        this.connectToSendBird().done();
        console.log('camera granted');
        
      } else {
        this.connectToSendBird().done();
        console.log('camera denied');
        _this.props.updateCameraPermission(granted)
      }
    } catch (err) {
      console.error('Failed to request permission ', err);
    }
  }

  async connectToSendBird() {
    let _this = this;
    _this.props.updateConnectionStatus('connecting')
    if(!sb) {
      sb = new SendBird({ appId: SENDBIRD_APPID });
    }
     
    sb.connect(this.props.userId, (user, error) => {
      if (error) {
        console.log('SendBird Login Failed.');
        _this.props.updateConnectionStatus('failed')
      } else {
        sb.updateCurrentUserInfo(_this.props.name, _this.props.profilePic, async function (user, error) {
          if (error) {
            console.log('Update user Failed!');
            return;
          } else {
            console.log('SB User updated: ');
            _this.props.updateConnectionStatus('connected')
            _this._connect()
            _this.setupPush()
          }
        })
      }
    });
  }

  async setupPush() {
    let _this = this;
    if (Platform.OS === "ios") {
      if (this.state.apnsToken && this.state.apnsToken !== '') {
        sb.registerAPNSPushTokenForCurrentUser(this.state.apnsToken, function (response, error) {
          if (error) {
            console.error(error);
            return;
          }
          AsyncStorage.setItem('SendBirdToken', JSON.stringify(_this.state.apnsToken))
          console.log('ios push res success already having: ');
        });
      } else {
        await FCM.getAPNSToken().then(token => {
          console.log("APNS TOKEN (getFCMToken)", token);
          if (token) {
            sb.registerAPNSPushTokenForCurrentUser(token, function (response, error) {
              if (error) {
                console.error(error);
                return;
              }
              AsyncStorage.setItem('SendBirdToken', JSON.stringify(token))
              console.log('ios push res success: ');
            });
          }
        });
      }
    } else {
      if (this.state.apnsToken && this.state.apnsToken !== '') {
        sb.registerGCMPushTokenForCurrentUser(this.state.apnsToken, function (response, error) {
          if (error) {
            console.error(error);
            return;
          }
          AsyncStorage.setItem('SendBirdToken', JSON.stringify(_this.state.apnsToken))
          console.log('android push res success already having: ');
        });
        // sb.unregisterGCMPushTokenForCurrentUser('e_6x8ZFBFb4:APA91bFMgf-HQJ_QEphukv2o7zTyC4k-KSO-LdCWitfQiMog-Xorlbqp1wrwv42a0HCFYBl348czShfUByew34Db2qPQxHki0FYvMItwYR52yDXM4tWhJZTp6LbleIzuQWLvgZP1yBY0', function (response, error) {
        //   if (error) {
        //     console.error(error);
        //     return;
        //   }
        //   // AsyncStorage.setItem('SendBirdToken', JSON.stringify(_this.state.apnsToken))
        //   console.log('android token removed');
        // });
      } else {
        await FCM.getFCMToken().then(token => {
          if (token) {
            sb.registerGCMPushTokenForCurrentUser(token, function (response, error) {
              if (error) {
                console.error(error);
                return;
              }
              AsyncStorage.setItem('SendBirdToken', JSON.stringify(token))
              console.log('android push res success: ');
            });
          }
        });
      }
    }
  }

  _connect() {
    let _this = this;
    var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
    channelListQuery.includeEmpty = false;
    channelListQuery.limit = 100; // pagination limit could be set up to 100

    if (channelListQuery.hasNext) {
      channelListQuery.next(function (channelList, error) {
        if (error) {
          console.error(error);
          return;
        }
        _this.props.getAllMessages(channelList)
        AsyncStorage.setItem('Messages', JSON.stringify(channelList))
      });
    }
  }

  render() {
    return null;
  }
}


const mapStateToProps = state => ({
  messageCount: state.messages.messageCount,
  userId: state.login.userId,
  user: state.login,
  name: state.login.name,
  profilePic: state.login.profilePic
});

const dispatchToProps = (dispatch) => {
  return {
    updateConnectionStatus: (status) => dispatch(updateConnectionStatus(status)),
    getAllMessages: (messages) => dispatch(getAllMessages(messages)),
    updateCameraPermission: (status) => dispatch(updateCameraPermission(status)),
  }
}

export default connect(mapStateToProps, dispatchToProps)(SendBirdSetup);
