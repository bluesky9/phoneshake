import React, { Component } from 'react'
import { View, Text, StatusBar, Image, Modal, StyleSheet, Platform } from 'react-native';
import { Button } from 'native-base'
import { DotIndicator } from 'react-native-indicators';
import { Fonts, Metrics, Colors, Images } from '../../themes/'
import PhoneShakeButton from '../../components/phoneShakeButton/'
import { strings } from '../../locale/';

class OnboardSecond extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
    headerTitleStyle: {
      fontFamily: Fonts.type.SFTextRegular,
      fontWeight: "200",
      fontSize: Metrics.screenWidth * 0.053,
      alignSelf: 'center',
      textAlign: 'center',
      width: '70%'
    },
  });

  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  }
  constructor() {
    super();
    this.state = {
      loading: true
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setState({ modalVisible: this.props.modalVisible })
    setTimeout(() => {
      this.setState({ loading: false })
    }, 5000);
  }

  handleSubmit() {
    this.setState({ modalVisible: false })
    this.props.changeTab('Me')
  }

  renderLoading() {
    return (
      <View style={{ paddingTop: (Platform.OS === 'ios') ? 22 : 0, flex: 1, alignItems: 'center', justifyContent: 'center' }} >
        <View style={{ height: 50, width: 150 }}>
          <DotIndicator
            size={16}
            count={3}
            color={Colors.blueTheme}
            animating={true} />
        </View>
        <Text style={styles.loadingText}>{strings('login.weAreGettingYourContact')}</Text>
      </View>
    )
  }

  renderProfilePreview() {
    return (
      <View style={{ paddingTop: (Platform.OS === 'ios') ? 22 : 0, flex: 1 }} >
        <StatusBar barStyle='dark-content' />
        <View style={styles.header}>
          <Button transparent style={{ width: 45, justifyContent: 'flex-end', marginRight: 11}} onPress={() => this.setState({ modalVisible: false })} >
            <Text style={styles.skipButton}>{strings('constants.skip')}</Text>
          </Button>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>{strings('login.buildYourProfile')}</Text>
          <View style={styles.imageContainer}>
            <Image source={Images.dummyProfile} style={styles.dummyProfileImage} resizeMode='contain' />
          </View>
        </View>
        <PhoneShakeButton
          disabled={false}
          onPress={() => this.handleSubmit()}
          text={strings('constants.continue')}
          bottomHeight={15} />
      </View>
    )
  }

  render() {
    
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <StatusBar barStyle='dark-content' />
        
        {(this.state.loading) ? this.renderLoading(): this.renderProfilePreview()}
        
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 44,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  skipButton: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.045,
    color: Colors.blueTheme
  },
  title: {
    fontSize: Metrics.screenWidth * 0.053,
    textAlign: 'center',
    color: '#000',
    marginTop: 10,
    paddingBottom: Metrics.screenHeight * 0.029,
    fontFamily: Fonts.type.SFTextMedium
  },
  content: {
    // backgroundColor: 'red'
  },
  imageContainer: {
    width: Metrics.screenWidth * 0.65,
    height: Metrics.screenHeight * 0.65,
    shadowColor: '#000',
    marginTop: 10,
    alignSelf: 'center',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 2,
    borderWidth: 0,
    borderColor: '#fff'
  },
  dummyProfileImage: {
    flex: 1,
    alignSelf: 'center'
  },
  loadingText: {
    fontSize: Metrics.screenWidth * 0.064,
    textAlign: 'center',
    color: Colors.blueTheme,
    fontFamily: Fonts.type.SFTextRegular
  }
});




export default OnboardSecond;