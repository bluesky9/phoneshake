import React from 'react';
import { Text, Image, View, StyleSheet, Platform } from 'react-native';
import { Metrics, Colors } from '../../themes'
import { connect } from 'react-redux';
import { Badge } from "native-base";

class NotificationCounter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { friendRequestsCount } = this.props;

    if (Platform.OS === 'ios') {
      return (
        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
          width: 60
        }}>
          <Image source={this.props.source} style={styles.tabImage} />
          {friendRequestsCount > 0 ?
            <Badge style={(friendRequestsCount > 9) ? styles.badge : [styles.badge, { width: 22, height: 22 }]}><Text style={(friendRequestsCount > 9) ? styles.badgeText : [styles.badgeText, { marginLeft: 2, marginTop: -1 }]}>{friendRequestsCount}</Text></Badge>
            : null}
        </View>
      )
    } else {
      return (
        <View style={{
          zIndex: 0,
          flex: 1,
          alignSelf: 'stretch',
          justifyContent: 'space-around',
          alignItems: 'center'
        }}>
          <Image source={this.props.source} style={styles.tabImage} />
          {friendRequestsCount > 0 ?
            <View style={{
              position: 'absolute',
              top: 0,
              right: 0,
              borderRadius: 50,
              backgroundColor: Colors.themeColour,
              paddingHorizontal: 5,
              zIndex: 111
            }}>
              <Text style={[styles.badgeText]}>{friendRequestsCount}</Text>
            </View>
            : undefined}
        </View>

      );
    }

  }
}

const styles = StyleSheet.create({
  tabImage: {
    height: Metrics.screenWidth * 0.05,
    width: Metrics.screenWidth * 0.05
  },
  badge: {
    position: 'absolute',
    right: Metrics.screenWidth * 0.001,
    top: -7, //(Platform.OS === 'ios') ? -2 : -15,
    backgroundColor: Colors.red,
    padding: 0
  },
  badgeText: {
    fontSize: 10,
    lineHeight: 18,
    backgroundColor: 'transparent',
    color: 'white'
  },
});

const mapStateToProps = state => ({
  friendRequestsCount: state.login.friendRequestsCount,
  userId: state.login.userId
});

export default connect(mapStateToProps)(NotificationCounter);
