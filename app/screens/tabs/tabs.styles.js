import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../../themes/'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white
  },
  tab: {
    backgroundColor: Colors.white,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomWidth: 3,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    paddingBottom: 0
  },
  label: {
    fontSize: Metrics.screenWidth * 0.026,
  },
  footer: {
    backgroundColor: 'white',
    shadowColor: 'rgb(150, 150, 150)',
    shadowOffset: { width: 0, height: -1 },
    shadowOpacity: 0.5,
    shadowRadius: 1
  },
  tabImage: {
    // fontSize: 28
    height: Metrics.screenWidth * 0.064,
    width: Metrics.screenWidth * 0.0595
  },
  badge: {
    position: 'absolute',
    alignSelf: 'flex-end',
    top: 3,
    right: Metrics.screenWidth * 0.05,
    backgroundColor: Colors.themeColour,
    padding: 0
  },
  badgeText: {
    fontSize: 12,
    backgroundColor: 'transparent'
  }
});
