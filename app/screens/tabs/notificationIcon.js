import React from 'react';
import { Text, Image, View, StyleSheet, Platform } from 'react-native';
import { Metrics, Colors } from '../../themes'
import { connect } from 'react-redux';
import { Badge } from "native-base";
import SendBird from 'sendbird';
import { SENDBIRD_APPID } from '../../api/constants'
const sb = new SendBird({ appId: SENDBIRD_APPID });

class NotificationIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { messageCount } = this.props;

    if (Platform.OS === 'ios') {
      return (
        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
          width: 60
        }}>
          <Image source={this.props.source} style={styles.tabImage} />
          {messageCount > 0 ?
            <Badge style={(messageCount > 9) ? styles.badge : [styles.badge, { width: 22, height: 22 }]}><Text style={(messageCount > 9) ? styles.badgeText : [styles.badgeText, { marginLeft: 2, marginTop: -1 }]}>{messageCount}</Text></Badge>
            : null}
        </View>
      )
    } else {
      return (
        <View style={{
          zIndex: 111,
          flex: 1,
          alignSelf: 'stretch',
          justifyContent: 'space-around',
          alignItems: 'center',
          // backgroundColor: 'blue',
        }}>
          <Image source={this.props.source} style={styles.tabImage} />
          {messageCount > 0 ?
            <View style={{
              position: 'absolute',
              top: 0,
              right: 0,
              borderRadius: 50,
              backgroundColor: Colors.red,
              paddingHorizontal: 5,
              zIndex: 111
            }}>
              <Text style={[styles.badgeText]}>{messageCount}</Text>
            </View>
            : undefined}
        </View>

      );
    }

  }
}

const styles = StyleSheet.create({
  tabImage: {
    height: Metrics.screenWidth * 0.05,
    width: Metrics.screenWidth * 0.05
  },
  badge: {
    position: 'absolute',
    right: Metrics.screenWidth * 0.001,
    top: -7, //(Platform.OS === 'ios') ? -2 : -15,
    backgroundColor: Colors.red,
    padding: 0
  },
  badgeText: {
    fontSize: 10,
    lineHeight: 18,
    backgroundColor: 'transparent',
    color: 'white'
  },
});

const mapStateToProps = state => ({
  messageCount: state.messages.messageCount
});

export default connect(mapStateToProps)(NotificationIcon);
