import React from 'react';
import { AsyncStorage, Platform, View, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';
import { DotIndicator } from 'react-native-indicators';
import SendBird from 'sendbird';
import { SENDBIRD_APPID } from '../../api/constants'
let sb = new SendBird({ appId: SENDBIRD_APPID });
import { clearMessages } from '../message/message.actions'
import { autoLogout } from '../login/login.actions'
import { clearContacts } from '../contacts/contacts.actions'
import { Colors, Metrics, Fonts } from '../../themes/'

class ResetAccount extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    this.logoutUser()
  }

  async logoutUser() {
    this.props.clearContacts()
    this.props.clearMessages()
    this.props.autoLogout()
    await this.logoutSendBird().done();
    AsyncStorage.removeItem('MyContacts')
    AsyncStorage.removeItem('Messages')
    AsyncStorage.multiRemove(['PhoneShakeUser', 'SyncedContact', 'MyContacts', 'PhonebookContacts', 'Messages'])
    setTimeout(() => {
      this.props.screenProps.logout()  
    }, 2000);
    
  }

  async logoutSendBird() {
    var value = await AsyncStorage.getItem('SendBirdToken')
    if (value !== null) {
      let apnsToken = JSON.parse(value);
      if(sb) {
        if (Platform.OS === 'ios') {
          sb.unregisterAPNSPushTokenForCurrentUser(apnsToken, function (response, error) {
            if (error) {
              console.error(error);
              return;
            }
            sb.disconnect(function () {
              AsyncStorage.removeItem('SendBirdToken')
            });
          });
        } else {
          sb.unregisterGCMPushTokenForCurrentUser(apnsToken, function (response, error) {
            if (error) {
              console.error(error);
              return;
            }
            sb.disconnect(function () {
              AsyncStorage.removeItem('SendBirdToken')
            });
          });
        }
      }
    } else {

    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ height: 50, width: 150 }}>
          <DotIndicator
            size={16}
            count={3}
            color={Colors.blueTheme}
            animating={true} />
        </View>
        <Text style={styles.loadingText}>{`Session Expired\nPlease login again...`}</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userId: state.login.userId
});

const dispatchToProps = (dispatch) => {
  return {
    clearMessages: () => dispatch(clearMessages()),
    clearContacts: () => dispatch(clearContacts()),
    autoLogout: () => dispatch(autoLogout()),
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingText: {
    fontSize: Metrics.screenWidth * 0.064,
    textAlign: 'center',
    color: Colors.blueTheme,
    fontFamily: Fonts.type.SFTextRegular
  }
});

export default connect(mapStateToProps, dispatchToProps)(ResetAccount);
