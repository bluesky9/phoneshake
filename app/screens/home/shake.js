import React, { Component } from 'react'
import { View, Image, NativeEventEmitter, NativeModules, Platform, Alert } from 'react-native';
import { Text } from 'native-base';
import { connect } from 'react-redux'
import Permissions from 'react-native-permissions'
import Switch from 'react-native-switch-pro'
import SystemSetting from 'react-native-system-setting'
import { DotIndicator } from 'react-native-indicators';
import RNShakeEvent from 'react-native-shake-event';
var Base62 = require("base62/lib/ascii");
import * as actions from '../login/login.actions.js';
import styles from './home.styles'
import { Images, Colors, Metrics } from '../../themes/'
import { strings } from '../../locale/index.js';
const ChirpConnect = NativeModules.ChirpConnect;
const ChirpConnectEmitter = new NativeEventEmitter(ChirpConnect);
const key = 'c36673F9A0CE8fE4C286AceeD';
const secret = '24BcD224E4f5F5a6b2E7eCd6DBCf80eF6dE5342177eAaDD05c';

class Shake extends Component {
  handleViewRef = ref => this.view = ref;
  constructor() {
    super();
    this.state = {
      // loading: false,
      shaked: false,
      value: true,
      receiving: false
    };
    this.getUser = this.getUser.bind(this);
    this.startBroadCasting = this.startBroadCasting.bind(this);
    this._onSyncPress = this._onSyncPress.bind(this);
    this.renderSending = this.renderSending.bind(this);
    this.setupChirpConnect = this.setupChirpConnect.bind(this);
  }

  componentDidMount() {
    SystemSetting.setVolume(1, { type: (Platform.OS === 'ios') ? 'system' : 'music', playSound: true, showUI: false });
    // SystemSetting.getVolume().then((volume) => {
      // if (volume < 1) {
      //   SystemSetting.setVolume(1, { type: (Platform.OS === 'ios') ? 'system' : 'music', playSound: true, showUI: false });
      // }
    // });
    this.checkPermission()
  }

  checkPermission() {
    Permissions.check('microphone').then(response => {
      if (response === 'authorized') {
        this.setupChirpConnect()
      } else if (response === 'denied') {
        let msg = strings('home.microphonePermissionPathAlert')
        Alert.alert(
          strings('home.microphonePermissionAlert'),
          msg,
          [
            { text: strings('constants.ok'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: strings('constants.openSetting'), onPress: () => Permissions.openSettings() }
          ],
          { cancelable: false }
        )
        // return false
      } else if (response != 'authorized') {
        Permissions.request('microphone').then(userResponse => {
          console.log('Permission: ', userResponse);
          if (userResponse === 'authorized') {
            this.setupChirpConnect()
          }
        })
        // return false
      }
    })
  }

  setupChirpConnect() {
    ChirpConnect.init(key, secret);
    RNShakeEvent.addEventListener('shake', () => {
      console.log('Device shake!');
      if (this.state.value) {
        this.setState({ receiving: true })
        setTimeout(() => {
          if (this.state.receiving) {
            this.setState({ receiving: false })
          }
        }, 5000);
      }
      if (!this.state.shaked && !this.state.value) {
        this.setState({ shaked: true })
        this.startBroadCasting()
      }
    });

    if (Platform.OS === 'android') {
      this.onSetConfigSuccess = ChirpConnectEmitter.addListener(
        'onSetConfigSuccess',
        (event) => {
          console.log('on success: ', event);
          ChirpConnect.start();
        }
      );
    }

    this.onStateChanged = ChirpConnectEmitter.addListener(
      'onStateChanged',
      (event) => {
        console.log('event status: ', event.status);
      }
    );

    this.onSent = ChirpConnectEmitter.addListener(
      'onSent', (event) => { this.setState({ shaked: false }); console.log('Sent: ', event) }
    )
    this.onError = ChirpConnectEmitter.addListener(
      'onError', (event) => { console.log('Error: ', event); this.setState({ receiving: false, shaked: false }) }
    )
    if (Platform.OS === 'ios') {
      ChirpConnect.start();
    }

    this.setReceiveListner()
  }

  setReceiveListner() {
    this.onReceived = ChirpConnectEmitter.addListener(
      'onReceived',
      (event) => {
        console.log('received: ', event);
        if (event.data) {
          var arr = event.data.map(s => String.fromCharCode(s));
          let userId = arr.join('')
          console.log('userId Received: ', userId);
          let decodedUserId = Base62.decode(userId);
          // alert(decodedUserId)
          this.getUser(decodedUserId)
        }
      }
    )
    this.onReceiving = ChirpConnectEmitter.addListener(
      'onReceiving',
      (event) => {
        console.log('receiving');
        this.setState({ receiving: true })
      }
    )
  }

  _onSyncPress() {
    this.setState(previousState => {
      if (previousState.value && this.onReceived && this.onReceiving) {
        this.onReceived.remove()
        this.onReceiving.remove()
      } else {
        this.setReceiveListner()
      }
      return { value: !previousState.value, receiving: false };
    });
  }

  componentWillUnmount() {
    ChirpConnect.stop();
    if (this.onReceived && this.onError && this.onSent && this.onReceiving){
      this.onReceived.remove();
      this.onError.remove();
      this.onSent.remove();
      this.onReceiving.remove();
    }
  }

  startBroadCasting() {
    let userId = this.props.userId
    let encodedUserId = Base62.encode(userId);

    var arr = [...encodedUserId].map(s => s.charCodeAt());

    ChirpConnect.send(arr);
  }

  getUser(userId) {
    
    if(!userId) return false;
    if (!this.state.receiving)
      this.setState({ receiving: true })

    this.props.getUserByUserId(userId, this.props.userId)
      .then((response) => {
        console.log('Response', response);
        if (response && response.payload.data && response.payload.data.content && response.payload.data.content.name) {
          let user = response.payload.data.content;
          let udpatedUsernameArray = [];
          if (user.usernames.length) {
            user.usernames.map((username) => {
              udpatedUsernameArray.push(username.username);
            });
          }
          user.usernames = udpatedUsernameArray;
          user.username = user.usernames[0];
          user.handleName = user.handle_name;
          this.setState({ receiving: false, userId: user.id, error: null });
          this.props.navigation.navigate('UserDetailScreen', { user, isAllInfo: true })
        } else if (response && response.payload.data && response.payload.data.status === 401) {
          this.props.navigation.navigate('ResetAccount')
        } else {
          console.log('Error');
          this.setState({ receiving: false, userId: null, error: null });
        }
      });
  }

  renderShake() {
    return (
      <View style={{ alignItems: 'center' }}>
        <Image source={Images.shakeCenter} resizeMode='contain' style={styles.shakeImage} />
        <Text style={styles.shakeText}>{strings('home.shakeText')}</Text>
      </View>
    )
  }

  renderSending() {
    return (
      <View style={{ alignItems: 'center' }}>
        <View style={{ height: 70, width: 150 }}>
          <DotIndicator
            size={16}
            count={3}
            color={Colors.blueTheme}
            animating={true} />
        </View>
        <Text style={[styles.shakeText, { fontSize: Metrics.screenWidth * 0.064, color: Colors.blueTheme }]}>
          {(this.state.receiving) ? strings('home.gettingContact') : strings('home.sendingContact')}
        </Text>
      </View>
    )
  }

  render() {
    let content = this.renderShake();
    if (this.state.shaked || this.state.receiving) {
      content = this.renderSending()
    }
    return (
      <View style={styles.shakeContainer}>
        {content}
        <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', bottom: 30, width: Metrics.screenWidth, justifyContent: 'center' }}>
          <Text style={[styles.shakeText, { width: Metrics.screenWidth * 0.24, textAlign: 'right', fontSize: Metrics.screenWidth * 0.042, color: (this.state.value) ? '#000' : Colors.blueTheme }]}>{strings('constants.send')}</Text>
          <Switch
            onSyncPress={() => this._onSyncPress()}
            height={32}
            width={51}
            value={this.state.value}
            style={{ width: 60, marginHorizontal: 12 }}
            backgroundActive={Colors.blueTheme}
            backgroundInactive={Colors.blueTheme}
          />
          <Text style={[styles.shakeText, { width: Metrics.screenWidth * 0.24, fontSize: Metrics.screenWidth * 0.042, textAlign: 'left', color: (this.state.value) ? Colors.blueTheme : '#000' }]}>{strings('constants.receive')}</Text>
        </View>
      </View>
    );
  }
}

const stateToProps = (state) => ({
  handleName: state.login.handleName,
  userId: state.login.userId
});

const dispatchToProps = (dispatch) => {
  return {
    getUserByUserId: (userId, requestorUserId) => dispatch(actions.getUserByUserId(userId, requestorUserId)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(Shake)
export default Container;
