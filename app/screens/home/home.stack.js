import React, { Component } from 'react'
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import { createStackNavigator } from 'react-navigation'
import Home from './home.js'
import UserDetail from '../contacts/userDetail/userDetail.js'
import Chat from '../message/chat/chat'
import ResetAccount from '../tabs/resetAccount'
import { strings } from '../../locale/index.js';

const HomeStack = createStackNavigator({
  HomeScreen: {
    screen: Home,
    navigationOptions: {
      title: strings('home.home')
    }
  },
  UserDetailScreen: {
    screen: UserDetail,
    navigationOptions: {
      title: strings('constants.profile')
    }
  },
  ChatScreen: {
    screen: Chat,
    navigationOptions: {
      title: strings('constants.profile')
    }
  },
  ResetAccount: {
    screen: ResetAccount,
    navigationOptions: {
      title: strings('constants.resetAccount')
    }
  }
}, {
    headerMode: 'none',
    initialRouteName: 'HomeScreen',
    navigationOptions: {}
});

HomeStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default HomeStack;
