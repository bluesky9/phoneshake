import { StyleSheet, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  shakeContainer: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#fff',
    paddingTop: Metrics.screenHeight * 0.21
  },
  shakeText: {
    fontSize: Metrics.screenWidth * 0.053,
    fontFamily: Fonts.type.SFTextMedium,
    textAlign: 'center',
    color: '#000'
  },
  defaultTabbar: {
    borderBottomWidth: 0.7,
    borderBottomColor: 'rgb(204, 204, 204)',
    height: Metrics.screenHeight * 0.055
  },
  tabBorder: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Colors.blueTheme,
    borderLeftColor: 'transparent',
    marginBottom: (Platform.OS === 'android') ? -1 : 0
  },
  tabBarTextStyle: {
    fontSize: Metrics.screenWidth * 0.037,
    marginTop: Metrics.screenHeight * 0.01,
    fontFamily: Fonts.type.SFTextRegular,
  },
  unreadCount: {
    fontFamily: Fonts.type.SFTextSemibold,
    fontSize: Metrics.screenWidth * 0.032,
    color:'#fff'
  },
  unreadCountContainer: {
    backgroundColor: Colors.blueTheme,
    height: (Platform.OS === 'ios') ? Metrics.screenWidth * 0.08 : Metrics.screenHeight * 0.052,
    width: (Platform.OS === 'ios') ? Metrics.screenWidth * 0.08 : Metrics.screenHeight * 0.052,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  cancelButton: {
    height: (Platform.OS === 'ios') ? Metrics.screenWidth * 0.08 : Metrics.screenHeight * 0.052,
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputStyle: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032,
    //
    width: '88%',
    backgroundColor:'rgb(247,247,247)',
    height: (Platform.OS === 'ios') ? Metrics.screenWidth * 0.08 : Metrics.screenHeight * 0.052,
    borderRadius: 5,
    marginRight: 10,
    paddingLeft: Metrics.screenWidth * 0.093
  },
  // searchIconStyle: {
  //   color: 'rgb(168, 168, 168)',
  //   fontSize: Metrics.screenWidth * 0.048,
  //   position: 'absolute',
  //   left: Metrics.screenWidth * 0.064,
  //   top: Metrics.screenHeight * 0.0235
  //   // bottom: (Platform.OS === 'ios') ? (Metrics.screenHeight > 668) ? Metrics.screenHeight * 0.015 : '26%': '40%'
  // },
  searchIconStyle: {
    color: 'rgb(168, 168, 168)',
    fontSize: Metrics.screenWidth * 0.048,
    marginTop: (Platform.OS === 'ios') ? -Metrics.screenHeight * 0.0074 : -2.5
  },
  navbar: {
    height: (Platform.OS === 'ios') ? 54 : Metrics.screenHeight * 0.077,
    paddingHorizontal: Metrics.screenWidth * 0.025,
    paddingTop: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.017 : Metrics.screenHeight * 0.011, //Metrics.screenHeight * 0.037
    backgroundColor: '#fff',
    borderBottomWidth: 0,
  },
  cancelTextStyle: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.042
  },

  qrcodeContainer: {
    alignItems: 'center',
    marginTop: (Platform.OS === 'ios') ? '22%' : '15%' //Metrics.screenHeight * 0.112,
  },
  activeQrPanelOuter: {
    width: Metrics.screenWidth * 0.53,
    height: Metrics.screenWidth * 0.53,
    // backgroundColor: 'rgb(32, 142, 251)',
    borderRadius: 5,
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.9,
    shadowRadius: 5,
    elevation: 2,
    // borderWidth: 3,
    borderColor: 'rgb(255, 255, 255)',
    // padding: Metrics.screenWidth * 0.05666,
  },
  error: {
    marginTop: 5,
    fontSize: Metrics.screenWidth * 0.042,
    lineHeight: Metrics.screenWidth * 0.050,
    fontFamily: Fonts.type.SFTextRegular,
    color: Colors.red
  },
  profileName: {
    marginTop: '0.5%',
    fontSize: Metrics.screenWidth * 0.053,
    lineHeight: Metrics.screenWidth * 0.064,
    fontFamily: Fonts.type.SFTextMedium,
    color: "#000"
  },
  handleName: {
    marginTop: 5,
    fontSize: Metrics.screenWidth * 0.042,
    lineHeight: Metrics.screenWidth * 0.050,
    fontFamily: Fonts.type.SFTextRegular,
    color: "rgb(127, 127, 127)"
  },
  inactiveQrPanelOuter: {
    marginTop: '10%',
    width: Metrics.screenWidth * 0.16,
    height: Metrics.screenWidth * 0.16,
    borderRadius: 5,
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.9,
    shadowRadius: 5,
    elevation: 2,
    borderWidth: 3,
    borderColor: 'rgb(255, 255, 255)',
    // backgroundColor: Colors.blueTheme,
    // padding: 3,
  },
  shareText: {
    marginTop: '12%',
    fontSize: Metrics.screenWidth * 0.037,
    lineHeight: 16,
    fontFamily: Fonts.type.SFTextRegular,
    color: Colors.blueTheme
  },
  overlay: {
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0 
  },
  shakeImage: {
    height: Metrics.screenHeight * 0.16,
    width: Metrics.screenWidth * 0.282,
    marginBottom: 10
  }
});
