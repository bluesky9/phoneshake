import React, { Component } from 'react'
import { View, TouchableOpacity, TextInput, Keyboard, Platform } from 'react-native';
import { Text, Icon } from 'native-base';
import { NavigationActions, SafeAreaView } from 'react-navigation'
import { connect } from 'react-redux';
import FCM, { FCMEvent } from 'react-native-fcm';
import Search from 'react-native-search-box';
import styles from './home.styles'
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import Loader from '../../components/loader/';
import SearchView from '../../components/search/search';
import { Colors } from '../../themes/'
import QrCode from './qrCode'
import Invite from './invite'
import Shake from './shake'
import * as actions from '../login/login.actions.js';
import { getAllMessages } from '../message/message.actions'
import { getContactList } from '../contacts/contacts.actions'
import { strings } from '../../locale';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      searchText: '',
      searching: false,
      showSearchView: false,
      notificationOpen: false,
      loading: false
    };
    this.handlePushclick = this.handlePushclick.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.renderSearchView = this.renderSearchView.bind(this);
    this.handleSelectContact = this.handleSelectContact.bind(this);
    this.dismiss = this.dismiss.bind(this);
    this.getPendingRequests = this.getPendingRequests.bind(this);
  }

  componentDidMount() {
    const { screenProps } = this.props;
    this.getPendingRequests()
    if (screenProps.handleName && screenProps.handleName !== ''){
      this.navigate(screenProps.handleName)
    }
  }

  componentWillMount() {
    this.setupPushNotification()
  }

  setupPushNotification() {
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      // console.log('NOTIgsdhvjfdv ** * * ** * * ** * ** * : ', notif);
      if (notif.aps && notif.aps.alert && notif.aps.alert.title && notif.aps.alert.title === 'Contact Request Accepted') {
        // console.log('get contact now');
        this.props.getContactList(this.props.userId, 'I', 1)
      }
      FCM.setBadgeNumber(0);
      if (!notif.local_notification) {
        // console.log('Show Local');
        this.showLocalNotification(notif)
      }
      if (notif && notif.opened_from_tray) {
        this.handlePushclick(notif)
      }
    });

    FCM.getInitialNotification().then(notif => {
      FCM.setBadgeNumber(0);
      // console.log('Notif initial: ', notif);
    });

    FCM.on(FCMEvent.RefreshToken, token => {
      
    });
  }

  showLocalNotification(notif) {
    let title = ''
    let body = ''
    if(Platform.OS === 'ios') {
      title = (notif.aps && notif.aps.alert && notif.aps.alert.title) ? notif.aps.alert.title : ''
      body = (notif.aps && notif.aps.alert && notif.aps.alert.body) ? notif.aps.alert.body : ''
    }

    FCM.presentLocalNotification({
      title: title,
      body: body,
      priority: "high",
      show_in_foreground: true,
    });
  }

  handlePushclick(notif) {
    if (notif && notif.sendbird) {
      let user = {
        user_id: notif.sendbird.sender.id,
        nickname: notif.sendbird.sender.name,
        profile_pic: notif.sendbird.sender.profile_url
      }
      const navigateAction = NavigationActions.navigate({
        routeName: 'Message',
        params: { fromPushNotification: true },
        action: NavigationActions.navigate({ routeName: 'Chat', params: { user } }),
      });
      this.props.navigation.dispatch(navigateAction);
    } else {
      this.props.startFetching(true)
      let data = {
        userId: this.props.userId,
        pageNo: 1,
      }
      this.props.getFriendRequestsFromNotif(data, true)
      const navigateAction = NavigationActions.navigate({
        routeName: 'Notification',
        params: { fromPushNotification: true }
      });
      this.props.navigation.dispatch(navigateAction);
    }
  }

  getPendingRequests() {
    let data = {
      userId: this.props.userId,
      pageNo: 1,
    }
    this.props.getFriendRequests(data, false)
  }

  componentWillUnmount() {
    this.notificationListener.remove();
  }

  navigate = (handleName) => {
    const { navigate } = this.props.navigation;
    
    this.setState({ loading: true });
    if (handleName !== this.props.handleName) {
      
      this.props.getUserByHandlename(handleName, this.props.userId)
      .then((response) => {
        if (response && response.payload.data && response.payload.data.content && response.payload.data.content.isAvailable === false) {
          let user = response.payload.data.content;
          let udpatedUsernameArray = [];
          if (user.usernames.length) {
            user.usernames.map((username) => {
              udpatedUsernameArray.push(username.username);
            });
          }
          user.usernames = udpatedUsernameArray;
          user.username = user.usernames[0];
          user.handleName = handleName;
          this.setState({ loading: false, userId: user.id, error: null });
          navigate('UserDetailScreen', { user, isAllInfo: true })
        } else if (response && response.payload.data && response.payload.data.status === 401) {
          navigate('ResetAccount')
        } else {
          console.log('Error');
          this.setState({ loading: false, userId: null, error: null });
        }
      });
    } else if (handleName === this.props.handleName) {
      this.setState({ loading: false, error: "Its you!" });
    } else {
      this.setState({ loading: false, error: strings('constants.errorMessage') });
    }
  }

  handleSearch(searchText) {
    this.setState({ searching: true, searchText })
  }

  handleSelectContact = (user, handleName, isFriend, username) => {
    user.handleName = handleName
    user.isFriend = isFriend
    user.username = username
    this.props.navigation.navigate('UserDetailScreen', { user })
  }

  dismiss = () => {
    Keyboard.dismiss()
    this.setState({ showSearchView: false, searchText: '', searching: false })
  }

  renderSearchView() {
    if(this.state.showSearchView) {
      return (
        <SearchView
          searchText={this.state.searchText}
          handleSelectContact={this.handleSelectContact}
          dismiss={() => (this.state.searchText.trim().length === 0) ? this.dismiss() : console.log('dismiss search')}
          />
      )
    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
      <View style={styles.container}>
        <View style={styles.navbar}>
          {/* <TextInput
            onChangeText={(text) => this.handleSearch(text)}
            style={[styles.inputStyle, { width: (this.state.showSearchView) ? '80%': '100%'}]}
            placeholder='Search'
            value={this.state.searchText}
            autoCapitalize={'none'}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            returnKeyType='search'
            placeholderTextColor='rgb(168,168,168)'
            onFocus={() => { this.setState({ showSearchView: true }) }}
            />
          <Icon active name='search' style={styles.searchIconStyle} />
          
          {(this.state.showSearchView) && <TouchableOpacity activeOpacity={0.8} style={styles.cancelButton} onPress={this.dismiss}>
            <Text style={[styles.cancelTextStyle, { color: Colors.blueTheme}]}>{strings('constants.cancel')}</Text>
          </TouchableOpacity>}
            <Text style={[styles.cancelTextStyle, { color: Colors.blueTheme}]}>Cancel</Text>
          </TouchableOpacity>} */}

          <Search
            ref="search_box"
            onChangeText={(text) => this.handleSearch(text)}
            backgroundColor='transparent'
            placeholderTextColor='rgb(168,168,168)'
            titleCancelColor={Colors.blueTheme}
            tintColorSearch={Colors.blueTheme}
            returnKeyType='search'
            onFocus={() => { this.setState({ showSearchView: true })}}
            onCancel={() => { this.setState({ showSearchView: false, searchText: '' }) }}
            searchIconCollapsedMargin={30}
            searchIconExpandedMargin={16}
            placeholderCollapsedMargin={10}
            placeholderExpandedMargin={30}
            keyboardDismissOnSubmit={true}
            inputHeight={35}
            autoCapitalize={'none'}
            inputStyle={styles.inputStyle}
            cancelButtonTextStyle={styles.cancelTextStyle}
            iconDelete={null}
            tintColorDelete='transparent'
            cancelTitle={strings('constants.cancel')}
            placeholder={strings('constants.search')}
            iconSearch={(<Icon active name='search' style={styles.searchIconStyle} />)}
          />

        </View>
        <ScrollableTabView
          scrollWithoutAnimation={false}
          initialPage={1}
          tabBarActiveTextColor={Colors.blueTheme}
          onChangeTab={(tabIndex, ref) => { if (tabIndex.i < 2) Keyboard.dismiss() }}
          tabBarInactiveTextColor={'rgb(168, 168, 168)'}
          tabBarUnderlineStyle={styles.tabBorder}
          tabBarBackgroundColor={'white'}
          tabBarTextStyle={styles.tabBarTextStyle}
          renderTabBar={() => <DefaultTabBar style={styles.defaultTabbar} />} >
            <Shake tabLabel={strings('constants.shake')} navigation={this.props.navigation} />
            <QrCode tabLabel={strings('constants.QRCode')} user={this.props} getUserByHandlename={this.props.getUserByHandlename} navigation={this.props.navigation} permissionStatus={this.props.permissionStatus} />
            <Invite tabLabel={strings('constants.share')} navigation={this.props.navigation} />
        </ScrollableTabView>
        {this.renderSearchView()}
        <Loader loading={this.state.loading} />
      </View>
      </SafeAreaView>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  name: state.login.name,
  handleName: state.login.handleName,
  profilePic: state.login.profilePic,
  myContacts: state.contacts.myContacts,
  friendRequestsCount: state.login.friendRequestsCount,
  permissionStatus: state.messages.permissionStatus
});

const dispatchToProps = (dispatch) => {
  return {
    getUserByHandlename: (handleName, requestorUserId) => dispatch(actions.getUserByHandlename(handleName, requestorUserId)),
    getFriendRequests: (data, fromGeneral) => dispatch(actions.getFriendRequests(data, fromGeneral)),
    getFriendRequestsFromNotif: (data, fromGeneral) => dispatch(actions.getFriendRequestsFromNotif(data, fromGeneral)),
    startFetching: (isFetching) => dispatch(actions.startFetching(isFetching)),
    getAllMessages: (messages) => dispatch(getAllMessages(messages)),
    getContactList: (userId, type, pageNumber) => dispatch(getContactList(userId, type, pageNumber)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(Home)
export default Container;
