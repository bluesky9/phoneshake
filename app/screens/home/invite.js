import React, { Component } from 'react'
import { Text, View, TextInput, Platform, Keyboard, TouchableOpacity, LayoutAnimation } from 'react-native';
import { Content, Item } from 'native-base';
import PhoneInput from 'react-native-phone-input';
import { TextInputMask } from 'react-native-masked-text'
import { connect } from 'react-redux';
import * as actions from '../contacts/contacts.actions';
import branch from 'react-native-branch'
import { Fonts, Metrics, Colors } from '../../themes/'
import styles from '../login/login.styles'
import PhoneShakeButton from '../../components/phoneShakeButton/'
import Loader from '../../components/loader/';
import { PROFILE_LINK } from '../../api/constants';
import { strings } from '../../locale';

class Invite extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });
  constructor() {
    super();
    this.state = {
      phoneNumber: '',
      isPhoneSelected: true,
      email: '',
      contryCodeItemWidth: Metrics.screenWidth * 0.213,
      keyboardHeight: 30, //(Platform.OS === 'ios') ? Metrics.screenHeight * 0.24 : 15,
      loading: false,
      isValidEmail: false
    };
    this.inviteUser = this.inviteUser.bind(this);
    this.inviteUserBy = this.inviteUserBy.bind(this);
    this.handlePhoneChange = this.handlePhoneChange.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.onSelectCountry = this.onSelectCountry.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener.remove()
      this.keyboardDidHideListener.remove()
    }
  }

  handlePhoneChange = (phone) => {
    this.setState({ phoneNumber: phone })
  }

  keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    // let newSize = e.endCoordinates.height
    // this.setState({
    //   keyboardHeight: newSize - Metrics.screenHeight * 0.04
    // })
  }

  keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  async inviteUserBy(type) {
    let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
      automaticallyListOnSpotlight: true,
      contentMetadata: {
        customMetadata: {
          handleName: this.props.handleName
        }
      },
      title: 'Phoneshake',
      contentDescription: `Phoneshake`
    })
    let linkProperties = {
      feature: 'share',
      channel: type,
      alias: `${this.props.handleName}`
      
    }
//key_test_bdD0GGrnAXPQ7HZNHZaeieinECj0lPOm
    let controlParams = {
      $uri_redirect_mode: 1,
      
    }
    this.setState({ loading: true })
    
    try{
      let { url } = await branchUniversalObject.generateShortUrl(linkProperties, controlParams)
      this.shareLink( url, type )
    }
    catch(e) {
      if(e && e.code.includes('DuplicateResourceError')){
        let url = `${PROFILE_LINK}${this.props.handleName}`
        this.shareLink( url, type )
      }
    }
    
  }

  shareLink( url, type ){
    if(type === 'email') {
      data = {
        "toEmail": this.state.email,
        "handleName": this.props.handleName,
        "link": url,
        "userFullName": this.props.name,
        "userId": this.props.userId
      }
    } else {
      let phoneNumber = this.phoneInput.getRawValue()
      let countryCode = this.phone.getCountryCode()
      let formatedPhoneNumber = "+" + countryCode + phoneNumber
      data = {
        "toPhoneNo": formatedPhoneNumber,
        "message": `Hi, ${this.props.name} would like to invite you on PhoneShake App. ${url}`,
        "userId": this.props.userId
      }
    }

    
    this.props.sendInvitation(data, type)
    .then((response) => {
      this.setState({ loading: false })
      if(response.payload.data && response.payload.data.status === 200) {
        this.setState({ email: '', phoneNumber: '', isValidEmail: false })
        setTimeout(() => {
          alert('Invitation sent successfully.')
        }, 300);
      } else if (response.payload.data && response.payload.data.status === 401) {
        this.props.navigation.navigate('ResetAccount')
      } else {
        setTimeout(() => {
          alert('Error while sending invitation. Please try again')
        }, 300);
      }
    })
  }

  inviteUser = () => {
    if(!this.state.isPhoneSelected) {
      this.inviteUserBy('email')
    } else {
      this.inviteUserBy('phone')
    }
  }

  async toggleButton() {
    this.setState({ isPhoneSelected: !this.state.isPhoneSelected, email: '', phoneNumber: '', contryCodeItemWidth: Metrics.screenWidth * 0.213 })
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onSelectCountry(iso2) {
    var countryCode = this.phone.getCountryCode()
    if (countryCode.length > 2) {
      this.setState({ contryCodeItemWidth: Metrics.screenWidth * 0.25 })
    } else {
      this.setState({ contryCodeItemWidth: Metrics.screenWidth * 0.213 })
    }
  }

  handleEmail(text) {
    let isValid = this.validateEmail(text)
    this.setState({ email: text, isValidEmail: isValid })
  }

  renderInput() {
    if (this.state.isPhoneSelected) {
      return (
        <Item style={styles.itemStyle}>
          <PhoneInput
            ref={(ref) => {
              this.phone = ref;
            }}
            textStyle={{ paddingBottom: (Platform.OS === 'ios') ? 0 : 3, color: Colors.blueTheme }}
            initialCountry='us' //us
            textProps={{
              editable: false,
              fontSize: Metrics.screenWidth * 0.048,
              fontFamily: Fonts.type.medium,
              marginTop: 3,
              height: 45
            }}
            offset={5}
            isoStyle={{ fontSize: Metrics.screenWidth * 0.048, fontFamily: Fonts.type.SFTextRegular, color: Colors.blueTheme }}
            pickerButtonColor='rgb(70,70,70)'
            pickerButtonTextStyle={{ fontSize: 16, fontFamily: Fonts.type.SFTextRegular }}
            style={{ paddingLeft: 0, width: this.state.contryCodeItemWidth, paddingTop: -1 }}
            onSelectCountry={this.onSelectCountry}
          />

          <TextInputMask
            ref={(ref) => this.phoneInput = ref}
            placeholder={strings('home.mobileNumber')}
            placeholderTextColor={'rgba(0, 0, 0, 0.5)'}
            maxLength={16}
            autoFocus
            underlineColorAndroid='transparent'
            keyboardType='numeric'
            value={this.state.phoneNumber}
            type={'cel-phone'}
            options={{
              dddMask: '999 - 999 - 9999',
            }}
            style={[styles.phoneTextInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10 }]}
            onChangeText={(text) => this.handlePhoneChange(text)} />
        </Item>
      );
    } else {
      return (
        <Item style={styles.itemStyle}>
          <TextInput
            ref={(ref) => this.emailInput = ref}
            style={[styles.phoneTextInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10, width: '100%', paddingLeft: 0, textAlign: 'center' }]}
            placeholder={strings('constants.emailAddress')}
            value={this.state.email.trim()}
            keyboardType='email-address'
            placeholderTextColor={'rgba(0, 0, 0, 0.5)'}
            returnKeyType={'go'}
            autoFocus
            autoCorrect={false}
            underlineColorAndroid='transparent'
            autoCapitalize={'none'}
            enablesReturnKeyAutomatically
            onSubmitEditing={this.submitEmail}
            onChangeText={(text) => this.handleEmail(text)}
          />
        </Item>
      );
    }
  }

  render() {
    let disabled = (this.state.isPhoneSelected) ? this.state.phoneNumber.length < 15 : !this.state.isValidEmail
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <Loader loading={this.state.loading} />
        <Content contentContainerStyle={[styles.container, { paddingTop: Metrics.screenHeight * 0.08 }]} bounces={false} scrollEnabled={false}>
          <Text style={styles.topTitle}>
            {strings('home.inviteHeading')}
          </Text>
          <TouchableOpacity style={styles.linkButton} onPress={() => this.toggleButton()} activeOpacity={0.5}>
            <Text style={styles.link}>
              {(this.state.isPhoneSelected) ? strings('home.useEmail') : strings('home.usePhone')}
            </Text>
          </TouchableOpacity>
          {this.renderInput()}
          <Text style={styles.infoText}>
            {(this.state.isPhoneSelected) ? strings('home.willSendSMS'): strings('home.willSendEmail')}
          </Text>
          <PhoneShakeButton
            disabled={disabled}
            onPress={this.inviteUser}
            text={strings('home.shareMyContact')}
            bottomHeight={this.state.keyboardHeight} />

        </Content>
      </View>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  name: state.login.name,
  handleName: state.login.handleName,
});

const dispatchToProps = (dispatch) => {
  return {
    sendInvitation: (data, type) => dispatch(actions.sendInvitation(data, type)),
  }
}
const Container = connect(stateToProps, dispatchToProps)(Invite)
export default Container;
