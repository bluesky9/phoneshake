import React, { Component } from 'react'
import { View, TouchableOpacity, Animated, Alert, Platform, CameraRoll, AsyncStorage, PermissionsAndroid } from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import branch from 'react-native-branch'
import { RNCamera } from 'react-native-camera';
import Permissions from 'react-native-permissions'
import Share from 'react-native-share';
import { Text } from 'native-base';
import RNFS from "react-native-fs"
import styles from './home.styles'
import { Images, Metrics, Colors } from '../../themes/'
import Loader from "../../components/loader"
import { PROFILE_LINK } from '../../api/constants';
import { strings } from '../../locale';

class Contact extends Component {
  constructor() {
    super();
    this.state = {
      activePane: "qrcode",
      cameraPermission: 'denied',
      loading: false,
      userId: null,
      error: null,
      localQrCodePath: ''
    };
    this.togglePane = this.togglePane.bind(this);
    this.barCodeRead = this.barCodeRead.bind(this);
    this.checkCameraPermission = this.checkCameraPermission.bind(this);
    this.getPermission = this.getPermission.bind(this);
    this.saveQRtoGallery = this.saveQRtoGallery.bind(this);
    this.checkStorageWritePermission = this.checkStorageWritePermission.bind(this);
  }

  async componentDidMount() {
    this.mounted = true;
    let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
      automaticallyListOnSpotlight: true,
      contentMetadata: {
        customMetadata: {
          handleName: this.props.user.handleName
        }
      },
      title: 'Phoneshake',
      contentDescription: `Phoneshake`
    })
    let linkProperties = {
      feature: 'share',
      channel: "ScanQRCode",
      alias: `${this.props.user.handleName}`
      
    }
    try {
      await branchUniversalObject.generateShortUrl(linkProperties)
    }
    catch(e) {
      console.log('e', e)
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentWillMount() {
    if(Platform.OS === 'ios') {
      this.checkCameraPermission()
    }
    // AsyncStorage.removeItem('QRCode')
  }

  componentWillReceiveProps(newProps) {
    if (newProps.permissionStatus === 'authorized' && this.mounted) {
      this.setState({ cameraPermission: 'authorized'})
    }
    if (newProps.permissionStatus === 'denied' && this.mounted) {
      this.setState({ cameraPermission: 'denied' })
    }
  }

  checkCameraPermission() {
    Permissions.check('camera').then(response => {
      
      if (response === 'authorized' && this.mounted) {
        this.setState({ cameraPermission: 'authorized' })
      } else if (response === 'denied' && this.mounted) {
        this.showAlert()
        this.setState({ cameraPermission: 'denied' })
      } else {
        this.getPermission()
        if (this.mounted) {
          this.setState({ cameraPermission: '' })
        }
      }
    })
  }

  async checkStorageWritePermission() {
    try {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      console.log('granted is : ', granted);
      
      if (granted) {
        this.saveQRtoGallery().done()
      } else {
        let msg = strings('home.storagePermissionPathAlert')
        Alert.alert(
          strings('home.storagePermissionAlert'),
          msg,
          [
            { text: strings('constants.ok'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: strings('constants.openSetting'), onPress: () => Permissions.openSettings() }
          ],
          { cancelable: false }
        )
      }
    } catch (err) {
      console.log(err)
    }
  }

  saveQRtoGallery = async() => {
    this.setState({ activePane: "qrcode" })
      let isQrExists = await RNFS.exists(RNFS.CachesDirectoryPath + `/PhoneshakeQrCode${this.props.user.handleName}.png`)
      if (!isQrExists) {
        this.svg.toDataURL(async(data) => {
          await RNFS.writeFile(RNFS.CachesDirectoryPath + `/PhoneshakeQrCode${this.props.user.handleName}.png`, data, 'base64')
            .then((success) => {
              return CameraRoll.saveToCameraRoll(RNFS.CachesDirectoryPath + `/PhoneshakeQrCode${this.props.user.handleName}.png`, 'photo')
            })
            .then((imageContent) => {
              if(Platform.OS === 'ios') {
                AsyncStorage.setItem('QRCode', JSON.stringify(imageContent))
                this.openShareSheet(imageContent)
              } else {
                RNFS.readFile(imageContent, 'base64')
                .then((imageBase64) => {
                  AsyncStorage.setItem('QRCode', JSON.stringify(imageBase64))
                  this.openShareSheet(imageBase64)
                })
              }
            })
            .catch((err) => { err && console.log('Error save: ',err); });
        })
      } else {
        let value = await AsyncStorage.getItem('QRCode')
        if (value !== null) {
          let imageBase64 = JSON.parse(value)
          this.openShareSheet(imageBase64)
        }
      }
  }

  openShareSheet(imageBase64) {
    let shareOptions = {}
    if(Platform.OS === 'ios') {
      shareOptions = {
        title: this.props.user.name,
        // message: 'My Phoneshake QR code',
        url: imageBase64
      }
    } else {
      shareOptions = {
        title: this.props.user.name,
        // message: 'My Phoneshake QR code',
        url: 'data:image/png;base64,' + imageBase64
      }
    }

    Share.open(shareOptions)
    .then((res) => { console.log(res) })
    .catch((err) => { err && console.log(err); });  
  }

  showAlert() {
    if(Platform.OS === 'ios') {
      Alert.alert(
        strings('home.cameraPermissionAlert'),
        strings('home.cameraPermissionPathAlert'),
        [
          { text:  strings('constants.ok'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text:  strings('constants.openSetting'), onPress: () => Permissions.openSettings() }
        ],
        { cancelable: false }
      )
    }
  }

  getPermission() {
    Permissions.check('camera').then(response => {
      if (response === 'denied') {
          this.showAlert()
      } else if (response != 'authorized') {
        this.askForPermission()
      }
    })
  }

  askForPermission() {
      Permissions.request('camera').then(userResponse => {
        console.log('Permission: ', userResponse);
        if (userResponse === 'authorized') {
          this.setState({ cameraPermission: 'authorized' })
        }
      })
  }

  togglePane(){
    if(this.state.activePane === 'qrcode'){
      if(this.state.cameraPermission === 'denied') {
        this.showAlert()
      } else {
        this.setState({ activePane: 'camera', userId: null, error: null })
      }
    } else {
      this.setState({activePane: 'qrcode', userId: null, error: null})
    }
  }

  barCodeRead(qrCode) {
    if(qrCode && this.state.loading === false && this.state.userId === null){
      this.setState({loading: true, error: null});
      let handleName  = qrCode.data.substr(qrCode.data.lastIndexOf('/') + 1);
      let profileUrl  = qrCode.data.split('@')[0];
      
      console.log('handleName: ', handleName);
      console.log('profileUrl: ', profileUrl);
      
      if(profileUrl === PROFILE_LINK && handleName !== this.props.user.handleName){
        this.props.getUserByHandlename(handleName, this.props.user.userId)
        .then((response) => {
          if (response && response.payload.data && response.payload.data.content && response.payload.data.content.user_id){
            let user = response.payload.data.content;
            console.log('user: ', user);
            
            let udpatedUsernameArray = [];
            if(user.usernames.length){
              user.usernames.map((username) => {
                udpatedUsernameArray.push(username.username);
              });
            }
            user.usernames = udpatedUsernameArray;
            user.username = user.usernames[0];
            user.handleName = handleName;
            let isFriend = this.props.user.myContacts.filter((item) => {
              let matched = item.usernames.filter((contactUsernames) => {
                return user.usernames.indexOf(contactUsernames.username) !== -1
              })
              return matched.length
            });
            user.isFriend = (isFriend.length) ? true : false;
            this.setState({loading: false, userId: user.id, error: null});
            this.props.navigation.navigate('UserDetailScreen', { user, isAllInfo: true })
          } else if (response && response.payload.data && response.payload.data.status === 401) {
            this.props.navigation.navigate('ResetAccount')
          } else {
            this.setState({loading: false, userId: null, error: null});
          }
        });
      } else if(profileUrl !== PROFILE_LINK && profileUrl !== 'https://phoneshake.me/'){ //Old url 
        this.setState({loading: false, error: strings('home.notScanningError')});
      } else if(handleName === this.props.user.handleName){
        this.setState({loading: false, error: strings('home.itsYou')});
      } else {
        this.setState({loading: false, error: strings('constants.errorMessage')});
      }
    }
  }

  renderActivePanel(){
    const { handleName, profilePic } = this.props.user;
    let userImage = (profilePic) ? {uri: profilePic} : Images.defaultUserImage;
    if(this.state.activePane === 'qrcode'){
      return (
        <Animated.View style={[styles.activeQrPanelOuter]}>
          <QRCode
          value={`${PROFILE_LINK}${handleName}`}
          logo={userImage}
          logoSize={40}
          getRef={(c) => (this.svg = c)}
          ecl={'H'}
          logoMargin={0}
          margin={20}
          logoBorderRadius={20}
          //backgroundColor={Colors.blueTheme}
          logoBackgroundColor="rgb(32, 142, 251)"
          color={Colors.blueTheme}
            size={Metrics.screenWidth * 0.53}
           />
        </Animated.View>
      )
    }
    return (
      <Animated.View style={[styles.activeQrPanelOuter, {padding: 0}]}>
        <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style = {{flex: 1}}
            notAuthorizedView={<View style={[{ flex: 1, padding: 0, backgroundColor: 'rgba(25, 25, 25, 0.2)' }]} />}
            barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
            onBarCodeRead={this.barCodeRead}
            type={RNCamera.Constants.Type.back}
            permissionDialogTitle={strings('home.cameraPermissionDialogue')}
            permissionDialogMessage={strings('home.cameraPermissionDialogue')}
        />
      </Animated.View>
    )
  }

  renderInactivePanel(){
    const { handleName, profilePic } = this.props.user;
    let userImage = (profilePic) ? {uri: profilePic} : Images.defaultUserImage;
    if(this.state.activePane === 'camera'){
      return (
        <TouchableOpacity activeOpacity={0.6} style={styles.inactiveQrPanelOuter} onPress={this.togglePane}>
          <QRCode
          value={`${PROFILE_LINK}${handleName}`}
          logo={userImage}
          logoSize={13}
          logoMargin={0}
          margin={0}
          logoBorderRadius={13}
          // backgroundColor="rgb(32, 142, 251)"
          logoBackgroundColor="rgb(32, 142, 251)"
          color={Colors.blueTheme}
          size={Metrics.screenWidth * 0.144}
           />
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity activeOpacity={0.6} onPress={this.togglePane} style={[styles.inactiveQrPanelOuter, { padding: 0, backgroundColor: 'transparent'}]}>
        { 
          (Platform.OS === 'ios') ?
            (this.state.cameraPermission === 'authorized') ?
              <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style={{ flex: 1 }}
                //notAuthorizedView={() => <View style={[{ flex: 1, padding: 0, backgroundColor: 'rgba(25, 25, 25, 0.2)' }]} />}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
              />
              : <View style={[{ flex: 1, padding: 0, backgroundColor: 'rgba(25, 25, 25, 0.2)' }]} />
            : (this.state.cameraPermission === 'authorized') ? 
              <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style={{ flex: 1 }}
                notAuthorizedView={<View style={[{ flex: 1, padding: 0, backgroundColor: 'rgba(25, 25, 25, 0.2)' }]} />}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                permissionDialogTitle={strings('home.cameraPermissionDialogue')}
                permissionDialogMessage={strings('home.cameraPermissionDialogue')}
              />
              : <View style={[{ flex: 1, padding: 0, backgroundColor: 'rgba(25, 25, 25, 0.2)' }]} />
        }
        <View style={styles.overlay} />
      </TouchableOpacity>
    )
  }

  render() {
    const { handleName, name} = this.props.user;
    return (
      <View style={styles.qrcodeContainer}>
        <Loader loading={this.state.loading} />
        {this.renderActivePanel()}
        <Text style={styles.error}>{this.state.error}</Text>
        <Text style={styles.profileName}>{name}</Text>
        <Text style={styles.handleName}>{handleName}</Text>
        {this.renderInactivePanel()}
        <TouchableOpacity activeOpacity={0.7} onPress={(Platform.OS === 'ios') ? this.saveQRtoGallery : this.checkStorageWritePermission} style={{height: 50, width: 150, alignItems:'center', justifyContent: 'center'}}>
          <Text style={styles.shareText}>{strings('home.shareQRCode')}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Contact;
