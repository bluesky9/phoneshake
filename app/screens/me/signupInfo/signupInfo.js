import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, AsyncStorage, Platform } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../login/login.actions'
import _ from 'lodash'
import SendBird from 'sendbird';
import Modal from "react-native-modal";
import SettingsList from 'react-native-settings-list';
import styles from '../linkedAccount/linkedAccount.styles';
import modalStyle from './signupInfo.styles';
import Loader from '../../../components/loader/'
import { Fonts, Metrics, Images } from '../../../themes';
import { Button } from 'native-base';
import { strings } from '../../../locale';
const sb = SendBird.getInstance();

class SignupInfo extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={styles.headerLeft} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#fff'
    },
    headerTitleStyle: {
      fontFamily: Fonts.type.SFTextRegular,
      fontWeight: "200",
      fontSize: Metrics.screenWidth * 0.053,
      alignSelf: 'center',
      textAlign: 'center',
      width: '70%'
    },
    title: strings('meTab.signupInfo')
  });

  constructor() {
    super();
    this.state = {
      loading: false,
      modalVisible: false,
      removeAccountModalVisible: false,
      selectedAccount: null
    };
    this.gotoEditContactInfo = this.gotoEditContactInfo.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
    this.deleteAccount = this.deleteAccount.bind(this);
    this.removeAccount = this.removeAccount.bind(this);
    this.renderRemoveAccountModal = this.renderRemoveAccountModal.bind(this);
  }

  gotoEditContactInfo = (options) => () => {
    this.props.navigation.navigate('EditContactInfo', options)
  }

  setModalVisible = (visible) => () => {
    this.setState({ modalVisible: visible })
  }

  removeAccount() {
    let _this = this;
    this.setState({ removeAccountModalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      this.props.removeUsername(this.props.user.userId, this.state.selectedAccount.username)
      .then((response) => {
        console.log('response remove:', response);
        
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          this.updateUserLocal(_this.state.selectedAccount.username).done();
        } else if (response.payload.data && response.payload.data.status !== 200) {
          setTimeout(() => {
            alert(strings('meTab.signupInfo_removingAccountError'))
          }, 300);
        }
      })
    }, 500);
  }

  async updateUserLocal(usernameRemoved) {
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      let usrnms = asyncData.data.content.usernames.filter((item) => { return item.username !== usernameRemoved })
      
      asyncData.data.content.usernames = usrnms
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  deleteAccount() {
    this.setState({ modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      this.props.deleteAccount(this.props.user.userId)
      .then((response) => {
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          AsyncStorage.removeItem('MyContacts')
          AsyncStorage.removeItem('Messages')
          AsyncStorage.multiRemove(['PhoneShakeUser', 'SyncedContact', 'MyContacts', 'PhonebookContacts', 'Messages'])
          this.logoutSendBird()
          this.props.screenProps.logout()      
        } else {
          setTimeout(() => {
            alert(strings('meTab.signupInfo_detectingAccountError'))
          }, 300);
        }
      })
    }, 500);
  }

  async logoutSendBird() {
    var value = await AsyncStorage.getItem('SendBirdToken')

    if (value !== null) {
      let apnsToken = JSON.parse(value);
      if (Platform.OS === 'ios') {
        sb.unregisterAPNSPushTokenForCurrentUser(apnsToken, function (response, error) {
          if (error) {
            console.error(error);
            return;
          }
          
          sb.disconnect(function () {
            
          });
        });
      } else {
        sb.unregisterGCMPushTokenForCurrentUser(apnsToken, function (response, error) {
          if (error) {
            console.error(error);
            return;
          }
          
          sb.disconnect(function () {

          });
        });
      }
    } else {

    }
  }

  renderRemoveAccountModal() {
    let usernames = (this.props.user && this.props.user.usernames) ? this.props.user.usernames[0] : []
    let title = (usernames.type === 'EMAIL') ? strings('meTab.signupInfo_removeEmail') : strings('meTab.signupInfo_removeNumber')
    let description = strings('meTab.signupInfo_description',{data:`\n${(this.state.selectedAccount && this.state.selectedAccount.username) ? this.state.selectedAccount.username : ''}?`})
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.removeAccountModalVisible}
      >
        <View style={modalStyle.modalContainer}>
          <View style={[modalStyle.modalContent, { padding: 0, height: Metrics.screenHeight * 0.28 }]}>
            <View style={modalStyle.titleView}>
              <Text style={modalStyle.alertTitle}>{title}</Text>
              <Text style={modalStyle.alertDescription}>{description}</Text>
            </View>
            <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.removeAccount}>
              <Text style={modalStyle.distructiveText}>{strings('constants.remove')}</Text>
            </Button>
            <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ removeAccountModalVisible: false })}>
              <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
            </Button>
          </View>
        </View>
      </Modal>
    )
  }

  renderDeleteModal() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.modalVisible}
      >
        <View style={modalStyle.modalContainer}>
          <View style={[modalStyle.modalContent, { padding: 0, height: Metrics.screenHeight * 0.27 }]}>
        
            <View style={modalStyle.titleView}>
              <Text style={modalStyle.alertTitle}>{strings('meTab.signupInfo_deleteAccount')}</Text>
              <Text style={modalStyle.alertDescription}>{strings('meTab.signupInfo_deleteTitle')}</Text>
            </View>
            <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.deleteAccount}>
            <Text style={modalStyle.distructiveText}>{strings('constants.delete')}</Text>
            </Button>
            <Button block transparent style={modalStyle.modalCancelButton} onPress={this.setModalVisible(false)}>
              <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
            </Button>
        
        </View>
      </View>
      </Modal>
    )
  }

  render() {
    const {isRTL} = this.props.user;
    styles.titleStyle = [styles.titleStyle, {textAlign: (isRTL) ? 'right' : 'left'}]
    styles.titleInfoStyle = [styles.titleInfoStyle, {textAlign: (isRTL) ? 'left' : 'right'}]
    styles.titleStyleSecond = [styles.titleStyleSecond, {textAlign: (isRTL) ? 'right' : 'left'}]
    var userAccounts = [];
    this.props.user.usernames.map((account, index) => {
      if (account && account.type) {
        userAccounts.push(
          <SettingsList.Item
            isRTL={isRTL}
            key={index.toString()}
            title={(account.type === 'PHONE') ? strings('constants.mobileNumber') : strings('constants.email')}
            titleInfo={account.username}
            titleInfoStyle={styles.titleInfoStyle}
            titleStyle={styles.titleStyleSecond}
            onPress={() => {
              this.setState({ selectedAccount: account, removeAccountModalVisible: true })
            }}
          />
        )
      }
    })
    
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <SettingsList borderColor='#fff' defaultItemSize={47} defaultTitleStyle={styles.title} >
            {userAccounts}
            <SettingsList.Item
              isRTL={isRTL}
              title={strings('meTab.signupInfo_addEmailOrMobile')}
              titleStyle={[styles.titleStyleSecond]}
              titleInfo=''
              onPress={() => {
                this.props.navigation.navigate('AddSignupInfo')
              }}
            />
            <SettingsList.Item
              isRTL={isRTL}
              title={strings('meTab.signupInfo_deleteAccount')}
              titleStyle={styles.titleStyleSecond}
              titleInfo=''
              onPress={() => {
                this.setState({ modalVisible: true })
              }}
            />
          </SettingsList>
        </View>
        {this.renderDeleteModal()}
        {(this.props.user.usernames.length > 1) ? this.renderRemoveAccountModal() : null}
        <Loader loading={this.state.loading} />
      </View>
    );
  }
}

const stateToProps = (state) => ({
  user: state.login
});

const dispatchToProps = (dispatch) => {
  return {
    deleteAccount: (userId) => dispatch(actions.deleteAccount(userId)),
    removeUsername: (userId, username) => dispatch(actions.removeUsername(userId, username))
  }
}

const Container = connect(stateToProps, dispatchToProps)(SignupInfo)
export default Container;


