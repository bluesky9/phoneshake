import { StyleSheet, Platform } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../themes'
export default StyleSheet.create({
  modal: {
    position: "absolute",
    width: Metrics.screenWidth,
    height: null,
    top: 0,
    paddingBottom: 0,
    zIndex: 111111
  },
  modalContentBox: {
    // paddingTop: Metrics.screenHeight * 0.022,
    // height: null,
    justifyContent: 'flex-end'
  },
  alertTitle: {
    fontFamily: Fonts.type.SFTextMedium,
    fontSize: Metrics.screenWidth * 0.037,
    textAlign: 'center',
    color: 'rgb(168, 168, 168)',
    marginBottom: Metrics.screenHeight * 0.014,
  },
  alertDescription: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.037,
    textAlign: 'center',
    color: 'rgb(168, 168, 168)',
  },
  modalRemoveButton: {
    height: Metrics.screenHeight * 0.074,
    borderRadius: 0,
  },
  modalCancelButton: {
    height: Metrics.screenHeight * 0.074,
    borderRadius: 0,
    borderTopWidth: 0.5,
    borderTopColor: 'rgb(204, 204, 204)'
  },
  titleView: {
    height: Metrics.screenHeight * 0.123,
    justifyContent: 'center',
    paddingHorizontal: Metrics.screenWidth * 0.04,
    // paddingVertical: Metrics.screenHeight * 0.029
  },
  distructiveText: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.042,
    color: Colors.red
  },
  regularText: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.042,
    color: Colors.blueTheme
  },
  cancelText: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.042,
    color: 'rgb(168, 168, 168)'
  },
  alertUsername: {
    fontFamily: Fonts.type.SFTextSemibold,
    fontSize: Metrics.screenWidth * 0.042,
    color: '#000',
    textAlign: 'center'
  },
  alertUserImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: (Metrics.screenWidth * 0.12) / 2,
    marginBottom: 5
  },
  bottomButtonView: {
    flexDirection: 'row',
    height: Metrics.screenHeight * 0.082,
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingLeft: Metrics.screenWidth * 0.032
  },
  noteInputView: {
    height: Metrics.screenHeight * 0.242,
    padding: 16
  },
  itemStyle: {
    width: '100%',
    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 15,
    height: 109
  },
  textInput: {
    width: '100%',
    paddingLeft: 0,
    fontSize: Metrics.screenWidth * 0.048,
    fontFamily: Fonts.type.SFTextRegular,
    paddingLeft: 0,
    textAlign: 'center',
    textAlignVertical: 'top',
    color: '#000'
  },
  multiline: {
    height: Metrics.screenHeight * 0.135,
    fontSize: Metrics.screenWidth * 0.037,
    textAlign: 'left'
  },
  addNoteCancelButton: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    height: 35,
    marginRight: Metrics.screenWidth * 0.032,
    borderRadius: 5,
    backgroundColor: 'rgb(247, 247, 247)'
  },
  addNoteCancelText: {
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextMedium,
    color: Colors.blueTheme
  },
  addNoteSaveButton: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    height: 35,
    borderRadius: 5,
    marginRight: Metrics.screenWidth * 0.032,
    backgroundColor: Colors.blueTheme
  },
  addNoteSaveText: {
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextMedium,
    color: '#fff'
  },
  infoText: {
    marginTop: 7,
    fontSize: Metrics.screenWidth * 0.032,
    textAlign: 'right',
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgba(0, 0, 0, 0.34)',
    width: '100%'
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});
