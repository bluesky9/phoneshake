import React, { Component } from 'react'
import { Text, View, Image, TextInput, Platform, Keyboard, TouchableOpacity, LayoutAnimation } from 'react-native';
import { Content, Item } from 'native-base';
import PhoneInput from 'react-native-phone-input';
import { TextInputMask } from 'react-native-masked-text'
import { connect } from 'react-redux';
import * as actions from '../../login/login.actions';
import { Fonts, Metrics, Colors, Images } from '../../../themes/'
import styles from '../../login/login.styles'
import PhoneShakeButton from '../../../components/phoneShakeButton/'
import Loader from '../../../components/loader/';
import { strings } from '../../../locale/'

class AddSignupInfo extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={{ height: 24, width: 24, marginLeft: 12}} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  });
  constructor() {
    super();
    this.state = {
      phoneNumber: '',
      isPhoneSelected: false,
      email: '',
      contryCodeItemWidth: Metrics.screenWidth * 0.213,
      keyboardHeight: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.35 : 15,
      loading: false,
      isValidEmail: false
    };
    this.submitEmail = this.submitEmail.bind(this);
    this.submitPhoneNumber = this.submitPhoneNumber.bind(this);
    this.handlePhoneChange = this.handlePhoneChange.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.onSelectCountry = this.onSelectCountry.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
    this.generateOTP = this.generateOTP.bind(this);
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener.remove()
      this.keyboardDidHideListener.remove()
    }
  }

  generateOTP = () => {
    return otp = Math.floor(Math.random() * 899999 + 100000);
  }

  handlePhoneChange = (phone) => {
    this.setState({ phoneNumber: phone })
  }

  keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = e.endCoordinates.height
    this.setState({
      keyboardHeight: newSize + Metrics.screenHeight * 0.022
    })
  }

  keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  submitPhoneNumber() {
    // this.setState({ loading: true })
    Keyboard.dismiss();
    let phoneNumber = this.phoneInput.getRawValue()
    let countryCode = this.phone.getCountryCode()
    let formatedPhoneNumber = "+" + countryCode + phoneNumber
    const userInfo = {
      "username": formatedPhoneNumber,
      "organization": '',
      "designation": '',
      "type": 'PHONE'
    }

    this.props.navigation.navigate('Verification', { keyboardHeight: this.state.keyboardHeight, userInfo, isFromNewSignupInfo: true })
  }

  async toggleButton() {
    this.setState({ isPhoneSelected: !this.state.isPhoneSelected, email: '', phoneNumber: '', contryCodeItemWidth: Metrics.screenWidth * 0.213 })
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  submitEmail() {
    Keyboard.dismiss();
    let isValid = this.validateEmail(this.state.email)
    if (isValid) {
      const userInfo = {
        "username": this.state.email,
        "organization": '',
        "designation": '',
        "type": 'EMAIL'
      }
      this.props.navigation.navigate('Verification', { keyboardHeight: this.state.keyboardHeight, userInfo, isFromNewSignupInfo: true })
    } else {
      this.setState({ loading: false })
      setTimeout(() => {
        alert(strings('login.invalidMail'))
      }, 300)
    }
  }

  onSelectCountry(iso2) {
    var countryCode = this.phone.getCountryCode()
    if (countryCode.length > 2) {
      this.setState({ contryCodeItemWidth: Metrics.screenWidth * 0.25 })
    } else {
      this.setState({ contryCodeItemWidth: Metrics.screenWidth * 0.213 })
    }
  }

  handleEmail(text) {
    let isValid = this.validateEmail(text)
    this.setState({ email: text, isValidEmail: isValid })
  }

  renderInput() {
    if (this.state.isPhoneSelected) {
      return (
        <Item style={styles.itemStyle}>
          <PhoneInput
            ref={(ref) => {
              this.phone = ref;
            }}
            textStyle={{ paddingBottom: (Platform.OS === 'ios') ? 0 : 3, color: Colors.blueTheme }}
            initialCountry='us' //us
            textProps={{
              editable: false,
              fontSize: Metrics.screenWidth * 0.048,
              fontFamily: Fonts.type.medium,
              marginTop: 3,
              height: 45
            }}
            offset={5}
            isoStyle={{ fontSize: Metrics.screenWidth * 0.048, fontFamily: Fonts.type.SFTextRegular, color: Colors.blueTheme }}
            pickerButtonColor='rgb(70,70,70)'
            pickerButtonTextStyle={{ fontSize: 16, fontFamily: Fonts.type.SFTextRegular }}
            style={{ paddingLeft: 0, width: this.state.contryCodeItemWidth, paddingTop: -1 }}
            onSelectCountry={this.onSelectCountry}
          />

          <TextInputMask
            ref={(ref) => this.phoneInput = ref}
            placeholder={strings('constants.mobileNumber')}
            placeholderTextColor={'rgba(0, 0, 0, 0.5)'}
            maxLength={16}
            autoFocus
            underlineColorAndroid='transparent'
            keyboardType='numeric'
            value={this.state.phoneNumber}
            type={'cel-phone'}
            options={{
              dddMask: '999 - 999 - 9999',
            }}
            style={[styles.phoneTextInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10 }]}
            onChangeText={(text) => this.handlePhoneChange(text)} />
        </Item>
      );
    } else {
      return (
        <Item style={styles.itemStyle}>
          <TextInput
            ref={(ref) => this.emailInput = ref}
            style={[styles.phoneTextInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10, width: '100%', paddingLeft: 0, textAlign: 'center' }]}
            placeholder={strings('constants.emailAddress')}
            value={this.state.email.trim()}
            keyboardType='email-address'
            placeholderTextColor={'rgba(0, 0, 0, 0.5)'}
            returnKeyType={'go'}
            autoFocus
            autoCorrect={false}
            underlineColorAndroid='transparent'
            autoCapitalize={'none'}
            enablesReturnKeyAutomatically
            onSubmitEditing={this.submitEmail}
            onChangeText={(text) => this.handleEmail(text)}
          />
        </Item>
      );
    }
  }

  render() {
    let disabled = (this.state.isPhoneSelected) ? this.state.phoneNumber.length < 15 : !this.state.isValidEmail
    let verificationType = (this.state.isPhoneSelected) ? strings('login.verificationTypeSms') : strings('login.verificationTypeEmail')
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <Loader loading={this.state.loading} />
        <Content contentContainerStyle={styles.container} bounces={false} scrollEnabled={false}>
          <Text style={styles.topTitle}>
            {(this.state.isPhoneSelected) ? strings('login.whatsNumber') : strings('login.whatsEmail')}
          </Text>
          <TouchableOpacity style={styles.linkButton} onPress={() => this.toggleButton()} activeOpacity={0.5}>
            <Text style={styles.link}>
              {(this.state.isPhoneSelected) ? strings('login.linkUseEmail') : strings('login.linkUsePhone')}
            </Text>
          </TouchableOpacity>

          {this.renderInput()}

          <Text style={styles.infoText}>
            {strings('login.infoText', {name: `${verificationType}`})}
          </Text>

          <PhoneShakeButton
            disabled={disabled}
            onPress={(this.state.isPhoneSelected) ? this.submitPhoneNumber : this.submitEmail}
            text={strings('constants.continue')}
            bottomHeight={this.state.keyboardHeight} />

        </Content>
      </View>
    );
  }
}

const stateToProps = (state) => ({

});

const dispatchToProps = (dispatch) => {
  return {
    sendConfirmationCode: (email, confirmationCode) => dispatch(actions.sendConfirmationCode(email, confirmationCode)),
    sendConfirmationCodeBySMS: (phone, confirmationCode) => dispatch(actions.sendConfirmationCodeBySMS(phone, confirmationCode))
  }
}
const Container = connect(stateToProps, dispatchToProps)(AddSignupInfo)
export default Container;