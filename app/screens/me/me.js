import React, { Component } from 'react'
import { View } from 'react-native';
import { SafeAreaView } from "react-navigation";
import { connect } from 'react-redux';
import styles from './me.styles'
import Profile from "../../components/userProfile/userProfile";
import LocationModal from '../contacts/locationModal/locationModal'

class Me extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
  });

  constructor() {
    super();
    this.state = {
      showLocationOptions: false
    }    
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.user !== this.props.user || nextState.showLocationOptions !== this.state.showLocationOptions) {
      return true
    } else {
      return false
    }
  }

  renderLocationOptions() {
    return (
      <LocationModal
        key={'location'}
        visible={this.state.showLocationOptions}
        setModalVisible={(value) => this.setState({ showLocationOptions: false })}
        user={this.props.user} />
    )
  }

  render() {
    const { user } = this.props
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'rgb(247, 247, 247)'}}>
        <View style={styles.container}>
          <Profile navigation={this.props.navigation} user={user} me={true} openLocation={() => this.setState({ showLocationOptions: true })} />
          {(user && user.address) ? this.renderLocationOptions() : null}
        </View>
      </SafeAreaView>
    );
  }
}

const stateToProps = (state) => ({
  user: state.login
});

const Container = connect(stateToProps, null)(Me)
export default Container;