import React, { Component } from 'react';
import { View, Text, Image, FlatList, Modal as RNModal, RefreshControl, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-navigation'
import { Button, ListItem, Left, Right, Body } from "native-base";
import { connect } from 'react-redux';
import SendBird from 'sendbird';
import Modal from "react-native-modal";
import * as actions from '../../contacts/contacts.actions.js';
import styles from './settings.styles';
import Loader from '../../../components/loader/'
import { Images, Colors } from '../../../themes/index.js';
import { SENDBIRD_APPID } from '../../../api/constants.js';
import { strings } from '../../../locale/'

class BlockedList extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      isEmptyBlockList: false,
      refreshing: false,
      selectedUser: {}
    };
    this.getBlockedContactList = this.getBlockedContactList.bind(this);
    this._onRefresh = this._onRefresh.bind(this);
    this.onShow = this.onShow.bind(this);
    this.unblockUser = this.unblockUser.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.showModal = this.showModal.bind(this);
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.getBlockedContactList()
  }

  onShow() {
    this.getBlockedContactList()
  }

  getBlockedContactList() {
    this.props.getBlockedContactList(this.props.userId, 'I', 1)
    .then((response) => {
      this.setState({ loading: false, refreshing: false })
      if (response.payload.data && response.payload.data.status === 200) {

      } if (response.payload.data && response.payload.data.status === 404) {

      } else if (response.payload.data && response.payload.data.status === 401) {
        this.props.closeModal()
        this.props.navigation.navigate('ResetAccount')
      } else {
        this.setState({ isEmptyBlockList: true })
        setTimeout(() => {
          alert(strings('meTab.settings_blocErrorFetchingAlert'))
        }, 300);
      }
    })
  }

  showModal = (user) => () => {
    this.setState({ unblockModalVisible: true, selectedUser: user })
  }

  unblockUser = () => {
    this.setState({ unblockModalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": this.state.selectedUser.id,
      "isBlocked": false
      }
      this.props.contactBlockUnblock(data)
      .then((response) => {
        this.setState({ loading: false })
        if(response.payload.data && response.payload.data.status !== 200) {
          setTimeout(() => {
            alert(strings('meTab.settings_blockUnblockErrorAlert'))
          }, 300);
        } else {
          let userId = this.state.selectedUser.id
          const sb = SendBird.getInstance();
          if(sb) {
            sb.unblockUserWithUserId(userId, (user, error) => {
              if (error) {
                console.log('error unblocking: ', error);
              } else {
                console.log('unblock User: ', user);
              }
            })
          } else {
            const sb = new SendBird({ appId: SENDBIRD_APPID });
            sb.unblockUserWithUserId(userId, (user, error) => {
              if (error) {
                console.log('error unblocking: ', error);
              } else {
                console.log('unblock User: ', user);
              }
            })
          }
        }
      })
    }, 500);
  }

  renderItem({ item, index }) {
    let userImage = (item.profile_pic) ? { uri: item.profile_pic} : Images.defaultUserImage
    return (
      <ListItem key={index} style={styles.listItem}>
        <Left style={{ flex: 1.2, borderWidth: 0 }}>
          <Image source={userImage} style={styles.blockedUserImage} />
        </Left>
        <Body style={{ flex: 4.1, borderWidth: 0 }}>
          <Text style={styles.listName} numberOfLines={1}>{item.name}</Text>
          <Text style={styles.listUsername} numberOfLines={1}>
            {item.handle_name}
          </Text>
        </Body>
        <Right style={{ flex: 1.7, borderWidth: 0 }}>
          <Button bordered style={styles.btnUnblock} onPress={this.showModal(item)}>
            <Text style={styles.txtUnblock}>{strings('meTab.settings_blockUnblockTxt')}</Text>
          </Button>
        </Right>
      </ListItem>
    )
  }

  renderEmptyBlockList() {
    return(
      <View style={styles.emptyViewContainer}>
        <Text style={styles.emptyBlockList}>{strings('meTab.settings_blockNotblockMessage')}</Text>
        <Text style={[styles.privateAccountInfo, { textAlign: 'center' }]}>{strings('meTab.settings_blockEmptyMessage')}</Text>
      </View>
    )
  }

  renderModal() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.unblockModalVisible}
      >
      
        <StatusBar barStyle='dark-content' transparent={false} />
        <View style={styles.modalContainer}>
        <View style={[styles.modalContent, { padding: 0 }]}>
        <View style={styles.modalContentBox}>
          <View style={styles.titleView}>
            <Text style={styles.alertTitle}>{strings('meTab.settings_blockUnblockTxt')}</Text>
            <Text style={styles.alertDescription}>{strings('meTab.settings_blockEmptyMessage')}</Text>
          </View>
          <Button block transparent style={styles.modalRemoveButton} onPress={this.unblockUser}>
            <Text style={styles.distructiveText}>{strings('meTab.settings_blockUnblockAlert')}</Text>
          </Button>
          <Button block transparent style={styles.modalCancelButton} onPress={() => this.setState({ unblockModalVisible: false})}>
            <Text style={styles.cancelText}>{strings('constants.cancel')}</Text>
          </Button>
        </View>
       </View>
       </View>
      </Modal>
    )
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <RNModal
        animationType="slide"
        transparent={false}
        onShow={this.onShow}
        onDismiss={() => this.setState({ isEmptyBlockList: false })}
        visible={this.props.blockedOpen}
        onRequestClose={() => { }}>
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
      <View style={{ flex: 1 }}>
        <View style={styles.modalNav}>
          <Button transparent style={{ flex: 0.5 }} onPress={() => this.props.closeModal()} >
            <Text style={styles.skipButton}>{strings('constants.cancel')}</Text>
          </Button>
          <Text style={styles.modalTitle}>{strings('meTab.settings_blockContact')}</Text>
          <Button transparent style={{ flex: 0.35, marginLeft: 15 }} onPress={() => {}} >
            <Text style={styles.skipButton}></Text>
          </Button>
        </View>
        
        {(this.props.blockedList.length === 0 && !this.state.loading) ? 
          this.renderEmptyBlockList() 
          : 
          <FlatList
            data={this.props.blockedList}
            removeClippedSubviews={false}
            initialNumToRender={20}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            extraData={this.props}
            style={{ width: '100%' }}
            refreshControl={
              <RefreshControl
                tintColor={Colors.blueTheme}
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          />
        }
        {this.renderModal()}
        <Loader loading={this.state.loading} />
      </View>
      </SafeAreaView>
      </RNModal>
    )
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  blockedList: state.contacts.blockedList
});

const dispatchToProps = (dispatch) => {
  return {
    getBlockedContactList: (userId, type, pageNumber) => dispatch(actions.getBlockedContactList(userId, type, pageNumber)),
    contactBlockUnblock: (data) => dispatch(actions.contactBlockUnblock(data)),
  }
}

const Container = connect(stateToProps, dispatchToProps)(BlockedList)

export default Container;