import React, { Component } from 'react';
import { View, AsyncStorage, Text, Modal } from 'react-native';
import { Button, Right } from "native-base";
import SettingsList from 'react-native-settings-list';
import { connect } from 'react-redux';
import * as actions from '../../login/login.actions.js';
import styles from './settings.styles';
import Loader from '../../../components/loader/'
import _ from 'lodash'
import { strings } from "../../../locale/"

class NotificationSetting extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      receiveRequestWhenPrivate: false,
      acceptRequestWhenPrivate: false,
      addRequestWhenPublic: false
    };
    this.updatePrivateProfile = this.updatePrivateProfile.bind(this);
    //this.onShow = this.onShow.bind(this);
  }

  componentDidMount() {
    const { user } = this.props

    let receiveRequestWhenPrivate = false
    let acceptRequestWhenPrivate = false
    let addRequestWhenPublic = false
    if (user.notificationSettings) {
      receiveRequestWhenPrivate = (user.notificationSettings.receiveRequestWhenPrivate === "Y")
      acceptRequestWhenPrivate = (user.notificationSettings.acceptRequestWhenPrivate === "Y")
      addRequestWhenPublic = (user.notificationSettings.addRequestWhenPublic === "Y")
      this.setState({
        receiveRequestWhenPrivate,
        acceptRequestWhenPrivate,
        addRequestWhenPublic
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.user.notificationSettings.receiveRequestWhenPrivate !== this.props.user.notificationSettings.receiveRequestWhenPrivate
      || nextProps.user.notificationSettings.acceptRequestWhenPrivate !== this.props.user.notificationSettings.acceptRequestWhenPrivate
      || nextProps.user.notificationSettings.addRequestWhenPublic !== this.props.user.notificationSettings.addRequestWhenPublic
      || nextProps.notificationOpen !== this.props.notificationOpen
      || nextState.receiveRequestWhenPrivate !== this.state.receiveRequestWhenPrivate
      || nextState.acceptRequestWhenPrivate !== this.state.acceptRequestWhenPrivate
      || nextState.addRequestWhenPublic !== this.state.addRequestWhenPublic
      || nextState.loading !== this.state.loading) {
      return true
    }
    return false
  }

  updatePrivateProfile() {
    this.setState({ loading: true })
    let { receiveRequestWhenPrivate, acceptRequestWhenPrivate, addRequestWhenPublic } = this.state
    receiveRequestWhenPrivate = (receiveRequestWhenPrivate) ? "Y" : "N"
    acceptRequestWhenPrivate = (acceptRequestWhenPrivate) ? "Y" : "N"
    addRequestWhenPublic = (addRequestWhenPublic) ? "Y" : "N"

    let data = {
      "userId": this.props.user.userId,
      "columnName": "notification_settings",
      "receiveRequestWhenPrivate": receiveRequestWhenPrivate,
      "acceptRequestWhenPrivate": acceptRequestWhenPrivate,
      "addRequestWhenPublic": addRequestWhenPublic
    }
    this.props.updateProfileSettings(data)
      .then((response) => {
        this.setState({ loading: false })
        if (response.payload.data.status === 200) {
          this.updateUserLocal(response).done()
          this.props.closeModal()
        } else {
          setTimeout(() => {
            alert(response.payload.data.message)
          }, 300)
        }
      })
  }

  async updateUserLocal(response) {
    let key = _.camelCase(response.payload.type)
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = response.payload.value
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  render() {
    const {isRTL} = this.props.user;
    styles.titleStyleSecond = [styles.titleStyleSecond, {textAlign: (isRTL) ? 'right' : 'left'}]
    return (
      <View style={{ flex: 1 }}>
        <Loader loading={this.state.loading} />
        <View style={styles.modalNav}>
          <Button transparent style={{ flex: 0.5 }} onPress={() => this.props.closeModal()} >
            <Text style={styles.skipButton}>{strings('constants.cancel')}</Text>
          </Button>
          <Text style={styles.modalTitle}>{strings('constants.notifications')}</Text>
          <Button transparent style={{ flex: 0.35, justifyContent:'flex-end' }} onPress={this.updatePrivateProfile} >
            <Text style={styles.skipButton}>{strings('constants.save')}</Text>
          </Button>
        </View>

        <SettingsList borderColor='#fff' defaultItemSize={47} defaultTitleStyle={styles.title} >
          <SettingsList.Header isRTL={isRTL} headerText={strings('meTab.settings_notificationPrivateAccountHeader')} headerStyle={styles.header} />
          <SettingsList.Item
            isRTL={isRTL}
            title={strings('meTab.settings_notificationRequestReceive')}
            titleInfo=''
            hasNavArrow={false}
            hasSwitch
            switchState={this.state.receiveRequestWhenPrivate}
            switchOnValueChange={(value) => { this.setState({ receiveRequestWhenPrivate: value })}}
            titleStyle={styles.titleStyleSecond}
          />
          <SettingsList.Item
            isRTL={isRTL}
            title={strings('meTab.settings_notificationRequestAccept')}
            titleInfo=''
            hasNavArrow={false}
            hasSwitch
            switchState={this.state.acceptRequestWhenPrivate}
            switchOnValueChange={(value) => { this.setState({ acceptRequestWhenPrivate: value }) }}
            titleStyle={styles.titleStyleSecond}
          />
          <SettingsList.Header isRTL={isRTL} headerText={strings('meTab.settings_notificationPublicAccHeader')} headerStyle={styles.header} />
          <SettingsList.Item
            isRTL={isRTL}
            title={strings('meTab.settings_notificationContactAdded')}
            titleInfo=''
            hasNavArrow={false}
            hasSwitch
            switchState={this.state.addRequestWhenPublic}
            switchOnValueChange={(value) => { this.setState({ addRequestWhenPublic: value }) }}
            titleStyle={styles.titleStyleSecond}
          />
        </SettingsList>

      </View>
      
    )
  }
}

{/*
  <Modal
        animationType="slide"
        transparent={false}
        onShow={this.onShow}
        visible={this.props.notificationOpen}
        onRequestClose={() => { }}>
</Modal>
*/}

const stateToProps = (state) => ({
  user: state.login,
});

const dispatchToProps = (dispatch) => {
  return {
    updateProfileSettings: (data) => dispatch(actions.updateProfileSettings(data))
  }
}

const Container = connect(stateToProps, dispatchToProps)(NotificationSetting)

export default Container;