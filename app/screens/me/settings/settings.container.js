import { connect } from 'react-redux';
import Setting from './settings';
import { clearContacts } from '../../contacts/contacts.actions'
import { clearMessages } from '../../message/message.actions'
import * as actions from '../../login/login.actions.js';

const stateToProps = (state) => ({
  user: state.login,
});

const dispatchToProps = (dispatch) => {
  return {
    updateProfileSettings: (data) => dispatch(actions.updateProfileSettings(data)),
    logout: (userId, token) => dispatch(actions.logout(userId, token)),
    clearContacts: () => dispatch(clearContacts()),
    clearMessages: () => dispatch(clearMessages()),
  }
}

const Container = connect(stateToProps, dispatchToProps)(Setting)

export default Container;
