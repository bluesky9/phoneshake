import React, { Component } from 'react';
import { View, AsyncStorage, Text, Modal } from 'react-native';
import { Button } from "native-base";
import styles from './settings.styles';
import moment from 'moment'
import { strings } from '../../../locale/'
export default class APILog extends Component {
  constructor() {
    super();
    this.state = {
      LoadingContactProfileApi: null,
      LoadingContactListApi: null,
      NotificationsPageApi: null,
      SendOtpApi: null,
      UpdateLinkAccountApi: null
    };
    
    this.onShow = this.onShow.bind(this);
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  onShow() {
    this._loadInitialState().done();
  }

  async _loadInitialState() {
    var value = await AsyncStorage.multiGet(['LoadingContactProfileApi', 'LoadingContactListApi', 'NotificationsPageApi', 'SendOtpApi', 'UpdateLinkAccountApi'])

    if (value !== null) {
      let LoadingContactProfileApi = (value[0][1] != null) ? JSON.parse(value[0][1]) : null
      let LoadingContactListApi = (value[1][1] != null) ? JSON.parse(value[1][1]) : null
      let NotificationsPageApi = (value[2][1] != null) ? JSON.parse(value[2][1]) : null
      let SendOtpApi = (value[3][1] != null) ? JSON.parse(value[3][1]) : null
      let UpdateLinkAccountApi = (value[4][1] != null) ? JSON.parse(value[4][1]) : null
      this.setState({ LoadingContactProfileApi, LoadingContactListApi, NotificationsPageApi, SendOtpApi, UpdateLinkAccountApi });
    }
  }

  async updateUserLocal(response) {
    let key = _.camelCase(response.payload.type)
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = response.payload.value
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        onShow={this.onShow}
        visible={this.props.apilogOpen}
        onRequestClose={() => { }}>
        <View style={{ flex: 1 }}>
          <View style={styles.modalNav}>
            <Button transparent style={{ flex: 0.5 }} >
              <Text style={styles.skipButton}></Text>
            </Button>
            <Text style={styles.modalTitle}>{strings('meTab.settings_apiLog')}</Text>
            <Button transparent style={{ flex: 0.35, marginLeft: 15 }} onPress={() => this.props.closeModal()} >
              <Text style={styles.skipButton}>{strings('meTab.settings_apiLogClose')}</Text>
            </Button>
          </View>
          <View style={{ marginVertical: 10, width: '100%', paddingLeft: 10 }}>
            <Text style={{ fontSize: 18, fontWeight: '200', color:'red' }}>{strings('meTab.settings_apiLogTimeFormat')}</Text>
          </View>
          <View style={{ marginVertical: 10, width: '100%', paddingLeft: 10}}>
            <Text style={{fontSize: 18, fontWeight:'bold'}}>{strings('meTab.settings_apiLogLoadingProfile')} </Text>
            <Text>{strings('meTab.settings_apiLogStarttime')}  {(this.state.LoadingContactProfileApi) ? moment(this.state.LoadingContactProfileApi.startTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
            <Text>{strings('meTab.settings_apiLogEndTime')}  {(this.state.LoadingContactProfileApi) ? moment(this.state.LoadingContactProfileApi.endTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
          </View>
          <View style={{ marginVertical: 10, width: '100%', paddingLeft: 10 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{strings('meTab.settings_apiLogLoadingContact')} </Text>
            <Text>{strings('meTab.settings_apiLogStarttime')}   {(this.state.LoadingContactListApi) ? moment(this.state.LoadingContactListApi.startTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
            <Text>{strings('meTab.settings_apiLogEndTime')}  {(this.state.LoadingContactListApi) ? moment(this.state.LoadingContactListApi.endTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
          </View>
          <View style={{ marginVertical: 10, width: '100%', paddingLeft: 10 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{strings('meTab.settings_apiLogNotificationPage')} </Text>
            <Text>{strings('meTab.settings_apiLogStarttime')}   {(this.state.NotificationsPageApi) ? moment(this.state.NotificationsPageApi.startTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
            <Text>{strings('meTab.settings_apiLogEndTime')}  {(this.state.NotificationsPageApi) ? moment(this.state.NotificationsPageApi.endTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
          </View>
          <View style={{ marginVertical: 10, width: '100%', paddingLeft: 10 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{strings('meTab.settings_apiLogSendNumber')} </Text>
            <Text>{strings('meTab.settings_apiLogStarttime')}   {(this.state.SendOtpApi) ? moment(this.state.SendOtpApi.startTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
            <Text>{strings('meTab.settings_apiLogEndTime')}  {(this.state.SendOtpApi) ? moment(this.state.SendOtpApi.endTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
          </View>
          <View style={{ marginVertical: 10, width: '100%', paddingLeft: 10 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{strings('meTab.settings_apiLogUpdateLinkedAcc')} </Text>
            <Text>{strings('meTab.settings_apiLogStarttime')}   {(this.state.UpdateLinkAccountApi) ? moment(this.state.UpdateLinkAccountApi.startTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
            <Text>{strings('meTab.settings_apiLogEndTime')}   {(this.state.UpdateLinkAccountApi) ? moment(this.state.UpdateLinkAccountApi.endTime).format('hh:mm:ss') : strings('meTab.settings_apiLogApiNotCalled')}</Text>
          </View>
        </View>
      </Modal>
    )
  }
}

{/*
  <Modal
        animationType="slide"
        transparent={false}
        onShow={this.onShow}
        visible={this.props.notificationOpen}
        onRequestClose={() => { }}>
</Modal>
*/}
