import { StyleSheet, Platform } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  subContainer: {
    backgroundColor: '#fff',
    flex: 1
  },
  header: {
    marginTop: Metrics.screenHeight * 0.022,
    marginLeft: Metrics.screenWidth * 0.04,
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  label: {
    zIndex: 1,
    fontSize: Metrics.fontSizeSubheading,
    color: 'black',
    backgroundColor: 'transparent'
  },
  headerLeft: {
    height: 24,
    width: 24,
    marginLeft: 12
  },
  title: {
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  titleInfoStyle: {
    fontSize: Metrics.screenWidth * 0.037,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)',
    width: '50%'
  },
  titleStyleSecond: { 
    width: '100%',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  titleStyle: {
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  bottomView: {
    height: Metrics.screenHeight * 0.284,
    alignItems: 'center',
    justifyContent: 'center'
  },
  socialButtonView: {
    width: '40%',
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginTop: Metrics.screenHeight * 0.029
  },
  socialIcon: {
    height: Metrics.screenWidth * 0.042,
    width: Metrics.screenWidth * 0.042,
  },
  bottomText: {
    marginTop: Metrics.screenHeight * 0.02,
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  modal: {
    position: "absolute",
    width: Metrics.screenWidth,
    height: null,
    top: 0,
    paddingBottom: 0,
    zIndex: 111111
  },
  modalNav: {
    height: Metrics.screenHeight * 0.065,
    paddingTop: (Platform.OS === 'ios') ? 0 : (Metrics.screenHeight > 668) ? 3 : 0, //Metrics.screenHeight * 0.026
    // backgroundColor: 'red',
    width: '100%',
    paddingHorizontal: Metrics.screenWidth * 0.042,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)'
  },
  modalContentBox: {
    paddingTop: Metrics.screenHeight * 0.022,
    height: Metrics.screenHeight * 0.263,
    justifyContent: 'flex-end'
  },
  alertTitle: {
    fontFamily: Fonts.type.SFTextMedium,
    fontSize: Metrics.screenWidth * 0.037,
    textAlign: 'center',
    color: 'rgb(168, 168, 168)',
    marginBottom: Metrics.screenHeight * 0.014,
  },
  alertDescription: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.037,
    textAlign: 'center',
    color: 'rgb(168, 168, 168)'
  },
  modalRemoveButton: {
    height: Metrics.screenHeight * 0.074,
    borderRadius: 0,
  },
  modalCancelButton: {
    height: Metrics.screenHeight * 0.074,
    borderRadius: 0,
    borderTopWidth: 0.5,
    borderTopColor: 'rgb(204, 204, 204)'
  },
  titleView: {
    height: Metrics.screenHeight * 0.113,
    justifyContent: 'center',
    paddingHorizontal: 5
  },
  distructiveText: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.042,
    color: Colors.red
  },
  cancelText: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.042,
    color: 'rgb(168, 168, 168)'
  },
  modalTitle: {
    marginTop: 10,
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.05,
    color: '#000',
    flex: 2,
    textAlign: 'center'
  },
  skipButton: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.043,
    color: Colors.blueTheme
  },
  privateAccountInfo: {
    marginTop: 7,
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032,
    color: 'rgb(168, 168, 168)',
    paddingHorizontal: Metrics.screenWidth * 0.042
  },
  emptyViewContainer: {
    marginTop: Metrics.screenHeight * 0.277,
    flex: 1,
    alignItems: 'center'
  },
  emptyBlockList: {
    fontFamily: Fonts.type.SFTextMedium,
    fontSize: Metrics.screenWidth * 0.053,
    color: '#000',
    textAlign: 'center'
  },
  btnUnblock: {
    height: Metrics.screenHeight * 0.044,
    width: Metrics.screenWidth * 0.19,
    justifyContent: 'center',
    borderColor: Colors.red
  },
  txtUnblock: {
    fontSize: Metrics.screenWidth * 0.032,
    color: Colors.red,
    fontFamily: Fonts.type.SFTextSemibold
  },
  blockedUserImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: Metrics.screenWidth * 0.06
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    width: '75%'
  },
  listUsername: {
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextMedium,
    backgroundColor: 'transparent'
  },
  emptyList: {
    fontFamily: Fonts.type.SFTextMedium,
    fontSize: Metrics.screenWidth * 0.053,
    color: '#000',
    textAlign: 'center'
  },
  sectionHeaderView: {
    backgroundColor: 'rgb(247, 247, 247)',
    justifyContent: 'center',
    // paddingVertical: 16,
    paddingLeft: Metrics.screenWidth * 0.032,
    height: 20
  },
  sectionHeaderText: {
    color: 'rgb(168, 168, 168)',
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032
  },
  sectionListItem: {
    width: '100%',
    marginLeft: 0,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(227, 227, 227)',
    paddingVertical: Metrics.screenHeight * 0.014,
  },
  btnAllow: {
    height: Metrics.screenHeight * 0.044,
    width: Metrics.screenWidth * 0.19,
    justifyContent: 'center',
    backgroundColor: Colors.blueTheme,
    marginRight: 10
  },
  allowText: {
    fontSize: Metrics.screenWidth * 0.032,
    color: '#fff',
    fontFamily: Fonts.type.SFTextSemibold
  },
  btnDecline: {
    height: Metrics.screenHeight * 0.044,
    width: Metrics.screenWidth * 0.19,
    justifyContent: 'center',
    backgroundColor: 'rgb(168, 168, 168)'
  },
  sectionListName: {
    fontFamily: Fonts.type.SFTextRegular
  },
  listLeft: {
    height: '100%',
    borderBottomWidth: 0,
    justifyContent: 'flex-start',
    // borderBottomColor: 'rgb(204, 204, 204)'
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.25,
    backgroundColor: "#fff",
    // padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  closeIcon: {
    height: Metrics.screenWidth * 0.064,
    width: Metrics.screenWidth * 0.064
  },
  closeButton: {
    flex: 0.4,
    justifyContent: 'center',
    marginRight: Metrics.screenWidth * 0.042
  },
  defaultTabbar: {
    borderBottomWidth: 0.7,
    borderBottomColor: 'rgb(204, 204, 204)',
    height: Metrics.screenHeight * 0.055
  },
  tabBorder: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Colors.blueTheme,
    borderLeftColor: 'transparent',
    marginBottom: (Platform.OS === 'android') ? -1 : 0
  },
  tabBarTextStyle: {
    fontSize: Metrics.screenWidth * 0.037,
    marginTop: Metrics.screenHeight * 0.01,
    fontFamily: Fonts.type.SFTextRegular,
  }
});
