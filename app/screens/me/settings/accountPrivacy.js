import React, { Component } from 'react';
import { View, AsyncStorage, Text } from 'react-native';
import { Button } from "native-base";
import { SafeAreaView } from 'react-navigation'
import SettingsList from 'react-native-settings-list';
import { connect } from 'react-redux';
import * as actions from '../../login/login.actions.js';
import styles from './settings.styles';
import Loader from '../../../components/loader/'
import { strings } from '../../../locale/'

class PrivateAccount extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      isPrivateAccount: false
    };
    this.updatePrivateProfile = this.updatePrivateProfile.bind(this);
  }

  componentDidMount() {
    let switchValue = (this.props.user.isPrivateAccount === "Y") ? true : false
    this.setState({ isPrivateAccount: switchValue })
  }

  updatePrivateProfile() {
    this.setState({ loading: true })
    let value = (this.state.isPrivateAccount) ? "Y" : "N"
    let data = {
      "userId": this.props.user.userId,
      "columnName": "is_private_account",
      "value": value
    }
    this.props.updateProfileSettings(data)
      .then((response) => {
        this.setState({ loading: false })
        if (response.payload.data.status === 200) {
          this.updateUserLocal(response).done()
          this.props.closeModal()
        } else {
          setTimeout(() => {
            alert(response.payload.data.message)
          }, 300)
        }
      })
  }

  async updateUserLocal(response) {
    let key = response.payload.type
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = response.payload.value
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  render() {
    const {isRTL} = this.props.user;
    styles.titleStyleSecond = [styles.titleStyleSecond, {textAlign: (isRTL) ? 'right' : 'left'}]
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}} >
      <View style={{ flex: 1 }}>
        <Loader loading={this.state.loading} />
        <View style={styles.modalNav}>
          <Button transparent style={{ flex: 0.5 }} onPress={() => this.props.closeModal()} >
            <Text style={styles.skipButton}>{strings('constants.cancel')}</Text>
          </Button>
          <Text style={styles.modalTitle}>{strings('meTab.settings_accountPrivate')}</Text>
          <Button transparent style={{ flex: 0.35, justifyContent:'flex-end' }} onPress={this.updatePrivateProfile} >
            <Text style={styles.skipButton}>{strings('constants.save')}</Text>
          </Button>
        </View>

        <SettingsList borderColor='#fff' defaultItemSize={47} defaultTitleStyle={styles.title} >
          <SettingsList.Item
            isRTL={isRTL}
            title={strings('meTab.settings_privateAccountInfoTitle')}
            titleInfo=''
            hasNavArrow={false}
            hasSwitch
            switchState={this.state.isPrivateAccount}
            switchOnValueChange={(value) => { this.setState({ isPrivateAccount: value }) }}
            titleStyle={styles.titleStyleSecond}
          />
          <Text style={[styles.privateAccountInfo, {textAlign: (isRTL) ? "right": "left"}]}>
            {strings('meTab.settings_privateAccountInfo')}
          </Text>
        </SettingsList>
      </View>
      </SafeAreaView>
    )
  }
}

const stateToProps = (state) => ({
  user: state.login,
});

const dispatchToProps = (dispatch) => {
  return {
    updateProfileSettings: (data) => dispatch(actions.updateProfileSettings(data))
  }
}

const Container = connect(stateToProps, dispatchToProps)(PrivateAccount)

export default Container;