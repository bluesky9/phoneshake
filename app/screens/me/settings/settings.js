import React, { Component } from 'react';
import { View, AsyncStorage, TouchableOpacity, ActivityIndicator, Image, Text, Linking, Modal as RNModal, StyleSheet, StatusBar, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation'
import SettingsList from 'react-native-settings-list';
import { MaterialIndicator } from 'react-native-indicators';
import { Button } from "native-base";
import Modal from "react-native-modal";
import SendBird from 'sendbird';
import Loader from '../../../components/loader/'
import AccountModal from './accountPrivacy'
import NotificationSetting from './notificationSetting'
import APILog from './apilog'
import BlockedContact from './blockedContact'
import styles from './settings.styles';
import { Fonts, Metrics, Images, Colors } from '../../../themes';
import GooglePlacesInput from '../../../components/googleAutoComplete/googleAutoComplete'
import PhoneShakeWebView from './../../../components/phoneShakeWebView/phoneShakeWebView'
import { strings } from '../../../locale/'


let Indicator = ActivityIndicator
if (Platform.OS === 'ios') {
  Indicator = MaterialIndicator
}
const sb = SendBird.getInstance();

class Settings extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={styles.headerLeft} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#fff'
    },
    headerTitleStyle: {
      fontFamily: Fonts.type.SFTextRegular,
      fontWeight: "200",
      fontSize: Metrics.screenWidth * 0.053,
      alignSelf: 'center',
      textAlign: 'center',
      width: '70%'
    },
    title: strings('meTab.options')
  });

  constructor() {
    super();
    this.state = { 
      modalVisible: false,
      isPrivateModal: false,
      logoutModalVisible: false,
      loading: false,
      address: null,
      isPrivateAccount: false,
      notificationOpen: false,
      blockedOpen: false,
      apilogOpen: false,
      isWebView: false,
      loggingOut: false,
      linkUrl: 'http://google.com',
    };
    this.renderFooter = this.renderFooter.bind(this);
    this.gotoEditProfile = this.gotoEditProfile.bind(this);
    this.gotoEditContactInfo = this.gotoEditContactInfo.bind(this);
    this.onSelectAddress = this.onSelectAddress.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.saveAddress = this.saveAddress.bind(this);
    this.logoutUser = this.logoutUser.bind(this);
    this.renderAddressModal = this.renderAddressModal.bind(this);
    this.renderAddressContent = this.renderAddressContent.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
    this.openWebViewLink = this.openWebViewLink.bind(this)
  }

  componentDidMount() {
    this.mounted = true
    console.log("this.props.user", this.props.user)
  }

  componentWillUnmount() {
    this.mounted = false
  }

  gotoEditProfile = (options) => () => {
    this.props.navigation.navigate('EditProfile', options )
  }

  gotoEditContactInfo = (options) => () => {
    this.props.navigation.navigate('EditContactInfo', options)
  }

  onSelectAddress(details) {
    if(details !== null) {
      this.setState({
        address: details.formatted_address,
        latitude: details.geometry.location.lat,
        longitude: details.geometry.location.lng
      })
    }
  }

  renderAddressContent() {
    return(
      <View style={{ flex: 1 }}>
        <Loader loading={this.state.loading} />
        <View style={styles.modalNav}>
          <Button transparent style={{ flex: 0.7 }} onPress={() => this.setState({ modalVisible: false, isPrivateModal: false })} >
            <Text style={styles.skipButton}>{strings('constants.cancel')}</Text>
          </Button>
          <Text style={styles.modalTitle}>{strings('meTab.settingsEditAddress')}</Text>
          <Button transparent style={{ flex: 0.5, justifyContent:'flex-end' }} onPress={this.saveAddress} >
            <Text style={[styles.skipButton, {textAlign: 'right'}]}>{strings('meTab.settingsDone')}</Text>
          </Button>
        </View>

        <GooglePlacesInput
          defaultValue={''}
          onSelectAddress={(details) => this.onSelectAddress(details)}
        />

      </View>
    )
  }

  renderAddressModal() {
    return (
      <RNModal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {console.log('modal closed')}}>
        <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        {(this.state.isPrivateModal) ? 
          <AccountModal closeModal={() => this.setState({ modalVisible: false, isPrivateModal: false})}/>
          : 
          (this.state.notificationOpen) ?
            <NotificationSetting closeModal={() => this.setState({ modalVisible: false, notificationOpen: false })} />
          :
            this.renderAddressContent()
          }
        </SafeAreaView>
      </RNModal>
    )
  }

  saveAddress() {
    let data = {}

    if(this.state.address === null) {
      data = {
        "userId": this.props.user.userId,
        "columnName": "address",
        "value": '',
        "latitude": '',
        "longitude": ''
      }
    } else {
      data = {
        "userId": this.props.user.userId,
        "columnName": "address",
        "value": this.state.address,
        "latitude": this.state.latitude,
        "longitude": this.state.longitude
      }
    }

    this.setState({ loading: true })
    this.props.updateProfileSettings(data)
    .then((response) => {
      this.setState({ loading: false })
      if (response.payload.data.status === 200) {
        this.updateUserLocal(response).done()
        this.setState({
          modalVisible: false,
          address: null,
          latitude: null,
          longitude: null
        })
      } else {
        setTimeout(() => {
          alert(response.payload.data.message)
        }, 300)
      }
    })
  }

  async updateUserLocal(response) {
    let key = response.payload.type
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = response.payload.value
      asyncData.data.content.latitude = response.payload.latitude
      asyncData.data.content.longitude = response.payload.longitude
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  async logoutUser(){
    if(this.state.loggingOut) return false;

    this.setState({ loggingOut: true })
    
    this.props.logout(this.props.user.userId, this.props.user.token)
    .then((response) => {
      if (response.payload.data && response.payload.data.status === 200) {
        // if (this.mounted) {
        //   this.setState({ loggingOut: false, logoutModalVisible: false })
        // }
        this.props.clearContacts()
        this.props.clearMessages()
        this.logoutSendBird()
        AsyncStorage.removeItem('MyContacts')
        AsyncStorage.removeItem('Messages')
        AsyncStorage.multiRemove(['PhoneShakeUser', 'SyncedContact', 'MyContacts', 'PhonebookContacts', 'Messages'])
        this.props.screenProps.logout()
      } else {
        this.setState({ loggingOut: false })
        alert(strings('meTab.settingsLogputFailed'))
      }
    })
  }

  async logoutSendBird() {
    var value = await AsyncStorage.getItem('SendBirdToken')

    if (value !== null) {
      let apnsToken = JSON.parse(value);
      if(Platform.OS === 'ios') {
        sb.unregisterAPNSPushTokenForCurrentUser(apnsToken, function (response, error) {
          if (error) {
            console.error(error);
            return;
          }
          sb.disconnect(function () {
            AsyncStorage.removeItem('SendBirdToken')
          });
        });
      } else {
        sb.unregisterGCMPushTokenForCurrentUser(apnsToken, function (response, error) {
          if (error) {
            console.error(error);
            return;
          }
          sb.disconnect(function () {
            AsyncStorage.removeItem('SendBirdToken')
          });
        });
      }
    } else {
      
    }
  }

  openWebViewLink(key){
    
    if (key === 'Privacy Policy' ){  
      Linking.canOpenURL('medium://').then(supported => {
       return supported ? Linking.openURL('https://medium.com/phoneshake/privacy-policy-phoneshake-83719a06289f') : this.setState({isWebView: true, linkUrl: 'https://medium.com/phoneshake/privacy-policy-phoneshake-83719a06289f'})
      }).catch(err => console.error('An error occurred', err));
    } else if ( key === 'Terms of Service' ){
      Linking.canOpenURL('medium://').then(supported => {
       return supported ? Linking.openURL('https://medium.com/phoneshake/terms-of-service-f036a197d7f0') : this.setState({isWebView: true, linkUrl: 'https://medium.com/phoneshake/terms-of-service-f036a197d7f0'})
      }).catch(err => console.error('An error occurred', err));
    } else if( key === 'Twitter' ) {
      Linking.canOpenURL('twitter://').then(supported => {
        return supported ? Linking.openURL('https://twitter.com/@phoneshake') : this.setState({isWebView: true, linkUrl: 'https://twitter.com/@phoneshake'})
       }).catch(err => console.error('An error occurred', err));
    } else if( key === 'Facebook' ) {
      Linking.canOpenURL('fb://').then(supported => {
        return supported ? Linking.openURL('fb://PhoneshakeApp/') : this.setState({isWebView: true, linkUrl: 'https://www.facebook.com/PhoneshakeApp/'})
       }).catch(err => console.error('An error occurred', err));
    } else if( key === 'Instagram' ) {
      Linking.canOpenURL('instagram://').then(supported => {
        return supported ? Linking.openURL('instagram://user?username=phoneshake') : this.setState({isWebView: true, linkUrl: 'https://www.instagram.com/phoneshake'})
       }).catch(err => console.error('An error occurred', err));
    } 
  }

  renderModal() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.logoutModalVisible}
      >
        <StatusBar barStyle='dark-content' transparent={false} />
        <View style={logoutModal.modalContainer}>
        <View style={[logoutModal.modalContent, { padding: 0 }]}>
        <View style={styles.modalContentBox}>
          <View style={styles.titleView}>
            <Text style={styles.alertTitle}>{strings('meTab.settingsLogout')}</Text>
            <Text style={styles.alertDescription}>{strings('meTab.settingsLogoutAlertTitle')}</Text>
          </View>
          <Button block transparent style={[styles.modalRemoveButton, { flexDirection: 'row'}]} onPress={this.logoutUser}>
            <Text style={styles.distructiveText}>{strings('meTab.settingsLogout')}</Text>
            {
              (this.state.loggingOut) ?
                <View style={{height: 25, width: 25, marginLeft: 10 }}>
                  <Indicator
                    size={16}
                    color={Colors.red}
                    animating={this.state.loggingOut} />
                </View>
              : null
            }
          </Button>
              <Button block transparent style={styles.modalCancelButton} onPress={() => { if (this.state.loggingOut) return false; this.setState({ logoutModalVisible: false })}}>
            <Text style={styles.cancelText}>{strings('constants.cancel')}</Text>
          </Button>
        </View>
       </View>
       </View>
      </Modal>
    )
  }

  renderFooter() {
    const {isRTL} = this.props.user;
    let contents = [];
    if(isRTL){
      contents.push([
        <Text style={styles.bottomText} onPress={() => this.openWebViewLink('Terms of Service') }>{strings('meTab.settingsTermsOfService')} </Text>,
        <Text style={styles.bottomText}>{strings('meTab.settingsAnd')} &nbsp;</Text>,
        <Text style={styles.bottomText} onPress={() => this.openWebViewLink('Privacy Policy') }>{strings('meTab.settingsPrivacyPolicy')} &nbsp;</Text>
      ])
    } else {
      contents.push([
        <Text style={styles.bottomText} onPress={() => this.openWebViewLink('Privacy Policy') }>{strings('meTab.settingsPrivacyPolicy')} &nbsp;</Text>,
        <Text style={styles.bottomText}>{strings('meTab.settingsAnd')} &nbsp;</Text>,
        <Text style={styles.bottomText} onPress={() => this.openWebViewLink('Terms of Service') }>{strings('meTab.settingsTermsOfService')} &nbsp;</Text>,
      ])
    }
    return (
      <View style={styles.bottomView}>
        <View style={{flexDirection: 'row'}}>
          {contents}
        </View>
        <View style={styles.socialButtonView}>
          <TouchableOpacity onPress={() => this.openWebViewLink('Twitter')}>
            <Image source={Images.twitterIcon} style={styles.socialIcon} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            this.openWebViewLink('Facebook')            
          }}>
            <Image source={Images.fbIcon} style={styles.socialIcon} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() =>{
            this.openWebViewLink('Instagram')
          }}>
            <Image source={Images.instagramIcon} style={styles.socialIcon} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  setModalVisible(value){    
    this.setState({isWebView: false})
  }

  render() {
    const {isRTL} = this.props.user;
    styles.titleStyle = [styles.titleStyle, {textAlign: (isRTL) ? 'right' : 'left'}]
    styles.titleInfoStyle = [styles.titleInfoStyle, {textAlign: (isRTL) ? 'left' : 'right'}]

    return (
      <View style={styles.container}>
        <PhoneShakeWebView          
          visible={this.state.isWebView}
          setModalVisible= {this.setModalVisible}
          linkUrl={this.state.linkUrl}
        />
        <View style={styles.subContainer}>
          <SettingsList borderColor='#fff' defaultItemSize={47} defaultTitleStyle={styles.title} renderFooter={this.renderFooter} >
            <SettingsList.Header isRTL={isRTL} headerText={strings('meTab.settingsProfileHeader')} headerStyle={styles.header} />
            <SettingsList.Item
              isRTL={isRTL}
              title={strings('meTab.settingsName')}
              titleInfo={this.props.user.name}
              titleInfoStyle={styles.titleInfoStyle}
              titleStyle={styles.titleStyle}
              onPress={
                this.gotoEditProfile({
                  title: strings('meTab.settingsWhatsName'),
                  placeholder: strings('meTab.settingsFullName'),
                  maxLength: 60,
                  value: this.props.user.name,
                  informationText: strings('meTab.settingsAddFirstAndLastName'),
                  type: 'name'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL}
              title={strings('meTab.settingsHandle')}
              titleStyle={styles.titleStyle}
              titleInfo={this.props.user.handleName}
              titleInfoStyle={styles.titleInfoStyle}
            />
            <SettingsList.Item
            isRTL={isRTL}
            titleStyle={styles.titleStyle}
              title={strings('meTab.settingsOrganization')}
              titleInfo={this.props.user.organization}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditProfile({
                  title: strings('meTab.settingsCurrentOrganization'),
                  placeholder: strings('meTab.settingsOrganizationName'),
                  maxLength: 100,
                  value: this.props.user.organization,
                  informationText: strings('meTab.settingsOrganizationInformationTxt'),
                  type: 'organization'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.settingsPositionTitle')}
              titleInfo={this.props.user.designation}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditProfile({
                  title: strings('meTab.settingsPosition'),
                  placeholder: strings('meTab.settingsPositionTitle'),
                  maxLength: 100,
                  value: this.props.user.designation,
                  informationText: strings('meTab.settingsPositionDescTxt'),
                  type: 'designation'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.settingsBioTitle')}
              titleInfo={this.props.user.description}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditProfile({
                  title: strings('meTab.settingsBio'),
                  placeholder: strings('meTab.settingsBioYourself'),
                  maxLength: 150,
                  value: this.props.user.description,
                  informationText: strings('meTab.settingsBioLimit'),
                  type: 'description'
                })
              }
            />
            <SettingsList.Header isRTL={isRTL} headerText={strings('meTab.settingsLinkedAccHeader')} headerStyle={[styles.header, { marginTop: 5 }]} />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}            
              title={strings('meTab.settingsLinkedAccTitle')}
              titleInfo=''
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => this.props.navigation.navigate('LinkedAccount')}
            />
            <SettingsList.Header isRTL={isRTL} headerText={strings('meTab.settingsContactInfoHeader')} headerStyle={styles.header} />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('constants.mobileNumber')}
              titleInfo={this.props.user.mobileNo}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditContactInfo({
                  title: strings('login.whatsNumber'),
                  placeholder: strings('constants.mobileNumber'),
                  value: this.props.user.mobileNo,
                  informationText: strings('meTab.settingsContactByNumberInfo'),
                  type: 'mobile_no'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('constants.email')}
              titleInfo={this.props.user.email}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditProfile({
                  title:strings('login.whatsEmail'),
                  placeholder: strings('constants.emailAddress'),
                  value: this.props.user.email,
                  informationText: strings('meTab.settingsContactByEmail'),
                  type: 'email'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL} 
              titleStyle={styles.titleStyle}
              title={strings('meTab.settingsContactByLandline')}
              titleInfo={this.props.user.landLineNo}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditContactInfo({
                  title: strings('meTab.settingsLandlineNumber'),
                  placeholder: strings('meTab.settingsLandlineNumberPlaceholder'),
                  value: this.props.user.landLineNo,
                  informationText: strings('meTab.settingsLandlineNumberInfo'),
                  type: 'land_line_no'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.settingsFax')}
              titleInfo={this.props.user.fax}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={
                this.gotoEditContactInfo({
                  title: strings('meTab.settingsFaxNumber'),
                  placeholder: strings('meTab.settingsPlaceholderFaxNumber'),
                  value: this.props.user.fax,
                  informationText: strings('meTab.settingsFaxInfo'),
                  type: 'fax'
                })
              }
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.settingsAddress')}
              titleInfo={this.props.user.address}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.setState({ modalVisible: true })
              }}
            />
            <SettingsList.Header isRTL={isRTL} headerText={strings('meTab.settingsAccInfoHeader')} headerStyle={styles.header} />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.signupInfo')}
              titleInfo={this.props.user.usernames[0].username}
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.props.navigation.navigate('SignupInfo')
              }}
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.settingsPrivacy')}
              titleInfo=''
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.setState({ modalVisible: true, isPrivateModal: true })
              }}
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('constants.notifications')}
              titleInfo=''
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.setState({ modalVisible: true, notificationOpen: true })
              }}
            />
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('meTab.settings_blockContact')}
              titleInfo=''
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.setState({ blockedOpen: true })
              }}
            />
            {/* <SettingsList.Item
              title='API Log (Temp.)'
              titleInfo=''
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.setState({ apilogOpen: true })
              }}
            /> */}
            <SettingsList.Item
              isRTL={isRTL}
              titleStyle={styles.titleStyle}
              title={strings('constants.logout')}
              titleInfo=''
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => {
                this.setState({ logoutModalVisible: true })
              }}
            />
          </SettingsList>
        </View>
        <BlockedContact blockedOpen={this.state.blockedOpen} closeModal={() => this.setState({ blockedOpen: false })} navigation={this.props.navigation} />
        <APILog apilogOpen={this.state.apilogOpen} closeModal={() => this.setState({ apilogOpen: false })} />
        {this.renderAddressModal()}
        {this.renderModal()}
      </View>
    );
  }
}

export default Settings;

const logoutModal = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.25,
    backgroundColor: "#fff",
    // padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});
