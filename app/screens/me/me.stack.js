import { createStackNavigator } from 'react-navigation'
import Setting from './me'
import Options from "./settings/settings.container.js";
import EditProfile from "./editProfile/editProfile.js";
import EditContactInfo from "./editContactInfo/editContactInfo.js";
import LinkedAccount from "./linkedAccount/linkedAccount.js";
import SignupInfo from "./signupInfo/signupInfo.js";
import AddSignupInfo from "./signupInfo/addSignupInfo.js";
import ResetAccount from '../tabs/resetAccount'
import Verification from '../login/verifyOtp/verifyOtp'
import { strings } from '../../locale';

const SettingStack = createStackNavigator({
  SettingScreen: {
    screen: Setting,
    navigationOptions: {
      title: strings('home.home')
    }
  },
  SettingOptions: {
    screen: Options
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: {
      title: '',
    }
  },
  EditContactInfo: {
    screen: EditContactInfo,
    navigationOptions: {
      title: '',
    }
  },
  LinkedAccount: {
    screen: LinkedAccount
  },
  SignupInfo: {
    screen: SignupInfo
  },
  AddSignupInfo: {
    screen: AddSignupInfo,
    navigationOptions: {
      title: '',
    }
  },
  Verification: {
    screen: Verification,
    navigationOptions: {
      title: ''
    }
  },
  ResetAccount: {
    screen: ResetAccount,
    navigationOptions: {
      title: strings('constants.resetAccount')
    }
  }
}, {
    headerMode: 'screen',
    initialRouteName: 'SettingScreen',
    cardStyle: {
      // paddingTop: Platform.OS === 'ios' ? 0 : 12,
      backgroundColor: '#fff'
    },
    navigationOptions: {}
  });

SettingStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default SettingStack;
