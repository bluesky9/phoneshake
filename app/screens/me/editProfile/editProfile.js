import React, { Component } from 'react'
import { View, Image, Platform, TouchableOpacity, Keyboard, LayoutAnimation, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { SafeAreaView } from "react-navigation";
import * as actions from '../../login/login.actions.js';
import InputForm from '../../../components/inputForm/inputForm.js'
import Loader from '../../../components/loader/'
import { Images, Metrics } from '../../../themes/'
import styles from './editProfile.styles'
import moment from "moment";
import { strings } from '../../../locale/index.js';

class EditProfile extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={styles.headerLeft} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  });

  constructor() {
    super();
    this.state = {
      value: '',
      keyboardHeight: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.35 : 15,
      loading: false,
      invalidUrl: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
  }

  componentDidMount() {
    
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }
    let value = (this.props.navigation.state.params.value) ? this.props.navigation.state.params.value : ''
    this.setState({ value })
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener.remove()
      this.keyboardDidHideListener.remove()
    }
  }

  keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = e.endCoordinates.height
    this.setState({
      keyboardHeight: newSize + Metrics.screenHeight * 0.022
    })
  }

  keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  handleSubmit() {
     Keyboard.dismiss();
     this.setState({ invalidUrl: false, loading: true })
     let { actionType } = this.props.navigation.state.params
     if (actionType) {
        this.updateSocialAccount()
     } else {
       this.updateUserProfile()
     }
  }

  validateUrl(url) {
    var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if (!re.test(url)) {
      return false;
    }
    return true;
  }

  async updateSocialAccount() {
    let { type, actionType, id } = this.props.navigation.state.params

    if(actionType === 'url') {
      let isValidUrl = this.validateUrl(this.state.value)
      if (this.state.value && this.state.value.length > 0 && !isValidUrl){
        this.setState({ loading: false, invalidUrl: true })
        return;
      }
    }

    let data = {
      "id": id,
      "userId": this.props.userId,
      "type": type,
      "link": this.state.value
    }
    let startTime = moment()
    await this.props.addSocialMediaDetails(data)
    .then((response) => {
      let data = {
        'startTime': startTime,
        'endTime': moment()
      }
      AsyncStorage.setItem('UpdateLinkAccountApi', JSON.stringify(data));
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          this.updateUserLocalSocialMedia(response).done()
          this.props.navigation.goBack()
        } else if (response.payload.data && response.payload.data.status === 401) {
          this.props.navigation.navigate('ResetAccount')
        } else {
          let message = (response.payload && response.payload.data && response.payload.data.message) ? response.payload.data.message : strings('constants.errorMessage')
          setTimeout(() => {
            alert(message)
          }, 300)
        }
    })
  }

  async updateUserProfile() {
    let { type } = this.props.navigation.state.params
    let data = {
      "userId": this.props.userId,
      "columnName": type,
      "value": this.state.value
    }
    await this.props.updateProfileSettings(data)
    .then((response) => {
      this.setState({ loading: false })
      if (response.payload.data && response.payload.data.status === 200) {
        this.updateUserLocal(response).done()
        this.props.navigation.goBack()
      } else if (response.payload.data && response.payload.data.status === 401) {
        this.props.navigation.navigate('ResetAccount')
      } else {
        let message = (response.payload.data.message) ? response.payload.data.message : strings('constants.errorMessage')
        setTimeout(() => {
          alert(message)
        }, 300)
      }
    })
  }

  async updateUserLocalSocialMedia(response) {
    let newMedia = response.payload.data.content.socialMediaAccount
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      let accounts = (asyncData.data && asyncData.data.content && asyncData.data.content.socialMediaLinks) ? asyncData.data.content.socialMediaLinks : []
      if ( accounts && accounts.length > 0) {
        let flag = 0
        accounts.map((item) => {
          if(item.type.toLowerCase() === newMedia.type.toLowerCase()) {
            flag = flag + 1
            item.id = newMedia.id
            item.link = newMedia.link
          }
        })
        if(flag === 0) {
          accounts.push(newMedia)  
        }
      } else {
        accounts.push(newMedia)
      }
      asyncData.data.content.socialMediaLinks = accounts
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  async updateUserLocal(response) {
    let key = response.payload.type
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = response.payload.value
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  formatName(text) {
    if (text) {
      text = text.trim();
      text = text.split(" ");
      for (i = 0; i < text.length; i++) {
        let nameWord = text[i];
        nameWord = nameWord.charAt(0).toUpperCase() + nameWord.substr(1, nameWord.length).toLowerCase();
        text[i] = nameWord;
      }
      text = text.join(" ");
    }
    this.setState({ value: text })
  }

  render() {
    let { title, placeholder, informationText, maxLength, type } = this.props.navigation.state.params
    
    informationText = (this.state.invalidUrl) ? 'Invalid url' : informationText
    
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
      <View style={{flex: 1}}>
      <Loader loading={this.state.loading} />
      <InputForm
        titleText={title}
        isRTL={this.props.isRTL}
        invalidData={this.state.invalidUrl}
        placeholder={placeholder}
        value={this.state.value}
        maxLength={maxLength}
        name="bio"
        multiline={(type === 'description')}
        keyboardHeight={this.state.keyboardHeight}
        informationText={informationText}
        onChangeText={(value) => this.setState({ value })}
        autoCapitalize={(type === 'name' || type === 'organization' || type === 'designation' || type === 'description') ? 'sentences' : 'none' }
        handleSubmit={this.handleSubmit}
      />
      </View>
      </SafeAreaView>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  isRTL: state.login.isRTL
});

const dispatchToProps = (dispatch) => {
  return {
    updateProfileSettings: (data) => dispatch(actions.updateProfileSettings(data)),
    addSocialMediaDetails: (data) => dispatch(actions.addSocialMediaDetails(data))
  }
}
const Container = connect(stateToProps, dispatchToProps)(EditProfile)

export default Container;
