import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, AsyncStorage, Platform, Linking } from 'react-native';
import SettingsList from 'react-native-settings-list';
import { connect } from 'react-redux';
import LinkedInModal from 'react-native-linkedin'
import ActionSheet from '@yfuks/react-native-action-sheet';
import { GoogleSignin } from 'react-native-google-signin';
import * as actions from '../../login/login.actions.js';
import { LINKEDIN_CLIENT_ID, LINKEDIN_CLIENT_SECRET, LINKEDIN_REDIRECT_URL } from "../../../api/constants";
import Loader from '../../../components/loader/'
import styles from './linkedAccount.styles';
import { Fonts, Metrics, Images, Icons } from '../../../themes';
import PhoneShakeButton from '../../../components/phoneShakeButton/'
import { strings } from '../../../locale/index.js';

const FBSDK = require('react-native-fbsdk');
const {
  LoginManager, GraphRequest, GraphRequestManager, AccessToken
} = FBSDK;

const linkedInBaseApi = 'https://api.linkedin.com/v1/people/'
const linkedInParams = [
  'id',
  'first-name',
  'last-name',
  'site-standard-profile-request',
  'public-profile-url'
]



class LinkedAccount extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={styles.headerLeft} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#fff'
    },
    headerTitleStyle: {
      fontFamily: Fonts.type.SFTextRegular,
      fontWeight: "200",
      fontSize: Metrics.screenWidth * 0.053,
      alignSelf: 'center',
      textAlign: 'center',
      width: '70%'
    },
    title: strings('meTab.signupInfo')
  });

  constructor() {
    super();
    this.state = {
      loading: false,
      visiblity: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.gotoAddHandle = this.gotoAddHandle.bind(this);
    this.gotoEditContactInfo = this.gotoEditContactInfo.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onPressSocialItem = this.onPressSocialItem.bind(this);
    this.showActionSheet = this.showActionSheet.bind(this);
    this.removeSocialMediaAccount = this.removeSocialMediaAccount.bind(this);
  }

  handleSubmit() {
    let url = 'mailto:hello@phoneshake.me?subject=Request for new social account'
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  gotoAddHandle = (options) => () => {
    this.props.navigation.navigate('EditProfile', options)
  }

  gotoEditContactInfo = (options) => () => {
    this.props.navigation.navigate('EditContactInfo', options)
  }

  showActionSheet = (item) => {
    ActionSheet.showActionSheetWithOptions({
    options: ['Remove Account', 'Cancel'],
    cancelButtonIndex: 1,
    destructiveButtonIndex: 0,
    tintColor: 'rgb(0,122, 255)'
    },
    (buttonIndex) => {

      switch (buttonIndex) {
        case 0:
          this.removeSocialMediaAccount(item)
          break;          
        default:
          break;
      }
    });
  }

  removeSocialMediaAccount(item) {
    this.setState({ loading: true })
    let data = {
      "id": item.id,
      "userId": this.props.user.userId,
      "type": item.type,
      "link": ''
    }
    this.updateSocialAccount(data)
    if(item.type === 'Google+') {
      this._googleSignOut().done()
    }
    if (item.type === 'Facebook') {
      LoginManager.logOut()
    }
  }

  onPressSocialItem = (item) => () => {
    let titleArray = [
      'linkedAccount.whatWebsite',
      'linkedAccount.whatBlog',
      'linkedAccount.handleName',
      'linkedAccount.username',
      'linkedAccount.profileUrl',
      'linkedAccount.id',
      'linkedAccount.mobileNumber',
      'linkedAccount.userId'
      ]

    let informationTextArray = [
      'linkedAccount.websiteLink',
      'linkedAccount.blogLink',
      'linkedAccount.infoTextTwitter',
      'linkedAccount.infoTextInstagram',
      'linkedAccount.infoTextSnapchat',
      'linkedAccount.infoTextSkype',
      'linkedAccount.infoTextBehance',
      'linkedAccount.infoTextBitbucket',
      'linkedAccount.infoTextDribbble',
      'linkedAccount.infoTextGithub',
      'linkedAccount.numberInfoText',
      'linkedAccount.infoTextYoutube',
      'linkedAccount.infoTextWechat',
      'linkedAccount.infoTextYelp'
      ]

      let placeHolderArray = [
        'linkedAccount.placeHolderHttp',
        'linkedAccount.placeholderHandle',
        'meTab.settingsHandle',
        'constants.mobileNumber',
        'linkedAccount.infoTextUserId'
        ]

    if(titleArray.indexOf(item.title) !== -1){
      item.title = strings(`${item.title}`, {'app': item.type});
    }

    if(informationTextArray.indexOf(item.informationText) !== -1){
      item.informationText = strings(`${item.informationText}`);
    }

    if(placeHolderArray.indexOf(item.placeholder) !== -1){
      item.placeholder = strings(`${item.placeholder}`);
    }
    
    
    item.value = item.link
    if (item.actionType === 'url' || item.actionType === 'username' || item.actionType === 'Handle') {
      this.props.navigation.navigate('EditProfile', item)
    } else if (item.actionType === 'number') {
      this.props.navigation.navigate('EditContactInfo', item)
    } else if (item.actionType === 'integration') {
      if(item.type === 'Facebook') {
        if(!item.link) {
          this.handleFacebookLogin(item)
        } else {
          this.showActionSheet(item)
        }
      } else if (item.type === 'LinkedIn') {
        if (!item.link) {
          this.handleLinkedIn(item)
        } else {
          this.showActionSheet(item)
        }
      } else if (item.type === 'Google+') {
        if (!item.link) {
          this._googleSignIn(item)
        } else {
          this.showActionSheet(item)
        }
      } 
    }
  }

  _googleSignIn = async (item) => {
    this.setState({ loading: true })
    await this._configureGoogleSignIn();
    try {
      const user = await GoogleSignin.signIn();
      console.log('Google User: ', user);
      this.getGoogleAccountDetails(item, user.id, user.accessToken)
    } catch (error) {
      this.setState({ loading: false })
      if (error.code === 'CANCELED') {
        error.message = 'user canceled the login flow';
      }
    }
  }

  _googleSignOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ user: null });
    } catch (error) {
      this.setState({
        error,
      });
    }
  }

  getGoogleAccountDetails(item, googleId, access_token) {
    fetch(`https://www.googleapis.com/plus/v1/people/${googleId}?fields=url&key=AIzaSyBckLH2WPx547m4mfdUAMonID75iAxBvdY`, {
      headers: {
        Accept: 'application/json',
        'Authorization': `Bearer ${access_token}`,
      },
    }).then((response) => response.json())
      .then((responseJson) => {
        if(!responseJson.error) {
          let data = {
            "id": item.id,
            "userId": this.props.user.userId,
            "type": item.type,
            "link": responseJson.url
          }
          this.updateSocialAccount(data)
        } else {
          this.setState({ loading: false })
          this._googleSignOut().done()
          setTimeout(() => {
            let message = (responseJson.error) ? responseJson.error.message : strings('constants.errorMessage')
            alert(message)
          }, 300)
        }
      })
      .catch((error) => {
        this.setState({ loading: false })
        console.error(error);
      });
  }

  async _configureGoogleSignIn() {
    await GoogleSignin.hasPlayServices({ autoResolve: true });
    const configPlatform = {
      ...Platform.select({
        ios: {
          iosClientId: '159114009387-353gghc0vqn7m44jtjifu8m6u469ro1l.apps.googleusercontent.com',
        },
        android: {},
      }),
    };

    await GoogleSignin.configure({
      ...configPlatform,
      webClientId: 'AIzaSyCAcWZUM-KeahV-wu9WinuzGE9MkL1eUTA',
      offlineAccess: false,
    });
  }

  async handleLinkedIn(item) {
    await this.setState({ visiblity: true, linkedinItem: item })
  }

  updateSocialAccount(data) {
    this.setState({ visiblity: false, linkedinItem: null })
    this.props.addSocialMediaDetails(data)
    .then((response) => {
      this.setState({ loading: false })
      if (response.payload.data && response.payload.data.status === 200) {
        this.updateUserLocalSocialMedia(response).done()
      } else if (response.payload.data && response.payload.data.status === 401) {
        this.props.navigation.navigate('ResetAccount')
      } else {
        setTimeout(() => {
          let message = (response.payload.data.message) ? response.payload.data.message : strings('constants.errorMessage')
        }, 300)
      }
    })
  }

  async updateUserLocalSocialMedia(response) {
    let newMedia = response.payload.data.content.socialMediaAccount
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      
      let accounts = (asyncData.data && asyncData.data.content && asyncData.data.content.socialMediaLinks) ? asyncData.data.content.socialMediaLinks : []
      if (accounts.length > 0) {
        let flag = 0
        accounts.map((item) => {
          if (item.type.toLowerCase() === newMedia.type.toLowerCase()) {
            flag = flag + 1
            item.id = newMedia.id
            item.link = newMedia.link
          }
        })
        if (flag === 0) {
          accounts.push(newMedia)
        }
      } else {
        accounts.push(newMedia)
      }
      asyncData.data.content.socialMediaLinks = accounts
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  handleFacebookLogin(item) {
    LoginManager.logInWithReadPermissions(['public_profile', 'email', 'user_link']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          setTimeout(() => {
            this.setState({ loading: true })
            AccessToken.getCurrentAccessToken().then(
              (data) => {
                let accessToken = data.accessToken

                const responseInfoCallback = (error, result) => {
                  if (error) {
                    console.log('Error fetching data: ', error);
                  } else {
                    console.log('FB Result: ', result);

                    var name = result.name;
                    let data = {
                      "id": item.id,
                      "userId": this.props.user.userId,
                      "type": item.type,
                      "link": result.link
                    }
                    this.updateSocialAccount(data)
                  }
                }

                const infoRequest = new GraphRequest(
                  '/me',
                  {
                    accessToken: accessToken,
                    parameters: {
                      fields: {
                        string: 'id,email,name,first_name,middle_name,last_name,friends,link'
                      }
                    }
                  },
                  responseInfoCallback
                );
                new GraphRequestManager().addRequest(infoRequest).start()
              }
            )
          }, 700);
        }
      }.bind(this),
      function (error) {
        console.log('Login fail with error: ', error);
        console.log(error);
      }
    );
  }

  async getLinkedInProfile(access_token) {
    this.setState({ loading: true })
    const response = await fetch(
      `${linkedInBaseApi}~:(${linkedInParams.join(',')})?format=json`,
      {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + access_token
        }
      }
    )
    const payload = await response.json()
    let data = {
      "id": this.state.linkedinItem.id,
      "userId": this.props.user.userId,
      "type": 'LinkedIn',
      "link": payload.publicProfileUrl
    }
    
    this.updateSocialAccount(data)
    
  }

  renderFooter() {
    return (
      <View style={styles.bottomView}>
        <Text style={styles.bottomText}>{strings('meTab.linked_bottomText')}</Text>
        <PhoneShakeButton
          disabled={false}
          onPress={() => this.handleSubmit()}
          text={strings('meTab.setting_linked_request')}
          />
      </View>
    );
  }

  render() {
    const {isRTL} = this.props.user;
    console.log('this.props.user.linkedAccounts', this.props.user.linkedAccounts)
    styles.titleStyle = [styles.titleStyle, {textAlign: (isRTL) ? 'right' : 'left'}]
    styles.titleInfoStyle = [styles.titleInfoStyle, {textAlign: (isRTL) ? 'left' : 'right'}]
    let popularAccount = [];
    let otherAccount = [];
    this.props.user.linkedAccounts.map((item, index) => {
      if(item.popular === true) {
        popularAccount.push(
          <SettingsList.Item
            isRTL={isRTL}
            titleStyle={styles.titleStyle}
            key={index.toString()}
            icon={
              <Image style={styles.socialIcon} source={Icons[item.type]} />
            }
            title={item.type}
            titleInfo={item.link}
            titleInfoStyle={styles.titleInfoStyle}
            onPress={this.onPressSocialItem(item)}
          />
        )
      } else {
        otherAccount.push(
          <SettingsList.Item
            isRTL={isRTL}
            titleStyle={styles.titleStyle}
            key={index.toString()}
            icon={
              <Image style={styles.socialIcon} source={Icons[item.type]} />
            }
            title={item.type}
            titleInfo={item.link}
            titleInfoStyle={styles.titleInfoStyle}
            onPress={this.onPressSocialItem(item)}
          />
        )
      }
    })
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <SettingsList borderColor='#fff' defaultItemSize={47} defaultTitleStyle={styles.title} renderFooter={this.renderFooter} >
            <SettingsList.Header isRTL={isRTL} headerText={strings('constants.mostPopular')} headerStyle={styles.header} />
            
            {popularAccount}

            <SettingsList.Header isRTL={isRTL} headerText={strings('constants.other')} headerStyle={[styles.header, { marginTop: 5 }]} />
            
            {otherAccount}
            
          </SettingsList>
        </View>
        <Loader loading={this.state.loading} />
        <LinkedInModal
          visiblity={this.state.visiblity}
          onRequestClose={() => {}}
          clientID={LINKEDIN_CLIENT_ID} 
          clientSecret={LINKEDIN_CLIENT_SECRET} 
          redirectUri={LINKEDIN_REDIRECT_URL}
          onSuccess={token => this.getLinkedInProfile(token.access_token)}
          onClose={() => { this.setState({ visiblity: false, linkedinItem: null }) }}
        />
      </View>
    );
  }
}

const stateToProps = (state) => ({
  user: state.login
});

const dispatchToProps = (dispatch) => {
  return {
    addSocialMediaDetails: (data) => dispatch(actions.addSocialMediaDetails(data))
  }
}

const Container = connect(stateToProps, dispatchToProps)(LinkedAccount)
export default Container;
