import { StyleSheet } from 'react-native'

import { Metrics, Fonts } from '../../../themes'
export default StyleSheet.create({
  container: {
    flex: 1,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  subContainer: {
    backgroundColor: '#fff',
    flex: 1
  },
  header: {
    marginTop: Metrics.screenHeight * 0.022,
    marginLeft: Metrics.screenWidth * 0.04,
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)'
  },
  label: {
    zIndex: 1,
    fontSize: Metrics.fontSizeSubheading,
    color: 'black',
    backgroundColor: 'transparent'
  },
  headerLeft: {
    height: 24,
    width: 24,
    marginLeft: 12
  },
  title: {
    fontSize: 16,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  titleInfoStyle: {
    fontSize: Metrics.screenWidth * 0.037,
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgb(168, 168, 168)',
    textAlign: 'right',
    width: '60%',
    maxWidth: '60%'
  },
  titleStyle: {
    fontSize: 16,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  titleStyleSecond: { 
    width: '100%',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#000'
  },
  bottomView: {
    height: Metrics.screenHeight * 0.419,
    alignItems: 'center',
    justifyContent: 'center'
  },
  socialIcon: {
    height: Metrics.screenWidth * 0.053,
    width: Metrics.screenWidth * 0.053,
    marginLeft: Metrics.screenWidth * 0.042,
    alignSelf: 'center'
  },
  bottomText: {
    marginTop: Metrics.screenHeight * 0.02,
    fontSize: Metrics.screenWidth * 0.053,
    fontFamily: Fonts.type.SFTextMedium,
    color: '#000',
    textAlign: 'center',
    marginBottom: 23
  }
});
