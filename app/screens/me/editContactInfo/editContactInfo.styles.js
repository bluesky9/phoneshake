import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  headerLeft: {
    height: 24,
    width: 24,
    marginLeft: 12
  }
});
