import React, { Component } from 'react'
import { View, Image, Platform, TouchableOpacity, Keyboard, LayoutAnimation, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../login/login.actions.js';
import Loader from '../../../components/loader/'
import ContactForm from '../../../components/contactForm/contactForm.js'
import PhoneNumber from "../../../../node_modules/react-native-phone-input/lib/phoneNumber";
import { Images, Metrics } from '../../../themes/'
import styles from './editContactInfo.styles'
import moment from 'moment';
import { strings } from '../../../locale/index.js';

class EditContactInfo extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Images.backIcon} style={styles.headerLeft} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      elevation: null
    },
  });

  constructor() {
    super();
    this.state = {
      value: '',
      countryCode: '',
      initialCountry: '',
      keyboardHeight: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.35 : 15,
      loading: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }
    if (this.props.navigation.state.params.value) {
      let number = this.props.navigation.state.params.value
      var value = number.substr(number.length - 10);
      const initialCountry = PhoneNumber.getCountryCodeOfNumber(this.props.navigation.state.params.value);
      this.setState({ value, initialCountry })
    } else {
      this.setState({ value: '' })
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener.remove()
      this.keyboardDidHideListener.remove()
    }
  }

  keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = e.endCoordinates.height
    this.setState({
      keyboardHeight: newSize + Metrics.screenHeight * 0.022
    })
  }

  keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  handleSubmit(countryCode, mobileNo) {
    this.setState({ loading: true })
    let { actionType } = this.props.navigation.state.params
    if (mobileNo) {
      mobileNo = '+' + countryCode + mobileNo
    }
    
    if (actionType) {
      this.updateSocialAccount(mobileNo)
    } else {
      this.updateUserProfile(mobileNo)
    }
  }

  async updateSocialAccount(mobileNo) {
    let { type, id } = this.props.navigation.state.params

    let data = {
      "id": id,
      "userId": this.props.userId,
      "type": type,
      "link": mobileNo
    }
    let startTime = moment()
    await this.props.addSocialMediaDetails(data)
      .then((response) => {
        console.log('response: ', response);
        let data = {
          'startTime': startTime,
          'endTime': moment()
        }
        AsyncStorage.setItem('UpdateLinkAccountApi', JSON.stringify(data));
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          this.updateUserLocalSocialMedia(response).done()
          this.props.navigation.goBack()
        } else if (response.payload.data && response.payload.data.status === 401) {
          this.props.navigation.navigate('ResetAccount')
        } else {
          console.log('response: ', response);
          let message = (response.payload && response.payload.data && response.payload.data.message) ? response.payload.data.message : strings('constants.errorMessage')
          setTimeout(() => {
            alert(message)
          }, 300)
        }
      })
  }

  async updateUserProfile(mobileNo) {
    let { type } = this.props.navigation.state.params
    let data = {
      "userId": this.props.userId,
      "columnName": type,
      "value": mobileNo
    }

    await this.props.updateProfileSettings(data)
      .then((response) => {
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          this.updateUserLocal(response).done()
          this.props.navigation.goBack()
        } else if (response.payload.data && response.payload.data.status === 401) {
          this.props.navigation.navigate('ResetAccount')
        } else {
          setTimeout(() => {
            alert(response.message)
          }, 300)
        }
      })
  }

  async updateUserLocalSocialMedia(response) {
    let newMedia = response.payload.data.content.socialMediaAccount
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      let accounts = (asyncData.data && asyncData.data.content && asyncData.data.content.socialMediaLinks) ? asyncData.data.content.socialMediaLinks : []
      if (accounts.length > 0) {
        let count = 0
        accounts.map((item) => {
          if (item.type.toLowerCase() === newMedia.type.toLowerCase()) {
            count ++;
            item.id = newMedia.id
            item.link = newMedia.link
          }
        })
        if(count === 0 ){
          accounts.push(newMedia)
        }
      } else {
        accounts.push(newMedia)
      }
      asyncData.data.content.socialMediaLinks = accounts
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  async updateUserLocal(response) {
    let key = response.payload.type
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = response.payload.value
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  render() {
    let { title, placeholder, informationText } = this.props.navigation.state.params
    return (
      <View style={{flex: 1}}>
        <Loader loading={this.state.loading} />
        <ContactForm
          isRTL={this.props.isRTL}
          titleText={title}
          initialCountry={this.state.initialCountry}
          placeholder={placeholder}
          phoneNumber={this.state.value}
          keyboardHeight={this.state.keyboardHeight}
          informationText={informationText}
          onChangeText={(value) => this.setState({ value })}
          handleSubmit={this.handleSubmit}
        />
      </View>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId,
  isRTL: state.login.isRTL,
});

const dispatchToProps = (dispatch) => {
  return {
    updateProfileSettings: (data) => dispatch(actions.updateProfileSettings(data)),
    addSocialMediaDetails: (data) => dispatch(actions.addSocialMediaDetails(data))
  }
}
const Container = connect(stateToProps, dispatchToProps)(EditContactInfo)

export default Container;
