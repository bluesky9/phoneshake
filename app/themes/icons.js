
const icons = {
  Location: require('../resources/socialIcons/location.png'),
  Fax: require('../resources/socialIcons/fax.png'),
  Behance: require('../resources/socialIcons/behance.png'),
  Bitbucket: require('../resources/socialIcons/BitbucketIcon.png'),
  Blog: require('../resources/socialIcons/blog.png'),
  Dribbble: require('../resources/socialIcons/dribbble.png'),
  Facebook: require('../resources/socialIcons/facebook.png'),
  Github: require('../resources/socialIcons/github.png'),
  'Google+': require('../resources/socialIcons/googlePlus.png'),
  Instagram: require('../resources/socialIcons/instagram.png'),
  LINE: require('../resources/socialIcons/LineIcon.png'),
  LinkedIn: require('../resources/socialIcons/linkedin.png'),
  Medium: require('../resources/socialIcons/medium.png'),
  Pinterest: require('../resources/socialIcons/pinterest.png'),
  Reddit: require('../resources/socialIcons/reddit.png'),
  Skype: require('../resources/socialIcons/skpye.png'),
  Snapchat: require('../resources/socialIcons/snapchat.png'),
  Telegram: require('../resources/socialIcons/telegramIcon.png'),
  Tumblr: require('../resources/socialIcons/tumblr.png'),
  Twitch: require('../resources/socialIcons/twitch.png'),
  Twitter: require('../resources/socialIcons/twitter.png'),
  Upwork: require('../resources/socialIcons/upwork.png'),
  WeChat: require('../resources/socialIcons/wechatIcon.png'),
  Whatsapp: require('../resources/socialIcons/whatsapp.png'),
  VK: require('../resources/socialIcons/vk.png'),
  Website: require('../resources/socialIcons/www.png'),
  Yelp: require('../resources/socialIcons/yelp.png'),
  Youtube: require('../resources/socialIcons/youtube.png')
}

export default icons
