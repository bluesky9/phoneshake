
const images = {
	defaultUserImage: require('../resources/user.png'),
	appLogo: require('../resources/AppIcon.png'),
	backIcon: require('../resources/backButton.png'),
	moreIcon: require('../resources/more.png'),
	dummyProfile: require('../resources/dummyProfile.png'),
	fbIcon: require('../resources/fb.png'),
	twitterIcon: require('../resources/twitter.png'),
	instagramIcon: require('../resources/insta.png'),
	closeIcon: require('../resources/close.png'),
	shakeCenter: require('../resources/shakeCenter.png'),
	homeActive: require('../resources/homeActive.png'),
	homeInactive: require('../resources/homeInactive.png'),
	contactActive: require('../resources/contactActive.png'),
	contactInactive: require('../resources/contactInactive.png'),
	meActive: require('../resources/meActive.png'),
	backWebIcon: require('../resources/backWebIcon.png'),
	backInactive: require('../resources/backInactive.png'),
	meInactive: require('../resources/meInactive.png'),
	mute: require('../resources/mute.png'),
	notificationInactive: require('../resources/notificationInactive.png'),
	notificationActive: require('../resources/notificationActive.png'),
	messageActive: require('../resources/messageActive.png'),
	messageInactive: require('../resources/messageInactive.png'),
	nextIconInactive: require('../resources/nextButtonInactive.png'),
	nextActive: require('../resources/nextActive.png'),
	uploadIcon: require('../resources/upload.png'),
	reloadIcon: require('../resources/round.png'),
	individualIcon: require('../resources/individual.png'),
	businessIcon: require('../resources/business.png')
}

export default images
