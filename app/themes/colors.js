const colors = {
	blueTheme: 'rgb(32, 142, 251)',
  lightBlueTheme: 'rgba(32, 142, 251, 0.5)',
	red: 'rgb(243, 60, 87)',
	lightBlack: 'rgba(0, 0, 0, 0.5)',
	grey:'rgba(0, 0, 0, 0.2)',
	lightGrey: 'rgb(247, 247, 247)'
}

export default colors
