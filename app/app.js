import React, { Component } from "react";
import { AsyncStorage, YellowBox } from 'react-native'
import { connect } from 'react-redux';
import * as actions from './screens/login/login.actions.js';
import { saveSyncedContacts, saveMyContacts, savePhonebookContacts } from './screens/contacts/contacts.actions';
import Login from './screens/login/login.stack.js';
import { Provider } from 'react-redux';
import { getAllMessages } from './screens/message/message.actions'
import store from './store.js';
import { setLanguage } from './locale/';
YellowBox.ignoreWarnings(['Class RCTCxxModule']);

class App extends Component {
	constructor() {
		super();
		this.state = {
			isReady: false,
			isLogin: false
		};
	}

	componentWillMount() {
		this._loadInitialState().done();
	}

	async componentDidMount(){
		var language = await AsyncStorage.getItem('PhoneShakeLocal')
		if (language !== null) {
			// language = 'en'
			setLanguage(language);
			this.props.setLocal(language);
			Component.forceUpdate();
		} else {
			setLanguage('en');
			this.props.setLocal('en');
			Component.forceUpdate();
		}
	}

	async _loadInitialState() {
		var value = await AsyncStorage.multiGet(['PhoneShakeUser', 'SyncedContact', 'MyContacts', 'PhonebookContacts', 'Messages'])
		
		if (value[0][1] !== null) {
			let userData = JSON.parse(value[0][1]);
			let syncedContacts = (value[1][1] != null) ? JSON.parse(value[1][1]) : []
			let myContacts = (value[2][1] != null) ? JSON.parse(value[2][1]) : []
			let phonebookContacts = (value[3][1] != null) ? JSON.parse(value[3][1]) : []
			let messages = (value[4][1] != null) ? JSON.parse(value[4][1]) : []
			await this.props.resumeUser(userData);
			await this.props.saveSyncedContacts(syncedContacts);
			await this.props.saveMyContacts(myContacts);
			await this.props.savePhonebookContacts(phonebookContacts);
			await this.props.getAllMessages(messages);
			this.setState({ isReady: true, isLogin: true });
		} else {
			this.setState({ isReady: true });
		}
	}

	render() {
		if (!this.state.isReady) {
			return null;
		} else {
			return (
				<Login isLogin={this.state.isLogin} />
			);
		}
	}
}

const dispatchToProps = (dispatch) => {
	return {
		resumeUser: (data) => dispatch(actions.resumeUser(data)),
		setLocal: (local) => dispatch(actions.setLocal(local)),
		saveSyncedContacts: (contacts) => dispatch(saveSyncedContacts(contacts)),
		saveMyContacts: (contacts) => dispatch(saveMyContacts(contacts)),
		savePhonebookContacts: (contacts) => dispatch(savePhonebookContacts(contacts)),
		getAllMessages: (messages) => dispatch(getAllMessages(messages))
	}
}

const AppContainer = connect(null, dispatchToProps)(App)

const Root = () => (
	<Provider store={store}>
		<AppContainer/>
	</Provider>
)

export default Root;
