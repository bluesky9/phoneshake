import React, { Component } from 'react';
import { StyleSheet, Image, Text, Platform, View } from 'react-native';
import { connect } from 'react-redux';
import { Images, Metrics, Fonts } from '../../themes/index.js'
import { ListItem, Left, Body } from 'native-base';

class MyContactItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
      
    };
    this.handleItemPress = this.handleItemPress.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.contact !== this.props.contact) {
      return true
    }
    return false
  }

  handleItemPress = (contact) => {
    this.props.onPress(contact.handle_name, true, contact.usernames[0].username, contact.profile_pic)
  }

  handleItemLongPress = (contact) => {
    this.props.onLongPress(contact)
  }

  render() {
    const { contact } = this.props    
    return (
      <ListItem style={styles.listItem} onPress={() => this.handleItemPress(contact)} onLongPress={() => this.handleItemLongPress(contact)}>
        <Left style={styles.listLeft}>
          <View style={styles.userImage}>
            <Image source={(contact.profile_pic) ? { uri: contact.profile_pic } : Images.defaultUserImage} style={styles.userImage} resizeMode='contain' />
          </View>
        </Left>
        <Body style={styles.listBody}>
          <Text style={styles.listName} numberOfLines={1}>{contact.name}</Text>
          <Text style={styles.listUsername} numberOfLines={1}>{(contact.designation && contact.organization) ? contact.designation + " at " + contact.organization : (contact.designation) ? contact.designation : contact.handle_name}</Text>
          {(contact.description) ? <Text style={styles.listNote} numberOfLines={3}>{contact.description.trim()}</Text> : null }
        </Body>
      </ListItem>
    )
  }
}

const styles = StyleSheet.create({
  listItem: {
    width: Metrics.screenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0.4,
    borderColor: 'rgb(204, 204, 204)',
    paddingHorizontal: Metrics.screenWidth * 0.04,
    marginLeft: 0,
    paddingTop: 5
  },
  userImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: (Platform.OS === 'android') ? Metrics.screenWidth * 0.12 : Metrics.screenWidth * 0.06,
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    width: (Platform.OS === 'ios') ? '65%' : '75%',
    lineHeight: 22
  },
  listUsername: {
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextMedium,
    backgroundColor: 'transparent',
    lineHeight: 14
  },
  listNote: {
    marginTop: 5,
    color: '#000',
    fontSize: Metrics.screenWidth * 0.037,
    fontFamily: Fonts.type.SFTextRegular
  },
  listLeft: {
    height: '100%',
    borderBottomWidth: 0,
    justifyContent: 'flex-start',
    // borderBottomColor: 'rgb(204, 204, 204)'
  },
  listBody: {
    flex: 5,
    height: '100%',
    borderBottomWidth: 0,
    alignItems: 'flex-start'
  },
});

const Container = connect(null, null)(MyContactItem)
export default Container;

