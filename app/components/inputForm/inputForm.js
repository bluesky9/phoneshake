import React, { Component } from 'react'
import { Text, View, TextInput, Platform, Keyboard, ActivityIndicator } from 'react-native';
import { Content, Item } from 'native-base';
import { MaterialIndicator } from 'react-native-indicators';
import { Colors } from '../../themes/'
import styles from'./inputForm.styles'
import PhoneShakeButton from  '../phoneShakeButton/'
import { strings } from '../../locale/index.js';
let Indicator = ActivityIndicator
if (Platform.OS === 'ios') {
	Indicator = MaterialIndicator
}

class OnboardComponent extends Component {
	constructor() {
		super();
		this.state = {
			name: '',
			oldValue: '',
			keyboardHeight: 0
		};
    this.handleSubmit = this.handleSubmit.bind(this);
		this.renderInput = this.renderInput.bind(this);
		this.renderHandleInput = this.renderHandleInput.bind(this);
	}

	componentDidMount() {
		let value = this.props.value
		this.setState({ oldValue: value })
	}

  handleSubmit() {
    Keyboard.dismiss();
    this.props.handleSubmit()
    //this.props.navigation.navigate('HomeScreen')
  }

	renderInput() {
		let isMultiline = this.props.multiline
		let multiLineStyle = (isMultiline) ? [styles.multiline, {textAlign: (this.props.isRTL) ? "right": "left"}] : null
		let multiLineItemStyle = (isMultiline) ? styles.multiLineItem : null
		return(
			<Item style={[styles.itemStyle, multiLineItemStyle]}>
 			 <TextInput
 				 ref={(ref) => this.emailInput = ref }
 				 style={[styles.textInput, multiLineStyle, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10, width:'100%'}]}
 				 placeholder={this.props.placeholder}
 				 value={this.props.value}
 				 keyboardType='default'
 				 placeholderTextColor='rgba(0, 0, 0, 0.5)'
 				 returnKeyType={'go'}
				 autoFocus
				 multiline={isMultiline}
				 maxLength={this.props.maxLength}
 				 autoCorrect={false}
 				 underlineColorAndroid='transparent'
 				 autoCapitalize={this.props.autoCapitalize}
 				 enablesReturnKeyAutomatically
 				 onSubmitEditing={this.handleSubmit}
 				 onChangeText={(text) => this.props.onChangeText(text)}
 				 />
 		 </Item>
 	 );
	}

	renderHandleInput() {
		let textLength = this.props.value.length * 1
		let labelLength = 40 - textLength
		let inputLength = 60 + textLength
	 return(
		 <Item style={styles.itemStyle}>
			 <Text style={[styles.handle, { width:`${labelLength}%` }]}>@</Text>
			 <TextInput
				 ref={(ref) => this.emailInput = ref }
				 style={[styles.textInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10, paddingLeft:0, textAlign:'left', width:`${inputLength}%` }]}
				 placeholder={this.props.placeholder}
				 value={this.props.value}
				 keyboardType='default'
				 placeholderTextColor='rgba(0, 0, 0, 0.5)'
				 returnKeyType={'go'}
				 autoFocus
				 maxLength={20}
				 autoCorrect={false}
				 underlineColorAndroid='transparent'
				 autoCapitalize={'none'}
				 enablesReturnKeyAutomatically
				 onSubmitEditing={this.handleSubmit}
				 onChangeText={(text) => this.props.onChangeText(text)}
				 />
				 {
					 (this.props.fromOnboard && this.props.showLoading) ?
					 <View style={styles.loaderView}>
						 <Indicator
							 size={15}
							 color={Colors.blueTheme}
							 animating={this.props.showLoading} />
					 </View>
						: null
				 }
		 </Item>
	 );
	}

	renderInfoText() {
		if (this.props.multiline) {
			let infoTextStyle = (this.props.name === 'bio') ? [styles.infoText, { textAlign: (this.props.isRTL) ? "left" : 'right' }] : styles.infoText
			return (
				<Text style={infoTextStyle}>
					{`${this.props.informationText} ${this.props.value.length}/150`}
				</Text>
			)
		} else {
			let infoTextStyle = (this.props.isHandleNotUnique || this.props.invalidData) ? [styles.infoText, { color: Colors.red }] : (this.props.titleText === 'Your Bio') ? [styles.infoText, { textAlign: 'right' }] : (this.props.handleAvailable) ? [styles.infoText, { color: Colors.blueTheme }] : styles.infoText
			return (
				<Text style={infoTextStyle}>
					{this.props.informationText}
				</Text>
			)
		}
	}

	render() {
		let disabled = true
		if (this.props.name === 'username' && this.props.fromOnboard) {
			disabled = this.props.value.trim().length < 3 || this.props.isHandleNotUnique || this.state.oldValue === this.props.value
		} else if (this.props.name === 'username') {
			disabled = this.props.isHandleNotUnique || this.state.oldValue === this.props.value
		} else {
			disabled = (this.props.name === 'fullName') ? (this.props.value.trim().length < 1 || this.state.oldValue === this.props.value) : this.state.oldValue === this.props.value
		}
		
    return (
			<View style={{flex:1, backgroundColor:'#fff'}}>
				<Content contentContainerStyle={styles.container} bounces={false} scrollEnabled={false}>
				<Text style={styles.topTitle}>
				{this.props.titleText}
				</Text>
					{(this.props.placeholder === 'Username') ? this.renderHandleInput() : this.renderInput()}
					{this.renderInfoText()}
				<PhoneShakeButton
					disabled={disabled}
					onPress={() => this.handleSubmit()}
					text={strings('constants.continue')}
					bottomHeight={this.props.keyboardHeight}/>
      			</Content>
		</View>
    );
	}
}



export default OnboardComponent;
