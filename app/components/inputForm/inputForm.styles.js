import { StyleSheet, Dimensions, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
	container: {
		flex: 1,
    paddingTop: Metrics.screenHeight * 0.015,
		alignItems: 'center',
		backgroundColor: '#fff',
	},
  topTitle: {
    fontSize: Metrics.screenWidth * 0.053,
    textAlign: 'center',
    paddingBottom: Metrics.screenHeight * 0.029,
		fontFamily: Fonts.type.SFTextMedium,
		color: '#000'
  },
  infoText: {
		marginTop: 7,
    fontSize: Metrics.screenWidth * 0.032,
    textAlign: 'center',
		fontFamily: Fonts.type.SFTextRegular,
		color: 'rgba(0, 0, 0, 0.34)',
		width: '75%'
  },
	itemStyle: {
		width: '75%',
		height: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.052: Metrics.screenHeight * 0.07,
		borderBottomWidth: 0.6,
		borderBottomColor: 'rgba(0, 0, 0, 0.2)',
		paddingHorizontal: 15,
		marginTop: Metrics.screenHeight * 0.082
	},
	linkButton: {
		alignSelf: 'center',
		marginBottom: Metrics.screenHeight * 0.105
	},
	textInput: {
		width:'100%',
		paddingLeft:0,
		fontSize: Metrics.screenWidth * 0.048,
		fontFamily: Fonts.type.SFTextRegular,
		paddingLeft: 0,
		textAlign: 'center',
		color: '#000'
	},
	handle: {
		fontSize: Metrics.screenWidth * 0.048,
		fontFamily: Fonts.type.SFTextRegular,
		textAlign: 'right',
		marginRight: 5,
		color: '#000'
	},
	multiline: {
		height: Metrics.screenHeight * 0.135,
		fontSize: Metrics.screenWidth * 0.037,
		textAlign: 'left',
	},
	multiLineItem: {
		height: Metrics.screenHeight * 0.135
	},
	loaderView: {
		height: 40,
		width: 40,
		alignItems: 'center',
		justifyContent: 'center',
		position: 'absolute',
		right: 0,
		backgroundColor: 'transparent'
	}
});
