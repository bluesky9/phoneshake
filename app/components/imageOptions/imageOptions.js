import React, { Component } from 'react';
import { View, Text, StyleSheet, } from 'react-native';
import Modal from "react-native-modal";
import modalStyle from '../../screens/me/signupInfo/signupInfo.styles';
import { Button } from 'native-base';
import { Metrics } from '../../themes/'

export default class ImageOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
    this.renderContent = this.renderContent.bind(this);
  }


  renderContent() {
    const { user, type } = this.props
    var imageOptions = []
    if(user.profilePic){
      imageOptions.push(
        <Button key={'removePhoto'} block transparent style={modalStyle.modalRemoveButton} onPress={() => this.props.removeImage(type)} >
          <Text style={modalStyle.distructiveText}>Remove Current Photo</Text>
        </Button>
      )
    }

    return (
      <View style={styles.modalContainer}>
        <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.075 * (imageOptions.length + 4) }]}>
          <View style={[styles.modalActionSheet]}>
            <View style={[modalStyle.titleView, { height: Metrics.screenHeight * 0.074 }]}>
              <Text style={modalStyle.alertDescription} numberOfLines={1}>{'Change Profile Photo'}</Text>
            </View>
            {imageOptions}
            <Button block transparent style={modalStyle.modalRemoveButton} onPress={() => this.props.openCamera(type)}>
              <Text style={modalStyle.regularText}>Take Photo</Text>
            </Button>
            <Button block transparent style={modalStyle.modalRemoveButton} onPress={() => this.props.openGallery(type)}>
              <Text style={modalStyle.regularText}>Choose from Library</Text>
            </Button>
            <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.props.setModalVisible(false)}>
              <Text style={modalStyle.cancelText}>Cancel</Text>
            </Button>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        isVisible={this.props.visible}
        backdropOpacity={0.5}
        style={{ margin: 0 }}
      >
        {this.renderContent()}
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});