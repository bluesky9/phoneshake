import { StyleSheet, Platform } from 'react-native'
import { Metrics, Colors, Fonts } from '../../themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  settingButton: {
    position: 'absolute',
    right: 5,
    top: 5
  },
  settingIcon: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.048,
  },
  topView: {
    paddingBottom: 12,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(247, 247, 247)',
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgb(204, 204, 204)'
  },
  btnImg: {
    backgroundColor: Colors.grey,
    height: Metrics.screenWidth * 0.266,
    width: Metrics.screenWidth * 0.266,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Metrics.screenWidth * 0.266 / 2,
    marginTop: Metrics.screenHeight * 0.06,
    borderWidth: Metrics.screenWidth * 0.006,
    borderColor: '#fff',
    shadowColor: '#000',
    shadowRadius: Metrics.screenWidth * 0.008,
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 1 },
  },
  txtImage: {
    fontSize: Metrics.screenWidth * 0.042,
    color: 'white',
    fontFamily: Fonts.type.SFTextRegular
  },
  btnLogo: {
    position: 'absolute',
    backgroundColor: Colors.blueTheme,
    height: Metrics.screenWidth * 0.093,
    width: Metrics.screenWidth * 0.093,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Metrics.screenWidth * 0.093 / 2,
    zIndex: 11111,
    top: (Metrics.screenHeight > 667) ? Metrics.screenHeight * 0.135 : Metrics.screenHeight * 0.15,
    right: Metrics.screenWidth * 0.35, //Metrics.screenWidth * 0.173,
  },
  txtLogo: {
    fontSize: Metrics.screenWidth * 0.026,
    color: 'white',
    fontFamily: Fonts.type.SFTextRegular
  },
  socialSection: {
    width: '100%',
    paddingTop: Metrics.screenWidth * 0.042,
    paddingLeft: Metrics.screenWidth * 0.053,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  addButton: {
    height: Metrics.screenWidth * 0.18,
    width: Metrics.screenWidth * 0.18,
    backgroundColor: 'rgb(247, 247, 247)',
    justifyContent: 'center',
    borderRadius: Metrics.screenWidth * 0.01
  },
  add: {
    fontSize: Metrics.screenWidth * 0.074,
    color: '#000'
  },
  username: {
    fontSize: Metrics.screenWidth * 0.053,
    color: '#000',
    fontFamily: Fonts.type.SFTextSemibold,
    marginTop: Metrics.screenHeight * 0.017
  },
  orgnization: {
    fontSize: Metrics.screenWidth * 0.032,
    color: 'rgb(127, 127, 127)',
    fontFamily: Fonts.type.SFTextMedium,
    marginTop: 3
  },
  socialText: {
    fontSize: Metrics.screenWidth * 0.026,
    color: 'rgb(127, 127, 127)',
    fontFamily: Fonts.type.SFTextMedium,
    textAlign: 'center'
  },
  socialItem: {
    height: Metrics.screenHeight * 0.131,
    justifyContent: 'space-between',
    marginRight: Metrics.screenWidth * 0.053,
    marginBottom: Metrics.screenHeight * 0.029
  },
  scrollView: {
    height: Metrics.screenHeight - Metrics.screenHeight * 0.335,
    width: '100%'
  },
  socialIcon: {
    height: Metrics.screenWidth * 0.08,
    width: Metrics.screenWidth * 0.08,
    alignSelf: 'center'
  },
  description: {
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032,
    color: '#000',
    marginTop: 5,
    marginHorizontal: Metrics.screenWidth * 0.042,
    textAlign: 'center'
  }
});
