import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView, Platform, AsyncStorage, Linking, Alert, Clipboard, ActivityIndicator } from 'react-native';
import { Button, Icon } from "native-base";
import { connect } from 'react-redux';
import SendBird from 'sendbird';
import Permissions from 'react-native-permissions'
import * as actions from '../../screens/login/login.actions';
import ImagePicker from 'react-native-image-crop-picker';
import { MaterialIndicator } from 'react-native-indicators';
import ActionSheet from '@yfuks/react-native-action-sheet';
import { Icons, Metrics, Colors } from "../../themes/";
import Loader from '../loader/'
import ImageOptions from '../imageOptions/imageOptions'
import PhoneShakeWebView from '../phoneShakeWebView/phoneShakeWebView'
import styles from './userProfile.styles'
let Indicator = ActivityIndicator
if (Platform.OS === 'ios') {
  Indicator = MaterialIndicator
}
const imageOptions = {
  width: 200,
  height: 200,
  compressImageQuality: 0.3,
  includeBase64: true,
  mediaType: 'photo',
  cropperCircleOverlay: true, //ios only
  cropping: true
};

class Profile extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    // headerStyle: { marginTop: 24 }
  });

  constructor() {
    super();
    this.state = {
      pickedImage: '',
      pickedLogo: '',
      loading: false,
      isWebView: false,
      linkUrl: 'http://google.com',
      imageOptionVisible: false,
      type: 'userImage'
    }
    this.renderSocialAccounts = this.renderSocialAccounts.bind(this);
    this.gotoLinkedAccounts = this.gotoLinkedAccounts.bind(this);
    this.showActionSheet = this.showActionSheet.bind(this);
    this.uploadProfileImageOnServer = this.uploadProfileImageOnServer.bind(this);
    this.updateProfilePic = this.updateProfilePic.bind(this);
    this.removeImage = this.removeImage.bind(this);
    this.openCamera = this.openCamera.bind(this);
    this.openGallery = this.openGallery.bind(this);
    this.renderImageOptions = this.renderImageOptions.bind(this);   
    this.setModalVisible = this.setModalVisible.bind(this);
  }

  componentDidMount() {
    if (this.props.user.profilePic || this.props.user.businessLogo){
      this.setState({
        pickedImage: (this.props.user.profilePic) ? this.props.user.profilePic : '',
        pickedLogo: (this.props.user.businessLogo) ? this.props.user.businessLogo : '',
      })
    }

    if (this.props.user.profile_pic || this.props.user.business_logo) {
      this.setState({
        pickedImage: (this.props.user.profile_pic) ? this.props.user.profile_pic : '',
        pickedLogo: (this.props.user.business_logo) ? this.props.user.business_logo : '',
      })
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.user.profilePic || newProps.user.businessLogo) {
      this.setState({
        pickedImage: (newProps.user.profilePic) ? newProps.user.profilePic : '',
        pickedLogo: (newProps.user.businessLogo) ? newProps.user.businessLogo : '',
      })
    }

    if (newProps.user.profile_pic || newProps.user.business_logo) {
      this.setState({
        pickedImage: (newProps.user.profile_pic) ? newProps.user.profile_pic : '',
        pickedLogo: (newProps.user.business_logo) ? newProps.user.business_logo : '',
      })
    }
  }

  removeImage = (type) => {
    this.setState({ imageOptionVisible: false })
  }

  openCamera = (type) => {
    this.setState({ imageOptionVisible: false })
    setTimeout(() => {
      if (this.checkPermission('camera')) {
        ImagePicker.openCamera(imageOptions).then(image => {
          this.updateProfilePic(image, type);
        }).catch(error => {
          console.log(error)
        });
      }
    }, 400);
  }

  openGallery = (type) => {
    this.setState({ imageOptionVisible: false })
    setTimeout(() => {
      if (this.checkPermission('photo')) {
        ImagePicker.openPicker(imageOptions).then(image => {
          this.updateProfilePic(image, type);
        }).catch(error => {
          console.log(error)
        });
      }
    }, 500);
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if(nextProps.user === this.props.user || nextState.pickedImage === this.state.pickedImage
  //     || nextState.pickedLogo === this.state.pickedLogo ) {
  //     return false
  //   }
  //   return true
  // }

  gotoLinkedAccounts = () => () => {
    this.props.navigation.navigate('LinkedAccount')
  }

  handleNumber(item) {
    if (item === 'fax') {
      Alert.alert(
        this.props.user.fax,
        '',
        [
          { text: 'Cancel', onPress: () => { }, style: 'cancel' },
          { text: 'Copy', onPress: async () => await Clipboard.setString(this.props.user.fax) },
        ],
        { cancelable: true }
      )
    } else {
      Alert.alert(
        item.link,
        '',
        [
          { text: 'Cancel', onPress: () => { }, style: 'cancel' },
          { text: 'Copy', onPress: async () => await Clipboard.setString(item.link) },
        ],
        { cancelable: true }
      )
    }
  }

  openFacebook(item) {
    this.setState({ isWebView: true, linkUrl: item.link })
    // Linking.canOpenURL('fb://').then(supported => {
    //   return supported ? Linking.openURL('http://www.facebook.org/info.prakashsaini') : this.setState({ isWebView: true, linkUrl: 'https://www.facebook.com/'})
    //  }).catch(err => console.error('An error occurred', err));
  }

  handleUsernames(item) {
    if (item.type === 'LINE') {
      this.handleNumber(item)
      // let url = `https://line.me/R/ti/p/@${item.link}`
      // let url = `line://home/public/profile/id=prakash.saini`
      
      // let url = `line://nv/addFriends`
      // let url = `line://oaMessage/@${item.link}/?test`
      // console.log('Url: ', url);
      // Linking.openURL(url)
    } else if (item.type === 'Skype') {      
      Linking.canOpenURL('skype://').then(supported => {
      return supported ? Linking.openURL(`skype:${item.link}?chat`) : this.setState({isWebView: true, linkUrl: `https://www.skype.com/${item.link}?chat`})
     }).catch(err => console.error('An error occurred', err));
    } else if (item.type === 'WeChat') {
      Linking.canOpenURL('weixin://').then(supported => {
      return supported ? Linking.openURL(`weixin://dl/chat?${item.link}`) : alert('WeChat is not installed on your device.')
     }).catch(err => console.error('An error occurred', err));
    } else if (item.type === 'Instagram') {
      Linking.canOpenURL('instagram://').then(supported => {
      return supported ? Linking.openURL(`instagram://user?username=${item.link}`) : this.setState({isWebView: true, linkUrl: `https://instagram.com/${item.link}`})
     }).catch(err => console.error('An error occurred', err));
    } else if (item.type === 'Twitter') {
      Linking.canOpenURL('twitter://').then(supported => {
      return supported ? Linking.openURL(`twitter://user?screen_name=${item.link}`) : this.setState({isWebView: true, linkUrl: `https://twitter.com/${item.link}`})
     }).catch(err => console.error('An error occurred', err));
    } else if (item.type === 'Snapchat') {
      Linking.canOpenURL('snapchat://').then(supported => {
        return supported ? Linking.openURL(`snapchat://add/${item.link}`) : this.setState({isWebView: true, linkUrl: `https://www.snapchat.com/add/${item.link}`})
       }).catch(err => console.error('An error occurred', err));
    }
  }
  
  onSocialAccountPress = (item) => () => {
    if (item === 'address' && this.props.openLocation) {
      this.props.openLocation()
    } else if (item.actionType === 'url') { 
      let link = (item.link.indexOf('://') === -1) ? 'http://' + item.link : item.link;
      this.setState({isWebView: true, linkUrl: link})
      // Linking.openURL(link)
    } else if (item.actionType === 'number' || item === 'fax') {
      if (item.type === 'Whatsapp') {
        Linking.canOpenURL(`whatsapp://send?phone=${item.link}`).then(supported => {
          if (!supported) {
            console.log('Can\'t handle url: ' + item.link);
          } else {
            return Linking.openURL(`whatsapp://send?phone=${item.link}`);
          }
        }).catch(err => console.error('An error occurred', err));
      } else {
        this.handleNumber(item)
      }
    } else if (item.actionType === 'username' || item.actionType === 'Handle') {
      this.handleUsernames(item)
    } else if (item.actionType === 'integration') {
      if (item.type === 'Facebook') {
        this.openFacebook(item)
      } else if (item.type === 'LinkedIn') {
        Linking.canOpenURL('linkedin://').then(supported => {
          return supported ? Linking.openURL(item.link) : this.setState({ isWebView: true, linkUrl: item.link })
        }).catch(err => console.error('An error occurred', err));
      } else {
        // Linking.openURL(item.link)
        this.setState({isWebView: true, linkUrl: item.link})
      }
    }
  }

  renderSocialAccounts() {
    var accounts = [];
    if(this.props.loading) return null;
    if (this.props.user.address) {
      accounts.push(
        <View key={'address'} style={styles.socialItem}>
          <Button transparent style={styles.addButton} onPress={this.onSocialAccountPress('address')}>
            <Image style={styles.socialIcon} source={Icons.Location} />
          </Button>
          <Text style={styles.socialText}>Location</Text>
        </View>
      )
    }
    if (this.props.user && this.props.user.linkedAccounts && this.props.user.linkedAccounts.length) {
      this.props.user.linkedAccounts.map((account, index) => {
        if (account.link !== '')
          accounts.push(
            <View key={index.toString()} style={styles.socialItem}>
              <Button transparent style={styles.addButton} onPress={this.onSocialAccountPress(account)}>
                <Image style={styles.socialIcon} source={Icons[account.type]} />
              </Button>
              <Text style={styles.socialText}>{account.type}</Text>
            </View>
          )
      })
    }

    if (this.props.user.fax) {
      accounts.push(
        <View key={'fax'} style={styles.socialItem}>
          <Button transparent style={styles.addButton} onPress={this.onSocialAccountPress('fax')}>
            <Image style={styles.socialIcon} source={Icons.Fax} />
          </Button>
          <Text style={styles.socialText}>Fax</Text>
        </View>
      )
    }
    if(this.props.me) {
      accounts.push(
        <View key={'add'} style={styles.socialItem}>
          <Button transparent style={styles.addButton} onPress={this.gotoLinkedAccounts()}>
            <Icon name='md-add' style={styles.add} />
          </Button>
          <Text style={styles.socialText}>Add</Text>
        </View>
      )
    }
    
    return accounts;
  }

  renderImageOptions() {
    if (this.props.me) {
      return (
        <ImageOptions
          key={'image-options'}
          visible={this.state.imageOptionVisible}
          removeImage={(type) => this.removeImage(type)}
          openCamera={(type) => this.openCamera(type)}
          openGallery={(type) => this.openGallery(type)}
          setModalVisible={(value) => this.setState({ imageOptionVisible: false })}
          user={this.props.user}
          type={this.state.type}
          />
      )
   }
  }

  

  setModalVisible(value){    
    this.setState({isWebView: false})
  }

  checkPermission(type) {
   return Permissions.check(type).then(response => {
      if (response === 'authorized') {
        return true
      } else if(response === 'denied') {
        let accessType = (type === 'photo') ? 'Photos' : 'Camera'
        let msg = (type === 'photo') ? `Tap "Go to Settings" > ${accessType} > Check Read and Write` : `Tap "Go to Settings" > ${accessType} > Toggle on ${accessType}`
        Alert.alert(
          `Allow "PhoneShake" to access your ${accessType} in Settings`,
          msg,
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'Open Settings', onPress: () => Permissions.openSettings() }
          ],
          { cancelable: false }
        )
        return false
      } else if(response != 'authorized') {
        Permissions.request(type).then(userResponse => {
          // console.log('Permission: ', userResponse);
          if (userResponse === 'authorized') {
            return true
          }
        })
        return false
      }
    })
  }

  showActionSheet = (type) => () => {
    if(this.props.me) {
      ActionSheet.showActionSheetWithOptions({
      options: ['Camera', 'Gallery', 'Cancel'],
      cancelButtonIndex: 2,
      DESTRUCTIVE_INDEX: 2,
      tintColor: 'rgb(0,122, 255)'
    },
      (buttonIndex) => {

        switch (buttonIndex) {
          case 1:
            if (this.checkPermission('photo')) {
              ImagePicker.openPicker(imageOptions).then(image => {
                this.updateProfilePic(image, type);
              }).catch(error => {
                console.log(error)
              });
            }
            break;
          case 0:
            if(this.checkPermission('camera')) {              
              ImagePicker.openCamera(imageOptions).then(image => {
                this.updateProfilePic(image, type);
              }).catch(error => {
                console.log(error)
              });
            }
            break;
          default:
            break;
        }
      });
    }
  }

  updateProfilePic(response, type) {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      let source;
      let data = response.data;
      if (Platform.OS === 'ios') {
        source = { uri: response.path.replace('file://', ''), data: data, isStatic: true };
      } else {
        source = { uri: response.path, data: data, isStatic: true };
      }
      if (type === 'logo') {
        this.setState({
          pickedLogo: source.uri
        });
      } else {
        this.setState({
          pickedImage: source.uri
        });
      }
      //update image as soon as choosen
      this.uploadProfileImageOnServer(source.data, type)
    }
  }

  uploadProfileImageOnServer(imageData, type) {
    this.setState({ loading: true })
    let data = {
      "userId": this.props.user.userId,
      "fileName": this.props.user.userId + '.png',
      "base64": imageData.toString()
    }
    
    this.props.uploadPicture(data, type)
      .then((response) => {
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          if (response.payload.data.content && response.payload.data.content.user) {
            this.updateUserLocal(response, type).done()
            if (type !== 'logo') {
              this.updateImageOnSendBird(response).done()
            }
          }
        } else {
          setTimeout(() => {
            alert('Error while uploading image. Please try again')
          }, 300);
        }
      })
  }

  async updateUserLocal(response, type) {
    let key = (type === 'logo') ? 'business_logo' : 'profile_pic'
    let userImage = response.payload.data.content.user[key]
    var value = await AsyncStorage.getItem('PhoneShakeUser')
    if (value !== null) {
      let asyncData = JSON.parse(value);
      asyncData.data.content[key] = userImage
      AsyncStorage.setItem('PhoneShakeUser', JSON.stringify(asyncData));
    }
  }

  async updateImageOnSendBird(response) {
    const sb = SendBird.getInstance()
    if(sb) {
      let profileUrl = response.payload.data.content.user['profile_pic']
      sb.updateCurrentUserInfo(this.props.user.name, profileUrl, function (response, error) {
        if(error) {
          console.log('Sendbird Image upload error', error);
        }
      });
    }
  }

  renderTopButtons() {
    if(this.props.me) {
      return (
        <Button transparent style={styles.settingButton} onPress={() => this.props.navigation.navigate('SettingOptions')} >
          <Icon name='ios-settings' style={styles.settingIcon} />
        </Button>
      )
    }
  }

  renderLoader() {
    if(!this.props.loading) return null;
    return(
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Indicator
          size={35}
          color={Colors.blueTheme}
          animating={this.props.loading} />
      </View>
    )
  }

  render() {
    let position = (this.props.user.designation && this.props.user.organization) ? this.props.user.designation + " at " + this.props.user.organization : (this.props.user.designation) ? this.props.user.designation : ''
    let activeOpacity = (this.props.me) ? 0.8 : 1.0
    return (
      <View style={styles.container}>
        <Loader loading={this.state.loading} /> 
        <PhoneShakeWebView          
          visible={this.state.isWebView}
          setModalVisible= {this.setModalVisible}
          linkUrl={this.state.linkUrl}
        />      
        <View style={styles.topView}>
          <TouchableOpacity activeOpacity={activeOpacity} onPress={this.showActionSheet('userImage')}>
            <View style={styles.btnImg}>
              {(this.state.pickedImage === '') ?
                <Text style={styles.txtImage}>Image</Text>
                :
                <Image source={{ uri: this.state.pickedImage }}
                  style={{
                    height: Metrics.screenWidth * 0.255,
                    width: Metrics.screenWidth * 0.255,
                    borderRadius: Metrics.screenWidth * 0.255 / 2,
                  }} />}
            </View>
          </TouchableOpacity>
          {
            (this.props.me || (!this.props.me && this.state.pickedLogo !== '')) ?
              <TouchableOpacity activeOpacity={activeOpacity} onPress={this.showActionSheet('logo')} style={styles.btnLogo}>
                <View >
                  {(this.state.pickedLogo === '') ?
                    <Text style={styles.txtLogo}>Logo</Text>
                    :
                    <Image source={{ uri: this.state.pickedLogo }}
                      style={{
                        height: Metrics.screenWidth * 0.093,
                        width: Metrics.screenWidth * 0.093,
                        borderRadius: Metrics.screenWidth * 0.093 / 2,
                      }} />}
                </View>
              </TouchableOpacity>
            :
            null
          }
          <Text style={styles.username} >{this.props.user.name}</Text>
          <Text style={styles.orgnization} >{position}</Text>
          <Text style={styles.description} numberOfLines={3} >{this.props.user.description}</Text>
          {this.renderTopButtons()}
        </View>
        {this.renderLoader()}
        <ScrollView style={styles.scrollView}>
          <View style={styles.socialSection} >
            { this.renderSocialAccounts() }
          </View>
        </ScrollView>
        
        {/*this.renderImageOptions()*/}
      </View>
    );
  }
}

const stateToProps = (state) => ({
  userId: state.login.userId
});

const dispatchToProps = (dispatch) => {
  return {
    uploadPicture: (data, type) => dispatch(actions.uploadPicture(data, type))
  }
}

const Container = connect(stateToProps, dispatchToProps)(Profile)
export default Container;