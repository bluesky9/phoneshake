import React, { Component } from 'react'
import { Text, View, Platform, Keyboard } from 'react-native';
import { Content, Item } from 'native-base';
import { Colors, Metrics, Fonts } from '../../themes/'
import PhoneInput from 'react-native-phone-input';
import { TextInputMask } from 'react-native-masked-text'
import styles from './contactForm.styles'
import PhoneShakeButton from '../phoneShakeButton/'
import { strings } from '../../locale/index.js';

class OnboardComponent extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      contryCodeItemWidth: Metrics.screenWidth * 0.213,
      initialCountry: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderInput = this.renderInput.bind(this);
    this.onSelectCountry = this.onSelectCountry.bind(this);
    this.checkText = this.checkText.bind(this);
  }

  handleSubmit() {
    Keyboard.dismiss();
    var countryCode = this.phone.getCountryCode()
    var mobileNo = this.phoneInput.getRawValue()
    this.props.handleSubmit(countryCode, mobileNo)
  }

  onSelectCountry(iso2) {
    var countryCode = this.phone.getCountryCode()
    if (countryCode.length > 2) {
      this.setState({ contryCodeItemWidth: Metrics.screenWidth * 0.25 })
    } else {
      this.setState({ contryCodeItemWidth: Metrics.screenWidth * 0.213 })
    }
  }

  checkText(previous, next){
    console.log('previous', previous)
    console.log('next', next)
    var regex = /^[0-9]+$/;
    console.log('next.match(regex)', next.match(regex))
    return next === 1
  }

  renderInput() {
    const {isRTL} = this.props;
    return (
      <Item style={styles.itemStyle}>
        <PhoneInput
          ref={(ref) => {
            this.phone = ref;
          }}
          textStyle={{ paddingBottom: (Platform.OS === 'ios') ? 0 : 3, color: Colors.blueTheme }}
          initialCountry={this.props.initialCountry || 'us'} //us
          textProps={{
            editable: false,
            fontSize: Metrics.screenWidth * 0.048,
            fontFamily: Fonts.type.medium,
            marginTop: 3,
            height: 45
          }}
          offset={5}
          isoStyle={{ fontSize: Metrics.screenWidth * 0.048, fontFamily: Fonts.type.SFTextRegular, color: Colors.blueTheme }}
          pickerButtonColor='rgb(70,70,70)'
          pickerButtonTextStyle={{ fontSize: 16, fontFamily: Fonts.type.SFTextRegular }}
          style={{ paddingLeft: 0, width: this.state.contryCodeItemWidth, paddingTop: -1 }}
          onSelectCountry={this.onSelectCountry}
        />

        <TextInputMask
          ref={(ref) => this.phoneInput = ref}
          placeholder={this.props.placeholder || strings('constants.mobileNumber')}
          placeholderTextColor={'rgba(0, 0, 0, 0.5)'}
          maxLength={16}
          autoFocus
          underlineColorAndroid='transparent'
          keyboardType='numeric'
          value={this.props.phoneNumber}
          type={'cel-phone'}
          options={{
            dddMask: '999 - 999 - 9999',
          }}
          style={[styles.phoneTextInput, { paddingBottom: (Platform.OS === 'ios') ? 0 : 10, width: (this.props.isRTL === true) ? '55%' : "100%", textAlign: (this.props.isRTL === true) ? "center" : "left" }]}
          onChangeText={(text) => this.props.onChangeText(text)} />
      </Item>
    );
  }

  render() {
    let disabled = (this.props.phoneNumber.length < 15 && this.props.phoneNumber.length > 0)
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <Content contentContainerStyle={styles.container} bounces={false} scrollEnabled={false}>
          <Text style={styles.topTitle}>
            {this.props.titleText}
          </Text>

          {this.renderInput()}

          <Text style={styles.infoText}>
            {this.props.informationText}
          </Text>

          <PhoneShakeButton
            disabled={disabled}
            onPress={() => this.handleSubmit()}
            text={strings('constants.continue')}
            bottomHeight={this.props.keyboardHeight} />

        </Content>
      </View>
    );
  }
}



export default OnboardComponent;
