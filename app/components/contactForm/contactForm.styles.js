import { StyleSheet, Dimensions, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Metrics.screenHeight * 0.015,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  topTitle: {
    fontSize: Metrics.screenWidth * 0.053,
    textAlign: 'center',
    paddingBottom: Metrics.screenHeight * 0.029,
    fontFamily: Fonts.type.SFTextMedium,
    color: '#000'
  },
  infoText: {
    marginTop: 7,
    fontSize: Metrics.screenWidth * 0.032,
    textAlign: 'center',
    fontFamily: Fonts.type.SFTextRegular,
    color: 'rgba(0, 0, 0, 0.34)'
  },
  itemStyle: {
    width: '75%',
    height: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.052 : Metrics.screenHeight * 0.07,
    borderBottomWidth: 0.6,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: Metrics.screenWidth * 0.04,
    marginTop: Metrics.screenHeight * 0.082,
    // justifyContent: 'center'
  },
  phoneTextInput: {
    width: '55%',
    paddingLeft: 0,
    fontSize: Metrics.screenWidth * 0.048,
    fontFamily: Fonts.type.SFTextRegular,
    textAlign: 'center',
    // backgroundColor: '#ccc'
  }
});
