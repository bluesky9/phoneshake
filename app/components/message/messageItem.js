import React, { Component } from 'react';
import { StyleSheet, Image, Text, Platform, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash'
import moment from 'moment'
import SendBird from 'sendbird';
import { SENDBIRD_TOKEN } from '../../api/constants'
import { Metrics, Fonts, Images, Colors } from '../../themes/index.js'
const sb = SendBird.getInstance();

class MessageItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
      is_muted: false
    };
    this.ChannelHandler = new sb.ChannelHandler();
    this.gotoChat = this.gotoChat.bind(this);
  }

  componentWillMount() {
    if(this.props.connectionStatus === 'connected')
    this.checkForMute()
  }

  componentWillReceiveProps(newProps) {
    if (newProps.connectionStatus === 'connected') {
      this.checkForMute()
    }
  }

  componentDidMount() {
    
    var _this = this;
    let { item } = this.props;
    this.ChannelHandler.onChannelChanged = function (channel) {
      if (channel.url === _this.props.item.url) {
        _this.props._connect()
      }
    };

    this.ChannelHandler.onUserMuted = function (channel, user) {
      if (channel.url === _this.props.item.url) {
        _this.setState({ is_muted: true })
        _this.props._connect()
      }
    };

    this.ChannelHandler.onUserUnmuted = function (channel, user) {
      if (channel.url === _this.props.item.url) {
        _this.setState({ is_muted: false })
        _this.props._connect()
      }
    };

    sb.addChannelHandler(item.url, this.ChannelHandler);
    
  }

  checkForMute() {
    fetch(`https://api.sendbird.com/v3/group_channels/${this.props.item.url}/mute/${this.props.user.userId}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Api-Token': SENDBIRD_TOKEN,
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(!responseJson.error)
      this.setState({ is_muted: responseJson.is_muted })
    })
    .catch((error) => {
      console.error(error);
      return;
    });
  }

  componentWillUnmount() {
    let { item } = this.props;
    sb.removeChannelHandler(item.url);
  }

  gotoChat = (item, user) => {
    this.props.onPress(item, user)
  }

  render() {
    let { item , user } = this.props;
    let createdAt = (item.lastMessage && item.lastMessage.createdAt) ? item.lastMessage.createdAt : null
    
    return (
      <TouchableOpacity activeOpacity={0.7} style={styles.listItem} onPress={() => this.gotoChat(item, user)}>
        <View style={styles.listLeft}>
          <Image source={(user.profileUrl && user.profileUrl.search('sendbird.com') === -1) ? { uri: user.profileUrl } : Images.defaultUserImage} style={styles.userImage} />
        </View>
        <View style={styles.listBody}>
        <View style={styles.nameView}>
          <Text style={styles.listName} numberOfLines={1} >{user.nickname}</Text>
            {(this.state.is_muted) ? <Image source={Images.mute} style={styles.mute} resizeMode='contain' /> : null}
        </View>
          <Text style={styles.listNote} numberOfLines={1}>{(item.lastMessage && item.lastMessage.message) ? item.lastMessage.message : null}</Text>
          {/* <View style={styles.separator} /> */}
        </View>
        <View style={styles.listRight}>
          {
            (createdAt) ?
              <Text style={styles.listTime}>
                {
                  moment(createdAt).calendar(null, {
                    lastDay: '[Yesterday]',
                    sameDay: 'h:mm A',
                    lastWeek: 'ddd',
                    sameElse: 'MM/DD/YY'
                  })
                }
              </Text>
              : null
          }
          {
            (item.unreadMessageCount > 0) ?
              <View style={styles.badge}>
                <Text style={styles.badgeCount}>{item.unreadMessageCount}</Text>
              </View>
              : null
          }
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  listItem: {
    width: Metrics.screenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0,
    borderColor: 'rgb(204, 204, 204)',
    paddingLeft: Metrics.screenWidth * 0.024,
    marginLeft: 0,
    paddingTop: 0,
    // backgroundColor: 'red',
    height: 70
  },
  nameView: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: (Platform.OS === 'ios') ? '90%' : '75%',
    // backgroundColor: 'red'
  },
  mute: {
    height: 15,
    width: 15,
    marginLeft: 3
  },
  userImage: {
    height: Metrics.screenWidth * 0.13,
    width: Metrics.screenWidth * 0.13,
    borderRadius: (Platform.OS === 'android') ? Metrics.screenWidth * 0.13 : Metrics.screenWidth * 0.065,
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    maxWidth: (Platform.OS === 'ios') ? '85%' : '75%',
    lineHeight: 22
  },
  listNote: {
    marginTop: 5,
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.037,
    fontFamily: Fonts.type.SFTextRegular,
    marginBottom: 9
  },
  listTime: {
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextRegular,
    backgroundColor: 'transparent',
    lineHeight: 14
  },
  listLeft: {
    height: '100%',
    borderBottomWidth: 0,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginRight: 10
    // borderBottomColor: 'rgb(204, 204, 204)'
  },
  listRight: {
    height: '100%',
    borderBottomWidth: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingRight: 9,
    paddingTop: 11,
    borderBottomColor: 'rgb(204, 204, 204)'
  },
  listBody: {
    flex: 5,
    height: '100%',
    borderBottomWidth: 0.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomColor: 'rgb(204, 204, 204)'
  },
  badge: {
    marginTop: 6,
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: Colors.blueTheme,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end'
  },
  badgeCount: {
    fontSize: 12,
    fontFamily: Fonts.type.SFTextRegular,
    color: '#fff',
    textAlign: 'center'
  }
});

const stateToProps = (state) => ({
  userId: state.login.userId,
  connectionStatus: state.messages.connectionStatus
});

const dispatchToProps = (dispatch) => {
  return {
    
  }
}

const Container = connect(stateToProps, dispatchToProps)(MessageItem)
export default Container;