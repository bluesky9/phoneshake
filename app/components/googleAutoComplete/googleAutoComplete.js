import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Icon } from "native-base";
import { connect } from 'react-redux';
import { Metrics, Fonts } from '../../themes';
import { strings } from '../../locale/index.js';
//AIzaSyAOx_hGTDMAlm_nbuPp6kElGwCK6GYv1Ew
class AutoComplete extends Component {

  render() {
    return (
      <GooglePlacesAutocomplete
        placeholder={strings("meTab.settings_search_address")}
        minLength={2}
        autoFocus={false}
        returnKeyType={'search'}
        listViewDisplayed='true'
        fetchDetails={true}
        onPress={(data, details = null) => {
          this.props.onSelectAddress(details)
        }}

        getDefaultValue={() => (this.props.userAddress) ? this.props.userAddress : ''}

        query={{
          key: 'AIzaSyAOx_hGTDMAlm_nbuPp6kElGwCK6GYv1Ew',
          language: 'en',
          types: ['address', '(cities)']
        }}

        styles={{
          textInputContainer: {
            width: '95%',
            backgroundColor: 'rgb(247, 247, 247)',
            alignSelf: 'center',
            borderRadius: 10,
            height: 39,
            marginTop:5,
            borderWidth: 0,
            borderTopColor: 'rgb(247,247,247)',
            borderBottomColor: 'rgb(247,247,247)'
          },
          textInput: {
            fontFamily: Fonts.type.SFTextRegular,
            fontSize: Metrics.screenWidth * 0.032,
            backgroundColor: 'transparent',
            height: 25
          },
          listView: {
          },
          description: {
            fontFamily: Fonts.type.SFTextRegular,
          },
          predefinedPlacesDescription: {
            color: '#1faadb'
          }
        }}

        currentLocation={false}
        enablePoweredByContainer={false}
        nearbyPlacesAPI='GooglePlacesSearch'
        GoogleReverseGeocodingQuery={{
          
        }}
        GooglePlacesSearchQuery={{
          rankby: 'distance'
        }}

        filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}

        debounce={200}
        renderLeftButton={() => <Icon name='search' style={styles.searchIcon} />}
        renderRightButton={() => { }}
      />
    );
  }
}

const styles = StyleSheet.create({
  searchIcon: {
    fontSize: 18,
    color: 'rgb(168, 168, 168)',
    marginLeft: 12,
    marginTop: 10
  }
})

const stateToProps = (state) => ({
  userAddress: state.login.address
});

const Container = connect(stateToProps, null)(AutoComplete)
export default Container;