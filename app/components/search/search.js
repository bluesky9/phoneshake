import React, { Component } from 'react';
import { SectionList, Text, View, TouchableWithoutFeedback, Platform, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { getUserByHandlename } from '../../screens/login/login.actions'
import * as actions from '../../screens/contacts/contacts.actions';
import Contact from '../contactItem/contactItem'
import { Button } from 'native-base';
import { MaterialIndicator } from 'react-native-indicators';
import { Metrics, Colors } from '../../themes/index.js'
import Loader from '../loader/'
import { PROFILE_LINK } from '../../api/constants';
import Modal from "react-native-modal";
import styles from './search.styles'
import modalStyle from '../../screens/me/signupInfo/signupInfo.styles';
import MyContactItem from '../myContactItem/myContactItem'
import BlockModal from '../../screens/contacts/blockModal/blockModal'
import RemoveContactModal from '../../screens/contacts/removeContactModal/removeContactModal'
import AddNote from '../../screens/contacts/addNote/addNote'
import { strings } from '../../locale/index.js';
let SearchLoader = ActivityIndicator
if (Platform.OS === 'ios') {
  SearchLoader = MaterialIndicator
}

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isHandleSearchLoading: false,
      addingContact: false,
      addContactModalVisible: false,
      note: '',
      modalVisible: false,
      blockModalVisible: false,
      removeContactModalVisible: false,
      addNoteModalVisible: false,
      selectedContact: {},
      fromContacts: [{
        name: 'No result found',
        description: ''
      }],
      addOrInvite: [{
        name: 'No result found',
        description: ''
      }],
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.renderInvite = this.renderInvite.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
    this.renderActionModal = this.renderActionModal.bind(this);
    this.handleActionPress = this.handleActionPress.bind(this);
    this.handleItemPress = this.handleItemPress.bind(this);
    this.blockContact = this.blockContact.bind(this);
    this.removeContact = this.removeContact.bind(this);
    this.addNote = this.addNote.bind(this);
    this.inviteUser = this.inviteUser.bind(this);
    this.inviteUserBy = this.inviteUserBy.bind(this);
    this.handleSelectContact = this.handleSelectContact.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this.addContactWithNote = this.addContactWithNote.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if(newProps.searchText !== this.props.searchText || newProps.myContacts !== this.props.myContacts) {
      this.handleSearch(newProps.searchText, newProps.myContacts)
    }
  }

  handleSearch(searchText, myContact) {
    searchText = searchText.trim()
    if(searchText.length < 3 || searchText.length > 20) return false;
    
    let phonebookContacts = this.props.phonebookContacts
    let myContactResult = [];
    let phonebookResult = [];
    let text = searchText.toLowerCase();
    if (myContact) {
      myContact.forEach(function (contact) {
        let givenName = contact.name.toLowerCase();
        let handleName = contact.handle_name.toLowerCase();
        let isMatched = contact.usernames.filter((username) => {
            return username.username.search(searchText) !== -1
        })
        if (givenName.search(text) !== -1 || handleName.search(text) !== -1 || isMatched.length > 0) {
          myContactResult.push(contact)
        }
      });
    }

    if (phonebookContacts) {
      phonebookContacts.forEach(function (contact) {
        let givenName = contact.name.toLowerCase();
        let handleName = contact.handleName.toLowerCase();
        if (givenName.search(text) !== -1 || handleName.search(text) !== -1 || contact.usernames.indexOf(searchText) !== -1) {
          phonebookResult.push(contact)
        }
      });
    }

    if(myContactResult.length === 0 && phonebookResult.length === 0) {
      this.setState({ isHandleSearchLoading: true })
      let userWithHandleName = []
      if (searchText.charAt(0) !== '@') {
        searchText = '@' + searchText
      }
      this.props.getUserByHandlename(searchText, this.props.userId)
      .then((response) => {
        if (response && response.payload && response.payload.data && response.payload.data.content && response.payload.data.content.isAvailable === false) {
          this.setState({ isHandleSearchLoading: false })
          let userData = response.payload.data.content
          
          let contact = {
            name: userData.name,
            handleName: userData.handle_name,
            usernames: [userData.usernames[0].username],
            id: userData.user_id,
            profilePic: userData.profile_pic,
            connectionState: userData.connection_state,
            isPrivateAccount: userData.is_private_account
          }
          userWithHandleName.push(contact)
          this.setState({ addOrInvite: userWithHandleName, isHandleSearchLoading: false})
        } else {
          this.setState({ isHandleSearchLoading: false })
        }
      })
    }

    this.setState({ fromContacts: myContactResult, addOrInvite: phonebookResult })
  }

  setModalVisible = (modal, value) => () => {
    this.setState({ [modal]: value })
  }

  handleActionPress = (modalKey) => () => {
    this.setState({ [modalKey]: true })
  }

  handleSelectContact = (user, handleName, isFriend, username, image, connectionStatus) => {
    user.handleName = handleName
    user.isFriend = isFriend
    user.username = username
    user.profile_pic = image
    user.connectionState = connectionStatus
    this.setState({ selectedContact: user, addContactModalVisible: true })
  }

  addContactWithNote() {
    const { selectedContact, note } = this.state
    this.setState({ addingContact: true })
    let data = {
      "requestorUserId": this.props.userId,
      "requesteeUserId": selectedContact.id,
      "description": note.trim(),
      "type": "I",
      "reference": "INVITE"
    }

    this.props.addContact(data, selectedContact)
    .then((response) => {
      console.log('response', response)
      this.setState({ addingContact: false, addContactModalVisible: false, note: '' })
      if (response.payload.data && response.payload.data.status === 200) {
        if (selectedContact.isPrivateAccount === 'Y') {
          let contact = this.state.selectedContact
          contact.connectionState = 'PENDING'
          this.setState({
            selectedContact: contact
          })
        } else {
          let contact = this.state.selectedContact
          contact.connectionState = 'ACCEPTED'
          this.setState({
            selectedContact: contact
          })
        }
      } else if (response.payload.data && response.payload.data.status === 409) {
        let message = (response.payload.data.message) ? response.payload.data.message : 'Something went wrong. Try again.'
        setTimeout(() => {
          alert(message)
        }, 500);
      } else {
        setTimeout(() => {
          alert('Error while adding contact. Please try again.')
        }, 500);
      }
    })
  }

  addNote() {
    this.setState({ addNoteModalVisible: false, modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
        "requestorUserId": this.props.userId,
        "requesteeUserId": this.state.selectedContact.id,
        "description": this.state.note
      }
      this.props.addNote(data)
      .then((response) => {
        this.setState({ loading: false })
        if (response.payload.data && response.payload.data.status === 200) {
          this.setState((previousState) => ({
            selectedContact: { ...previousState.selectedContact, ['description']: response.payload.description },
            note: '',
          }))
        } else {
          setTimeout(() => {
            alert('Error while updating description. Please try again.')
          }, 300);
        }
      })
    }, 500);
  }

  removeContact() {
    this.setState({ removeContactModalVisible: false, modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
        "requestorUserId": this.props.userId,
        "requesteeUserId": this.state.selectedContact.id
      }
      this.props.removeContact(data)
        .then((response) => {
          this.setState({ loading: false })
          if (response.payload.data && response.payload.data.status !== 200) {
            setTimeout(() => {
              alert('Error while removing contact. Please try again.')
            }, 300);
          }
        })
    }, 500);
  }

  blockContact() {
    this.setState({ blockModalVisible: false, modalVisible: false })
    setTimeout(() => {
      this.setState({ loading: true })
      let data = {
        "requestorUserId": this.props.userId,
        "requesteeUserId": this.state.selectedContact.id,
        "isBlocked": true
      }
      this.props.contactBlockUnblock(data)
        .then((response) => {
          this.setState({ loading: false })
          if (response.payload.data && response.payload.data.status !== 200) {
            setTimeout(() => {
              alert('Error while blocking contact. Please try again.')
            }, 300);
          }
        })
    }, 500);
  }

  renderActionModal() {
    let content = []
    if (this.state.addNoteModalVisible) {
      content.push(
        <AddNote
          key={'add'}
          isRTL={this.props.isRTL}
          visible={this.state.addNoteModalVisible}
          setModalVisible={(value) => this.setState({ addNoteModalVisible: value, modalVisible: false })}
          selectedContact={this.state.selectedContact}
          handleSubmit={this.addNote}
          note={this.state.note}
          onChangeText={(text) => this.setState({ note: text })}
          callback={this.addNote} />
      )
    } else if (this.state.blockModalVisible) {
      content.push(
        <BlockModal
          key={'block'}
          visible={this.state.blockModalVisible}
          setModalVisible={(value) => this.setState({ blockModalVisible: value, modalVisible: false })}
          selectedContact={this.state.selectedContact}
          callback={this.blockContact} />
      )
    } else if (this.state.removeContactModalVisible) {
      content.push(
        <RemoveContactModal
          key={'remove'}
          visible={this.state.removeContactModalVisible}
          setModalVisible={(value) => this.setState({ removeContactModalVisible: value, modalVisible: false })}
          selectedContact={this.state.selectedContact}
          callback={this.removeContact} />
      )
    } else if (this.state.modalVisible) {
      content.push(
        <View style={styles.modalContainer} key={'actions'}>
          <View style={[styles.modalContent, { padding: 0, height: Metrics.screenHeight * 0.3 }]}>
            <View style={styles.modalActionSheet}>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('addNoteModalVisible')}>
                <Text style={modalStyle.regularText}>{strings('contactTab.contact_editNotes')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('blockModalVisible')}>
                <Text style={modalStyle.distructiveText}>{strings('constants.blockContact')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalRemoveButton} onPress={this.handleActionPress('removeContactModalVisible')}>
                <Text style={modalStyle.distructiveText}>{strings('constants.removeContact')}</Text>
              </Button>
              <Button block transparent style={modalStyle.modalCancelButton} onPress={() => this.setState({ modalVisible: false })}>
                <Text style={modalStyle.cancelText}>{strings('constants.cancel')}</Text>
              </Button>
            </View>
          </View>
        </View>
      )
    }
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.modalVisible}
        animationDuration={200}
      >
        {content}
      </Modal>
    )
  }

  inviteUserBy(username, type) {
    let data = {}
    if(type === 'email') {
      data = {
        "toEmail": username,
        "handleName": this.props.handleName,
        "userFullName": this.props.name,
        "userId": this.props.userId
      }
    } else {
      data = {
        "toPhoneNo": username,
        "message": `Hi, ${this.props.name} would like to invite you on PhoneShake App. ${PROFILE_LINK}${this.props.handleName}`,
        "userId": this.props.userId
      }
    }
    
    this.setState({ loading: true })
    this.props.sendInvitation(data, type)
    .then((response) => {
      this.setState({ loading: false })
      if (response.payload.data && response.payload.data.status === 200) {
        setTimeout(() => {
          alert('Invitation sent successfully.')
        }, 300);
      } else {
        setTimeout(() => {
          alert('Error while sending invitation. Please try again.')
        }, 300);
      }
    })
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  inviteUser = (user) => {
    let username = user.usernames[0]
    if (this.validateEmail(username)) {
      this.inviteUserBy(username, 'email')
    } else if (username.length > 10) {
      this.inviteUserBy(username, 'phone')
    }
  }

  handleItemPress(item) {
    this.setState({
      modalVisible: true,
      selectedContact: item,
      note: (item.description) ? item.description : '' 
    })
  }

  renderSectionHeader(data) {
    if(data.section.title === '' && this.props.searchText.trim().length > 2) return null
    return (
      <View style={styles.sectionHeaderView}>
        <Text style={styles.sectionHeaderText}>{data.section.title}</Text>
      </View>
    )
  }

  renderInvite({ item, index, section }) {
    if (item.name === 'No result found' && this.props.searchText.trim().length > 0) {
      return (
        <View style={styles.emptyViewContainer}>
          <Text style={styles.emptyList}>{`No results found`}</Text>
        </View>
      );
    } else {
      if (section.title === 'CONTACTS') {
        return (
          <MyContactItem
            key={index}
            contact={(this.state.selectedContact.id && item.handle_name === this.state.selectedContact.handleName) ? this.state.selectedContact : item}
            onLongPress={() => this.handleItemPress(item)}
            onPress={(handleName, isFriend, username, image) => this.props.handleSelectContact(item, handleName, isFriend, username, image)}
            />
        );
      } else {
        return (
          <Contact
            key={index}
            contact={item}
            onPress={(handleName, isFriend, username, connectionStatus) => this.props.handleSelectContact(item, handleName, isFriend, username, connectionStatus)} 
            inviteUser={() => this.inviteUser(item)}
            handleAddPress={(handleName, isFriend, username, image, connectionStatus, isPrivateAccount) => this.handleSelectContact(item, handleName, isFriend, username, image, connectionStatus, isPrivateAccount)}
            />
        ); 
      }
    }
  }

  renderAddContactModal() {
    console.log('this.props.isRTL', this.props.isRTL)
    return (
      <Modal
        backdropColor='rgb(25, 25, 25)'
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        isVisible={this.state.addContactModalVisible}
        animationDuration={200}
      >
        <AddNote
          key={'add'}
          isRTL={this.props.isRTL}
          saveButtonText={'Add'}
          showLoading={this.state.addingContact}
          visible={this.state.addContactModalVisible}
          placeholder={'Add a note to remember this person...'}
          setModalVisible={(value) => this.setState({ addContactModalVisible: false })}
          selectedContact={this.state.selectedContact}
          handleSubmit={this.addContactWithNote}
          note={this.state.note}
          onChangeText={(text) => this.setState({ note: text })}
          callback={this.addContactWithNote} />
      </Modal>
    )
  }

  keyExtractor = (item, index) => index.toString();

  render() {
    let backgroundStyle = (this.props.searchText.trim().length > 2) ? styles.searchActiveStyle : {}
    let sections = []
    if(this.state.fromContacts.length > 0) {
      sections.push({
        data: this.state.fromContacts,
        title: 'CONTACTS'
      })
    }
    if (this.state.addOrInvite.length > 0) {
      sections.push({
        data: this.state.addOrInvite,
        title: 'ADD/INVITE'
      })
    }
    if (sections.length === 0 && !this.state.isHandleSearchLoading) {
      sections.push({
        data: [{
          name: 'No result found',
          description: ''
        }],
        title: ''
      })
    }
    let topConstraint = { top: (this.props.top) ? this.props.top : Metrics.screenHeight * 0.07 }
    return (
      <TouchableWithoutFeedback style={[styles.searchView, topConstraint]} onPress={() => this.props.dismiss()}>
      <View style={[styles.searchView, topConstraint, backgroundStyle]}>
      
        { 
          (this.state.isHandleSearchLoading) ?
            <SearchLoader
              size={20}
              color={Colors.blueTheme}
              animating={this.state.isHandleSearchLoading} />
          :
          (this.props.searchText.trim().length > 2) ?
          <SectionList
            stickySectionHeadersEnabled={true}
            sections={sections}
            keyExtractor={this.keyExtractor}
            initialNumToRender={20}
            extraData={this.state}
            renderSectionHeader={this.renderSectionHeader}
            renderItem={this.renderInvite}
            style={styles.listContainer} />

          : null
        }
        {this.renderActionModal()}
        {this.renderAddContactModal()}
        <Loader loading={this.state.loading} />
      </View>
      </TouchableWithoutFeedback >
    )
  }
}

const stateToProps = (state) => ({
  syncedContacts: state.contacts.syncedContacts,
  myContacts: state.contacts.myContacts,
  phonebookContacts: state.contacts.phonebookContacts,
  userId: state.login.userId,
  name: state.login.name,
  handleName: state.login.handleName,
  isRTL: state.login.isRTL
});

const dispatchToProps = (dispatch) => {
  return {
    contactBlockUnblock: (data) => dispatch(actions.contactBlockUnblock(data)),
    removeContact: (data) => dispatch(actions.removeContact(data)),
    addContact: (data, addedUser) => dispatch(actions.addContact(data, addedUser)),
    addNote: (data) => dispatch(actions.addNote(data)),
    sendInvitation: (data, type) => dispatch(actions.sendInvitation(data, type)),
    getUserByHandlename: (handleName, requestorUserId) => dispatch(getUserByHandlename(handleName, requestorUserId))
  }
}

const Container = connect(stateToProps, dispatchToProps)(Search)
export default Container;

