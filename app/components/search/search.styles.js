import { StyleSheet, Platform } from 'react-native'
import { Fonts, Metrics, Colors } from '../../themes/'

export default StyleSheet.create({
  searchView: {
    backgroundColor: 'rgba(25, 25, 25, 0.5)',
    position: 'absolute',
    top: (Platform.OS === 'ios') ? Metrics.screenHeight * 0.105 : Metrics.screenHeight * 0.085,
    left: 0,
    right: 0,
    bottom: 0
  },
  listContainer: {
    width: '100%'
  },
  sectionListItem: {
    width: '100%',
    marginLeft: 0,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(227, 227, 227)',
    paddingVertical: Metrics.screenHeight * 0.014,
  },
  noContactText: {
    color: 'rgb(73, 73, 73)',
    textAlign: 'center',
    fontSize: Metrics.screenWidth * 0.042
  },
  sectionHeaderView: {
    backgroundColor: 'rgb(247, 247, 247)',
    justifyContent: 'center',
    // paddingVertical: 16,
    paddingLeft: Metrics.screenWidth * 0.032,
    height: 20
  },
  sectionHeaderText: {
    color: 'rgb(168, 168, 168)',
    fontFamily: Fonts.type.SFTextRegular,
    fontSize: Metrics.screenWidth * 0.032
  },
  searchActiveStyle: {
    backgroundColor: '#fff',
    borderTopWidth: 0.2,
    borderTopColor: 'rgb(204, 204, 204)'
  },
  userImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: Metrics.screenWidth * 0.06
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    width: (Platform.OS === 'ios') ? '65%' : '75%'
  },
  listUsername: {
    color: 'rgb(127, 127,127)',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextMedium,
    backgroundColor: 'transparent'
  },
  txtInvite: {
    fontSize: Metrics.screenWidth * 0.032,
    color: Colors.blueTheme,
    fontFamily: Fonts.type.SFTextSemibold
  },
  btnInvite: {
    height: Metrics.screenHeight * 0.044,
    width: Metrics.screenWidth * 0.19,
    justifyContent: 'center',
    borderColor: Colors.blueTheme
  },
  modalContent: {
    height: Metrics.screenHeight * 0.33,
    backgroundColor: "#fff",
    padding: 22,
    margin: 0,
    justifyContent: "center",
    borderRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  emptyViewContainer: {
    marginTop: Metrics.screenHeight * 0.277,
    flex: 1,
    alignItems: 'center'
  },
  emptyList: {
    fontFamily: Fonts.type.SFTextMedium,
    fontSize: Metrics.screenWidth * 0.053,
    color: '#000',
    textAlign: 'center'
  },
});
