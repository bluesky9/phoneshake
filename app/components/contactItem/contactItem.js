import React, { Component } from 'react';
import { StyleSheet, Image, Text, Platform, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import branch from 'react-native-branch'
import * as actions from '../../screens/contacts/contacts.actions';
import { MaterialIndicator } from 'react-native-indicators';
import { Colors, Metrics, Fonts, Images } from '../../themes/index.js'
import { ListItem, Left, Right, Body, Button } from 'native-base';
import { strings } from '../../locale';
let Loader = ActivityIndicator
if(Platform.OS === 'ios') {
  Loader = MaterialIndicator
}

class ContactItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
      handleName: (this.props.contact && this.props.contact.handleName) ? this.props.contact.handleName : '',
      isFriend: false,
      image: '',
      username: '',
      invited: false,
      isPrivateAccount: '',
      connectionStatus: ''
    };
    this.renderButton = this.renderButton.bind(this);
    this.handleItemPress = this.handleItemPress.bind(this);
    this.handleAddPress = this.handleAddPress.bind(this);
    this.inviteUser = this.inviteUser.bind(this);
    this.inviteUserBy = this.inviteUserBy.bind(this);
  }

  componentDidMount() {
    const { contact, syncedContacts } = this.props
    let updatedContact = syncedContacts.filter((item) => contact.usernames.indexOf(item.username) !== -1)[0]
    var handleName = (updatedContact) ? updatedContact.handleName : ''
    var username = (updatedContact) ? updatedContact.username : (contact.usernames.length > 0) ? contact.usernames[0] : ''
    var image = (updatedContact) ? updatedContact.profilePic : (contact.profilePic) ? contact.profilePic : ''
    var connectionStatus = (updatedContact) ? updatedContact.connectionState : (contact.connectionState) ? contact.connectionState : ''

    let isFriend = (connectionStatus === 'ACCEPTED') ? true : false

    if(!handleName) {
      handleName = (this.props.contact && this.props.contact.handleName) ? this.props.contact.handleName : ''
    }
    this.setState({ handleName, isFriend, image, username, connectionStatus })
  }

  componentWillReceiveProps(newProps) {
    const { contact, syncedContacts } = newProps;
    
    let updatedContact = syncedContacts.filter((item) => contact.usernames.indexOf(item.username) !== -1)[0]
    var handleName = (updatedContact) ? updatedContact.handleName : ''
    var username = (updatedContact) ? updatedContact.username : (contact.usernames.length > 0) ? contact.usernames[0] : ''
    var image = (updatedContact) ? updatedContact.profilePic : (contact.profilePic) ? contact.profilePic : ''
    var connectionStatus = (updatedContact) ? updatedContact.connectionState : (contact.connectionState) ? contact.connectionState : ''

    let isFriend = (connectionStatus === 'ACCEPTED') ? true : false
    if (!handleName) {
      handleName = (this.props.contact && this.props.contact.handleName) ? this.props.contact.handleName : ''
    }
    this.setState({ handleName, isFriend, image, username, connectionStatus })
    
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.contact !== this.props.contact
        || nextState.handleName !== this.state.handleName
        || nextState.isFriend !== this.state.isFriend
        || nextProps.syncedContacts !== this.props.syncedContacts
        || nextState.connectionStatus !== this.state.connectionStatus
        || nextState.image !== this.state.image
        || nextState.invited !== this.state.invited
        || nextState.loading !== this.state.loading) {
      return true
    }
    return false
  }

  async inviteUserBy(username, type) {
    let data = {}
    let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
      automaticallyListOnSpotlight: true,
      contentMetadata: {
        customMetadata: {
          handleName: this.props.user.handleName
        }
      },
      title: 'Phoneshake',
      contentDescription: `Phoneshake`,
    })
    let linkProperties = {
      feature: 'share',
      channel: 'facebook'
    }

    let controlParams = {
      $uri_redirect_mode: 1
    }

    let { url } = await branchUniversalObject.generateShortUrl(linkProperties, controlParams)
    if (type === 'email') {
      data = {
        "toEmail": username,
        "handleName": this.props.user.handleName,
        "message": `Hi, ${this.props.user.name} would like to invite you on PhoneShake App. ${url}`,
        "userFullName": this.props.user.name,
        "userId": this.props.user.userId
      }
    } else {
      data = {
        "toPhoneNo": username,
        "message": `Hi, ${this.props.user.name} would like to invite you on PhoneShake App. ${url}`,
        "userId": this.props.user.userId
      }
    }
    
    this.setState({ loading: true })
    this.props.sendInvitation(data, type)
    .then((response) => {
      if (response.payload.data && response.payload.data.status === 200) {
        this.setState({ loading: false, invited: true })
      } else {
        this.setState({ loading: false })
        setTimeout(() => {
          alert('Error while sending invitation. Please try again')
        }, 300);
      }
    })
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  inviteUser = (user) => {
    let username = user.usernames[0]
    if (this.validateEmail(username)) {
      this.inviteUserBy(username, 'email')
    } else if (username.length > 10) {
      this.inviteUserBy(username, 'phone')
    }
  }

  renderButton(contact) {
    if(this.state.handleName === this.props.user.handleName){
      return null;
      // return (
      //   <Button bordered style={[styles.btnInvite, styles.buttonAdded]}>
      //     <Text style={[styles.txtInvite, styles.addedButtonText]}>Me</Text>
      //   </Button>
      // )
    }

    if(this.state.handleName && this.state.isFriend) {
      return null;
      // return (
      //   <Button bordered style={[styles.btnInvite, styles.buttonAdded]}>
      //     <Text style={[styles.txtInvite, styles.addedButtonText]}>Added</Text>
      //   </Button>
      // )
    } else if (this.state.connectionStatus === 'ACCEPTED') {
      return (
        <Button bordered style={[styles.btnInvite, styles.buttonAdded]}>
          <Text style={[styles.txtInvite, styles.addedButtonText]}>{strings('constants.added')}</Text>
        </Button>
      )
    } else if (this.state.connectionStatus === 'PENDING') {
      return (
        <Button bordered style={[styles.btnInvite, styles.buttonAdded]}>
          <Text style={[styles.txtInvite, styles.addedButtonText]}>{strings('constants.requested')}</Text>
        </Button>
      )
    } else if (this.state.handleName && this.state.connectionStatus === 'UNKNOWN') {
      return (
        <Button bordered style={styles.btnInvite} onPress={this.handleAddPress}>
          <Text style={styles.txtInvite}>+ {strings('constants.add')}</Text>
        </Button>
      )
    } else {
      if(this.state.loading) {
        return (
          <Button bordered style={styles.btnInvite}>
            <Loader
              size={20}
              color={Colors.blueTheme}
              animating={this.state.loading} /> 
          </Button>
        )  
      } else if(this.state.invited) {
        return (
          <Button bordered style={[styles.btnInvite, styles.buttonAdded]}>
            <Text style={[styles.txtInvite, styles.addedButtonText]}>{strings('constants.invited')}</Text>
          </Button>
        )  
      } else {
        return (
          <Button bordered style={styles.btnInvite} onPress={() => this.inviteUser(contact)}>
            <Text style={styles.txtInvite}>+ {strings('constants.invite')}</Text>
          </Button>
        )
      }
    }
  }

  handleAddPress = () => {
    if (this.props.handleAddPress && this.state.handleName && this.state.handleName !== this.props.user.handleName) {
      this.props.handleAddPress(this.state.handleName, this.state.isFriend, this.state.username, this.state.image, this.state.connectionStatus)
    }
  }

  handleItemPress = () => {
    if(this.state.handleName && this.state.handleName !== this.props.user.handleName) {
      this.props.onPress(this.state.handleName, this.state.isFriend, this.state.username, this.state.image)
    }
  }

  render() {
    let { contact } = this.props
    let userImage = (this.state.image) ? { uri: this.state.image } : Images.defaultUserImage
    return (
      <ListItem style={styles.listItem} onPress={this.handleItemPress}>
        <Left style={{ flex: 1.2, borderWidth: 0 }}>
          <Image source={userImage} style={styles.userImage} />
        </Left>
        <Body style={{ flex: 4.1, borderWidth: 0 }}>
          <Text style={styles.listName} numberOfLines={1}>{contact.name}</Text>
          <Text style={styles.listUsername} numberOfLines={1}>
            {(this.state.handleName) ? this.state.handleName : (contact.usernames.length > 0) ? contact.usernames[0] : '@nousername'}
          </Text>
        </Body>
        <Right style={{ flex: 1.7, borderWidth: 0 }}>
          {this.renderButton(contact)}
        </Right>
      </ListItem>
    )
  }
}

const styles = StyleSheet.create({
  buttonAdded: {
    backgroundColor: Colors.blueTheme
  },
  addedButtonText: {
    color: '#fff'
  },
  userImage: {
    height: Metrics.screenWidth * 0.12,
    width: Metrics.screenWidth * 0.12,
    borderRadius: Metrics.screenWidth * 0.06
  },
  listName: {
    color: '#000',
    fontSize: Metrics.screenWidth * 0.042,
    fontFamily: Fonts.type.SFTextSemibold,
    backgroundColor: 'transparent',
    width: '75%'
  },
  listUsername: {
    color: 'rgb(127, 127, 127)',
    fontSize: Metrics.screenWidth * 0.032,
    fontFamily: Fonts.type.SFTextMedium,
    backgroundColor: 'transparent'
  },
  txtInvite: {
    fontSize: Metrics.screenWidth * 0.032,
    color: Colors.blueTheme,
    fontFamily: Fonts.type.SFTextSemibold
  },
  btnInvite: {
    height: Metrics.screenHeight * 0.044,
    width: Metrics.screenWidth * 0.19,
    justifyContent: 'center',
    borderColor: Colors.blueTheme,
    borderRadius: 5
  }
});

const stateToProps = (state) => ({
  syncedContacts: state.contacts.syncedContacts,
  myContacts: state.contacts.myContacts,
  user: state.login
});

const dispatchToProps = (dispatch) => {
  return {
    sendInvitation: (data, type) => dispatch(actions.sendInvitation(data, type))
  }
}

const Container = connect(stateToProps, dispatchToProps)(ContactItem)
export default Container;

