import React, { Component } from 'react';
import { Dimensions, Text, View, WebView, Image, Platform, Modal, TouchableOpacity, SafeAreaView } from 'react-native';
import Share from 'react-native-share';
import { UIActivityIndicator } from 'react-native-indicators';
import { Fonts, Images } from '../../themes'
import styles from './phoneShakeWebView.styles'

let Indicator = UIActivityIndicator

const WEBVIEW_REF = "WEBVIEW_REF";
var { height } = Dimensions.get('window');

class PhoneShakeWebView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      canGoBack: false,
      visible: false,
      isError: false,
      title: '',
      key: 1
    };
    this.renderContent = this.renderContent.bind(this);
    this.renderWebNav = this.renderWebNav.bind(this);
    this.topLeftButtonAction = this.topLeftButtonAction.bind(this);
    this.topRightButtonAction = this.topRightButtonAction.bind(this);
    this.bottomBackButtonAction = this.bottomBackButtonAction.bind(this);
    this.bottomNextButtonAction = this.bottomNextButtonAction.bind(this);
    this.shareButtonActoin = this.shareButtonActoin.bind(this);
  }


  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps !== this.props || nextState !== this.state) {
      return true
    } else {
      return false
    }
  }

  topLeftButtonAction() {
    this.props.setModalVisible(false)
  }

  topRightButtonAction() {
    this.setState({ key: this.state.key + 1 })
    this.refs.WEBVIEW_REF.reload();
  }

  bottomBackButtonAction() {
    this.refs[WEBVIEW_REF].goBack();
  }

  bottomNextButtonAction() {
    this.refs[WEBVIEW_REF].goForward();
  }

  shareButtonActoin() {
    const options = {
      url: this.state.title,
    }
    Share.open(options)
      .then((res) => { console.log(res) })
      .catch((err) => { err && console.log(err); });
  }

  renderBottomTab() {
    let BottomTab = null
    if (!this.state.loading && !this.state.isError) {
      BottomTab = (
        <View style={{ height: 44, flex: 1, flexDirection: 'row', justifyContent: 'space-between', borderRadius: 10 }}>
          <TouchableOpacity style={{ marginLeft: 12, justifyContent: 'center', alignItems: 'center' }} disabled={!this.state.canGoBack} onPress={() => { this.bottomBackButtonAction() }}  >
            <Image source={this.state.canGoBack ? Images.backWebIcon : Images.backInactive} style={{ height: 20, width: 14 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} disabled={!this.state.canGoBack} onPress={() => { this.bottomNextButtonAction() }} >
            <Image source={this.state.canGoForward ? Images.nextActive : Images.nextIconInactive} style={{ height: 20, width: 14 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => { this.shareButtonActoin() }} >
            <Image source={Images.uploadIcon} style={{ height: 20, width: 18 }} />
          </TouchableOpacity>
        </View>
      )
    }

    return (
      <View style={{ height: 44, marginHorizontal: 0, flexDirection: 'row', backgroundColor: "rgba(247, 247, 247, 1)", borderRadius: 10 }}>
        {BottomTab}
        <View style={{ height: 44, flex: 1, flexDirection: 'row', borderRadius: 10 }}></View>
      </View>
    )
  }

  renderWebNav() {
    return (
      <View style={{ height: 44, flexDirection: 'row', backgroundColor: "rgba(247, 247, 247, 1)", borderTopRightRadius: 20, borderTopLeftRadius: 20 }}>
        <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} onPress={() => { this.topLeftButtonAction() }}  >
          <Image source={Images.closeIcon} style={{ height: 26, width: 26 }} />
        </TouchableOpacity>
        <View style={{ flex: 5, justifyContent: 'center', alignItems: 'center' }}>
          <Text numberOfLines={1} style={{ fontSize: 16, fontFamily: Fonts.type.SFTextMedium, color: "rgba(44, 49, 55, 1)" }}>{this.state.loading ? 'Loading...' : this.state.title}</Text>
        </View>
        <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} onPress={() => { this.topRightButtonAction() }}  >
          {this.state.loading ?
            <Indicator
              size={20}
              animating={this.props.loading} />
            :
            <Image source={Images.reloadIcon} style={{ height: 20, width: 18 }} />}
        </TouchableOpacity>
      </View>
    )
  }

  renderErrorScreen(error) {
    if (error)
      return (
        <View style={{ marginTop: height * 0.35, height: 120, width: '100%', alignItems: 'center' }}>
          <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => { this.topRightButtonAction() }}  >
            <Image source={Images.reloadIcon} style={{ height: 35, width: 32 }} />
            <Text numberOfLines={1} style={{ marginTop: 15, fontSize: 16, fontFamily: Fonts.type.SFTextRegular, color: "rgba(44, 49, 55, 1)" }}>Can't Connect</Text>
          </TouchableOpacity>
        </View>
      )
    else return null
  }

  renderContent() {
    let webUrl = this.state.key === 1 ? this.props.linkUrl : this.state.title
    return (
      <View style={styles.container}>
        <View style={{ flex: 1, borderColor: 'white', borderRadius: 10 }}>
          {this.renderWebNav()}
          <WebView
            ref={WEBVIEW_REF}
            key={this.state.key}
            source={{ uri: webUrl }}
            onLoadStart={() => { this.setState({ loading: true, title: 'Loading...', isError: false }) }}
            onLoadEnd={() => { this.setState({ loading: false }) }}
            onNavigationStateChange={(navState) => { this.setState({ canGoBack: navState.canGoBack, canGoForward: navState.canGoForward, title: navState.url }) }}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            renderError={(error) => { return (this.renderErrorScreen(error)) }}
          />
          {this.renderBottomTab()}
        </View>
      </View>
    )
  }

  render() {
    return (
      <Modal
        visible={this.props.visible}
        backdropOpacity={0.5}
        style={{ margin: 0 }}
        onRequestClose={() => console.log('modal closed')}
      >
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        {this.renderContent()}
      </SafeAreaView>
      </Modal>
    )
  }
}

export default PhoneShakeWebView;
