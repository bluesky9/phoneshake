import { StyleSheet, Platform } from 'react-native'
export default StyleSheet.create({
 container: {
   flex: 1,
   marginTop: 0,
   justifyContent: 'center'  
 },
 headerLeft: {
  height: 24,
  width: 24,
  marginLeft: 12
},

})