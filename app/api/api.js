import axios from 'axios';
import { API_HOST } from './constants.js';
import store from '../store.js';

store.subscribe(loginListener);
axios.defaults.headers.common['Auth'] = 'asdgasht3432k3423k3432';
axios.defaults.headers.common['language'] = 'en';
const instance = axios.create({
  baseURL: API_HOST,
  // timeout: 1000,
	headers: { 'Content-Type': 'application/json' } // null
});

function loginListener() {
	let token = store.getState().login.token;
	if(token) {
		axios.defaults.headers.common['Auth'] = token;
	} else {
		axios.defaults.headers.common['Auth'] = 'asdgasht3432k3423k3432';
	}
}
const API = {
	auth: {
    sendConfirmationCode: (email, confirmationCode) => {
      const params = {
      	"toEmail": email,
      	"confirmationCode": confirmationCode
      }
			return instance.post('/sendConfirmationCode', params, { headers: { 'Auth': 'asdgasht3432k3423k3432' }})
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
    sendConfirmationCodeBySMS: (phone, confirmationCode) => {
      const params = {
      	"toPhoneNo": phone,
      	"confirmationCode": confirmationCode
      }
			return instance.post('/sendConfirmationCodeBySMS', params, { headers: { 'Auth': 'asdgasht3432k3423k3432' } })
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
	},
  user: {
		acceptFriendRequest: (userInfo) => {
			return instance.post('/registerUser', userInfo, { headers: { 'Auth': 'asdgasht3432k3423k3432', 'Content-Type': 'application/json' } })
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		registerUser: (userInfo) => {
			delete userInfo.accountType;
			return instance.post('/registerUser', userInfo)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		getBusinessTypes: () => {
			return instance.get('/v2/businesstype')
			.then((res) => { 
				 return res;
			})
			.catch((err) => { return err; });
		},
		logout: (userId, token='Allow') => {
			return instance.delete(`/logout?userId=${userId}&token=${token}`, {})
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		deleteAccount: (userId) => {
			return instance.delete(`/deleteAccount?userId=${userId}`)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
    isHandleNameUnique: (handleName) => {
			let encodedHandleName = encodeURIComponent(handleName)
			return instance.get(`/isHandleNameUnique?handlename=@${encodedHandleName}`, { headers: { 'Auth': 'asdgasht3432k3423k3432', 'Content-Type': 'application/json' } }, { data: null })
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		addNewUsername: (data) => {
			return instance.post(`/addNewUsername`, data)
			.then((res) => {
				res.addedInfo = data
				return res;
			})
			.catch((err) => { return err; });
		},
		deleteUsername: (userId, username) => {
			let encodedusername = encodeURIComponent(username)
			return instance.delete(`/deleteUsername?userId=${userId}&username=${encodedusername}`)
			.then((res) => {
				res.deletedUsername = username
				return res;
			})
			.catch((err) => { return err; });
		},
		updateDeviceToken: (data) => {
			return instance.put(`/updateDeviceToken`, data)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		getUserByUsername: (username, checkForLogin = false, isSocialMediaLinksRequired = false, isNotificationSettingsRequired = false, requestorUserId) => {
			let encodedusername = encodeURIComponent(username)
			return instance.get(`/getUserByUsername?username=${encodedusername}&checkForLogin=${checkForLogin}&isSocialMediaLinksRequired=${isSocialMediaLinksRequired}&isNotificationSettingsRequired=${isNotificationSettingsRequired}&requestorUserId=${requestorUserId}`, { data: null })
			.then((res) => {
				res.checkForLogin = checkForLogin
				return res;
			 })
			.catch((err) => { return err; });
		},
		getUserByHandlename: (handleName, requestorUserId) => {
			return instance.get(`/getUserByHandlename?handlename=${handleName}&requestorUserId=${requestorUserId}`)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		getUserByUserId: (userId, requestorUserId) => {
			return instance.get(`/getUserByUsername?userId=${userId}&isSocialMediaLinksRequired=true&requestorUserId=${requestorUserId}`)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		updateProfileSettings: (data) => {
			return instance.post(`/updateProfileSettings`, data )
			.then((res) => {
				res.type = data.columnName
				if (data.columnName === 'notification_settings') {
					res.value = {
						"receiveRequestWhenPrivate": data.receiveRequestWhenPrivate,
						"acceptRequestWhenPrivate": data.acceptRequestWhenPrivate,
						"addRequestWhenPublic": data.addRequestWhenPublic
					}
				} else {
					res.value = data.value
				}
				if(data.columnName === 'address') {
					res.latitude = data.latitude
					res.longitude = data.longitude
				}
				return res;
			})
			.catch((err) => { return err; });
		},
		addSocialMediaDetails: (data) => {
			var type = data.type
			data.type = type.toUpperCase();
			return instance.post(`/addSocialMediaDetails`, data) //, { headers: { 'auth': 'allow' } }
			.then((res) => {
				return res;
			})
			.catch((err) => { return err; });
		},
		getSocialMediaDetailsByUserId: (userId) => {
			return instance.get(`/getSocialMediaDetailsByUserId?userId=${userId}`)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		uploadPicture: (data, type) => {
			let endpoint = (type === 'logo') ? 'uploadBusinessLogoByUserId' : 'uploadProfilePicByUserId'
			return instance.post(`/${endpoint}`, data )
			.then((res) => {
				res.type = type;
				return res;
			})
			.catch((err) => { return err; });
		},
		getContactListWhoAddedMe: (data) => {
			return instance.get(`/getContactListWhoAddedMe?userId=${data.userId}&type=${data.type}&pageNo=${data.pageNo}&pageSize=100&requestType=${data.requestType}`)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		getNotifications: (data, fromGeneral) => {
			return instance.get(`/getNotifications?userId=${data.userId}&pageNo=${data.pageNo}&pageSize=100`)
			.then((res) => {
				res.fromGeneral = fromGeneral;
				return res;
			})
			.catch((err) => { return err; });
		},
		acceptFriendRequest: (data, user) => {
			return instance.post(`/acceptFriendRequest`, data)
			.then((res) => {
				res.user = user;
				res.type = data.isRequestAccepted
				return res;
			})
			.catch((err) => { return err; });
		},
	},
	contacts: {
		getContactList: (userId, type, pageNumber) => { //&requestType=Y
			return instance.get(`/getContactList?userId=${userId}&type=${type}&pageNo=${pageNumber}&pageSize=200&requestType=Y`)
			.then((res) => { console.log('getContactList', res); return res; })
			.catch((err) => { return err; });
		},
		contactBlockUnblock: (data) => {
			return instance.post(`/contactBlockUnblock`, data)
			.then((res) => {
				res.userId = data.requesteeUserId
				res.isBlocked = data.isBlocked
				return res;
			 })
			.catch((err) => { return err; });
		},
		addContact: (data, addedUser) => {
			return instance.post(`/addContact`, data)
			.then((res) => {
				console.log('addContact', res);
				res.addedUser = addedUser
				return res;
			})
			.catch((err) => { return err; });
		},
		removeContact: (data) => {
			return instance.put(`/removeContact?requestorUserId=${data.requestorUserId}&requesteeUserId=${data.requesteeUserId}`)
			.then((res) => {
				res.removedUserId = data.requesteeUserId
				return res;
			})
			.catch((err) => { return err; });
		},
		addNote: (data) => {
			return instance.post(`/updateContactDescription`, data)
			.then((res) => {
				res.userId = data.requesteeUserId
				res.description = data.description
				return res;
			})
			.catch((err) => { return err; });
		},
		syncContacts: (data) => {
			return instance.post(`/syncContacts`, data)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		getBlockedContactList: (userId, type, pageNumber) => {
			return instance.get(`/getBlockedContactList?userId=${userId}&type=${type}&pageNo=${pageNumber}&pageSize=100`)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
		sendInvitation: (data, type) => {
			let endpoint = (type === 'email') ? `/inviteContactByEmail` : `/sendSMS`
			return instance.post(endpoint, data)
			.then((res) => { return res; })
			.catch((err) => { return err; });
		},
	},
};

module.exports = API;
