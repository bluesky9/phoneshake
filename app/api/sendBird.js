import { Platform } from 'react-native';
import FCM from 'react-native-fcm';
import SendBird from 'sendbird';
import { SENDBIRD_APPID } from './constants'
// const sb = new SendBird({ appId: SENDBIRD_APPID });

export async function setupPushForChat(userId, nickname, profileUrl) {
  if(sb) {
    sb.connect(userId, (user, error) => {
      if (error) {
        console.log('SendBird Login Failed.');
      } else {
        sb.updateCurrentUserInfo(nickname, profileUrl, async function (user, error) {
          if (error) {
            console.log('Update user Failed!');
            return;
          } else {
            console.log('SB User updated: ');
            let fcmToken = await FCM.getFCMToken().then(token => {
              if (token) {
                return token
              } else {
                return ''
              }
            });
            // const sb = SendBird.getInstance();
            if (sb) {
              if (Platform.OS === 'ios') {
                sb.registerAPNSPushTokenForCurrentUser(fcmToken, function (response, error) {
                  if (error) {
                    console.error(error);
                    return;
                  }
                  console.log('ios push res success: ');
                });
              } else {
                sb.registerGCMPushTokenForCurrentUser(fcmToken, function (response, error) {
                  if (error) {
                    console.error(error);
                    return;
                  }
                });
              }
            }
          }
        })
      }
    });
  } else {
    console.log('send bird instance is null');
    
  }
}

export async function updateProfileInfo(nickname, profileUrl) {
  sb.updateCurrentUserInfo(nickname, profileUrl, async function (user, error) {
    if (error) {
      console.log('Update user Failed!');
    } else {
      
    }
  });
}


export async function _sbRegisterPushToken(fcmToken) {
  const sb = SendBird.getInstance()
  if (sb) {
    if (Platform.OS === 'ios') {
      sb.registerAPNSPushTokenForCurrentUser(fcmToken, function (response, error) {
        if (error) {
          console.error(error);
          return;
        }
        console.log('ios push res success: ');
      });
    } else {
      sb.registerGCMPushTokenForCurrentUser(fcmToken, function (response, error) {
        if (error) {
          console.error(error);
          return;
        }
      });
    }
  }
}


export async function fetchAllMessages() {
  var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
  channelListQuery.includeEmpty = false;
  channelListQuery.limit = 100; // pagination limit could be set up to 100

  if (channelListQuery.hasNext) {
    let res = await channelListQuery.next((channelList, error) => {
      if (error) {
        console.error(error);
        // _this.setState({ fetching: false })
        return (error, null);
      }
      // console.log('All chats', channelList);
      return (null, channelList);
      
      // _this.setState({ messages: channelList, fetching: false });
    });
    console.log('res: ');
    
    return res;
  }
}
