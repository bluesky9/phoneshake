import { combineReducers } from 'redux';
import LoginReducer from './screens/login/login.reducer.js';
import MessageReducer from './screens/message/message.reducer'
import ContactReducer from './screens/contacts/contacts.reducer.js';

const reducers = combineReducers({
	login: LoginReducer,
	contacts: ContactReducer,
	messages: MessageReducer
});
export default reducers;
