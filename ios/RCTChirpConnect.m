//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//

#import "RCTChirpConnect.h"

@implementation RCTChirpConnect

ChirpConnect *sdk;

RCT_EXPORT_MODULE();

- (NSDictionary *)constantsToExport
{
  return @{
    @"CHIRP_CONNECT_STATE_STOPPED": [NSNumber numberWithInt:CHIRP_CONNECT_STATE_STOPPED],
    @"CHIRP_CONNECT_STATE_PAUSED": [NSNumber numberWithInt:CHIRP_CONNECT_STATE_PAUSED],
    @"CHIRP_CONNECT_STATE_RUNNING": [NSNumber numberWithInt:CHIRP_CONNECT_STATE_RUNNING],
    @"CHIRP_CONNECT_STATE_SENDING": [NSNumber numberWithInt:CHIRP_CONNECT_STATE_SENDING],
    @"CHIRP_CONNECT_STATE_RECEIVING": [NSNumber numberWithInt:CHIRP_CONNECT_STATE_RECEIVING]
  };
}

- (NSArray<NSString *> *)supportedEvents
{
  return @[
    @"onStateChanged",
    @"onSending",
    @"onSent",
    @"onReceiving",
    @"onReceived",
    @"onError"
  ];
}

/**
 * init(key, secret)
 *
 * Initialise the SDK with an application key and secret.
 * Callbacks are also set up here.
 */
RCT_EXPORT_METHOD(init:(NSString *)key secret:(NSString *)secret)
{
  sdk = [[ChirpConnect alloc] initWithAppKey:key
                                   andSecret:secret];
//  [sdk setConfigFromNetworkWithCompletion:^(NSError * _Nullable error) {
//    NSLog(@"Error settingConfig : %@", error);
//  }];
  [sdk setConfig:@"WugjoADWpV93thxzqpLEpeX15+6rLm10IYILvpMxxqgP+oeTZCZ2kd2yQd89uWvAJq1NGKya5hoo2Q2oPhqvy3Gn7dZpYWkEOsw0X4Ys+3dJgCEf26heJhttARdNl147NRplliUCM3bQD6P5V/Q1vMNw9Il/cLWLY3Lk47ZgPQ563CXtC02BbbS/hUU+qGQ19cSEjxQxs9yscQ1kpk0MNxQiyO46Yf19498eUVwCwvEdjZzIB9w3Ay9LPWtdZmiqyQbuJ77q/epTCB05XVm1hh8LabwXiqvOTrEwtJgJa3gSWRfVSdlQPQjuWQjNOuI0o2R5hv+FqNdGCLI5612XST/+a2V5WpA8aKHTMMQ5YJjEIimFeyufxLgmFMMY4QkvNElQDETgRfwOEi1ekj9YZWp4srdV+hRqmc70pTwjzsL/02p5JBliTxp3OePTenIy7wXzcq+HCoyHRALAaV/60S4IZPLKmS11gPgNlXV2kFu3MGXdlmmm50/n2SfRxjVKodunfZZ2cgVKqyIHWP+NljoT4b5RNTL2geVYGi9zygB0GJ4OhWFxVSeormRA68IRqphMf/CiZua/JO9Pg0qJPvZddpTZgsHu8ZBKxrhuPH+bCPBFjDqT7RAnrDkEjHGaBkW2QOjD7XyQqHEBIVINXWUrBCFQ4Be3HZwEF27RwaePEEnSxpjOI+RNTWZorTDdGeubl6eXMmW4Gp1BhHwigcEJ84u6d8H+XxcQnKC/3a96jGEacEEDeuOmS06Rq4NgtveZl5EYU8k2kEl+OodRq0lnj//NqCvMQ/dlK17plpwvidJDjUAdoIMgMR4Syrr2dyRG3/hMu1woDW9nmmiOTb2oxzOu3t70RzFIopdP06FkaY+4Js24cl8bO7xRjsgAMxVAzzCCzbJDAC+2GXSVK/gLaFv3YD5xiXGhHItqaseC05g5Gw5IVW64N/S1fkSyLxWXUEJs8ALRVvFtftEmVefW0NAgtchMqeGohPSQaIZgBzczTsIxEh7dUlWrQDxr675hiGL98Y4VB5iegLa6Pr+pxbKjPNbL3EGcgSaBfgCgU/yTqHSC8WZtyzVxQ2OHTROctlT5cPHllel2m4Qy7E0hn35d7Iyo9Dc8ajzGD5l8uzs/MHddH2UsHyOQeu+YKWwn2wJorJ/JZwYmc/xdKjUgPmL50/6vjJko4eJKDqGCuoHcFjmO8eJzHWbZBByUwIuOwcCj5bf1sVXIUHAxju3ObkfZK751dX0epPIzBybYD9nS+Ezg/x5bVXlT1g6TBUfVpsdcsu59XyU91cmW/xerVy+3yg6Fnpot9GgMNcVNE42Tm/2GCgDaP5+7rUFxXCHnfhosv18UycKn6UrFM1HzONDgzfptvwEleMdY8qFoqfYrinS197q7TGFpz9e0qqEYy9innz0dKOqqTDB0NxRdv8AgK5lcd4zGr3JXke6GPjSL+arnGMnZQsd/ygYo3ktWBsrhW9+cy+NZry9CV2Y5hcssfbWo1hdG1i2A8dObKoMK8yz/zntGXaBQ2R1er9NHffdN7o6ZrlkIM2Joffzz7LkGUqjIxB6+sHsasD5tMeE7IPIg1VPoInMDzazLMoQga6WDHYLq5Z3yU97VPGjQvRCkuDR1mMYZOMHCw1p1gVGr4lmym5n+jECzq0As6PmHKWc88+qjVrzqG6AOuyMyzTYss0KNe6SEI5E/nmBy1b+wd+t3wMkMtT3KpzSt5eWCHqCAArL9zTbiE5FrJM6ybNWu1uDk7chfTx+ao7YKnVNPCs4u/ZN4XaxED+JMhw22uvyd+Eb2EuX8cvlgvAa/dwwG+zA0iXlzoW2oSla12t1MlvUrPiZdCR7khGydgiCwJLKMxjSShgQ9uYOXTdRTntpKWC4H0BFmomi7IokzccT9/mzzDGQr5fIHdgF7Hfvv1oZoCOtNWkh+EokVTslZp69vQsQK4CUMjAuX4eOnAAwrHA1d1KcPzDXUPFbQ5/nCXSZJVexQ87e7gGgjRkawbB5COJdCrYpcIuV6QGVyMcCWO4VgSZp7QFPBLHd3fQyFowYwkfrkrpLuTOw/YpOkxIxhdLtX9Keb8wif6kEPw0bkZDvpvfUii7cUTcMMo7lDZfyGSOn2nBKV6JXTB58eRW7TBe18Rfju5Q1wyg+ARy1bRRGkunfKfRQETqaVZd1DW2w82w8fdSfKw3hbO7lXmjtKwR/FienoxPLwO9a/ebDHik03jCoyCmTHE2UXEmGcqIMbpJ7QR/Q5qoZ2GIdhxZElNLTgt2GDQuhqmddMA34m2t3Uy7s0ptHvFdPesc180PWZUlJSwnfN1bxfbRsSO8aMLlGNXpm5vEIMQUKUznkWzgiRoWvNjWxqAIYiw1/L0BEt7mUHYAomoIRAiHUCyex+vDhCyIdIsczhXv0rDKH+V55VjFuTxEeyN7g1y31iQgVnzmKq2p4vP8tV0vam6Nb7qAGvMenaXAFwcbJqYiYjf0HyiROJeykSDOUSts8uudzBzAd7OfgglaUNUh+yopyOaWZwxzVtc1vnos7K5Cutg9RBBd8f8/WMmINcSkR6lr9braGlYX1moYstgAZP/JPlcWwVnoOKy3HsTtk="];

  [sdk setShouldRouteAudioToBluetoothPeripherals:YES];

  [sdk setStateUpdatedBlock:^(CHIRP_CONNECT_STATE oldState,
                              CHIRP_CONNECT_STATE newState)
   {
     [self sendEventWithName:@"onStateChanged" body:@{@"status": [NSNumber numberWithInt:newState]}];
   }];

  [sdk setSendingBlock:^(NSData * _Nonnull data)
   {
     NSArray *payload = [self dataToArray: data];
     [self sendEventWithName:@"onSending" body:@{@"data": payload}];
   }];
  [sdk setSentBlock:^(NSData * _Nonnull data) {
    NSArray *payload = [self dataToArray: data];
    [self sendEventWithName:@"onSent" body:@{@"data": payload}];
  }];
  
  [sdk setReceivingBlock:^(NSUInteger channel) {
    [self sendEventWithName:@"onReceiving" body:@{}];
  }];
  [sdk setReceivedBlock:^(NSData * _Nonnull data, NSUInteger channel) {
    NSArray *payload = [self dataToArray: data];
    [self sendEventWithName:@"onReceived" body:@{@"data": payload}];
  }];
  [sdk setAuthenticatedBlock:^(NSError * _Nullable error) {
    if (error) {
      [self sendEventWithName:@"onError" body:@{@"message": [error localizedDescription]}];
    }
  }];
}

/**
 * getLicence()
 *
 * Fetch default licence from network to configure the SDK.
 */
//RCT_EXPORT_METHOD(getLicence:(RCTPromiseResolveBlock)resolve
//                    rejecter:(RCTPromiseRejectBlock)reject)
//{
//
//    [sdk getLicenceStringWithCompletion:^(NSString * _Nullable licence, NSError * _Nullable error) {
//      if (error) {
//        reject(@"Error", @"Authentication Error", error);
//      } else {
//        NSError *licenceErr = [sdk setLicenceString:licence];
//        if (licenceErr) {
//          reject(@"Error", @"Licence Error", licenceErr);
//        } else {
//          resolve(@"Initialisation Success");
//        }
//      }
//    }];
//}

/**
 * setLicence(licence)
 *
 * Configure the SDK with a licence string.
 */
//RCT_EXPORT_METHOD(setLicence:(NSString *)licence)
//{
//  NSError *err = [sdk setLicenceString:licence];
//  if (err) {
//    [self sendEventWithName:@"onError" body:@{@"message": [err localizedDescription]}];
//  }
//}

/**
 * start()
 *
 * Starts the SDK.
 */
RCT_EXPORT_METHOD(start)
{
  NSError *err = [sdk start];
  if (err) {
    [self sendEventWithName:@"onError" body:@{@"message": [err localizedDescription]}];
  }
}

/**
 * stop()
 *
 * Stops the SDK.
 */
RCT_EXPORT_METHOD(stop)
{
  NSError *err = [sdk stop];
  if (err) {
    [self sendEventWithName:@"onError" body:@{@"message": [err localizedDescription]}];
  }
}

/**
 * send(data)
 *
 * Sends a payload of NSData to the speaker.
 */
RCT_EXPORT_METHOD(send: (NSArray *)data)
{
//  NSString *str = @"tarun";
//  NSData *strData = [str dataUsingEncoding:NSUTF8StringEncoding];
////  NSData *payload = [identifier dataUsingEncoding:NSUTF8StringEncoding];
  NSData *payload = [self arrayToData: data];
  NSError *err = [sdk send:payload];
  if (err) {
    [self sendEventWithName:@"onError" body:@{@"message": [err localizedDescription]}];
  }
}

/**
 * sendRandom()
 *
 * Sends a random payload to the speaker.
 */
RCT_EXPORT_METHOD(sendRandom)
{
  NSUInteger length = 1 + arc4random() % (sdk.maxPayloadLength - 1);
  NSData *data = [sdk randomPayloadWithLength:length];
  NSError *err = [sdk send:data];
  if (err) {
    [self sendEventWithName:@"onError" body:@{@"message": [err localizedDescription]}];
  }
}

/**
 * dataToArray
 *
 * Internal function to convert NSData payloads
 * to NSArray of bytes. React Native doesn't support NSData.
 */
- (NSArray *)dataToArray: (NSData *) data
{
//  NSString *string = [[NSString alloc] initWithData:data
//                                           encoding:NSASCIIStringEncoding];
//
//  return [NSArray arrayWithObjects:string, nil];
  Byte *bytes = (Byte*)[data bytes];
  NSMutableArray *payload = [NSMutableArray arrayWithCapacity:data.length];
  for (int i = 0; i < data.length; i++) {
    [payload addObject:[NSNumber numberWithInt:bytes[i]]];
  }
  return [NSArray arrayWithArray:payload];
}

/**
 * arrayToData
 *
 * Internal function to convert NSArray payloads
 * to NSData. React Native doesn't support NSData.
 */
- (NSData *)arrayToData: (NSArray *) array
{
  Byte bytes[[array count]];
  for (int i = 0; i < [array count]; i++) {
    bytes[i] = [[array objectAtIndex:i] integerValue];
  }
  NSData *payload = [[NSData alloc] initWithBytes:bytes length:[array count]];
  return payload;
}

@end

